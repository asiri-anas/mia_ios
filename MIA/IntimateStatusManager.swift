//
//  IntimateStatusManager.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 14/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import CoreData

class IntimateStatusManager: NSObject {
    /// Returns a singleton instance of IntimateStatusManager.
    class func shared() -> IntimateStatusManager? {
        
         var _sharedManager: IntimateStatusManager? = nil
         var onceToken: Int = 0
        if (onceToken == 0) {
                    /* TODO: move below code to a static variable initializer (dispatch_once is deprecated) */
            _sharedManager = IntimateStatusManager()
        }
                onceToken = 1
        return _sharedManager
        
    }
    /*!
     @brief This method will create, save, and update the IntimateState object.
     @param key The localization key of the status name.
     @param statusID The unique identifier of the IntimateSatus object.
     */
    func create(withKey key: String?, andID statusID: NSNumber?) -> IntimateStatus? {
        
        var status = IntimateStatus.create(withKey: key)
        status?.statusID = statusID
        var state = IntimateStateManager.instance.fetchIntimateState()
        if state != nil {
            state?.intimateStatus = status
        }
        CDManager.instance!.saveContext()
        return status
        
    }
    /// Will fetch the current IntimateStatus object based on the IntimateState object.
    func fetchCurrentIntimateStatus() -> IntimateStatus? {
        
        var fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        var entity = NSEntityDescription.entity(forEntityName: "IntimateStatus", in: (CDManager.instance!.managedObjectContext)!)
            fetchRequest.entity = entity
            fetchRequest.fetchLimit = 1
        var state = IntimateStateManager.instance.fetchIntimateState()
        var format = "statusID == \(state?.intimateStatus?.statusID)"
            var predicate = NSPredicate(format: format)
            fetchRequest.predicate = predicate
            var error: Error?
        var result = try? CDManager.instance!.managedObjectContext?.fetch(fetchRequest)
            if error != nil {
                return nil
            }
        return result??.last as! IntimateStatus
        
    }
    
    /// Will fetch and return all IntimateStatus objects, with statusID's ascending order.
    func fetchAllIntimateStatus() -> [Any]? {
        
        var fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        var entity = NSEntityDescription.entity(forEntityName: "IntimateStatus", in: (CDManager.instance!.managedObjectContext)!)
        var sd = NSSortDescriptor(key: "statusID", ascending: true)
        fetchRequest.entity = entity
        fetchRequest.sortDescriptors = [sd]
        var error: Error?
        var result = try? CDManager.instance!.managedObjectContext?.fetch(fetchRequest)
        if error != nil {
            return nil
        }
        return result!
        
    }
    
    
    
}
