//
//  AuthTutoViewControllerSwift.swift
//  mia
//
//  Created by Xavier Bohin on 17/04/2018.
//  Copyright © 2018 V-Labs. All rights reserved.
//

import Foundation




@objc
class AuthTutoViewControllerSwift: AbstractViewController, UIScrollViewDelegate {
    
let NUMBER_OF_PAGES = 6

var screenBounds = CGRect.zero
var sc1: UIView?
var sc2: UIView?
var sc4: UIView?
var ict1: IntimateCoachTuto1?
var ict2: IntimateCoachTuto2?
var ict3: IntimateCoachTuto3?
var ict4: IntimateCoachTuto4?
var ict5: IntimateCoachTuto5?
var ict6: IntimateCoachTuto6?
    
func viewDidLoad() {
    super.viewDidLoad()
    GradientHelper.setGradientOnLayer(view.layer, from: UIColor.white, to: UIColor(hexString: "#F3DCD7"))
    screenBounds = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height - (navigationController?.navigationBar.frame.size.height ?? 0.0) - 20)
    closeButton.isHidden = true
    initScrollViewIntimate()
    if navigationController?.viewControllers[0] == self {
        setLeftCloseButtonForModalNavigationController()
        //[_accountButton setTitle:[NSLocalizedString(@"close", nil) uppercaseString] forState:UIControlStateNormal];
        accountButton.isHidden = true
        closeButton.isHidden = false
    } else {
        accountButton.alpha = 0
        setBackButtonForNavigationController()
        if user != nil {
            accountButton.setTitle(NSLocalizedString("add_device", comment: "").uppercased(), for: .normal)
            setRightCloseButtonForModalNavigationController()
        } else {
            //  [_accountButton setTitle:[NSLocalizedString(@"create_profile", nil) uppercaseString] forState:UIControlStateNormal];
        }
    }
    scrollView.alwaysBounceVertical = false
}
    
    
func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    navigationController?.setNavigationBarHidden(true, animated: true)
}
func initScrollViewIntimate() {
    pageControl.numberOfPages = NUMBER_OF_PAGES
    ict1 = IntimateCoachTuto2.createView()
    ict2 = IntimateCoachTuto1.createView()
    ict3 = IntimateCoachTuto3.createView()
    ict4 = IntimateCoachTuto5.createView()
    ict5 = IntimateCoachTuto4.createView()
    ict6 = IntimateCoachTuto6.createView()
    scrollView.addSubview(ict1)
    scrollView.addSubview(ict2)
    scrollView.addSubview(ict3)
    scrollView.addSubview(ict4)
    scrollView.addSubview(ict5)
    scrollView.addSubview(ict6)
}
func scrollViewDidScroll(_ scrollView: UIScrollView) {
    let index = Int(scrollView.contentOffset.x / scrollView.frame.size.width)
    scrollViewDidScroll(at: index)
}
    
    
    
func scrollViewDidScroll(at index: Int) {
    pageControl.currentPage = index
    if index == NUMBER_OF_PAGES - 1 {
        showAccountButton()
    } else {
        if navigationController?.viewControllers[0] != self {
            hideAccountButton()
        }
    }
}
func hideAccountButton() {
    UIView.animate(withDuration: 0.3, animations: {() -> Void in
        accountButton.alpha = 0
    })
    UIView.animate(withDuration: 0.3, animations: {() -> Void in
        orderButton.alpha = 0
    })
}
func showAccountButton() {
    UIView.animate(withDuration: 0.3, animations: {() -> Void in
        accountButton.alpha = 1
    })
    UIView.animate(withDuration: 0.3, animations: {() -> Void in
        orderButton.alpha = 1
    })
}
    
    
@IBAction func orderAction(_ sender: Any) {
    if let aString = URL(string: "http://shop.urgotech.fr/") {
        UIApplication.shared.openURL(aString)
    }
}
@IBAction func createAccount(_ sender: Any) {
    if navigationController?.viewControllers[0] == self {
        dismiss(animated: true) {() -> Void in }
    } else {
        if user == nil {
            if deviceType == COACH_INTIMATE_TYPE {
                performSegue(withIdentifier: "intimateProfile", sender: nil)
            } else {
                performSegue(withIdentifier: "createAccount", sender: nil)
            }
        } else {
            if deviceType == COACH_INTIMATE_TYPE {
                let intimateCoach = IntimateCoachManager.shared().createInitialIntimateCoach()
                NotificationCenter.default.post(name: NSNotification.Name("coach_created"), object: nil, userInfo: ["intimate_coach": intimateCoach])
                performSegue(withIdentifier: "intimateProfile", sender: nil)
            }
        }
    }
}
    
func viewWillLayoutSubviews() {
    super.viewWillLayoutSubviews()
    scrollView.translatesAutoresizingMaskIntoConstraints = false
    if deviceType == COACH_INTIMATE_TYPE {
        ict1.translatesAutoresizingMaskIntoConstraints = false
        ict2.translatesAutoresizingMaskIntoConstraints = false
        ict3.translatesAutoresizingMaskIntoConstraints = false
        ict4.translatesAutoresizingMaskIntoConstraints = false
        ict5.translatesAutoresizingMaskIntoConstraints = false
        ict6.translatesAutoresizingMaskIntoConstraints = false
        let views = NSDictionaryOfVariableBindings(ict1, ict2, ict3, ict4, ict5, ict6)
        var metrics = AutoLayoutMetrics.get()
        let screenHeight = metrics["screenHeight"]
        let realHeight = Int(screenHeight) - 20
        metrics["realHeight"] = realHeight
        scrollView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[_ict1(==realHeight)]|", options: [], metrics: metrics, views: views))
        scrollView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[_ict2(==realHeight)]|", options: [], metrics: metrics, views: views))
        scrollView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[_ict3(==realHeight)]|", options: [], metrics: metrics, views: views))
        scrollView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[_ict4(==realHeight)]|", options: [], metrics: metrics, views: views))
        scrollView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[_ict5(==realHeight)]|", options: [], metrics: metrics, views: views))
        scrollView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[_ict6(==realHeight)]|", options: [], metrics: metrics, views: views))
        scrollView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[_ict1(==screenWidth)][_ict2(==screenWidth)][_ict3(==screenWidth)][_ict4(==screenWidth)][_ict5(==screenWidth)][_ict6(==screenWidth)]|", options: [], metrics: AutoLayoutMetrics.get(), views: views))
    }
}
    
func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    switch deviceType {
        case COACH_INTIMATE_TYPE:
            scrollView.contentSize = CGSize(width: ict1.frame.size.width * NUMBER_OF_PAGES, height: view.frame.size.height - 20)
    }
    view.layer.masksToBounds = true
}
// MARK: - Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if (segue.identifier == "intimateProfile") {
        let aipfvc = segue.destination as? AuthIntimateProfileFormViewController
        aipfvc?.user = user
    }
}
    
    
    
    
    
}
