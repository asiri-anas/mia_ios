//
//  FilterButton.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 13/09/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit


@IBDesignable


class FilterButton: UIButton {
    
    @IBInspectable var selectedColor: UIColor?
    
    
func setSelected(_ selected: Bool) {
    //super.setSelected(selected)
    if selected {
        backgroundColor = selectedColor
        setTitleColor(UIColor.white, for: .selected)
        if let aSize = UIFont(name: "SourceSansPro-Bold", size: (titleLabel?.font.pointSize)!) {
            titleLabel?.font = aSize
        }
    } else {
        backgroundColor = UIColor.clear
        setTitleColor(UIColor.darkGrayTextColorMercu(), for: .normal)
        if let aSize = UIFont(name: "SourceSansPro-Regular", size: (titleLabel?.font.pointSize)!) {
            titleLabel?.font = aSize
        }
    }
}
    
func setHighlighted(_ highlighted: Bool) {
    //super.setHighlighted(highlighted)
    if highlighted {
        if !isSelected {
            backgroundColor = UIColor(white: 1, alpha: 1)
        }
    } else {
        if !isSelected {
            backgroundColor = UIColor(white: 1, alpha: 0)
        }
    }
}
    
    func setImageWithName(_ imageName: String?) {
        
        if isSelected {
            setImage(UIImage(named: "\(imageName)-wh"), for: .selected)
        } else {
            setImage(UIImage(named: imageName!), for: .normal)
        }
        
    }
    
    
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
    super.draw(rect)
    layer.cornerRadius = frame.size.height / 2
    layer.borderWidth = 1.0
    layer.masksToBounds = true
        if isSelected {
        backgroundColor = selectedColor
    } else {
        backgroundColor = UIColor.clear
    }
    if selectedColor != nil {
        layer.borderColor = selectedColor?.cgColor
    } else {
        layer.borderColor = UIColor.veryLightGrayMercu()?.cgColor
    }
    imageEdgeInsets = UIEdgeInsetsMake(5, -5, 5, 5)
    imageView?.contentMode = .scaleAspectFit
}
    
    
    
}
