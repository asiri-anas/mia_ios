//
//  IntimateProgramParser.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 04/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation

/// This object is responsible of the parsing of the IntimatePrograms plist file.
class IntimateProgramParser: NSObject {
    
    var currentProgram: IntimateProgram?
    
    
    /// Returns the IntimateProgram object corresponding to the given program key.
    func getBaseProgram(forKey programKey: String?) -> IntimateProgram? {
        
        if currentProgram == nil || !(currentProgram?.programType?.label == programKey) {
            
            var myDict: NSDictionary?
            if let path = Bundle.main.path(forResource: "IntimatePrograms", ofType: "plist") {
                myDict = NSDictionary(contentsOfFile: path)
            }
            var dict: NSDictionary?
            dict = myDict?[programKey] as! NSDictionary
            currentProgram = IntimateProgram(dictionary: dict)
            
        }
        return currentProgram
        
    }
    
    
    override init() {
        
        super.init()
        
    }
    
    
}
