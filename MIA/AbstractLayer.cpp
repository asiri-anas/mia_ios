/*
 * AbstractLayer.cpp
 *
 *  Created on: Dec 16, 2015
 *      Author: kevin
 */

#include "AbstractLayer.h"
#include "Constants.h"
#include "ActivityService.h"
#include "ExerciseLayer.h"
#include "CalibrationLayer.h"
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include "../NDKHelper/NDKHelper.h"
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#include "NDKHelper.h"
#endif
#include <iomanip>
#include <sstream>
#include <string>

USING_NS_CC;

AbstractLayer::~AbstractLayer()
{
	unscheduleAllCallbacks();
	_segments.clear();
}

void AbstractLayer::onKeyReleased(EventKeyboard::KeyCode keyCode, Event* event) {
	if(keyCode == EventKeyboard::KeyCode::KEY_ESCAPE) {
	    Value params = Value();
		sendMessageWithParams(ON_BACK_PRESSED, params);
	}
}

bool AbstractLayer::initWithNameAndDuration(std::string nameExercise, int duration, int exerciseNumber /*= -1*/)
{
    m_currentExerciseNumber = exerciseNumber;
    Layer::setKeypadEnabled(true);
	setMaxDuration(duration);

	_visibleSize = Director::getInstance()->getWinSize();
	_originDirector = Director::getInstance()->getVisibleOrigin();
    _newOrigin.set(_visibleSize.width, _visibleSize.height / 3);
    float maxPressureAirbee = (float) MAX_PRESSURE_AIRBEE;
	float maxPressureAlgo = (float) MAX_PRESSURE_ALGO;

	m_button = cocos2d::ui::Button::create("drawable/pause_white.png");
	m_button->setPosition(Vec2(Vec2(_visibleSize.width/ 2, m_button->getContentSize().height)));
	m_button->addClickEventListener([=](Ref *){
			toggleButton();
	    });
	m_button->setScale(1.2);

    this->addChild(m_button, 15);
	


	int minutes = truncf(m_maxDuration / 60);
	int seconds = m_maxDuration % 60;
    
    
    NDKHelper::removeSelectorsInGroup("pauseSelector");
    NDKHelper::addSelector("pauseSelector", TOGGLE_PAUSE, CC_CALLBACK_2(AbstractLayer::shouldPauseSelector, this), this);


	char* buf = new char[15];
	if (nameExercise.size() > 0 ) {

		std::ostringstream oss;
		oss << COUNTDOWN;
		sprintf(buf, "%s", oss.str().c_str());

		m_labelTimer = Label::createWithTTF(buf, "fonts/SourceSansPro-Light.otf", 18);
		m_labelTimer->setPosition(m_button->getPosition().x, m_button->getPosition().y + m_button->getContentSize().height * 1.5);
		this->addChild(m_labelTimer, 15);

		m_labelNameExercise = Label::createWithTTF(nameExercise, "fonts/SourceSansPro-Light.otf", 16);
		// position the label on the center of the screen
		m_labelNameExercise->setPosition(m_button->getPosition().x, m_labelTimer->getPosition().y + m_button->getContentSize().height * 2);
		this->addChild(m_labelNameExercise, 16);

		m_inCountdown = true;

	} else {
		m_inCountdown = false;
		std::ostringstream oss;
		oss << formatTime(0, 0);
		oss << " / ";
		oss << formatTime(minutes, seconds);
		sprintf(buf, "%s", oss.str().c_str());
		m_labelTimer = Label::createWithTTF(buf, "fonts/SourceSansPro-Light.otf", 18);
		m_labelTimer->setPosition(m_button->getPosition().x, m_button->getPosition().y + m_button->getContentSize().height * 1.5);
		this->addChild(m_labelTimer, 15);
	}
	delete buf;

	return true;
}

void AbstractLayer::shouldPauseSelector(Node *sender, Value data) {
    
    if (!m_isPause) {
        AbstractLayer::toggleButton();
    }
    
}



void AbstractLayer::setMaxDuration(int maxDuration) {
	m_maxDuration = maxDuration;
}

bool AbstractLayer::startTimer() {

	m_minutes = 0;
	m_seconds = 0;
	m_currentSeconds = 0;
	m_isPause = false;
	//scheduleUpdate();
	schedule(CC_SCHEDULE_SELECTOR(AbstractLayer::updateParallax), 0.01f);
	schedule(CC_SCHEDULE_SELECTOR(AbstractLayer::updateTimer), 1.0f);
	return true;
}

bool AbstractLayer::startTimerCalibration() {

	m_minutes = 0;
	m_seconds = 0;
	m_currentSeconds = 0;
	m_isPause = false;
	m_inCountdown = false;
	//scheduleUpdate();
	schedule(CC_SCHEDULE_SELECTOR(AbstractLayer::updateParallax), 0.01f);
	float duration = DURATION_CYCLE_EXERCICE_CALIBRATION;
	schedule(CC_SCHEDULE_SELECTOR(CalibrationLayer::updateTitle), duration);
	schedule(CC_SCHEDULE_SELECTOR(AbstractLayer::updateTimer), 1.0f);
	return true;
}

bool AbstractLayer::startTimerBreath() {
	m_minutes = 0;
	m_seconds = 0;
	m_currentSeconds = 0;
	m_inCountdown = false;
	m_isPause = false;
	//scheduleUpdate();
	schedule(CC_SCHEDULE_SELECTOR(AbstractLayer::updateTimer), 1.0f);
	return true;
}

void AbstractLayer::updateTimer(float dt) {

	std::ostringstream oss;

	m_currentSeconds += 1;
	if (m_inCountdown) {
		int countDown = COUNTDOWN - m_currentSeconds;
		oss << countDown;
		char* buf = new char[15];
		sprintf(buf, "%s", oss.str().c_str());
		m_labelTimer->setString(buf);
		delete buf;
		if (m_currentSeconds == COUNTDOWN - 1) {
			m_inCountdown = false;
			m_seconds = 0;
			m_currentSeconds = 0;
		}
	} else {
		if (m_seconds == 59) {
			m_minutes += 1;
			m_seconds = 0;
		} else {
			m_seconds += 1;
		}
		int minutes = truncf(m_maxDuration / 60);
		int seconds = m_maxDuration % 60;

		oss << formatTime(m_minutes, m_seconds);
		oss << " / ";
		oss << formatTime(minutes, seconds);
		char* buf = new char[15];
		sprintf(buf, "%s", oss.str().c_str());

		m_labelTimer->setString(buf);
		delete buf;
		if (m_currentSeconds == m_maxDuration) {
			m_minutes = 0;
			m_seconds = 0;
			m_currentSeconds = 0;
			//unschedule(schedule_selector(AbstractLayer::updateTimer));
			ActivityService::getInstance()->sendActivitySteps();
		}
	}

}

bool AbstractLayer::isInCountdown()
{
	return m_inCountdown;
}

bool AbstractLayer::isPaused()
{
	return m_isPause;
}

void AbstractLayer::calculMaxPressureDraw()
{
	m_maxHeightPressureToDraw = (4*(_visibleSize.height - _newOrigin.y)) / 5 + _newOrigin.y;
}

void AbstractLayer::calculMaxDraw()
{
	float percent = (m_maxHeightPressureToDraw - _newOrigin.y) * PERCENT_PRESSURE_ABOVE;
	m_maxHeightGraphToDraw = m_maxHeightPressureToDraw - percent;
}

float AbstractLayer::getHeightLeaf(int pressure, int maxPressure, int minPressure)
{
	float offset =(float) OFFSET_AIRBEE;
	float ratioHeight = (m_maxHeightGraphToDraw - _newOrigin.y) / (maxPressure - minPressure);

	if (pressure < minPressure) pressure = minPressure;
	float height = (float)pressure * ratioHeight + _newOrigin.y - minPressure * ratioHeight;
	if(height< _newOrigin.y) height =  _newOrigin.y;
	if(height > m_maxHeightPressureToDraw) height = m_maxHeightPressureToDraw;
	return height;
}

void AbstractLayer::toggleButton() {
 
	if (m_isPause) {
		m_button->loadTextureNormal("drawable/pause_white.png");
        if (m_currentExerciseNumber == BREATH_EXERCISE_TYPE) {
            resumeBreath();
        } else {
            resumeScene();
        }
		
	}
	else {
		m_button->loadTextureNormal("drawable/play_white.png");
        if (m_currentExerciseNumber == BREATH_EXERCISE_TYPE) {
            pauseBreath();
        } else {
            pauseScene();
        }
		
	}
}

void AbstractLayer::pauseBreath()
{
    unschedule(schedule_selector(AbstractLayer::updateTimer));
    m_isPause = true;
    
    EventCustom event("pause_breath");
    getEventDispatcher()->dispatchEvent(&event);
}

void AbstractLayer::resumeBreath()
{
    schedule(CC_SCHEDULE_SELECTOR(AbstractLayer::updateTimer), 1.0f);
    m_isPause = false;
    
    EventCustom event("resume_breath");
    getEventDispatcher()->dispatchEvent(&event);
}

void AbstractLayer::pauseScene()
{
	unschedule(schedule_selector(AbstractLayer::updateParallax));
	unschedule(schedule_selector(AbstractLayer::updateTimer));
	m_isPause = true;
	//Director::getInstance()->pause();
}

void AbstractLayer::resumeScene()
{
	schedule(CC_SCHEDULE_SELECTOR(AbstractLayer::updateParallax), 0.01f);
	schedule(CC_SCHEDULE_SELECTOR(AbstractLayer::updateTimer), 1.0f);
	m_isPause = false;
	//Director::getInstance()->resume();
}

void AbstractLayer::updateParallax(float dt) {
	float ratioWidth = _visibleSize.width / TIME_BY_SCREEN;
	Vec2 backgroundScroll = Vec2(-1, 0);

	Vec2 actualPoint = Vec2(_leafSprite->getPosition().x - _parallaxNode->getPosition().x, _leafSprite->getPosition().y);
	auto segment = DrawNode::create();
    
    if (_lastPoint.isZero()) {
        _lastPoint = actualPoint;
    }

	segment->drawLine(_lastPoint, actualPoint, Color4F::WHITE);
	segment->setLineWidth(6);

	 _parallaxNode->addChild(segment, 0, Vec2(1, 1), Vec2::ZERO);


	if (_segments.size() > 450) {

		_parallaxNode->removeChild(_segments.at(0), true);
		_segments.erase(0);

	}
	_segments.pushBack(segment);

	_lastPoint = actualPoint;

    Vec2 diff = backgroundScroll * dt * ratioWidth;
    int parallaxX = _parallaxNode->getPosition().x * 100;
    int diffX = diff.x * 100;
    int diffInt = parallaxX + diffX;
    double diffFinal = ((double) diffInt) / 100;
    Vec2(diffFinal, 0);
//    log("ratio: %f, dt: %f, position x: %.2f, add: %f, diff: %f", ratioWidth, dt, _parallaxNode->getPosition().x ,(backgroundScroll * dt * ratioWidth).x, diffFinal);
	_parallaxNode->setPosition(Vec2(diffFinal, _parallaxNode->getPosition().y));

}

std::string AbstractLayer::formatTime(int minute, int second) {
	std::ostringstream oss;
	oss << std::setfill('0') <<std::setw(2) << minute;
	oss << ":";
	oss << std::setfill('0') <<std::setw(2) <<second;
	return oss.str();
}
