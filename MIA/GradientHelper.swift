//
//  GradientHelper.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 02/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit


class GradientHelper: NSObject {
    class func setGradientOnLayer(_ layer: CALayer?, from color1: UIColor?, to color2: UIColor?) {
        

        if (layer?.sublayers![0].name == "gradient") {
            layer?.sublayers![0].removeFromSuperlayer()
        }
        var stopOne = 0.0
        var stopTwo = 1.0
        var locations = [stopOne, stopTwo]
        var gradient = CAGradientLayer()
        gradient.name = "gradient"
        gradient.frame = (layer?.bounds)!
        gradient.colors = [color1?.cgColor, color2?.cgColor]
        gradient.locations = locations as [NSNumber]
        layer?.insertSublayer(gradient, at: 0)
        
    }
}
