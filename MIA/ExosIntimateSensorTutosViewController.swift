//
//  ExosIntimateSensorTutosViewController.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 02/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit



class ExosIntimateSensorTutosViewController: AbstractViewController, UIScrollViewDelegate {
    
    enum SensorTutoType {
        
    case SENSOR_TUTO_TYPE_CONNECT       //0
    case SENSOR_TUTO_TYPE_INSERT        //1
    case SENSOR_TUTO_TYPE_HOW_TO_USE    //2
    case SENSOR_TUTO_TYPE_HOW_TO_CLEAN  //5
    case SENSOR_EXPLAIN_EXERCICE        //3
    case SENSOR_EXPLAIN_PERINE          //4
    case SENSOR_TUTO_TYPE_ALL           //6
        
    }
    
    var screenBounds = CGRect.zero

    
    var tutoSensorConnect1: TutoSensorConnect1?
    var tutoSensorConnect2: TutoSensorConnect2?
    var tutoSensorInsert: TutoSensorInsert?
    var tutoSensorHowToUse2: TutoSensorHowToUse?
    var tutoSensorHowToUse: TutoSensorHowToUse2?
    var tutoSensorExplainExercises: TutoSensorExplainExercises?
    var tutoSensorExplainPerine: TutoSensorExplainPerine?
    var tutoSensorHowToClean: TutoSensorHowToClean?
    
    var sensorTutoType: SensorTutoType?
    
    var numberOfPages: Int = 0
    
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var startButton: UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideStartButton()
        GradientHelper.setGradientOnLayer(view.layer, from: UIColor.white, to: UIColor.colorWithHexString(stringToConvert: "#F3DCD7"))
        screenBounds = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height - (navigationController?.navigationBar.frame.size.height ?? 0.0) - 20)

        startButton.setTitle(NSLocalizedString("close", comment: "").uppercased(), for: .normal)
        closeButton.isHidden = false
        
        if  sensorTutoType == .SENSOR_TUTO_TYPE_ALL || sensorTutoType == .SENSOR_TUTO_TYPE_CONNECT {
            tutoSensorConnect1 = TutoSensorConnect1.createView()
            tutoSensorConnect2 = TutoSensorConnect2.createView()
            scrollView.addSubview(tutoSensorConnect1!)
            scrollView.addSubview(tutoSensorConnect2!)
            numberOfPages = 2
        }
        
        if  sensorTutoType == .SENSOR_TUTO_TYPE_ALL || sensorTutoType == .SENSOR_TUTO_TYPE_INSERT {
            tutoSensorInsert = TutoSensorInsert.createView()
            scrollView.addSubview(tutoSensorInsert!)
            numberOfPages = 1
        }
        
        if  sensorTutoType == .SENSOR_TUTO_TYPE_ALL || sensorTutoType == .SENSOR_TUTO_TYPE_HOW_TO_USE {
            tutoSensorHowToUse2 = TutoSensorHowToUse.createView()
            tutoSensorHowToUse = TutoSensorHowToUse2.createView()
            scrollView.addSubview(tutoSensorHowToUse!)
            scrollView.addSubview(tutoSensorHowToUse2!)
            numberOfPages = 2
        }
        
        if  sensorTutoType == .SENSOR_TUTO_TYPE_ALL || sensorTutoType == .SENSOR_TUTO_TYPE_HOW_TO_CLEAN {
            tutoSensorHowToClean = TutoSensorHowToClean.createView()
            scrollView.addSubview(tutoSensorHowToClean!)
            numberOfPages = 1
        }
        
        if  sensorTutoType == .SENSOR_TUTO_TYPE_ALL || sensorTutoType == .SENSOR_EXPLAIN_EXERCICE {
            tutoSensorExplainExercises = TutoSensorExplainExercises.createView()
            scrollView.addSubview(tutoSensorExplainExercises!)
            numberOfPages = 1
        }
        
        if  sensorTutoType == .SENSOR_TUTO_TYPE_ALL || sensorTutoType == .SENSOR_EXPLAIN_PERINE {
            tutoSensorExplainPerine = TutoSensorExplainPerine.createView()
            scrollView.addSubview(tutoSensorExplainPerine!)
            numberOfPages = 1
        }
        
        
        
        
        if  sensorTutoType == .SENSOR_TUTO_TYPE_ALL {
            if !Registry.has("CONNECTION_TUTO_READ") {
                startButton.setTitle(NSLocalizedString("start", comment: "").uppercased(), for: .normal)
                closeButton.isHidden = true
            }
            numberOfPages = 8
        }
        pageControl.numberOfPages = numberOfPages
        if numberOfPages == 1 {
            pageControl.isHidden = true
            scrollView.bounces = false
        }
    }
    
    

    override func viewWillDisappear(_ animated: Bool) {
        
        
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let index = Int(scrollView.contentOffset.x / scrollView.frame.size.width)
        pageControl.currentPage = index
        if sensorTutoType == .SENSOR_TUTO_TYPE_ALL && !Registry.has("CONNECTION_TUTO_READ") {
            if index == numberOfPages - 1 {
                showStartButton()
            } else {
                hideStartButton()
            }
        } else {
            //[self showStartButton];
        }
    }
    
    
    func hideStartButton() {
        UIView.animate(withDuration: 0.3, animations: {() -> Void in
            self.startButton.alpha = 0
        })
    }
    
    
    func showStartButton() {
        UIView.animate(withDuration: 0.3, animations: {() -> Void in
            self.startButton.alpha = 1
        })
    }
    
    
    @IBAction func startButtonTapped(_ sender: Any) {
        if sensorTutoType == .SENSOR_TUTO_TYPE_ALL {
            if !Registry.has("CONNECTION_TUTO_READ") {
                let transition = CATransition()
                transition.duration = 0.3
                transition.type = kCATransitionFade
                view.window?.layer.add(transition, forKey: kCATransition)
                Registry.set("CONNECTION_TUTO_READ", withValue: true)
                dismiss(animated: false) {() -> Void in }
                return
            }
            dismiss(animated: true) {() -> Void in }
        }
    }
    
    

    override func viewWillLayoutSubviews() {
        
        super.viewWillLayoutSubviews()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        
            tutoSensorConnect1?.translatesAutoresizingMaskIntoConstraints = false
            tutoSensorConnect2?.translatesAutoresizingMaskIntoConstraints = false
            tutoSensorInsert?.translatesAutoresizingMaskIntoConstraints = false
            tutoSensorHowToUse?.translatesAutoresizingMaskIntoConstraints = false
            tutoSensorHowToUse2?.translatesAutoresizingMaskIntoConstraints = false
            tutoSensorExplainExercises?.translatesAutoresizingMaskIntoConstraints = false
            tutoSensorExplainPerine?.translatesAutoresizingMaskIntoConstraints = false
            tutoSensorHowToClean?.translatesAutoresizingMaskIntoConstraints = false
        
        
        var metrics = AutoLayoutMetrics()
        var autoMetrics: NSDictionary
        autoMetrics = metrics.get()
        var screenHeight = autoMetrics["screenHeight"] as!Int
        var realHeight = Int(screenHeight) - 20
        //autoMetrics["realHeight"] = realHeight
        
        if sensorTutoType == .SENSOR_TUTO_TYPE_ALL || sensorTutoType == .SENSOR_TUTO_TYPE_CONNECT {
        
            let views = ["tutoSensorConnect1": tutoSensorConnect1, "tutoSensorConnect2": tutoSensorConnect2]
            
            scrollView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[tutoSensorConnect1(==screenHeight)]|", options: [], metrics: autoMetrics as! [String : Any], views: views))
            scrollView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[tutoSensorConnect2(==screenHeight)]|", options: [], metrics: autoMetrics as! [String : Any], views: views))
            
            scrollView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[tutoSensorConnect1(==screenWidth)][tutoSensorConnect2(==screenWidth)]|", options: [], metrics: metrics.get() as! [String : Any], views: views))
            
        }
        
        if sensorTutoType == .SENSOR_TUTO_TYPE_ALL || sensorTutoType == .SENSOR_TUTO_TYPE_INSERT {
            
            let views = ["tutoSensorInsert": tutoSensorInsert]
            
            scrollView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[tutoSensorInsert(==screenHeight)]|", options: [], metrics: autoMetrics as! [String : Any], views: views))
            
            
            scrollView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[tutoSensorInsert(==screenWidth)]|", options: [], metrics: metrics.get() as! [String : Any], views: views))
            
            
        }
        
        
        
        if sensorTutoType == .SENSOR_TUTO_TYPE_ALL || sensorTutoType == .SENSOR_TUTO_TYPE_HOW_TO_USE {
            
            let views = ["tutoSensorHowToUse": tutoSensorHowToUse, "tutoSensorHowToUse2": tutoSensorHowToUse2]
            
            scrollView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[tutoSensorHowToUse(==screenHeight)]|", options: [], metrics: autoMetrics as! [String : Any], views: views))
            scrollView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[tutoSensorHowToUse2(==screenHeight)]|", options: [], metrics: autoMetrics as! [String : Any], views: views))
            
            scrollView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[tutoSensorHowToUse(==screenWidth)][tutoSensorHowToUse2(==screenWidth)]|", options: [], metrics: metrics.get() as! [String : Any], views: views))
            
            
        }
        
        
        if sensorTutoType == .SENSOR_TUTO_TYPE_ALL || sensorTutoType == .SENSOR_TUTO_TYPE_HOW_TO_CLEAN {
        
            let views = ["tutoSensorHowToClean": tutoSensorHowToClean]
            
            scrollView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[tutoSensorHowToClean(==screenHeight)]|", options: [], metrics: autoMetrics as! [String : Any], views: views))
            
            
            scrollView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[tutoSensorHowToClean(==screenWidth)]|", options: [], metrics: metrics.get() as! [String : Any], views: views))
        
            }
        
        
        
        if sensorTutoType == .SENSOR_TUTO_TYPE_ALL || sensorTutoType == .SENSOR_EXPLAIN_EXERCICE
            {
            
                let views = ["tutoSensorExplainExercises": tutoSensorExplainExercises]
                
                scrollView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[tutoSensorExplainExercises(==screenHeight)]|", options: [], metrics: autoMetrics as! [String : Any], views: views))
                
                
                scrollView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[tutoSensorExplainExercises(==screenWidth)]|", options: [], metrics: metrics.get() as! [String : Any], views: views))
    
    
            }
        
        
        
        if sensorTutoType == .SENSOR_TUTO_TYPE_ALL || sensorTutoType == .SENSOR_EXPLAIN_PERINE
        {
            
            let views = ["tutoSensorExplainPerine": tutoSensorExplainPerine]
            
            scrollView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[tutoSensorExplainPerine(==screenHeight)]|", options: [], metrics: autoMetrics as! [String : Any], views: views))
            
            
            scrollView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[tutoSensorExplainPerine(==screenWidth)]|", options: [], metrics: metrics.get() as! [String : Any], views: views))
            
            
        }
    
        
        }
        
    
    
    
}
