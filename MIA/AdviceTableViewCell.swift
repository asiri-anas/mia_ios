//
//  AdviceTableViewCell.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 05/07/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

class AdviceTableViewCell: UITableViewCell {
    
 
    @IBOutlet weak var adviceHud: UIActivityIndicatorView!
    
    @IBOutlet weak var adviceBackgroundIV: UIImageView!
    
    @IBOutlet weak var adviceTitleLabel: UILabel!
    
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    
    
    func setContentWith(_ advice: Advice?) {
        
        if adviceBackgroundIV.image == nil {
            adviceHud.isHidden = false
        } else {
            adviceHud.isHidden = true
        }
        adviceHud.startAnimating()
        //adviceTitleLabel.text = advice?.title as! String
        adviceBackgroundIV.alpha = 0
        /*adviceBackgroundIV.sd_setImage(with: URL(string: (advice?.imageBlurURL)! as String), completed: { image, error, cacheType, imageURL in
            self.adviceHud.isHidden = true
            UIView.animate(withDuration: 0.3, animations: {
                self.adviceBackgroundIV.alpha = 1.0
            })
        })*/
        
    }

    
    
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        adviceBackgroundIV.layer.masksToBounds = true
    }
    
    
    
    
}
