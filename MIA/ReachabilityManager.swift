//
//  ReachabilityManager.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 12/06/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation


class ReachabilityManager: NSObject {
    /// An instance of Reachability object.
    var reachability: Reachability?
    
    static let instance = ReachabilityManager()
    /// Returns a singleton instance of ReachabilityManager.

    
    
    /// Returns a BOOL that indicates if connectivity is sufficient.
    func isReachable() -> Bool {
        
        return (ReachabilityManager.instance.reachability?.isReachable)!
        
    }
    
    
    override init() {
        super.init()
        
        // Initialize Reachability
        reachability = Reachability()
        // Start Monitoring
        
        do {
            try reachability?.startNotifier()
        } catch {
            print(error)
        }
        
        
        
        
    }
    
    
    
}
