//
//  IntimateGoalsWidget.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 21/06/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit

class IntimateGoalsWidget: IntimateAbstractWidget {
    @IBOutlet var sessionsGoalButton: UIButton!
    @IBOutlet var comfortGoalButton: UIButton!

    @IBOutlet var comfortIV: UIImageView!
    @IBOutlet var comfortLabel: UILabel!
    
    var intimateCoach: IntimateCoach?
    
    @IBOutlet weak var dateLastSeance: UILabel!//misstyped those two
    @IBOutlet weak var dateNextSeance: UILabel!//can't change name without loosing link. just inverse those two
    @IBOutlet weak var leafImage: UIImageView!
    @IBOutlet weak var currentGoal: UILabel!
    @IBOutlet weak var finalGoal: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        do {
            var results = try CDManager.instance!.managedObjectContext!.fetch(IntimateCoach.intimateCoachFetchRequest())
            print("goals", "fetch intimateCoach")
            for singleData in results {
                if singleData.intimateState != nil {
                    print("goals", "intimateState fetched:", singleData.intimateState!.intimateProgramType)
                }
                intimateCoach = singleData
            }
        } catch {
            print("Failed")
        }
        
        var lastSceance = intimateCoach?.lastActivity
        //var nextSceance = intimateCoach?.futureIntimateSessions.firstObject.fireDate
        //var score = intimateCoach?.intimateSessions.lastObject.score
        var goal = intimateCoach?.intimateGoal?.goalPerWeek
        var goalInWeek = 0
        /*while ((intimateCoach?.intimateSessions?.lastObject.createdAt) - Date() < Date.oneWeek)
        {
            intimateCoach?.intimateSessions?.dropLast()
            goalInWeek = goalInWeek + 1
        }*/
        
        //setNextSeance(date: nextSceance)
        setLastSeance(date: lastSceance)
        //setLeaf(score: score)
        setCurrentGoal(current: goalInWeek)
        //setFinalGoal(final: Int(goal!))
        
    }
    
    
    func setNextSeance (date: Date?)
    {
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date / server String
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let myString = formatter.string(from: date!) // string purpose I add here
        // convert your string to date
        let yourDate = formatter.date(from: myString)
        //then again set the date format whhich type of output you need
        formatter.dateFormat = "dd/MM/yyyy - HH:mm"
        // again convert your date to string
        let myStringafd = formatter.string(from: yourDate!)
        
        dateLastSeance.text = myStringafd + " Planifier ma séance"
    }
    
    func setLastSeance (date: Date?)
    {
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date / server String
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        if (date != nil)
        {
        let myString = formatter.string(from: date!) // string purpose I add here
        // convert your string to date
        let yourDate = formatter.date(from: myString)
        //then again set the date format whhich type of output you need
        formatter.dateFormat = "dd/MM/yyyy à HH:mm"
        // again convert your date to string
        let myStringafd = formatter.string(from: yourDate!)
        
        
        dateNextSeance.text = "Dernière séance\n" + myStringafd
        } else {
        dateNextSeance.text = "Aucune séance réalisée"
        }
        
        
    }
    
    
    func setLeaf(score: Int)
    {
        leafImage.image = UIImage(named: "feuille_0")
        if (score>20)
        {
            leafImage.image = UIImage(named: "feuille_1")
        }
        if (score>40)
        {
            leafImage.image = UIImage(named: "feuille_2")
        }
        if (score>60)
        {
            leafImage.image = UIImage(named: "feuille_3")
        }
        if (score>80)
        {
            leafImage.image = UIImage(named: "feuille_4")
        }
        
    }
    
    func setCurrentGoal(current: Int)
    {
        currentGoal.text = String(current)
    }
    
    func setFinalGoal(final: Int)
    {
        finalGoal.text = String(final)
    }
    
    
    @IBAction func calendarTapped(_ sender: Any) {
        parent?.performSegue(withIdentifier: "showCalendar", sender: nil)
    }
    
    
    @IBAction func showComfortGoal(_ sender: Any) {
        
        var sb = UIStoryboard(name: "Intimate", bundle: nil)
        var nc = sb.instantiateViewController(withIdentifier: "MainIntimateWeeklyGoalNavigationController") as? UINavigationController
        //var miwgvc = nc?.viewControllers[0] as? MainIntimateWeeklyGoalViewController
        //miwgvc?.intimateGoals = intimateGoals
        if let aNc = nc {
            parent?.present(aNc, animated: true)
        }
        
    }
    
}
