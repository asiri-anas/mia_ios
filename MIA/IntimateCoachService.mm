//
//  IntimateCoachService.m
//  mymercurochrome
//
//  Created by Rémi Caroff on 09/12/2015.
//  Copyright © 2015 V-Labs. All rights reserved.
//

#import "IntimateCoachService.h"
#import <AFNetworking/AFHTTPRequestOperationManager.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <EMAccordionTableViewController/EMAccordionTableViewController.h>
#import <FSCalendar/FSCalendar.h>
#import <iOS_Slide_Menu/SlideNavigationController.h>
#import <TTTAttributedLabel/TTTAttributedLabel.h>
#import "mia-Swift.h"


@implementation IntimateCoachService

+ (IntimateCoachService *)sharedService
{
    static IntimateCoachService *_sharedService = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedService = [[self alloc] init];
    });
    
    return _sharedService;
}

- (IntimateCalibrationStructure *)getCalibrationForIntimateActivitySteps:(NSArray *)activitySteps
{
    __block std::vector<int> pressures;
    
    [activitySteps enumerateObjectsUsingBlock:^(NSDictionary *step, NSUInteger idx, BOOL * _Nonnull stop) {
        pressures.push_back([step[@"pressure"] intValue]);
    }];
    
    AlgoIntime::calibration calibStruct = AlgoIntime::getCalibration(pressures);
    
    IntimateCalibrationStructure* intimCalibStructure = [IntimateCalibrationStructure init];
    intimCalibStructure.minValue = @(calibStruct.minPressure);
    intimCalibStructure.maxValue = @(calibStruct.maxPressure);
    intimCalibStructure.calibrationValue = @(calibStruct.calibrationValue);
    
    return intimCalibStructure;
}
- (NSNumber *)getTonicityForIntimateActivitySteps:(NSArray *)activitySteps
{
    __block std::vector<int> pressures;
    
    [activitySteps enumerateObjectsUsingBlock:^(NSDictionary *step, NSUInteger idx, BOOL * _Nonnull stop) {
        pressures.push_back([step[@"pressure"] intValue]);
    }];
    
    return @(AlgoIntime::getTonicity(pressures));
}


- (NSArray *)getActivitySkeletonForActivityType:(NSNumber *)activityType calibrationStructure:(IntimateCalibrationStructure *)calibrationStructure breathStructure:(IntimateCalibrationStructure *)breathStructure andLevel:(NSNumber *)level
{
    AlgoIntime::activityDescription desc;
    desc.name = [activityType intValue];
    desc.lvl = [level intValue];
    
    NSMutableArray *coll = [NSMutableArray new];
    
    AlgoIntime::calibration calib = {
        [calibrationStructure.minValue intValue],
        [calibrationStructure.maxValue intValue],
        [calibrationStructure.calibrationValue intValue]
    };
    
    AlgoIntime::calibration breath = {
        [breathStructure.minValue intValue],
        [breathStructure.maxValue intValue],
        [breathStructure.calibrationValue intValue]
    };
    
    std::vector<AlgoIntime::pairRawData> datas = AlgoIntime::getActivitySkeleton(desc, calib, breath);
    
    for (int i=0; i<datas.size(); i++) {
        AlgoIntime::pairRawData pairRaw = datas[i];
        AlgoIntime::rawData startRaw = pairRaw.startData;
        AlgoIntime::rawData endRaw = pairRaw.endData;
        
        PressureRawData *startRawObj = [PressureRawData init];
        startRawObj.time = @(startRaw.time);
        startRawObj.pressure = @(startRaw.pressure);
        
        PressureRawData *endRawObj = [PressureRawData init];
        endRawObj.time = @(endRaw.time);
        endRawObj.pressure = @(endRaw.pressure);
        
        PressurePairRawData *pairPressure =[PressurePairRawData init];
        pairPressure.startData = startRawObj;
        pairPressure.endData = endRawObj;
        [coll addObject:pairPressure];
    }
    
    return coll;
}


- (IntimateActivityResult *)getResultWithActivitySteps:(NSArray *)activitySteps
{
    IntimateActivityResult *result = [[IntimateActivityResult alloc] init];
    __block std::vector<AlgoIntime::activityStep> steps;
    
    NSMutableArray *stepsArr = [NSMutableArray new];

    [activitySteps enumerateObjectsUsingBlock:^(NSDictionary *stepDic, NSUInteger idx, BOOL * _Nonnull stop) {
        AlgoIntime::activityStep sp;
        sp.pressure = [stepDic[@"pressure"] doubleValue];
        sp.timestamp = [stepDic[@"time"] doubleValue];
        sp.target = [stepDic[@"target"] doubleValue];
        
        [stepsArr addObject:[[IntimateActivityStep init] initWithDictionary:stepDic]];
        
        steps.push_back(sp);
    }];
    
    AlgoIntime::activityResult res = AlgoIntime::getResult(steps);
    
    result.score = @(res.score);
    result.hearts = @(res.hearts);
    result.isLevelUp = res.levelUp ? [NSNumber numberWithInt:1] : [NSNumber numberWithInt:0];
    result.activitySteps = [NSKeyedArchiver archivedDataWithRootObject:stepsArr];
    result.maxScore = @(res.maxScore);
    
    return result;
}

- (NSNumber *)getLeafLevelForIntimateState:(IntimateState *)intimateState
{
    return @(AlgoIntime::getLeafLevel([intimateState.currentSession intValue]-1, [intimateState.maxSession intValue]));
}

- (NSNumber *)getHeartsForIntimateSession:(IntimateSession *)intimateSession
{
    __block std::vector<int> hearts;
    [intimateSession.intimateActivities enumerateObjectsUsingBlock:^(IntimateActivity *activity, NSUInteger idx, BOOL * _Nonnull stop) {
        int val = [activity.hearts intValue];
        hearts.push_back(val);
    }];
    
    int result = AlgoIntime::getSessionHearts(hearts);
    
    return @(result);
}



@end
