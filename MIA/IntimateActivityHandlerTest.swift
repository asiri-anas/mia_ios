//
//  IntimateActivityHandler.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 16/07/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation


@objc protocol IntimateActivityHandlerDelegateTest: NSObjectProtocol {
    /*!
     @brief This method is called when the exercise is completed (when timer ends).
     @param activity The IntimateActivityObject that has been saved right after the end of the exercise.
     */
    func didFinish(_ activity: IntimateActivity?, levelUp: Bool)
    /// This method is called when a calibration exercise finishes.
    func didFinishCalibration()
    /// This method is called when a breath exercise finishes.
    //func didFinishBreath(_ calibrationStructure: IntimateCalibrationStructure?)
    /// This method is the callback of @c stopExercise method. It's called when pressure request are stopped.
    func didExerciseStop()
    /// This method is called when the bluetooth connection breaks involuntarily.
    func didConnectionInterrupted()
}
class IntimateActivityHandlerTest: NSObject, BleServiceOutput {
    func onConnected() {
        
    }
    
    func onReady() {
        
    }
    
    func onDisconnected() {
        
    }
    
    func onBatteryLevel(level: Int) {
        
    }
    
    func onGettingPressureStarted() {
        
    }
    
    func onGettingPressureStopped() {
        
    }
    
    func onPressureDataReceived(data: Int) {
        
    }
    
    func onScanStarted() {
        
    }
    
    func onScanStopped() {
        
    }
    
    
    var activityType: IntimateActivityType?
    var activityLevel: NSNumber?
    var exoDuration: NSNumber?
    weak var delegate: IntimateActivityHandlerDelegate?
    var isFreeExo = false
    var name: NSNumber?
    var exoViewModel: IntimateExoViewModel?
    var calibrationHistory: CalibrationHistory?
    var shouldCalibrate = false
    var isDisconnectVolunteer = false
    
    init(_ viewModel: IntimateExoViewModel?, andSelectedLevel selectedLevel: NSNumber?) {
        super.init()
        self.exoViewModel = viewModel
        let an = viewModel?.activityType?.type as? NSNumber
        self.name = an
        activityLevel = selectedLevel
        if let aType = viewModel?.activityType {
            activityType = aType
        }
        //IOSNDKHelper.NDKReceiver = self
        BleService.instance.bleOutput = self
        isDisconnectVolunteer = false
    }
    
    //initForCalibration
    init(withDuration duration: NSNumber?) {
        super.init()
        self.exoDuration = duration
        self.name = Constants.CALIBRATION
        //IOSNDKHelper.NDKReceiver = self
        BleService.instance.bleOutput = self
        isDisconnectVolunteer = false
    }
    
    //initForBreath
    init(withDuration duration: NSNumber?, withCalibration calibration: Bool) {
        super.init()
        self.exoDuration = duration
        self.name = Constants.BREATH
        //IOSNDKHelper.NDKReceiver = self
        BleService.instance.bleOutput = self
        shouldCalibrate = calibration
        isDisconnectVolunteer = false
    }
    
    
    
}
