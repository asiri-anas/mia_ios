//
//  CalibrationLayer.hpp
//  mymercurochrome
//
//  Created by Rémi Caroff on 16/12/2015.
//  Copyright © 2015 V-Labs. All rights reserved.
//

#ifndef CalibrationLayer_h
#define CalibrationLayer_h

#include <stdio.h>
#include "cocos2d.h"
#include "AbstractLayer.h"

using namespace cocos2d;

/**
 * Layer for Calibration exercise
 */
class CalibrationLayer : public AbstractLayer {
    
public:
    ~CalibrationLayer();

    /** Create a Calibration Scene */
    static Scene *createScene(Value data);

    /** Init the view */
    virtual bool init();

    /** Macro definition to initialize the view */
    CREATE_FUNC(CalibrationLayer);
    
    void updateTitle(float dt);

private:
    /** Unserialize data from Native (Android iOS) */
    void unserializeData();

    /** Method called with scheduleUpdate() to draw line */
    //void update(float dt);

    cocos2d::EventListenerCustom* _customlistener;
	Label* _actionLabel;
	Size _visibleSize; // Visible from director;
	Vec2 _originDirector;
	float _maxPressure; // the max pressure of the pattern
	float _maxPressureAirbee;
	int _duration;
	float _currentTimeStamp;
    int _lastCalibrationValue;

    bool _isContract;
};



#endif /* CalibrationLayer_h */
