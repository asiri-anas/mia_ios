//
//  IntimateManager.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 03/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import CoreData

@objc class IntimateManager: NSObject {
    
    override private init() {}
    @objc static let instance = IntimateManager()
    
    /// Returns all created IntimateActivityTypes objects.
    @objc func fetchIntimateActivityTypes(withFilters filters: NSArray?) -> NSArray? {
        let managedObjectContext = CDManager.instance!.managedObjectContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        let entity = NSEntityDescription.entity(forEntityName: "IntimateActivityType", in: managedObjectContext!)
        fetchRequest.entity = entity
        if filters != nil && (filters?.count)! > 0 {
            let predicate: NSPredicate? = generatePredicate(withFilters: filters as! NSArray)
            fetchRequest.predicate = predicate
        }
        let sd = NSSortDescriptor(key: "type", ascending: true)
        fetchRequest.sortDescriptors = [sd]
        let error: Error?
        let result = try? managedObjectContext!.fetch(fetchRequest)
        return result! as NSArray
        
    }
    
    
    @objc func createIntimateActivity(with activityResult: IntimateActivityResult?, forType activityType: IntimateActivityType?, andLevel level: NSNumber?) -> IntimateActivity? {
        let intimateState = IntimateStateManager.instance.fetchIntimateState()
        let activity = IntimateActivity.create()
        activity?.intimateActivityType = activityType
        activity?.score = activityResult?.score
        activity?.hearts = activityResult?.hearts
        activity?.level = level
        activity?.maxScore = activityResult?.maxScore
        activity?.createdAt = Date()
        activity?.datas = activityResult?.activitySteps
        activity?.intimateStatus = intimateState?.intimateStatus
        let coach = IntimateCoachManager.instance.fetchIntimateCoach()
        coach?.lastActivity = Date()
        CDManager.instance?.saveContext()
        NotificationCenter.default.post(name: NSNotification.Name("intimate_data_source_updated"), object: nil)
        return activity
    }
    
    /*
     
     /// Returns all created IntimateProgramTypes objects.
     func fetchIntimateProgramTypes() -> [Any]? {
     let managedObjectContext = CDManager.instance!.managedObjectContext
     var fetchRequest = NSFetchRequest<NSFetchRequestResult>()
     //var entity = NSEntityDescription.entity(forEntityName: "IntimateProgramType", in: (CDManager.instance?.managedObjectContext)!)
     //fetchRequest.entity = entity
     var sd = NSSortDescriptor(key: "typeID", ascending: true)
     fetchRequest.sortDescriptors = [sd]
     var error: Error?
     var result = try? managedObjectContext!.fetch(fetchRequest)
     if error != nil {
     return nil
     }
     return result!
     
     }
     /// Will create a new IntimateSession object, and save it in base.
     func createIntimateSession() -> IntimateSession? {
     
     var state = IntimateStateManager.shared()?.fetchIntimateState()
     var session = IntimateSession.create()
     var coach = IntimateCoachManager.shared()?.fetchIntimateCoach()
     session?.intimateCoach = coach
     session?.intimateProgramType = state?.intimateProgramType
     session?.createdAt = Date()
     session?.level = state?.currentSession
     var base = IntimateProgramHandler.sharedManager()?.getIntimateSessionBase(at: Int((state?.currentSession)!) - 1, for: state?.intimateProgramType)
     session?.maxScore = base?.maxScore
     session?.duration = base?.totalDuration
     session?.intimateStatus = state?.intimateStatus
     CDManager.instance!.saveContext()
     NotificationCenter.default.post(name: NSNotification.Name("intimate_sessions_updated"), object: nil)
     return session
     
     }
     /*!
     @brief Will return an array of IntimateSession objects.
     @discussion The returned array is ordered from the most recent to the oldest.
     @param filters An array of Filter objects.
     @param count The fetch limit. 0 = no limit.
     
     
     */*/
    func fetchIntimateSessions(withFilters filters: [Any]?, withCount count: Int) -> NSArray? {
        let managedObjectContext = CDManager.instance!.managedObjectContext
        var fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        var entity = NSEntityDescription.entity(forEntityName: "IntimateSession", in: managedObjectContext!)
        fetchRequest.entity = entity
        if count > 0 {
            fetchRequest.fetchLimit = count
        }
        if filters != nil && (filters?.count)! > 0 {
            fetchRequest.predicate = generatePredicate(withFilters: filters as! NSArray)
        }
        //DebugLog("Predicate : %@", fetchRequest.predicate)
        var sd = NSSortDescriptor(key: "createdAt", ascending: false)
        fetchRequest.sortDescriptors = [sd]
        var error: Error?
        var result: NSArray? = try? managedObjectContext!.fetch(fetchRequest) as! NSArray
        if error != nil {
            return nil
        }
        return result!
        
    }
    
    
    /*!
     @brief Will return an array of IntimateActivity objects.
     @param filters An array of Filter objects.
     */
    func fetchIntimateActivities(withFilters filters: [Any]?) -> [Any]? {
        return nil //TODO
    }
    /*!
     @brief Will return an array of id<IntimateDataSourceProtocol> objects.
     @param filters An array of Filter objects.
     */
    func fetchIntimateDataSource(withFilters filters: [Any]?) -> [Any]? {
        return nil //TODO
    }
    /*!
     @brief Will create, save and return an IntimateActivity object.
     @param activityResult The IntimateActivityResult object built from the AlgoIntime.
     @param activityType The IntimateActivityType objct whitch the exercise has been made with.
     @param level The selected level of the exercise.
     */
    /*func createIntimateActivity(with activityResult: IntimateActivityResult?, for activityType: IntimateActivityType?, andLevel level: NSNumber?) -> IntimateActivity? {
     
     var intimateState = IntimateStateManager.shared()?.fetchIntimateState()
     var activity = IntimateActivity.create()
     activity?.intimateActivityType = activityType
     activity?.score = activityResult?.score
     activity?.hearts = activityResult?.hearts
     activity?.level = level
     activity?.maxScore = activityResult?.maxScore
     activity?.createdAt = Date()
     activity?.datas = activityResult?.activitySteps
     activity?.intimateStatus = intimateState?.intimateStatus
     var coach = IntimateCoachManager.shared()?.fetchIntimateCoach()
     coach?.lastActivity = Date()
     CDManager.instance!.saveContext()
     NotificationCenter.default.post(name: NSNotification.Name("intimate_data_source_updated"), object: nil)
     return activity
     
     }*/
    
    
    /*!
     @brief Will update the IntimateGoals of the week.
     @discussion If the last IntimateGoals has been created during the passed week, a new one will be created.
     @param sessionsCount The number of sessions the user has to do.
     */
    func updateIntimateGoals(withSessionsCount sessionsCount: NSNumber?) -> IntimateGoal? {
        var intimateGoals = fetchLastIntimateGoals()
        /*if intimateGoals == nil || !DateHelper.isDate(intimateGoals.createdAt, inRangeFirstDate: DateHelper.getStartOfWeek(for: Date()), lastDate: DateHelper.getEndOfWeek(for: Date())) {
         intimateGoals = IntimateGoal.create()
         intimateGoals?.createdAt = Date()
         }*/
        intimateGoals?.weeklySessionsCount = sessionsCount
        var coach = IntimateCoachManager.instance.fetchIntimateCoach()
        coach?.intimateGoal = intimateGoals
        CDManager.instance?.saveContext()
        NotificationCenter.default.post(name: NSNotification.Name("intimate_goals_updated"), object: ["intimate_goals": intimateGoals])
        return intimateGoals
    }
    
    
    /// Will fetch and return the last IntimateGoals object created.
    func fetchLastIntimateGoals() -> IntimateGoal? {
        /*var fetchRequest = NSFetchRequest<NSFetchRequestResult>()
         var entity = NSEntityDescription.entity(forEntityName: "IntimateGoals", in: (CDManager.shared()?.managedObjectContext)!)
         fetchRequest.entity = entity
         fetchRequest.fetchLimit = 1
         var sd = NSSortDescriptor(key: "createdAt", ascending: false)
         fetchRequest.sortDescriptors = [sd]
         var error: Error?
         var result = try? CDManager.shared()?.managedObjectContext?.fetch(fetchRequest)
         if error != nil {
         return nil
         }
         return result??.last as! IntimateGoal*/
        return nil
    }
    
    
    
    
    
    /// Returns the total score made for the given IntimateActivity type, in program context.
    func getTotalScore(forIntimateActivityType activityType: NSNumber?, andIntimateState intimateState: IntimateState?) -> NSNumber? {
        return nil //TODO
    }
    /// Returns the dictionary to send to remote server.
    func getDatasToSynchronize() -> [AnyHashable: Any]? {
        return nil //TODO
    }
    
    
    /// Will generate and return a NSPredicate object based on given Filter objects.
    func generatePredicate(withFilters filters: NSArray?) -> NSPredicate? {
        
        // Les parenthèses inutiles seront supprimées automatiquement.
        var lastAttribute = ""
        var predicateStr = "("
        var args = NSMutableArray()
        
        /*filters?.enumerateObjects({ filter, idx, stop in
         if idx > 0 {
         if (lastAttribute == (filter.attribute) {
         predicateStr += " OR "
         } else {
         predicateStr += ") AND ("
         }
         }
         if ((filter.relation == "BETWEEN") {
         var valueArray = (filter.value)
         predicateStr += "%K >= %@ AND %K <= %@"
         args.add([filter?.attribute, valueArray?[0], filter?.attribute, valueArray?[1]])
         } else {
         if let anAttribute = (filter as! Filter).attribute, let aRelation = (filter as AnyObject).relation {
         predicateStr += "\(anAttribute) \(aRelation) \("%@")"
         }
         if let aValue = (filter).value {
         args.add(aValue)
         }
         }
         lastAttribute = (filter).attribute ?? ""
         })*/
        predicateStr += ")"
        return (NSPredicate(format: predicateStr, argumentArray: args as! [Any]))
        
    }
    
    
}
