//
//  IntimateDataSourceProtocol.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 03/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation


protocol IntimateDataSourceProtocol: NSObjectProtocol {
    func getCreationDate() -> Date?
}
