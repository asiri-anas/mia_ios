//
//  Constants.h
//  Mercurochrome
//
//  Created by Rémi Caroff on 10/06/2015.
//  Copyright (c) 2015 V-Labs. All rights reserved.
//

#ifndef MIA_Constants_h
#define MIA_Constants_h


#define COACH_INTIMATE_TYPE     7

#define SPACE_INTIMATE  5
#define SPACE_EXOS      6

#define MENU_BUTTON_TAG 5


#define DAY_ID_SUNDAY       1
#define DAY_ID_MONDAY       2
#define DAY_ID_TUESDAY      3
#define DAY_ID_WEDNESDAY    4
#define DAY_ID_THURSDAY     5
#define DAY_ID_FRIDAY       6
#define DAY_ID_SATURDAY     7





#define BREATH_ACQUISITION_FREQUENCY 2

#define BREATH_COUNT_DOWN 3

#define PLAY_BUTTON_TAG 84765


#define BT_FITNESS_SERVICE                  @"0000FED0-494C-4F47-4943-544543480000"
#define BT_FITNESS_SETTINGS_CHARACTERISTIC  @"0000FED2-494C-4F47-4943-544543480000"
#define BT_FITNESS_DATA_CHARACTERISTIC      @"0000FED1-494C-4F47-4943-544543480000"

#define CLIENT_ID       @"1_1s8t0y2m5g4k4cwcg4080kw8s4o4ow0koo4og4g08wks44ogk0"
#define CLIENT_SECRET   @"23irfm179casc4w0osc4kw8k0s0k008o0c4w8w08ccgsg0gsg4"


#define ADVICE_CATEGORY_FAQ_ID      4
#define ADVICE_CATEGORY_INTIMATE_ID 7

#define FILTER_BY_DAY   1
#define FILTER_BY_WEEK  2
#define FILTER_BY_MONTH 3


#define FILTER_TYPE_DATE                1
#define FILTER_TYPE_INTIMATE_EXO        8
#define FILTER_TYPE_INTIMATE_PROGRAM    9
#define FILTER_TYPE_INTIMATE_HEARTS     10



/*!
 @typedef enum graphType
 
 @brief  A struct about the Graph type.
 
 @discussion
All types of graphs that are currently available.
 
 @param vrc Used to type the VRC graph in ExosBreathDetailsViewController.
 @param consistencyScoreEvolution  Used to type the Consistency Score Evolution graph in ExosBreathDetailsViewController.
 @param consistencyStackedBar Used to type the Consistency Evolution stacked bar graph in ZenConsistencyEvolutionWidget.
 @param consistencyLine Used to type the Consistency Evolution line graph in ZenConsistencyEvolutionWidget.
 
 @param stepsNumber Used to type the steps number line graph in FitnessStepsGraphWidget.
 @param activityDuration Used to type the bar graph in FitnessActivityLevelWidget.
 @param absorbedCalories Used to type the line graph in FitnessCaloriesEvolutionWidget.
 @param burntCalories Used to type the line graph in FitnessCaloriesEvolutionWidget.
 
 @param nightGraph Used to type the horizontal stacked bar in SleepNightGraphWidget.
 @param nightBarGraph Used to type the multiple bar graph in SleepBarGraphWidget.
 @param nightDurationsGraph Used to type the bar graph in SleepNightsDurationWidget.
 @param nightDurationsGoalsGraph Used to type the line graph in SleepNightsDurationWidget.
 @param timeToFallAsleepGraph Used to type the bar graph in SleepTimeToFallAsleepGraphWidget.
 @param timeToFallAsleepGoalsGraph Used to type the line graph in SleepTimeToFallAsleepGraphWidget.
 @param awakeningsNumberGraph Used to type the line graph representing the evolution of awakenings in SleepAwakeningsEvolutionGraphWidget.
 @param awakeningsNumberGoalsGraph Used to type the line graph representing the awakenings goals in SleepAwakeningsEvolutionGraphWidget.
 */
typedef enum {
 
    // Intimate
    intimateEvolution
    
} graphType;


typedef enum {
    
    DEEP_STRENGTHENING_FLAT,   // 0
    DEEP_STRENGTHENING_SQUARE, // 1
    DEEP_STRENGTHENING_SAW,    // 2
    SUPERFICIAL_STRENGTHENING, // 3
    LATCHING,                  // 4
    SLACKENING,                // 5
    BREATH,                    // 6
    CALIBRATION                // 7
    
} activityName;


typedef enum {
    
    SENSOR_TUTO_TYPE_CONNECT,
    SENSOR_TUTO_TYPE_RELOAD,
    SENSOR_TUTO_TYPE_INSERT,
    SENSOR_TUTO_TYPE_HOW_TO_USE,
    SENSOR_TUTO_TYPE_ALL
    
} SensorTutoType;




/***** Cocos constants *****/

#define SETUP_VIEW_EXERCISE "setupViewExercise"
#define SETUP_VIEW_CALIBRATION "setupViewCalibration"
#define SETUP_VIEW_BREATH "setupViewBreath"
#define POST_PRESSURE "postPressure"
#define ON_PRELOADER_INITIALIZED "onPreloaderInitialized"
#define ON_EXERCISE_INITIALIZED "onExerciseInitialized"
#define ON_EXERCISE_FINISHED "onExerciseFinished"
#define ON_BACK_PRESSED "onBackPressed"
#define END_SCENE "endScene"
#define TOGGLE_PAUSE "togglePause"

#define KEY_COLOR "color"
#define KEY_DATAS "datas"
#define KEY_DURATION "duration"
#define KEY_LAST_PRESSURE_MAX "lastPressureMax"
#define KEY_TIME "time"
#define KEY_PRESSURE "pressure"
#define KEY_TARGET "target"
#define KEY_LABEL "label"
#define KEY_EXERCISE_NUMBER "exerciseNumber"
#define KEY_ACTIVITY_STEPS "activitySteps"
#define KEY_POST_PRESSURE_EVENT "postPressureEvent"
#define KEY_MAX_PRESSURE_CALIBRATION "maxPressureCalibration"

#define PRELOADER_LAYER_SELECTORS "PreloaderLayerSelectors"
#define EXERCISE_LAYER_SELECTORS "ExerciseLayerSelectors"
#define ABSTRACT_LAYER_SELECTORS "AbstractLayerSelectors"
#define ACTIVITY_SERVICE_SELECTORS "ActivityServiceSelectors"
#define LEAF_SERVICE_SELECTORS "LeafServiceSelectors"

#define TIME_BY_SCREEN 10
#define MAX_PRESSURE_AIRBEE 600
#define PERCENT_PRESSURE_ABOVE 0.2
#define COUNTDOWN 5

#define OFFSET_AIRBEE 0
#define MAX_PRESSURE_ALGO 40

#define VALUE_RATIO_QUAD 4
#define VALUE_NUMBER_QUAD 5

#define STEP_FREQUENCY 0.25

#define DURATION_INSPIRATION_BREATH 4
#define DURATION_EXPIRATION_BREATH 6
#define DURATION_CYCLE_EXERCICE_CALIBRATION 5

#define BREATH_EXERCISE_TYPE 1

/***** end *****/


#define MAX_CALIBRATION 100
#define FREE_EXO_DURATION 120
#define PROGRAM_EXO_DURATION 60
#define INTIMATE_BREATH_DURATION 20
#define CALIBRATION_DURATION 30

#define SURVEY_WIDGET_TYPE_DATE 0
#define SURVEY_WIDGET_TYPE_SELECT 1
#define SURVEY_WIDGET_TYPE_SLIDER 2


#define DEEP_STRENGTHENING_FLAT 0
#define LATCHING_WITH_TEXT 1
#define SUPERFICIAL_WITH_TEXT 2
#define SUPERFICIAL_STRENGTHENING 3
#define LATCHING 4
#define SLACKENING 5
#define LATCHING_STRENGTHENING 6
#define BREATH 111
#define CALIBRATION 222
#define MESSAGE 333

#define DEFAULT_INTIMATE_GOALS 3

#define FLURRY_API_KEY @"RZ7WH69XXZ785RBGBPC5"

#define CONNECTION_TUTO_READ @"connectionTutoRead"


/***** Kegel *****/

#define DEVICE_STATUS_CONNECT       @"device_status_connect"
#define DEVICE_STATUS_DISCONNECT    @"device_status_disconnect"
#define DEVICE_BATTERY_UPDATED      @"device_battery_updated"
#define KEY_MODEL_NUMBER            @"modelNumber"
#define KEY_MODEL_TYPE              @"modelType"

#endif
