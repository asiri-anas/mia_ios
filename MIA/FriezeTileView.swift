//
//  FriezeTileView.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 21/06/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit

protocol FriezeTileDelegate: NSObjectProtocol {
    /// Called when a user tap on a tile.
    func friezeTileView(_ friezeTileView: FriezeTileView?, didSelect friezeTile: FriezeTile?)
    /*!
     @brief Called when the tile corresponds to the next session to make.
     @param point The point to scroll to.
     */
    func shouldScrollToCurrentFriezeTile(at point: CGPoint)
}
class FriezeTileView: UIView {
    @IBOutlet var cursorIV: UIImageView!
    @IBOutlet var tileButton: UIButton!
    @IBOutlet var heartsIV: UIImageView!
    weak var delegate: FriezeTileDelegate?
    func setContentWith(_ tile: FriezeTile?) {
    }
    @IBAction func tileTapped(_ sender: Any) {
    }
    
    
    
}
