//
//  LeafService.cpp
//  mymercurochrome
//
//  Created by Rémi Caroff on 16/12/2015.
//  Copyright © 2015 V-Labs. All rights reserved.
//

#include "LeafService.h"
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include "../NDKHelper/NDKHelper.h"
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#include "NDKHelper.h"
#endif

USING_NS_CC;

LeafService::~LeafService()
{
	NDKHelper::removeSelectorsInGroup(LEAF_SERVICE_SELECTORS);
}


static LeafService* s_leafService = nullptr;

LeafService* LeafService::getInstance()
{
    if (s_leafService == nullptr)
    {
    	s_leafService = new (std::nothrow) LeafService;
    }

    return s_leafService;
}

void LeafService::destroyInstance()
{
    if (s_leafService)
    {
        delete s_leafService;
        s_leafService = nullptr;
    }
}

bool LeafService::init(Node* node)
{

	NDKHelper::removeSelectorsInGroup(LEAF_SERVICE_SELECTORS);
	m_currentNode = node;

	NDKHelper::addSelector(LEAF_SERVICE_SELECTORS,
								   POST_PRESSURE,
								   CC_CALLBACK_2(LeafService::onPostPressure, this),
								   node);
    return true;
};

void LeafService::onPostPressure(Node *sender, Value data)
{
	if (!data.isNull() && data.getType() == Value::Type::MAP) {
		ValueMap valueMap = data.asValueMap();
		EventCustom event(KEY_POST_PRESSURE_EVENT);
		Value pressure = valueMap[KEY_PRESSURE];
        
		int pressureInt = pressure.asInt();
		void* pre = &pressureInt;
		event.setUserData(pre);
		m_currentNode->getEventDispatcher()->dispatchEvent(&event);
	}
}
