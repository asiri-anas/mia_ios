//
//  AuthDeviceSelectViewController.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 02/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit


class AuthDeviceSelectViewController: AbstractViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet var devicesTableView: UITableView!
    var user: User?
    var intimateStatus: IntimateStatus?
    let COACH_INTIMATE_TYPE = 7
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if user == nil {
            Registry.set("is_first_connection", withValue: (1))
        } else {
            setRightCloseButtonForModalNavigationController()
        }
        view.layoutIfNeeded()
        // Tests
        //_intimateStatus = [[IntimateStatusManager sharedManager] fetchAllIntimateStatus][0];
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setMercuNavigationBar()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        performSegue(withIdentifier: "showTuto", sender: indexPath)
        //[self performSegueWithIdentifier:@"showSurvey" sender:indexPath];
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 50
        } else {
            return 150
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
    if section == 0 {
        return 1
    } else {
        return 1
    }
        
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
    var selectCellIdentifier = "selectCell"
    var deviceCellIdentifier = "deviceCell"
    
    if indexPath.section == 0 {
        var selectCell = devicesTableView.dequeueReusableCell(withIdentifier: selectCellIdentifier, for: indexPath) as? TitleTableViewCell
        if selectCell == nil {
            selectCell = TitleTableViewCell()
        }
        selectCell?.setContentWithTitle(NSLocalizedString("select_device", comment: ""))
        return selectCell!
    } else {
        var deviceCell = devicesTableView.dequeueReusableCell(withIdentifier: deviceCellIdentifier, for: indexPath) as? DeviceTableViewCell
        if deviceCell == nil {
            deviceCell = DeviceTableViewCell()
        }
        deviceCell?.setContentWithDeviceType(COACH_INTIMATE_TYPE)
        return deviceCell!
    }
        
    }
    
    
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
        override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "showTuto") && (sender is IndexPath) {
            let atvc = segue.destination as? AuthTutoViewController
            atvc?.user = user
            atvc?.deviceType = COACH_INTIMATE_TYPE
        }
        //    if ([[segue identifier] isEqualToString:@"showSurvey"]) {
        //        AuthIntimateSurveyViewController *dest = [segue destinationViewController];
        //        dest.intimateStatus = _intimateStatus;
        //    }
    }
    
    
}
