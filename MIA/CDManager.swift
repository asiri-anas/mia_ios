//
//  CDManager.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 02/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import CoreData
import INSPersistentContainer

@objc class CDManager: NSObject {
    
 
    var managedObjectContext: NSManagedObjectContext?
    var persistentStoreCoordinator: NSPersistentStoreCoordinator?
    var managedObjectModel: NSManagedObjectModel?
    
    @objc static let instance: CDManager? = {
        let _sharedManager = CDManager()
        let stack = INSDataStackContainer(name: "MiaSwift")
        stack.loadPersistentStores(completionHandler: { desc, error in
            if error != nil {
                print(error)
                abort()
            }
        })
        _sharedManager.managedObjectContext = stack.viewContext
        //print(_sharedManager.managedObjectContext!.persistentStoreCoordinator)
        
        return _sharedManager
    }()
    
    
    /// Saves the current managedObjectContext.
    @objc func saveContext () {
        guard managedObjectContext!.hasChanges else { return }
        
        do {
            try managedObjectContext!.save()
        } catch let nserror as NSError {
            fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
        }
    }
    
    
    /// Clears the current managedObjectContext.
    @objc func clearContext() {
        
        // NSError * error;
        // retrieve the store URL
        // NSURL * storeURL = [[self.managedObjectContext persistentStoreCoordinator] URLForPersistentStore:[[[self.managedObjectContext persistentStoreCoordinator] persistentStores] lastObject]];
        // lock the current context
        managedObjectContext?.lock()
        managedObjectContext?.reset()
        //to drop pending changes
        //delete the store from the current managedObjectContext
        /* if ([[self.managedObjectContext persistentStoreCoordinator] removePersistentStore:[[[self.managedObjectContext persistentStoreCoordinator] persistentStores] lastObject] error:&error])
         {
         // remove the file containing the data
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:&error];
         //recreate the store like in the  appDelegate method
         [[self.managedObjectContext persistentStoreCoordinator] addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error];//recreates the persistent store
         }*/
        managedObjectContext?.unlock()
        UserManager.sharedManager()?.user = nil
        
    }
    
    func seedCoreDataContainerIfFirstLaunch(_ modelName: String) {
        
        // 1
        let previouslyLaunched = UserDefaults.standard.bool(forKey: "previouslyLaunched")
        if !previouslyLaunched {
            UserDefaults.standard.set(true, forKey: "previouslyLaunched")
            
            // Default directory where the CoreDataStack will store its files
            let directory = URL(fileURLWithPath: "")
            let url = directory.appendingPathComponent(modelName + ".sqlite")
            
            // 2: Copying the SQLite file
            let seededDatabaseURL = Bundle.main.url(forResource: modelName, withExtension: "sqlite")!
            _ = try? FileManager.default.removeItem(at: url)
            do {
                try FileManager.default.copyItem(at: seededDatabaseURL, to: url)
            } catch let nserror as NSError {
                fatalError("Error: \(nserror.localizedDescription)")
            }
            
            // 3: Copying the SHM file
            let seededSHMURL = Bundle.main.url(forResource: modelName, withExtension: "sqlite-shm")!
            let shmURL = directory.appendingPathComponent(modelName + ".sqlite-shm")
            _ = try? FileManager.default.removeItem(at: shmURL)
            do {
                try FileManager.default.copyItem(at: seededSHMURL, to: shmURL)
            } catch let nserror as NSError {
                fatalError("Error: \(nserror.localizedDescription)")
            }
            
            // 4: Copying the WAL file
            let seededWALURL = Bundle.main.url(forResource: modelName, withExtension: "sqlite-wal")!
            let walURL = directory.appendingPathComponent(modelName + ".sqlite-wal")
            _ = try? FileManager.default.removeItem(at: walURL)
            do {
                try FileManager.default.copyItem(at: seededWALURL, to: walURL)
            } catch let nserror as NSError {
                fatalError("Error: \(nserror.localizedDescription)")
            }
            
            print("Seeded Core Data")
        }
    }
    
    /// Returns a unique ID based on UUID generation.
    func generateUniqueID() -> String? {
        
        var uuidRef = CFUUIDCreate(nil)
        var uuidStringRef = CFUUIDCreateString(nil, uuidRef)
        return uuidStringRef as! String
        
    }
}
