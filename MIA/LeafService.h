//
//  LeafService.hpp
//  mymercurochrome
//
//  Created by Rémi Caroff on 16/12/2015.
//  Copyright © 2015 V-Labs. All rights reserved.
//

#ifndef LeafService_h
#define LeafService_h

#include "cocos2d.h"
#include "Constants.h"

using namespace cocos2d;

/*
 * This class handle all messages for leaf NDK Helper and send pressure
 */
class LeafService {
public:
	/** Get an instance of LeafService */
	static LeafService* getInstance();

	/** Destroy LeafService instance */
	static void destroyInstance();

    virtual ~LeafService();

	/** Init LeafService with the current node (ie: ExerciseLayer, BreathLayer, PreloaderLayer, etc.) */
    bool init(Node*);
private:

    /** NDK Method to receive pressure and post to the current node */
	void onPostPressure(Node* sender, Value data);


    Node* m_currentNode;
};



#endif /* LeafService_h */
