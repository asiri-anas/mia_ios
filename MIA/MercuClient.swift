//
//  MercuClient.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 14/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import AFNetworking


/// A singleton that will interact with the remote API.
@objc class MercuClient: AFHTTPRequestOperationManager {
    /// Returns a singleton instance of MercuClient.

    static let instance = MercuClient(baseURL: URL(string:"http://api.conseils.urgotech.xyz/"))
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(baseURL url: URL?) {
        super.init(baseURL: url)
        self.requestSerializer = AFJSONRequestSerializer()
        self.responseSerializer = AFJSONResponseSerializer()
        self.requestSerializer.setValue("application/json", forHTTPHeaderField: "Accept")
        self.requestSerializer.setValue("application/json", forHTTPHeaderField: "Content-Type")
        self.requestSerializer.setValue(NSLocale.current.identifier, forHTTPHeaderField: "Accept-Language")
        //[_sharedClient.requestSerializer setValue:@"1.0" forHTTPHeaderField:@"X-Api-Version"];
        print("LOCALE IDENTIFIER : %@", NSLocale.current.identifier)
    }
    

    
    func httpRequestOperation(_ request: URLRequest,_ success: @escaping (_ operation: AFHTTPRequestOperation, _ responseObject: Any?) -> Void,_ failure: @escaping (_ operation: AFHTTPRequestOperation, _ error: Error) -> Void) -> AFHTTPRequestOperation {
        return super.httpRequestOperation(with: request, success: success)
    }
    
    /*!
     @brief Will set the access token as authorization header for HTTP requests.
     @param token A string representing the access token.
     */
    func setAuthorizationHeader(_ token: NSString?) {
        print("ACCESS TOKEN : %@", token)
        if token == nil {
            requestSerializer.setValue("", forHTTPHeaderField: "Authorization")
        } else {
            requestSerializer.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        }
    }
    
    
    /*!
     @brief Will fetch and create a Token object.
     @discussion
     The Token object implements NSCoding protocol and will be recorded in the NSUserDefaults for key <b>"token"</b>.
     */
    func getAccessToken(withCompletion block: @escaping (Error?) -> Void) {
        
        var params = ["grant_type": "client_credentials", "client_id": Constants.CLIENT_ID, "client_secret": Constants.CLIENT_SECRET]
        post("oauth/v2/token", parameters: params, success: { operation, responseObject in
            var token = Token(dictionary: responseObject as! NSDictionary)
            Registry.setToken(token)
            self.setAuthorizationHeader(token.accessToken as NSString)
            print("GET ACCESS TOKEN", "success")
            block(nil)
        }, failure: { operation, error in
            var err = Error(message: nil, andStatusCode: operation?.response?.statusCode as! NSNumber)
            print("GET ACCESS TOKEN", err, operation?.response?.statusCode)
            block(err)
        })
        
    }
    
    
    
    /*!
     @brief Will fetch and create an AdviceCategoriesList object, if no error object is raised.
     @discussion If an error is raised, an Error object will be created and returned in the block.
     */
    func getAdviceCategoriesList(withCompletion block: @escaping (AdviceCategoriesList?, Error?) -> Void) {
        
        get("advices-categories", parameters: nil, success: { operation, responseObject in
            var list: AdviceCategoriesList = responseObject as! AdviceCategoriesList
            block(list, nil)
        }, failure: { operation, error in
            var err = Error(message: nil, andStatusCode: operation?.response?.statusCode as! NSNumber)
            block(nil, err)
        })
        
    }
    
    
    
    /*!
     @brief Will fetch and create an AdviceList object.
     @discussion If an error is raised, an Error object will be created and returned in the block.
     
     The categoryID possible values are listed below :
     
     • <b>ADVICE_CATEGORY_ZEN_ID</b> value : 1
     
     • <b>ADVICE_CATEGORY_SLEEP_ID</b> value : 2
     
     • <b>ADVICE_CATEGORY_FITNESS_ID</b> value : 3
     
     • <b>ADVICE_CATEGORY_FAQ_ID</b> value : 4
     */
    func getAdvicesList(fromCategoryID categoryID: NSNumber?, withCompletion block: @escaping (AdvicesList?, Error?) -> Void) {
        
            print("GET ADVICE LIST", requestSerializer)
            print("GET ADVICE LIST", self.baseURL!.absoluteString + "advice-categories/\(categoryID!)/advices")
            get("advice-categories/\(categoryID!)/advices", parameters: nil, success: { operation, responseObject in
                print(responseObject)
                var list: AdvicesList = AdvicesList(dictionary: responseObject as? NSDictionary)
                print("GET ADVICE LIST", list)
                block(list, nil)
            }, failure: { operation, error in
                var err = Error(message: nil, andStatusCode: operation?.response?.statusCode as! NSNumber)
                print("GET ADVICE LIST", err, operation?.response?.statusCode)
                block(nil, err)
            })
        
    }
    
    func setAuthorizationHeader(_ token: String?) {
        print("ACCESS TOKEN : ", token)
        if token == nil {
            requestSerializer.setValue("", forHTTPHeaderField: "Authorization")
        } else {
            requestSerializer.setValue("Bearer \(token ?? "")", forHTTPHeaderField: "Authorization")
        }
    }

    /*!
     @brief Will fetch and return the latest advice for a given categoryID.
     @discussion
     The returned advice is used to display itself in the last widget of a space synthesis screen.
     */
    func getLatestAdvice(fromCategoryID categoryID: NSNumber?, withCompletion block: @escaping (Advice?, Error?) -> Void) {
        
            get("advice-categories/\(categoryID)/latest-advice", parameters: nil, success: { operation, responseObject in
                var advice: Advice = responseObject as! Advice
                block(advice, nil)
            }, failure: { operation, error in
                var err = Error(message: nil, andStatusCode: operation?.response?.statusCode as! NSNumber)
                block(nil, err)
            })
        
    }
    
    
    
func getQuestionsForCategoryID(_ categoryID: NSNumber?, withCompletion block: @escaping (QuestionsList?, Error?) -> Void) {
    if let anID = categoryID {
        get("advice-categories/\(anID)/questions", parameters: nil, success: { operation, responseObject in
            let list = QuestionsList(dictionary: responseObject as! NSDictionary)
            block(list, nil)
        }, failure: { operation, error in
            let err = Error(message: nil, andStatusCode: operation?.response?.statusCode as! NSNumber)
            block(nil, err)
        })
    }
}
    
    
    /*
    /*!
     @brief Will post datas to synchronize.
     @discussion The datas to synchronize can be get with @c getDatasToSynchronize method from IntimateManager.
     */
    func postIntimateDatas(_ intimatDatas: [AnyHashable: Any]?) {
    }*/
}
