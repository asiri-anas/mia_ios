//
//  AppDelegate.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 30/04/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import UIKit
import Flurry_iOS_SDK
import UICKeyChainStore
import iOS_Slide_Menu
import CoreData
import Fabric
import Crashlytics
import AFNetworking

@UIApplicationMain
class NativeAppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    
    let MENU_BUTTON_TAG = 5
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        //Anaytics
        Fabric.with([Crashlytics.self])
        
        UIDevice.current.isProximityMonitoringEnabled = false
        window?.backgroundColor = UIColor.white
        
        if UIApplication.instancesRespond(to: #selector(UIApplication.registerUserNotificationSettings(_:))) {
            application.registerUserNotificationSettings(UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil))
        }
        
        application.isIdleTimerDisabled = true
        
        // Avoid background mode
        UIApplication.shared.statusBarStyle = .default
        
        //Fonts
        let attributes = [NSAttributedStringKey.font: UIFont(name: "SourceSansPro-Regular", size: 20), NSAttributedStringKey.foregroundColor: UIColor.pinkMercu()]
        try UINavigationBar.appearance().titleTextAttributes = attributes
        //Notif new version available
        shouldMigrate()
        
        
        reloadTokenIfNeeded()
        var keychain = UICKeyChainStore(service: "com.urgotech.mymercurochrome")
        if !(keychain["device_identifier"] != nil) {
            //DebugLog("idendifier for vendor : %@", UIDevice.current.identifierForVendor?.uuidString)
            keychain["device_identifier"] = UIDevice.current.identifierForVendor?.uuidString ?? ""
        }
        
        return true
    }
    
    
    func reloadTokenIfNeeded() {//TODO MercuClient
        if Registry.has("token") {
            print("HAS TOKEN")
            let token = Registry.getToken()
            MercuClient.instance.setAuthorizationHeader(token?.accessToken)
        } else {
            print("NO TOKEN")
            MercuClient.instance.getAccessToken(withCompletion: {(_ error: Error?) -> Void in
                if error != nil {
                    Validator.validateError(error, with: nil)
                }else{
                    print("NO ERROR")
                }
            })
        }
    }
    
    func shouldMigrate() {//TODO DBUpdater
        /*let dbUpdater = DBUpdater()
         DebugLog("BUNDLE VERSION : %@", Bundle.main.infoDictionary["CFBundleShortVersionString"])
         if Registry.has("last_version") {
         if !(Bundle.main.infoDictionary["CFBundleShortVersionString"] == Registry.get("last_version")) {
         dbUpdater.updateDataBase()
         }
         } else {
         dbUpdater.initialImport()
         }*/
    }
    
    func loadAuthStoryBoard() {
        let sb = UIStoryboard(name: "Auth", bundle: nil)
        let nc = sb.instantiateInitialViewController() as? UINavigationController
        UIView.transition(from: (window?.rootViewController?.view)!, to: (nc?.view)!, duration: 0.5, options: .transitionCrossDissolve, completion: {(_ finished: Bool) -> Void in
            self.window?.rootViewController = nc
        })
    }
    
    func loadExosStoryBoard() -> UINavigationController {
        let sb = UIStoryboard(name: "Exos", bundle: nil)
        return sb.instantiateInitialViewController() as! UINavigationController
    }
    
    
    func loadMainStoryboard() {
        let sb = UIStoryboard(name: "Intimate", bundle: nil)
        let mainSB = UIStoryboard(name: "Intimate", bundle: nil)
        let nc = sb.instantiateInitialViewController() as? SlideNavigationController
        let menuBtn = UIButton(type: .custom)
        menuBtn.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        menuBtn.setImage(UIImage(named: "menu_button"), for: .normal)
        menuBtn.addTarget(SlideNavigationController.sharedInstance(), action: #selector(SlideNavigationController.toggleLeftMenu), for: .touchUpInside)
        let `left` = UIBarButtonItem(customView: menuBtn)
        `left`.tag = MENU_BUTTON_TAG
        nc?.leftBarButtonItem = `left`
        if let aView = nc?.view {
            UIView.transition(from: (window?.rootViewController?.view)!, to: aView, duration: 0.5, options: .transitionCrossDissolve, completion: {(_ finished: Bool) -> Void in
                self.window?.rootViewController = nc
                let smvc = mainSB.instantiateViewController(withIdentifier: "SlideMenuViewController") as? SlideMenuViewController
                SlideNavigationController.sharedInstance().leftMenu = smvc
                
            })
        }
    }

    
    func loadMainStoryboardToStartIntimateProgram() {
         loadMainStoryboard()
         let exosSB = UIStoryboard(name: "Exos", bundle: nil)
         let exosNC = exosSB.instantiateInitialViewController() as? UINavigationController
         let eipvc = exosSB.instantiateViewController(withIdentifier: "ExosIntimateParamsViewController") as? ExosIntimateParamsViewController
         if let anEipvc = eipvc {
         exosNC?.pushViewController(anEipvc, animated: false)
         }
         if let aNC = exosNC {
         self.window?.rootViewController?.present(aNC, animated: true) {() -> Void in }
         }
        
         
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
}

