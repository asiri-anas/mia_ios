//
//  Singleton.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 02/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation


protocol MenuProtocol: class {
    func notifyVC()
}


class Singleton: NSObject {
    
    weak var vc: MenuProtocol?
    
    static let instance = Singleton()
    
    override init() {
        super.init()
        vc = nil
    }
    
    
    func setVC(_ vc: MenuProtocol?) {
        self.vc = vc
    }
    
    
}
