//
//  TutoSensorHowToUse2.swift
//  mia
//
//  Created by Xavier Bohin on 19/04/2018.
//  Copyright © 2018 V-Labs. All rights reserved.
//

import Foundation
import UIKit

class TutoSensorHowToUse2: UIView {
    class func createView() -> TutoSensorHowToUse2? {
        
        var view = Bundle.main.loadNibNamed("TutoSensorHowToUse2", owner: nil, options: nil)?.last as? TutoSensorHowToUse2
        if (view is TutoSensorHowToUse2) {
        }
        return view
        
    }
}
