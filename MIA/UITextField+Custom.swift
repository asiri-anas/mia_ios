//
//  UITextField+Custom.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 14/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit
extension UITextField {
    func setMercuStyle() {
        
        layer.borderColor = UIColor.veryLightGrayMercu()?.cgColor
        layer.borderWidth = 1.0
        backgroundColor = UIColor.white
        self.font = UIFont(name: "SourceSansPro-Regular", size: (font?.pointSize)!)
        setPadding()
        
    }
    
    func setPadding() {
        
        var paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: frame.size.height))
        paddingView.backgroundColor = UIColor.clear
        leftView = paddingView
        leftViewMode = .always
        
    }
}
