//
//  DotsPinkView.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 07/09/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit
class DotsPinkView: UIView {
    
    override func draw(_ rect: CGRect) {
    let path = UIBezierPath()
    path.move(to: CGPoint(x: frame.size.width / 2, y: 2))
    path.addLine(to: CGPoint(x: frame.size.width / 2, y: frame.size.height))
    path.lineWidth = 2.0
    let dashes: [CGFloat] = [0,10]
        path.setLineDash(dashes, count: 2, phase: 0)
    path.lineCapStyle = .round
        UIColor.pinkMercu()?.setStroke()
    path.stroke()
}
    
}
