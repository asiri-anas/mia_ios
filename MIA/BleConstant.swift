//
//  BleConstant.swift
//  mia
//
//  Created by Valentin Ferriere on 16/09/2017.
//  Copyright © 2017 V-Labs. All rights reserved.
//

import Foundation
import CoreBluetooth

// because objc cannot see swift structs
@objc class BleConstant: NSObject {
    static let names = ["Perifit_II", "Perifit(o)"]
    static let pressureNotifyCharacteristic = "AA41"
    static let batteryReadCharacteristic = "Battery Level"
    static let acquisitionFrequency = 0.265
    
    @NSManaged var sensorNames: [NSString]?
    
    override init() {
        super.init()
        self.sensorNames = BleConstant.names as? [NSString]
    }
    
}
