//
//  IntimateCoachManager.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 03/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import CoreData


class IntimateCoachManager: NSObject {
    
    // MARK: - Singleton -
    override private init() {}
    @objc static let instance = IntimateCoachManager()
    
    func create() -> IntimateCoach? {
        let intimateCoach = NSEntityDescription.insertNewObject(forEntityName: "IntimateCoach", into: (CDManager.instance!.managedObjectContext)!) as! IntimateCoach
        return intimateCoach
    }
    
    /// This method will create and save an IntimateCoach model.
    func createInitialIntimateCoach() -> IntimateCoach? {
        
        var coach = create()
        coach?.intimateCoachID = generateUniqueID()!
        coach?.createdAt = Date()
        if Registry.has("user_id") {
            coach?.user = UserManager.sharedManager()?.fetchUser()
        }
        coach?.lastSync = Date()
        CDManager.instance!.saveContext()
        return coach
        
    }
    
    /// Will fetch and return the IntimateCoach.
    func fetchIntimateCoach() -> IntimateCoach? {
        
        var fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        var entity = NSEntityDescription.entity(forEntityName: "IntimateCoach", in: (CDManager.instance!.managedObjectContext)!)
        fetchRequest.entity = entity
        fetchRequest.fetchLimit = 1
        var error: Error?
        var coach = try? CDManager.instance!.managedObjectContext?.fetch(fetchRequest).last as? IntimateCoach
        if error != nil {
            return nil
        }
        return coach!
        
    }
    
    
    func generateUniqueID() -> String? {
        let uuidRef = CFUUIDCreate(nil)
        let uuidStringRef = CFUUIDCreateString(nil, uuidRef)
        return uuidStringRef as! String
    }
    
}
