//
//  FriezeTile.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 04/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation

/// The value object necessary to set contents of a FriezeTileView object.
class FriezeTile: NSObject {
    /// The current IntimateState object.
    var intimateState: IntimateState?
    /// The IntimateSession object associated to the tile.
    var intimateSession: IntimateSession?
    /// The initializer.
    init(_intimateSession: IntimateSession?, andIntimateState _intimateState: IntimateState?) {
      
    super.init()
    intimateSession = _intimateSession
    intimateState = _intimateState
        
    }
}
