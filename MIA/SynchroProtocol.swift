//
//  SynchroProtocol.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 03/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation

protocol SynchroProtocol: NSObjectProtocol {
    /// The objects implementing this protocol should return a Dictionary representation of itself to post to the server.
    func getDictionaryRepresentation() -> [AnyHashable: Any]?
}
