//
//  ExosIntimateLockingViewController.m
//  mymercurochrome
//
//  Created by Rémi Caroff on 02/12/2015.
//  Copyright © 2015 V-Labs. All rights reserved.
//
@objc class CocosHandler : NSObject {
    
    private var intimateState: IntimateState?
    private var intimateSession: IntimateSession?
    private var currentActivityIndex: Int = 0
    private var shouldCreateSession = false
    private var programParser: IntimateProgramParser?
    private var programBase: IntimateProgram?
    private var isFreeExo: Bool?
    private var activity: IntimateActivityHandler?
    private var nextActivity: IntimateActivityBase?
    private var currentCalibrationStructure: IntimateCalibrationStructure?
    private var activityType: IntimateActivityType?
    private var exoViewModel: IntimateExoViewModel?
    private var selectedLevel: NSNumber?
    init(isFreeExo : Bool?) {
        self.programParser = IntimateProgramParser()
        self.currentActivityIndex = 0
        self.shouldCreateSession = true
        self.isFreeExo = isFreeExo
    }
    
    @objc func getIntimateState() -> IntimateState? {
        do {
            var results = try CDManager.instance!.managedObjectContext!.fetch(IntimateCoach.intimateCoachFetchRequest())
            print("AUTH", "fetch intimateCoach")
            for singleData in results {
                return singleData.intimateState
            }
        } catch {
            print("Failed")
        }
        return nil
    }
    
    
    @objc func startNewSessionForCurrentState() {
        intimateState = getIntimateState()
        programBase = programParser?.getBaseProgram(forKey: intimateState?.intimateProgramType?.label)
    }
    
    @objc func getCurrentSession() -> IntimateSessionBase? {
        let sessionBase: IntimateSessionBase? = programBase?.sessions![(intimateState?.currentSession?.intValue)! - 1]
        return sessionBase
    }
    
    @objc func getNextActivity() -> IntimateActivityBase? {
        let sessionBase: IntimateSessionBase? = getCurrentSession()
        var activityBase: IntimateActivityBase?
        if currentActivityIndex < (sessionBase?.activities?.count)! {
            activityBase = sessionBase?.activities![currentActivityIndex]
        } else {
            activityBase = nil
        }
        currentActivityIndex += 1
        return activityBase
    }
    
    
    /*func getFriezeTiles(for state: IntimateState?) -> [Any]? {
        let program: IntimateProgram? = programParser.getBaseProgram(forKey: state?.intimateProgramType.label)
        var tiles = [AnyHashable](repeating: 0, count: program?.sessions.count ?? 0)
        var filters = [AnyHashable](repeating: 0, count: 2)
        filters.append(Filter(attribute: "intimateProgramType.typeID", relation: "==", value: state?.intimateProgramType.typeID))
        filters.append(Filter(attribute: "createdAt", relation: ">=", value: state?.programStart))
        let sessions = IntimateManager.shared().fetchIntimateSessions(withFilters: filters, withCount: 0).reverseObjectEnumerator().allObjects
        program?.sessions.enumerateObjects({ sessionBase, idx, stop in
            if idx < sessions.count {
                let tile = FriezeTile(intimateSession: sessions[idx], andIntimateState: state)
                tiles.append(tile)
            } else {
                let tile = FriezeTile()
                tile.intimateState = state
                tiles.append(tile)
            }
        })
        return tiles
    }*/
    
    @objc func getCumulatedSessionsMaxScore(forProgramKey key: String?) -> NSNumber? {
        var totalScore: Int = 0
        let program: IntimateProgram? = programParser?.getBaseProgram(forKey: key)
        for sessionBase in (program?.sessions)! {
            let score: Int = sessionBase.maxScore?.intValue ?? 0
            totalScore += score
        }
        return NSNumber.init(integerLiteral: totalScore)
    }
    
    @objc func terminateSession() -> IntimateSession? {
        intimateSession?.finishedAt = Date()
        var increment : Int? = (intimateState?.currentSession?.intValue)! + 1
        intimateState?.currentSession = NSNumber(value: increment ?? 1)
        if (intimateState?.intimateProgramType?.label == "prog_perineal_pain") {
            intimateSession?.intimateVaginalPain = intimateState?.intimateVaginalPain
        } else {
            intimateSession?.intimateComfort = intimateState?.intimateComfort
        }
        CDManager.instance?.saveContext()
        NotificationCenter.default.post(name: NSNotification.Name("intimate_data_source_updated"), object: nil)
        performSelector(inBackground: #selector(self.synchronizeIntimateDatas), with: nil)
        return intimateSession
    }
    
    @objc func synchronizeIntimateDatas() {
        let datas = IntimateManager.instance.getDatasToSynchronize()
        //TODO: add
        //MercuClient.shared().postIntimateDatas(datas)
    }
    @objc func save(_ activity: IntimateActivity?, levelUp: Bool) -> IntimateActivity? {
        if shouldCreateSession {
            //TODO: add
            //intimateSession = IntimateManager.instance.createIntimateSession()
        }
        shouldCreateSession = false
        activity?.intimateSession = intimateSession
        var newScore : Int = intimateSession?.score?.intValue ?? 0 + (activity?.score?.intValue)!
        intimateSession?.score = NSNumber.init(integerLiteral: newScore ?? 0)
        if levelUp {
            let increment : Int? = (activity?.intimateActivityType?.programLevelReached?.intValue)! + 1
            activity?.intimateActivityType?.programLevelReached = NSNumber(value: increment ?? 1)
        }
        CDManager.instance?.saveContext()
        updateSessionHearts()
        return activity
    }
    
    func updateSessionHearts() {
        /*intimateSession = IntimateManager.shared().fetchIntimateSessions(withFilters: nil, withCount: 1).last
        intimateSession.hearts = IntimateCoachService.shared().getHeartsForIntimateSession(intimateSession)
        CDManager.shared().saveContext()
        NotificationCenter.default.post(name: NSNotification.Name("intimate_data_source_updated"), object: nil)*/
    }
    @objc func clean() {
        currentActivityIndex = 0
        shouldCreateSession = true
    }
    func getIntimateProgram(forKey programKey: String?) -> IntimateProgram? {
        return programParser?.getBaseProgram(forKey: programKey)
    }
    func getSessionsCount(forProgramKey progKey: String?) -> Int {
        let prog: IntimateProgram? = programParser?.getBaseProgram(forKey: progKey)
        return prog?.sessions?.count ?? 0
    }
    func getIntimateSessionBase(at index: Int, for programType: IntimateProgramType?) -> IntimateSessionBase? {
        let program: IntimateProgram? = programParser?.getBaseProgram(forKey: programType?.label)
        return program?.sessions![index]
    }
    
    func getIntimateActivityBase(forProgramKey progKey: String?) -> [Any]? {
        var toReturn = [AnyHashable]()
        var activities = [AnyHashable]()
        let program: IntimateProgram? = programParser?.getBaseProgram(forKey: progKey)
        for session in (program?.sessions)! {
            if let anActivities = session.activities {
                activities.append(contentsOf: anActivities)
            }
        }
        var isFirst = true
        for case let activity as IntimateActivityBase in activities {
            var hasAlready = false
            if isFirst {
                if activity.type ?? 0 != Constants.BREATH && activity.type ?? 0 != Constants.CALIBRATION {
                    toReturn.append(activity)
                    isFirst = false
                }
            } else {
                for case let obj as IntimateActivityBase in toReturn {
                    if obj.type ?? 0 == activity.type ?? 0 || activity.type ?? 0 == Constants.BREATH || activity.type ?? 0 == Constants.CALIBRATION {
                        hasAlready = true
                    }
                }
                if !hasAlready {
                    toReturn.append(activity)
                    
                }
            }
        }
        return toReturn
    }
    
}
