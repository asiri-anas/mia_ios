//
//  MainHelpViewController.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 05/07/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit
import iOS_Slide_Menu

class MainHelpViewController: AbstractViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet var tableView: UITableView!
    

    var isSlideStack = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = NSLocalizedString("help_faq", comment: "").uppercased()
        isSlideStack = navigationController is SlideNavigationController
        setGradientBackground()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //[self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"nav_pink"] forBarMetrics:UIBarMetricsDefault];
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        enableSwipeMenu(isSlideStack)
        if !isSlideStack {
            setLeftCloseButtonForModalNavigationController()
        } else {
            setMenuButtonForNavigationController()
        }
    }
    
    override func slideNavigationControllerShouldDisplayLeftMenu() -> Bool {
        return isSlideStack
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        switch indexPath.row {
            case 0:
                performSegue(withIdentifier: "showList", sender: nil)
        default:
                performSegue(withIdentifier: "showList", sender: nil)
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "deviceCell"
        var cell = self.tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? FAQCategoryTableViewCell
        if cell == nil {
            cell = FAQCategoryTableViewCell()
        }
        switch indexPath.row {
            case 0:
                cell?.setContentForDeviceType(0)
        default:
                cell?.setContentForDeviceType(0)
        }
        if let aCell = cell {
            return aCell
        }
        return UITableViewCell()
    }
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "showList") && (sender is NSNumber) {
            let mqlvc = segue.destination as? MainQuestionsListViewController
            mqlvc?.categoryID = sender as! NSNumber
        }
    }
    
    
}
