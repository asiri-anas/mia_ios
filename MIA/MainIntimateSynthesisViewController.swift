//
//  MainIntimateSynthesisViewController.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 20/06/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit
import  iOS_Slide_Menu


class MainIntimateSynthesisViewController: AbstractViewController, BarButtonItemDelegate {
    func didTapLeftBarButtonItem() {

        //
        
    }
    
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var contentView: UIView!
    

    var widgetsSB: UIStoryboard?
    var intimateGoalsWidget: IntimateGoalsWidget?
    var intimateProgramFriezeWidget: IntimateProgramFriezeWidget?
    //var intimateProgramFinishedWidget: IntimateProgramFinishedWidget?
    // intimateScoresWidget: IntimateScoresWidget?
    //var intimateEvolutionWidget: IntimateEvolutionWidget?
    var intimateState: IntimateState?
    var intimateGoals: IntimateGoal?
    var intimateAdviceWidget: IntimateAdviceWidget?
    var intimateSessions = [Any]()
    //var calibrationHistories = [Any]()
    var shouldDisplayEvolution = false
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setGradientBackground()
        
        setMenuButtonForNavigationController()
        
        
        let mainSB = UIStoryboard(name: "Intimate", bundle: nil)
        let smvc = mainSB.instantiateViewController(withIdentifier: "SlideMenuViewController") as? SlideMenuViewController
        SlideNavigationController.sharedInstance().leftMenu = smvc
        
        
        navigationItem.title = NSLocalizedString("intimate_space", comment: "").uppercased()
        
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.pinkMercu()]

        
        widgetsSB = UIStoryboard(name: "IntimateWidgets", bundle: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.sessionsGoalsUpdated(_:)), name: NSNotification.Name("intimate_goals_updated"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.sessionsGoalsUpdated(_:)), name: NSNotification.Name("update_intimate_pain"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.sessionsGoalsUpdated(_:)), name: NSNotification.Name("update_intimate_comfort"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.sessionsGoalsUpdated(_:)), name: NSNotification.Name("intimate_data_source_updated"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.sessionsGoalsUpdated(_:)), name: NSNotification.Name("calibration_history_updated"), object: nil)
        reloadWidgets()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.setPlayButtonWith(nil)
        super.barButtonItemDelegate = self
        enableSwipeMenu(true)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    func didTapRightBarButtonItem() {
        //performSegue(withIdentifier: "showPDF", sender: nil)
    }
    
    func reloadWidgets() {
        intimateState = IntimateStateManager.instance.fetchIntimateState()
        intimateGoals = IntimateManager.instance.fetchLastIntimateGoals()
        /*calibrationHistories = CalibrationHistoryManager.shared().fetchCalibrationHistory(withFiter: Filter(attribute: "createdAt", relation: ">", value: intimateState.programStart))
        shouldDisplayEvolution = false
        var lastDate: Date?
        calibrationHistories.enumerateObjects({ hist, idx, stop in
            if idx == 0 {
                lastDate = hist?.createdAt
            } else {
                if hist?.createdAt.isEqual(toDateIgnoringTime: lastDate) == nil {
                    shouldDisplayEvolution = true
                    stop = true
                }
            }
        })
        let progFilter = Filter(_attribute: "intimateProgramType.typeID", _relation: "==", _value: intimateState?.intimateProgramType?.typeID)
        let startFilter = Filter(_attribute: "createdAt", _relation: ">=", _value: intimateState?.programStart)
        intimateSessions = IntimateManager.shared()?.fetchIntimateSessions(withFilters: [progFilter, startFilter], withCount: 0)*/
        if intimateSessions.count > 0 {
            //self.setRightBarButtonItemWithImage = UIImage(named: "share_picto")
        } else {
            navigationItem.rightBarButtonItem = nil
        }
        (childViewControllers as NSArray).enumerateObjects({ widget, idx, stop in
            (widget as AnyObject).view.removeFromSuperview()
        })
        //addProgressionWidget()
        addProgramFriezeWidget()
        addGoalsWidget()
        /*if intimateSessions.count == intimateState?.maxSession {
            addProgramFinishedWidget()
        } else {
            addProgramFriezeWidget()
        }*/
        //addScoresWidget()
        if shouldDisplayEvolution {
            //addEvolutionWidget()
        }
        addAdviceWidget()
        view.layoutIfNeeded()
    }
    
    
    @objc func sessionsGoalsUpdated(_ notification: Notification?) {
        reloadWidgets()
    }
    func addProgressionWidget() {
        intimateProgramFriezeWidget = widgetsSB?.instantiateViewController(withIdentifier: "IntimateProgressionWidget") as? IntimateProgramFriezeWidget
        addChildViewController(intimateProgramFriezeWidget!)
        contentView.addSubview((intimateProgramFriezeWidget?.view)!)
        intimateProgramFriezeWidget?.didMove(toParentViewController: self)
        intimateProgramFriezeWidget?.setContentWith(intimateState)
    }
    
    func addGoalsWidget() {
        intimateGoalsWidget = widgetsSB?.instantiateViewController(withIdentifier: "IntimateGoalsWidget") as! IntimateGoalsWidget
        addChildViewController(intimateGoalsWidget!)
        contentView.addSubview((intimateGoalsWidget?.view)!)
        intimateGoalsWidget?.didMove(toParentViewController: self)
        //let createdAtFilter = Filter(_attribute: "createdAt", _relation: "BETWEEN", _value: [DateHelper.getStartOfWeek(for: Date()), DateHelper.getEndOfWeek(for: Date())])
        let startFilter = Filter(_attribute: "createdAt", _relation: ">=", _value: intimateState?.programStart)
        //let goalsSessions = IntimateManager.shared().fetchIntimateSessions(withFilters: [createdAtFilter, startFilter], withCount: 0)
        //intimateGoalsWidget.setContentWithIntimateGoals(intimateGoals, intimateState: intimateState, andIntimateSessions: goalsSessions)
    }
    
    
    func addProgramFriezeWidget() {
        intimateProgramFriezeWidget = widgetsSB?.instantiateViewController(withIdentifier: "IntimateProgramFriezeWidget") as! IntimateProgramFriezeWidget
        addChildViewController(intimateProgramFriezeWidget!)
        contentView.addSubview((intimateProgramFriezeWidget?.view)!)
        intimateProgramFriezeWidget?.didMove(toParentViewController: self)
        //intimateProgramFriezeWidget.contentWithIntimateState = intimateState
    }
    func addProgramFinishedWidget() {
        intimateProgramFriezeWidget = widgetsSB?.instantiateViewController(withIdentifier: "IntimateProgramFinishedWidget") as! IntimateProgramFriezeWidget
        addChildViewController(intimateProgramFriezeWidget!)
        contentView.addSubview((intimateProgramFriezeWidget?.view)!)
        intimateProgramFriezeWidget?.didMove(toParentViewController: self)
        //intimateProgramFriezeWidget.setContentWithIntimateState(intimateState, intimateSessions: intimateSessions)
    }
    
    /*func addScoresWidget() {
        intimateScoresWidget = widgetsSB.instantiateViewController(withIdentifier: "IntimateScoresWidget")
        addChildViewController(intimateScoresWidget)
        contentView.addSubview(intimateScoresWidget.view)
        intimateScoresWidget.didMove(toParentViewController: self)
        intimateScoresWidget.contentWithIntimateState = intimateState
    }*/
    
    
    /*func addEvolutionWidget() {
        intimateEvolutionWidget = widgetsSB.instantiateViewController(withIdentifier: "IntimateEvolutionWidget")
        addChildViewController(intimateEvolutionWidget)
        contentView.addSubview(intimateEvolutionWidget.view)
        intimateEvolutionWidget.didMove(toParentViewController: self)
        let filter = Filter(attribute: "createdAt", relation: "BETWEEN", value: [intimateState.programStart, Date()])
        DebugLog("PROGRAM START : %@", intimateState.programStart)
        intimateEvolutionWidget.setContentWithCalibrationHistories(calibrationHistories, andFilter: filter)
    }*/
    
    func addAdviceWidget() {
        intimateAdviceWidget = widgetsSB?.instantiateViewController(withIdentifier: "IntimateAdviceWidget") as? IntimateAdviceWidget
        addChildViewController(intimateAdviceWidget!)
        contentView.addSubview((intimateAdviceWidget?.view)!)
        intimateAdviceWidget?.didMove(toParentViewController: self)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        let goalsView: UIView? = intimateGoalsWidget?.view
        let friezeView: UIView? = intimateProgramFriezeWidget?.view
        let adviceView: UIView? = intimateAdviceWidget?.view
        
        contentView.translatesAutoresizingMaskIntoConstraints = false

        goalsView?.translatesAutoresizingMaskIntoConstraints = false
        friezeView?.translatesAutoresizingMaskIntoConstraints = false
        adviceView?.translatesAutoresizingMaskIntoConstraints = false
        
        let views = ["friezeView": friezeView, "goalsView": goalsView, "adviceView": adviceView]
        var metrics = AutoLayoutMetrics()
        var autoMetrics: NSDictionary
        autoMetrics = metrics.get()
        var screenHeight = autoMetrics["screenHeight"] as!Int
        var realHeight = Int(screenHeight) - 20
        //autoMetrics["realHeight"] = realHeight
        
        
        
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[friezeView(==screenWidth)]|", options: [], metrics: autoMetrics as! [String : Any], views: views))
        
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[goalsView(==screenWidth)]|", options: [], metrics: autoMetrics as! [String : Any], views: views))
        
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[adviceView(==screenWidth)]|", options: [], metrics: autoMetrics as! [String : Any], views: views))
        
        let verticalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "V:|[friezeView(==400)][goalsView(==400)][adviceView(==250)]|", options: [], metrics: metrics.get() as! [String : Any], views: views)
    
        
        
        contentView.addConstraints(verticalConstraints as! [NSLayoutConstraint])
    
    }
    
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "showCalendar") {
            let dest = segue.destination as? MainIntimateSessionsCalendarViewController
            dest?.intimateSessions = intimateSessions as NSArray
            dest?.intimateState = intimateState
        }
        /*if (segue.identifier == "showPDF") {
            let ipdfvc = segue.destination as? IntimatePDFViewController
            ipdfvc?.intimateState = intimateState
            ipdfvc?.calibrationHistories = calibrationHistories
            ipdfvc?.intimateSessions = intimateSessions.reverseObjectEnumerator().allObjects
        }*/
    }
    
    
    
}

