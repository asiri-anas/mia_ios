//
//  AuthTutoViewControllerSwift.swift
//  mia
//
//  Copyright © 2018 V-Labs. All rights reserved.
//

import Foundation
import UIKit

class AuthTutoViewController: AbstractViewController, UIScrollViewDelegate {
    
    let NUMBER_OF_PAGES = 3
    let COACH_INTIMATE_TYPE = 7
    
    var screenBounds = CGRect.zero
    var sc1: UIView?
    var sc2: UIView?
    var sc4: UIView?
    var ict1: TutoWhyPractice?
    var ict2: TutoMyPartner?
    var ict3: TutoHowMeasure?
    
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var pageControl: UIPageControl!
    @IBOutlet var accountButton: UIButton!
    @IBOutlet var orderButton: UIButton!
    var deviceType: Int = 0
    var user: User?
    
    func initScrollViewIntimate() {
        pageControl.numberOfPages = NUMBER_OF_PAGES
        ict1 = TutoWhyPractice.createView()
        ict2 = TutoMyPartner.createView()
        ict3 = TutoHowMeasure.createView()
        
        scrollView.addSubview(ict1!)
        scrollView.addSubview(ict2!)
        scrollView.addSubview(ict3!)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        GradientHelper.setGradientOnLayer(view.layer, from: UIColor.white, to: UIColor.colorWithHexString(stringToConvert: "#F3DCD7"))
        closeButton.isHidden = true
        initScrollViewIntimate()
        if navigationController?.viewControllers[0] == self {
            setLeftCloseButtonForModalNavigationController()
            //[_accountButton setTitle:[NSLocalizedString(@"close", nil) uppercaseString] forState:UIControlStateNormal];
            accountButton.isHidden = true
            closeButton.isHidden = false
        } else {
            accountButton.alpha = 0
            setBackButtonForNavigationController()
            if user != nil {
                accountButton.setTitle(NSLocalizedString("add_device", comment: "").uppercased(), for: .normal)
                setRightCloseButtonForModalNavigationController()
            } else {
                accountButton.alpha = 0
                setBackButtonForNavigationController()
                if user != nil {
                    accountButton.setTitle(NSLocalizedString("add_device", comment: "").uppercased(), for: .normal)
                    setRightCloseButtonForModalNavigationController()
                } else {
                    //[_accountButton setTitle:[NSLocalizedString(@"create_profile", nil) uppercaseString] forState:UIControlStateNormal];
                }
            }
            scrollView.alwaysBounceVertical = false
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let index = Int(scrollView.contentOffset.x / scrollView.frame.size.width)
        scrollViewDidScroll(at: index)
    }
    
    
    
    func scrollViewDidScroll(at index: Int) {
        pageControl.currentPage = index
        if index == NUMBER_OF_PAGES - 1 {
            showAccountButton()
        } else {
            if navigationController?.viewControllers[0] != self {
                hideAccountButton()
            }
        }
    }
    
    func hideAccountButton() {
        UIView.animate(withDuration: 0.3, animations: {() -> Void in
            self.accountButton.alpha = 0
        })
        UIView.animate(withDuration: 0.3, animations: {() -> Void in
            self.orderButton.alpha = 0
        })
    }
    
    func showAccountButton() {
        UIView.animate(withDuration: 0.3, animations: {() -> Void in
            self.accountButton.alpha = 1
        })
        UIView.animate(withDuration: 0.3, animations: {() -> Void in
            self.orderButton.alpha = 0
        })
    }
    
    
    /*@IBAction func orderAction(_ sender: Any) {
     if let aString = URL(string: "http://shop.urgotech.fr/") {
     UIApplication.shared.openURL(aString)
     }
     }*/
    
    @IBAction func createAccount(_ sender: Any) {
        if navigationController?.viewControllers[0] == self {
            dismiss(animated: true) {() -> Void in }
        } else {
            if user == nil {
                if deviceType == COACH_INTIMATE_TYPE {
                    performSegue(withIdentifier: "intimateProfile", sender: nil)
                } else {
                    performSegue(withIdentifier: "createAccount", sender: nil)
                }
            } else {
                if deviceType == COACH_INTIMATE_TYPE {
                    let intimateCoach = IntimateCoachManager.instance.createInitialIntimateCoach()
                    NotificationCenter.default.post(name: NSNotification.Name("coach_created"), object: nil, userInfo: ["intimate_coach": intimateCoach])
                    performSegue(withIdentifier: "intimateProfile", sender: nil)
                }
            }
        }
    }
    
    
    
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        if deviceType == COACH_INTIMATE_TYPE {
            ict1?.translatesAutoresizingMaskIntoConstraints = false
            ict2?.translatesAutoresizingMaskIntoConstraints = false
            ict3?.translatesAutoresizingMaskIntoConstraints = false
            
            let views = ["ict1": ict1, "ict2": ict2, "ict3": ict3]
            
            var metrics = AutoLayoutMetrics()
            var autoMetrics: NSDictionary
            autoMetrics = metrics.get()
            var screenHeight = autoMetrics["screenHeight"] as!Int
            var realHeight = Int(screenHeight) - 20
            //autoMetrics["realHeight"] = realHeight
            scrollView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[ict1(==screenHeight)]|", options: [], metrics: autoMetrics as! [String : Any], views: views))
            scrollView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[ict2(==screenHeight)]|", options: [], metrics: autoMetrics as! [String : Any], views: views))
            scrollView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[ict3(==screenHeight)]|", options: [], metrics: autoMetrics as! [String : Any], views: views))
            
            scrollView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[ict1(==screenWidth)][ict2(==screenWidth)][ict3(==screenWidth)]|", options: [], metrics: metrics.get() as! [String : Any], views: views))
        }
    }
    
    
    
    
    
    
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        switch deviceType {
        case COACH_INTIMATE_TYPE:
            scrollView.contentSize = CGSize(width: (ict1?.frame.size.width)! * CGFloat(NUMBER_OF_PAGES), height: view.frame.size.height - 20)
        default:
            scrollView.contentSize = CGSize(width: (ict1?.frame.size.width)! * CGFloat(NUMBER_OF_PAGES), height: view.frame.size.height - 20)
        }
        view.layer.masksToBounds = true
    }
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "intimateProfile") {
            let aipfvc = segue.destination as? AuthIntimateProfileFormViewController
            aipfvc?.user = user
        }
    }
    
    
    
    
    
}


