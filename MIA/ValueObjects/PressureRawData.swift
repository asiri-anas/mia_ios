//
//  PressureRawData.swift
//  mia
//
//  Created by ASIRI Anas on 19/09/2018.
//  Copyright © 2018 V-Labs. All rights reserved.
//

import Foundation
/// This object represent a pressure value for a given instant time.
class PressureRawData: NSObject, CocosSerializerProtocol {
    
    /// The instant time, in seconds.
    @NSManaged var time: NSNumber?
    /// The pressure value at the instant time.
    @NSManaged var pressure: NSNumber?
    
    required init(dictionary dic: [AnyHashable : Any]) {
    }
    
    init(_ time: NSNumber?, pressure: NSNumber?) {
        super.init()
        self.time = time
        self.pressure = pressure
    }
    
    func serialize() -> [AnyHashable : Any]? {
        return ["time": time, "pressure": pressure]
    }
}
