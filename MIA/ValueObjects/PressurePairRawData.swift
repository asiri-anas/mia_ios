//
//  PressurePairRawData.swift
//  mia
//
//  Created by ASIRI Anas on 19/09/2018.
//  Copyright © 2018 V-Labs. All rights reserved.
//

import Foundation
/// Represents a segments between 2 PressureRawData objects.
class PressurePairRawData: NSObject, CocosSerializerProtocol {
   
    /// The start of the segment.
    @NSManaged var startData: PressureRawData?
    /// The end of the segment.
    @NSManaged var endData: PressureRawData?
    
    required init(dictionary dic: [AnyHashable : Any]) {
    }
    
    init(_ start: PressureRawData?, end: PressureRawData?) {
        super.init()
        startData = start
        endData = end
    }
    
    func serialize() -> [AnyHashable : Any]? {
        return ["pairRaws": [startData?.serialize(), endData?.serialize()]]
    }
}
