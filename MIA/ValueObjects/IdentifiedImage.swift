//
//  IdentifiedImage.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 02/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit

class IdentifiedImage: NSObject, NSCoding {
    
    ///An integer that identifies the image object
    var tag: Int = 0
    ///The UIImage object to display
    var image: UIImage?
    ///The absolute path of the image file
    var path = ""
    /*!
     @brief Returns an instance of IdentifiedImage object.
     @param image The UIImage object to display.
     @param tag An integer that identifies the image object.
     @param path The absolute path of the image file.
     */
    init?(_image: UIImage?, _tag: Int, _path: String?) {
        
    super.init()
    image = _image
    tag = _tag
    path = _path!
    return nil
        
    }
    
//  The converted code is limited to 4 KB.
//  Upgrade your plan to remove this limitation.
//
    required init?(coder aDecoder: NSCoder) {
        
    super.init()
    let imageData = aDecoder.decodeObject(forKey: "image") as? Data

        image = UIImage(data: imageData!)
        tag = aDecoder.decodeObject(forKey: "tag") as! Int
        path = aDecoder.decodeObject(forKey: "path") as! String
        
    }
    
    
    
    func encode(with aCoder: NSCoder) {
        
        let imageData = UIImagePNGRepresentation(image!)
    aCoder.encode(imageData, forKey: "image")
    aCoder.encode(tag, forKey: "tag")
    aCoder.encode(path, forKey: "path")
        
    }
    

}
