//
//  PressureActivityParameter.swift
//  mia
//
//  Created by ASIRI Anas on 19/09/2018.
//  Copyright © 2018 V-Labs. All rights reserved.
//

import Foundation

class PressureActivityParameter: NSObject, CocosSerializerProtocol {

    /// A hexadecimal string color.
    @NSManaged var colorString: NSString?
    /// An array of dictionary representation of PressurePairRawData objects.
    @NSManaged var datas: [Any]
    /// The duration of the exercise.
    @NSManaged var duration: NSNumber?
    /// The localized name of the exercise.
    @NSManaged var label: NSString?
    
    required init(dictionary dic: [AnyHashable : Any]) {
    }
    
    func serialize() -> [AnyHashable : Any]? {
        var serializedPairRaws = [AnyHashable]()
        for case let pprd as PressurePairRawData in datas {
            let pprdDic = pprd.serialize()
            serializedPairRaws.append(pprdDic!["pairRaws"] as! AnyHashable)
        }
        
        var returnedDic = [AnyHashable : Any]()
        if serializedPairRaws.count > 0 {
            returnedDic["datas"] = serializedPairRaws
        }
        if colorString != nil {
            returnedDic["color"] = colorString
        }
        if label != nil {
            returnedDic["label"] = label
        }
        if duration != nil {
            returnedDic["duration"] = duration
        }
        return returnedDic
    }
}
