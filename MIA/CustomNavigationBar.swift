//
//  File.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 18/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit

class CustomNavigationBar: UINavigationBar {
    
let CUSTOM_NAV_BAR_EXTENSION_HEIGHT = 50
var leafIV: UIImageView?
    
    func setCustomImage(_ customImage: UIImage?) {
        
        leafIV?.removeFromSuperview()
        leafIV = UIImageView(frame: CGRect(x: 0, y: 0, width: bounds.size.width, height: 40))
        leafIV?.contentMode = .scaleAspectFit
        leafIV?.image = customImage
        addSubview(leafIV!)
        
    }
}
