//
//  MainIntimateProgramFilterViewController.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 13/09/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit


class MainIntimateProgramFilterViewController: AbstractViewController, UITableViewDataSource, UITableViewDelegate, BarButtonItemDelegate {
    func didTapLeftBarButtonItem() {
        
    }
    
    
    
    @IBOutlet var tableView: UITableView!
    var dataSource: NSArray?
    var selectedProgramTypes: NSMutableArray?

    var programTypes: NSMutableArray?
 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setGradientBackground()
        navigationItem.title = NSLocalizedString("filter_by_program", comment: "").uppercased()
        setBackButtonForNavigationController()
        self.setRightBarButtonItemWith(UIImage(named: "validate_white"))
        programTypes = NSMutableArray()
        dataSource?.enumerateObjects({ obj, idx, stop in
            if (obj is IntimateSession) {
                let session = obj as? IntimateSession
                if let aType = session?.intimateProgramType {
                    if !(programTypes?.contains(aType))! {
                        programTypes?.add(aType)
                    }
                }
            }
        })
        if !(selectedProgramTypes != nil) {
            selectedProgramTypes = [AnyHashable](repeating: 0, count: (programTypes?.count)!) as! NSMutableArray
        }
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        super.barButtonItemDelegate = self
        programTypes?.enumerateObjects({ type, idx, stop in
            selectedProgramTypes?.enumerateObjects({ selectedType, idx2, stop2 in
                if (type as! IntimateProgramType).typeID ?? 0 == (selectedType as AnyObject).typeID ?? 0 {
                    tableView.selectRow(at: IndexPath(row: idx, section: 0), animated: true, scrollPosition: .none)
                    //stop2 = true
                }
            })
        })
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedProgramTypes?.add(programTypes![indexPath.row])
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        //selectedProgramTypes = selectedProgramTypes?.filter(using: { ($0) as AnyObject !== (programTypes[indexPath.row]) as AnyObject })
    }
    func didTapRightBarButtonItem() {
        NotificationCenter.default.post(name: NSNotification.Name("intimate_program_filter"), object: nil, userInfo: ["selected_program_type": selectedProgramTypes])
        navigationController?.popViewController(animated: true)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return programTypes!.count
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "Cell"
        var cell = self.tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? ContextTableViewCell
        if cell == nil {
            cell = ContextTableViewCell()
        }
        let type: IntimateProgramType? = programTypes?[indexPath.row] as! IntimateProgramType
        cell?.setContentWithTitle(NSLocalizedString((type?.label)!, comment: ""))
        return cell!
    }
    /*
         #pragma mark - Navigation
     
         // In a storyboard-based application, you will often want to do a little preparation before navigation
         - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
         // Get the new view controller using [segue destinationViewController].
         // Pass the selected object to the new view controller.
         }
         */
    
}
