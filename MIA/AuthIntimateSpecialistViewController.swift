//
//  AuthIntimateSpecialistViewController.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 14/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import CoreData

class AuthIntimateSpecialistViewController: AbstractViewController, UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate, BarButtonItemDelegate, UIActionSheetDelegate {
    
    
    @IBOutlet weak var specialistPV: UIPickerView!
    @IBOutlet var nameTF: UITextField!
    @IBOutlet var emailTF: UITextField!
    @IBOutlet var phoneTF: UITextField!
    var user: User?
    var intimateCoach: IntimateCoach?

    var context: NSManagedObjectContext?
    var isEditingMode = false
    
    var selectedSpecialist = ""
    var specialists = [String]()
    var originalCenter = CGPoint.zero
    var isCompletionMode = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = "MON SPECIALISTE"

        setGradientBackground()
        
        if isEditingMode {
            setLeftCloseButtonForModalNavigationController()
        } else {
            setBackButtonForNavigationController()
        }
        
        // Connect data:
        self.specialistPV.delegate = self
        self.specialistPV.dataSource = self
        
        nameTF.setMercuStyle()
        emailTF.setMercuStyle()
        phoneTF.setMercuStyle()
        specialists = ["Mon spécialiste", "Sage-Femme", "Kinésithérapeute", "Gynécologue"]
        super.barButtonItemDelegate = self
        let gesture = UISwipeGestureRecognizer()
        gesture.direction = .down
        gesture.addTarget(self, action: #selector(self.dismissKeyboard))
        gesture.location(in: view)
        view.addGestureRecognizer(gesture)
        let tap = UITapGestureRecognizer()
        tap.addTarget(self, action: #selector(self.dismissKeyboard))
        tap.location(in: view)
        view.addGestureRecognizer(tap)
        preFillSpecialist()
        
    }
    
    func preFillSpecialist() {
        do {
            var intimate: [IntimateCoach] = []
            intimate = try CDManager.instance!.managedObjectContext!.fetch(IntimateCoach.intimateCoachFetchRequest())
             print("AUTH", "fetch intimateCoach")
            for data in intimate {
                print("AUTH","user fetched")
                if  data.intimateSpecialist != nil {
                    print("AUTH","specialist fetched")
                    nameTF.text = data.intimateSpecialist!.name!
                    phoneTF.text = data.intimateSpecialist!.phone!
                    emailTF.text = data.intimateSpecialist!.email!
                    let rowOfSpecialist = rowCheck(specialists, data.intimateSpecialist!.type)
                    specialistPV.selectRow(rowOfSpecialist!, inComponent: 0, animated: true)
                }
                intimateCoach = data
            }
        } catch {
            print("Failed")
        }
    }
    
    func rowCheck(_ list: [String], _ element: String?) -> (Int)! {
        var index = 0
        for item in list {
            if item == element {
                return index
            }
            index = index + 1
        }
        return index
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView {
        case specialistPV:
            return specialists.count
        default:
            return 1
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView {
        case specialistPV:
            return specialists[row]
        default:
            return "choisir"
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == nameTF {
            emailTF.becomeFirstResponder()
        }
        if textField == emailTF {
            phoneTF.becomeFirstResponder()
        }
        if textField == phoneTF {
            dismissKeyboard()
        }
        return false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //[self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"nav_pink"] forBarMetrics:UIBarMetricsDefault];
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        originalCenter = view.center
    }
    
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
        if IS_IPHONE_4 || IS_IPHONE_5 {
            UIView.animate(withDuration: 0.25, animations: {() -> Void in
                self.view.center = self.originalCenter
            })
        }
    }
    
    
    func didTapRightBarButtonItem() {
        
    }
    
    
    func didTapLeftBarButtonItem() {
        if navigationController?.viewControllers[0] == self {
            dismiss(animated: true) {() -> Void in }
        } else {
            if isCompletionMode {
                let app = UIApplication.shared.delegate as? NativeAppDelegate
                app?.loadMainStoryboard()
            } else {
                navigationController?.popViewController(animated: true)
            }
        }
    }
    
    
    @IBAction func didTapValidateButton(_ sender: Any) {
        
        if emailTF.text == "" && nameTF.text == "" && phoneTF.text == "" {
            
            if (isEditingMode)
            {
                dismiss(animated: true) {() -> Void in }
            } else {
                performSegue(withIdentifier: "showSurvey", sender: nil)
                return
            }
            
            
            
        }else {
            if emailTF.text == "" || nameTF.text == "" || phoneTF.text == "" || specialistPV.selectedRow(inComponent: 0) == 0 {
                let alert = UIAlertController(title: "Champs obligatoires", message: "Merci de saisir tous les champs", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self.present(alert, animated: true)
                return
            }
            if emailTF.text != "" && !isValidEmail(emailTF.text) {
                let alert = UIAlertController(title: "Adresse mail invalide", message: "Merci de saisir une adresse mail valide", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self.present(alert, animated: true)
                return
            }
            
            var specialist : IntimateSpecialist? = intimateCoach?.intimateSpecialist
            if specialist == nil {
                specialist = create()
            }
            //print("specialist ", specialist?.name)
            specialist!.name = nameTF.text!
            specialist!.email = emailTF.text!
            specialist!.phone = phoneTF.text!
            specialist!.type = specialists[specialistPV.selectedRow(inComponent: 0)]
            
            if intimateCoach != nil && intimateCoach!.user != nil {
                intimateCoach!.intimateSpecialist = specialist
            }
            CDManager.instance!.saveContext()
            print("AUTH","specialist saved")
            if isEditingMode {
                NotificationCenter.default.post(name: NSNotification.Name("update_intimate_specialist"), object: nil, userInfo: ["user": intimateCoach?.user])
                dismiss(animated: true) {() -> Void in }
            } else {
                NotificationCenter.default.post(name: NSNotification.Name("update_intimate_specialist"), object: nil, userInfo: ["user": intimateCoach?.user])
                performSegue(withIdentifier: "showSurvey", sender: nil)
            }
        }
        //print("USER", "we come to didTapValidateButton")
        
        //performSegue(withIdentifier: "showSurvey", sender: nil)
        
    }
    
    func create() -> IntimateSpecialist? {
        var specialist = NSEntityDescription.insertNewObject(forEntityName: "IntimateSpecialist", into: (CDManager.instance!.managedObjectContext)!) as! IntimateSpecialist
        specialist.specialistID = (specialist.generateUniqueID())!
        return specialist
    }
    
    func isValidEmail(_ testStr:String?) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "showSurvey") {
            print("USER", "we come to prepare")
            let aisvc = segue.destination as? AuthIntimateSurveyViewController
        }
    }
    
    
    
}
