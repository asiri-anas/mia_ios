//
//  AuthIntimatePainViewController.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 14/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit

class AuthIntimatePainViewController: AuthIntimateSliderViewController {
    
    var intimateState: IntimateState?
    var intimateCoach: IntimateCoach?
    
    var isEditingMode = false
    var isFromSurvey = false
    
    var pains = [Any]()
    var selectedPain: Int = 0
    
    let keyLoc = ["Aucune gêne", "Gêne légère", "Gêne modérée", "Gêne importante", "Gêne intense",]
    let imgName = ["curseur_eclair_1", "curseur_eclair_2", "curseur_eclair_3", "curseur_eclair_4", "curseur_eclair_5"]
    
    override func viewDidLoad() {
        isEditing = isEditingMode && !isFromSurvey
        super.viewDidLoad()
        //pains = (IntimateStateManager.shared()?.fetchIntimateVaginalPains()!)!
        if intimateState != nil {
            selectedPain = stringToInt(string: (intimateState?.uncomfortSensationLevel)!)
            cursorLabel.text = intimateState?.uncomfortSensationLevel
            cursorIV.image = UIImage(named: imgName[selectedPain])
            slider.setValue(Float(selectedPain), animated: false)
            
        } else {
            cursorLabel.text = keyLoc[0]
            cursorIV.image = UIImage(named: imgName[0])
            slider.setValue(0, animated: false)
        }
        
        preFillSlider()
        
    }
    
    func stringToInt(string: String) -> Int
    {
        var count = 0
        for i in keyLoc
        {
            if ( i == string )
            {
                return count
            }
            count = count + 1
        }
        return count
    }
    
    
    func preFillSlider() {
        do {
            var results = try CDManager.instance!.managedObjectContext!.fetch(IntimateCoach.intimateCoachFetchRequest())
            print("AUTH", "fetch intimateCoach")
            for singleData in results {
                if singleData.intimateState != nil {
                    print("AUTH", "intimateState fetched:", singleData.intimateState!.uncomfortSensationLevel)
                    selectedPain = stringToInt(string: singleData.intimateState!.uncomfortSensationLevel!)
                    slider.setValue(Float(selectedPain), animated: false)
                    cursorLabel.text = singleData.intimateState!.uncomfortSensationLevel!
                    cursorIV.image = UIImage(named: imgName[selectedPain])
                    
                }
                intimateCoach = singleData
            }
        } catch {
            print("Failed")
        }
    }
    
    
    
    override func sliderValueChanged(_ sender: Any?) {
        super.sliderValueChanged(sender)
        let val = Int(roundf(slider.value))
        //selectedPain = pains[val] as! IntimateVaginalPain
        cursorLabel.text = keyLoc[val]
        //let imgName = "curseur_eclair_\(selectedPain?.painID)"
        cursorIV.image = UIImage(named: imgName[val])
        
        
        intimateCoach = initStateAndSave()
    }
    
    
    func initStateAndSave() -> IntimateCoach? {
        if intimateCoach == nil {
            intimateCoach = IntimateCoachManager.instance.createInitialIntimateCoach()
            print("AUTH", "intimateCoach created")
        }
        intimateState = intimateCoach?.intimateState
        if intimateState == nil {
            intimateState = IntimateState.create()
            print("AUTH", "intimateState created")
        }
        
        intimateState?.uncomfortSensationLevel = keyLoc[Int(roundf(slider.value))]
        
        intimateCoach?.intimateState = intimateState
        print("AUTH", "profil survey pain comfort", intimateCoach?.intimateState?.uncomfortSensationLevel)
        //print("Intimate to send: ", intimateCoach)
        CDManager.instance!.saveContext()
        
        return intimateCoach
    }
    
    
}
