//
// Created by kevin on 11/26/15.
//

#ifndef MYAPPLICATION_EXERCISELAYER_H
#define MYAPPLICATION_EXERCISELAYER_H

#include "cocos2d.h"
#include "AbstractLayer.h"
#include "ActivityService.h"

using namespace cocos2d;

/**
 * Layer for common exercise like deep strengthening, superficial strengthening, etc.
 */
class ExerciseLayer : public AbstractLayer {

public:

	~ExerciseLayer();
	/** Create a Exercise Scene */
    static Scene* createScene(Value data);

    /** Init the view */
    virtual bool init();

    /** Macro definition to initialize the view */
    CREATE_FUNC(ExerciseLayer);


	void pauseEventListener();

	void resumeEventListener();

private:

	/** Unserialize data from Native (Android iOS) */
	void unserializeData();

	/** Extend the current pattern to repeat for max duration / max time for a pattern */
	void extendsPatterns();

	/** Convert each real point to view point 10 seconds width and max pressure equal to 2/3 of available visible screen / 2 */
	void convertPointsToView();

	/** Draw graph with a given color and a given speed */
	void drawGraph(Vec2 speed, Color4B color);

	/** Calcule the Y value for bezier curve */
	float calculYvalue(Vec2 p0, Vec2 p1, bool isFirstPair);

	/** Draw quad bezier and fill with color */
	void drawQuadBezier(const Vec2 &origin, const Vec2 &control, const Vec2 &destination, unsigned int segments, const Color4F &color);


	DrawNode* node;
	DrawNode* nodeBase;
	Node* backNode;

	cocos2d::EventListenerCustom* _customlistener;


	Label *_label;
	float _maxPressure; // the max pressure of the pattern
	float _minPressure; // the max pressure of the pattern
	float _timePattern; // The last time stamp of the pattern
	std::string _nameExercise;
	int _duration; // Duration of exercise in seconds
	std::string _exerciseNumber;
    Label* actionLabel;

	Size _visibleSize; // Visible from director;
	Vec2 _originDirector;

	float _ratioWidth;
	float _ratioHeight;
	float _currentTimeStamp;

	Color4B defaultColor; //
	std::string _hexColorString;

	std::vector<Vec2> _originalPatternPoint;
	std::vector<Vec2> _scalePatternVector;
	std::vector<Vec2> _originalVectorExtended;
	std::vector<Vec2> _scaleVectorExtended;



	//Leaf

};


#endif //MYAPPLICATION_EXERCISELAYER_H
