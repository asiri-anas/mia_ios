//
//  IntimateActivity.h
//  mymercurochrome
//
//  Created by Rémi Caroff on 07/12/2015.
//  Copyright © 2015 V-Labs. All rights reserved.
//

#import <Foundation/Foundation.h>

@class IntimateActivity;
@class IntimateCalibrationStructure;
@class IntimateActivityType;
@class IntimateExoViewModel;
@class IntimateActivityBase;

@protocol IntimateActivityHandlerDelegate <NSObject>

/*!
 @brief This method is called when the exercise is completed (when timer ends).
 @param activity The IntimateActivityObject that has been saved right after the end of the exercise.
 */
- (void)didFinishIntimateActivity:(IntimateActivity *)activity levelUp:(BOOL)levelUp;

/// This method is called when a calibration exercise finishes.
- (void)didFinishCalibration;

/// This method is called when a breath exercise finishes.
- (void)didFinishBreath:(IntimateCalibrationStructure *)calibrationStructure;

/// This method is the callback of @c stopExercise method. It's called when pressure request are stopped.
- (void)didExerciseStop;

/// This method is called when the bluetooth connection breaks involuntarily.
- (void)didConnectionInterrupted;

@end

@interface IntimateActivityHandler : NSObject

@property (nonatomic, strong) IntimateActivityType *activityType;
@property (nonatomic) NSNumber *name;
@property (nonatomic, strong) NSNumber *activityLevel;
@property (nonatomic, strong) NSNumber *exoDuration;

@property (nonatomic, strong) id<IntimateActivityHandlerDelegate> delegate;
@property (nonatomic) BOOL isFreeExo;

/*!
 @brief A constructor of the IntimateActivityHandler.
 @param activityType The IntimateActivityType object that corresponds to the exercise we want to do.
 */
- (IntimateActivityHandler *)initWithViewModel:(IntimateExoViewModel *)viewModel andSelectedLevel:(NSNumber *)selectedLevel;

/// Will initialize the IntimateActivityHandler to prepare a calibration exercise.
- (IntimateActivityHandler *)initForCalibrationWithDuration:(NSNumber *)duration;

/// Will initialize the IntimateActivityHandler to prepare a breath exercise.
- (IntimateActivityHandler *)initForBreathWithDuration:(NSNumber *)duration withCalibration:(BOOL)calibration;

/*!
 @brief A constructor of the IntimateActivityHandler.
 @param activityBase The IntimateActivityBase object that corresponds to the next exercise we want to do within a session.
 */
- (IntimateActivityHandler *)initWithActivityBase:(IntimateActivityBase *)activityBase;

/// Will request the Airbee to start an exercise, and start pressure requests.
- (void)startExerciseWithCalibrationStructure:(IntimateCalibrationStructure *)calibrationStructure;

/// Will start a calibration exercise. IntimateActivityHandler should be initialized with @c initForCalibrationWithDuration constructor.
- (void)startCalibration;

/// Will start a breath exercise. IntimateActivityHandler should be initialized with @c initForBreathWithDuration constructor.
- (void)startBreath;

/// Will pause the current exercise.
- (void)pauseExercise;

/// Will request the Airbee to stop an exercise. This will also stop the pressure requests.
- (void)stopExercise;

/// Will disconnect the Airbee.
- (void)disconnectAirbee;

@end
