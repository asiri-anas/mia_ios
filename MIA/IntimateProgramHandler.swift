//
//  IntimateProgramHandler.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 03/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation

class IntimateProgramHandler: NSObject {
    
    var intimateState: IntimateState?
    var intimateSession: IntimateSession?
    var currentActivityIndex: Int = 0
    var isShouldCreateSession = false
    var programParser: IntimateProgramParser?
    
    let DEEP_STRENGTHENING_FLAT: NSNumber = 0
    let DEEP_STRENGTHENING_SQUARE: NSNumber = 1
    let DEEP_STRENGTHENING_SAW: NSNumber = 2
    let SUPERFICIAL_STRENGTHENING: NSNumber = 3
    let LATCHING: NSNumber = 4
    let SLACKENING: NSNumber = 5
    let BREATH: NSNumber = 6
    let CALIBRATION: NSNumber = 7
    
    
    
    /// Returns a singleton instance of IntimateProgramHandler.
    class func sharedManager() -> IntimateProgramHandler? {
        
        var _sharedManager: IntimateProgramHandler? = nil
        var onceToken: Int = 0
        if (onceToken == 0) {
            /* TODO: move below code to a static variable initializer (dispatch_once is deprecated) */
            _sharedManager = IntimateProgramHandler()
            _sharedManager?.programParser = IntimateProgramParser()
            _sharedManager?.currentActivityIndex = 0
            _sharedManager?.isShouldCreateSession = true
        }
        onceToken = 1
        return _sharedManager
        
    }
    
    func getCurrentSession() -> IntimateSessionBase? {
        let sessionBase: IntimateSessionBase? = programParser?.currentProgram?.sessions![Int(intimateState!.currentSession!) - 1] as! IntimateSessionBase
        return sessionBase
    }
    
    
    /// Will return the next IntimateActivityBase to do within the current IntimateSession.
    func getNextActivity() -> IntimateActivityBase? {
        
        var sessionBase: IntimateSessionBase? = getCurrentSession()
        var activityBase: IntimateActivityBase?
        if currentActivityIndex < (sessionBase?.activities?.count)! {
            activityBase = sessionBase?.activities![currentActivityIndex] as! IntimateActivityBase
        } else {
            activityBase = nil
        }
        currentActivityIndex += 1
        return activityBase
        
    }
    
    /// Will create a new IntimateSession object based on the current IntimateState. After calling this method, the IntimateSession instance will be ready to receive new IntimateActivity objects.
    func startNewSessionForCurrentState() {
        
        intimateState = IntimateStateManager.instance.fetchIntimateState()
        programParser?.getBaseProgram(forKey: intimateState?.intimateProgramType?.label)
        
    }
    
    /// This method should be called at the end of a session. It will set her @c finshedAt property, and update the IntimateState.
    func terminateSession() -> IntimateSession? {
        
        intimateSession?.finishedAt = Date()
        intimateState?.currentSession = Int((intimateState?.currentSession)!) + 1 as NSNumber
        if (intimateState?.intimateProgramType?.label == "prog_perineal_pain") {
            intimateSession?.intimateVaginalPain = intimateState?.intimateVaginalPain
        } else {
            intimateSession?.intimateComfort = intimateState?.intimateComfort
        }
        CDManager.instance!.saveContext()
        NotificationCenter.default.post(name: NSNotification.Name("intimate_data_source_updated"), object: nil)
        performSelector(inBackground: #selector(self.synchronizeIntimateDatas), with: nil)
        return intimateSession
        
    }
    
    
    /*
     func updateSessionHearts() {
     intimateSession = IntimateManager.shared()?.fetchIntimateSessions(withFilters: nil, withCount: 1)?.last as! IntimateSession
     intimateSession?.hearts = IntimateCoachService.shared().getHeartsForIntimateSession(intimateSession)
     CDManager.instance?.saveContext()
     NotificationCenter.default.post(name: NSNotification.Name("intimate_data_source_updated"), object: nil)
     }*/
    
    /// Will associate the passed IntimateActiivty object to the current session.
    func save(_ activity: IntimateActivity?, levelUp: Bool) -> IntimateActivity? {
        
        if isShouldCreateSession {
            //intimateSession = IntimateManager.shared()?.createIntimateSession()
        }
        isShouldCreateSession = false
        activity?.intimateSession = intimateSession
        intimateSession?.score = Int((intimateSession?.score)!) + Int((activity?.score)!) as NSNumber
        if levelUp {
            activity?.intimateActivityType?.programLevelReached = Int((activity?.intimateActivityType?.programLevelReached)!) + 1 as NSNumber
        }
        CDManager.instance!.saveContext()
        //updateSessionHearts()
        return activity
        
    }
    
    /// Will reset the current state of the program.
    func clean() {
        
        currentActivityIndex = 0
        isShouldCreateSession = true
        
    }
    
    /// Returns an array of FriezeTile objects, based on the passed IntimateState object.
    func getFriezeTiles(for state: IntimateState?) -> Int {//[Any]? {
        
        var program: IntimateProgram? = programParser?.getBaseProgram(forKey: state?.intimateProgramType?.label)
        var tiles = [AnyHashable]() /* TODO: .reserveCapacity(program?.sessions.count) */
        var filters = [AnyHashable]() /* TODO: .reserveCapacity(2) */
        filters.append(Filter(_attribute: "intimateProgramType.typeID", _relation: "==", _value: state?.intimateProgramType?.typeID))
        filters.append(Filter(_attribute: "createdAt", _relation: ">=", _value: state?.programStart))
        /*var sessions: NSArray = IntimateManager.shared()?.fetchIntimateSessions(withFilters: filters, withCount: 0)!.reverseObjectEnumerator().allObjects as! NSArray
         program?.sessions.enumerateObjects({(_ sessionBase: IntimateSessionBase?, _ idx: Int, _ stop: UnsafeMutablePointer<ObjCBool>) -> Void in
         if idx < sessions.count {
         var tile = FriezeTile(_intimateSession: sessions[idx] as! IntimateSession, andIntimateState: state)
         tiles.append(tile)
         } else {
         var tile = FriezeTile(_intimateSession: sessions[idx] as! IntimateSession, andIntimateState: state)
         tile.intimateState = state
         tiles.append(tile)
         }
         } as! (Any, Int, UnsafeMutablePointer<ObjCBool>) -> Void)
         return tiles*/
        return 0
        
    }
    
    /// Returns the total score to reach for the specified IntimateProgramType label.
    func getCumulatedSessionsMaxScore(forProgramKey key: String?) -> NSNumber? {
        
        var score: Int = 0
        var program: IntimateProgram? = programParser?.getBaseProgram(forKey: key)
        for session in (program?.sessions)! {
            var flag = session.maxScore
            score += Int(flag!)
        }
        return score as NSNumber
        
    }
    
    /// Returns the count of sessions for the give program key.
    func getSessionsCount(forProgramKey progKey: String?) -> Int {
        
        var prog: IntimateProgram? = programParser?.getBaseProgram(forKey: progKey)
        return (prog?.sessions!.count)!
        
    }
    
    /// Will fetch and post intimate datas to remote server.
    @objc func synchronizeIntimateDatas() {
        
        var datas = IntimateManager.instance.getDatasToSynchronize()
        //MercuClient.shared().postIntimateDatas(datas)
        
    }
    
    /*!
     @brief Returns an IntimateSessionBase object based on the given parameters.
     @param index The index of the wanted session.
     @param programType The IntimateProgramType object of the wanted session.
     */
    func getIntimateSessionBase(at index: Int, for programType: IntimateProgramType?) -> IntimateSessionBase? {
        var program: IntimateProgram? = programParser?.getBaseProgram(forKey: programType?.label)
        return program?.sessions![index] as? IntimateSessionBase
    }
    
    
    /*func getIntimateActivityBase(forProgramKey progKey: String?) -> [Any]? {
     
     var toReturn = [AnyHashable]()
     var activities = [AnyHashable]()
     var program: IntimateProgram? = programParser?.getBaseProgram(forKey: progKey)
     program?.sessions?.enumerateObjects(
     {(_ session: IntimateSessionBase?, _ idx: Int, _ stop: UnsafeMutablePointer<ObjCBool>) -> Void in
     if let anActivities = session?.activities {
     activities.append( anActivities)
     }
     } as! (Any, Int, UnsafeMutablePointer<ObjCBool>) -> Void)
     
     
     
     var isFirst = true
     (activities as NSArray).enumerateObjects({(_ activity: IntimateActivityBase?, _ idx: Int, _ stop: UnsafeMutablePointer<ObjCBool>) -> Void in
     
     
     var hasAlready = false
     if isFirst {
     if activity?.type ?? 0 != self.BREATH && activity?.type ?? 0 != self.CALIBRATION {
     if let anActivity = activity {
     toReturn.append(anActivity)
     }
     isFirst = false
     }
     } else {
     (toReturn as NSArray).enumerateObjects({(_ obj: IntimateActivityBase?, _ idx: Int, _ stop2: UnsafeMutablePointer<ObjCBool>) -> Void in
     if obj?.type ?? 0 == activity?.type ?? 0 || activity?.type ?? 0 == self.BREATH || activity?.type ?? 0 == self.CALIBRATION {
     hasAlready = true
     //stop2 = true
     }
     } as! (Any, Int, UnsafeMutablePointer<ObjCBool>) -> Void)
     if !hasAlready {
     if let anActivity = activity {
     toReturn.append(anActivity)
     }
     }
     }
     } as! (Any, Int, UnsafeMutablePointer<ObjCBool>) -> Void)
     return toReturn
     
     }*/
    
    /// Returns the IntimateProgram object corresponding to the given program key.
    func getIntimateProgram(forKey programKey: String?) -> IntimateProgram? {
        return programParser?.getBaseProgram(forKey: programKey)
    }
}
