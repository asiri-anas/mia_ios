//
//  MainIntimateExosFilterViewController.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 13/09/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit


class MainIntimateExosFilterViewController: AbstractViewController, UITableViewDataSource, UITableViewDelegate, BarButtonItemDelegate {
    func didTapLeftBarButtonItem() {
        
    }
    
    @IBOutlet var tableView: UITableView!
    var dataSource: NSArray?
    var selectedTypes: NSMutableArray?
    
    var types: NSMutableArray?
    
    
    
    override func viewDidLoad() {
            super.viewDidLoad()
            setGradientBackground()
            types = NSMutableArray()
        navigationItem.title = NSLocalizedString("filter_by_exercise", comment: "").uppercased()
            setBackButtonForNavigationController()
            self.setRightBarButtonItemWith(UIImage(named: "validate_white"))
        dataSource?.enumerateObjects({ obj, idx, stop in
                if (obj is IntimateActivity) {
                    let activity = obj as? IntimateActivity
                    if let aType = activity?.intimateActivityType {
                        if !(types?.contains(aType))! {
                            types?.add(aType)
                        }
                    }
                }
            })
        if !(selectedTypes != nil) {
            selectedTypes = [AnyHashable](repeating: 0, count: (types?.count)!) as! NSMutableArray
        }
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        super.barButtonItemDelegate = self
        types?.enumerateObjects({ activityType, idx, stop in
            selectedTypes?.enumerateObjects({ selected, idx2, stop2 in
                if (selected as! IntimateActivityType).type ?? 0 == (activityType as! IntimateActivityType).type ?? 0 {
                    tableView.selectRow(at: IndexPath(row: idx, section: 0), animated: true, scrollPosition: .none)
                    //stop2 = true
                }
            })
        })
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedTypes?.add(types![indexPath.row])
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        //selectedTypes = selectedTypes?.filter(using: { ($0) as AnyObject !== (types[indexPath.row]) as AnyObject })
    }
    func didTapRightBarButtonItem() {
        NotificationCenter.default.post(name: NSNotification.Name("intimate_exos_filter"), object: nil, userInfo: ["selected_type": selectedTypes])
        navigationController?.popViewController(animated: true)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return types!.count
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "Cell"
        var cell = self.tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? ContextTableViewCell
        if cell == nil {
            cell = ContextTableViewCell()
        }
        let type: IntimateActivityType? = types?[indexPath.row] as! IntimateActivityType
        cell?.setContentWithTitle(NSLocalizedString((type?.label)!, comment: ""))
        return cell!
    }
    
    
}
