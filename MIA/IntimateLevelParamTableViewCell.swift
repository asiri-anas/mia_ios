//
//  IntimateLevelParamTableViewCell.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 16/07/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit

protocol IntimateLevelCellDelegate: NSObjectProtocol {
    
    func didSelectIntimateLevel(_ selectedLevel: NSNumber?)
    
}


class IntimateLevelParamTableViewCell: UITableViewCell {
    

    var _currentLevel: NSNumber?
    var levelButtons: NSMutableArray?
    
    @IBOutlet var scrollView: UIScrollView!
    
    weak var delegate: IntimateLevelCellDelegate?
    
    func setContentWithLevels(_ levelsList: NSArray, currentLevel: NSNumber?, selectedLevel: NSNumber?, mainColor: UIColor?) {
        
        _currentLevel = currentLevel
        levelButtons = NSMutableArray()
        
        scrollView.contentSize = CGSize(width: (contentView.bounds.size.width / 4) * CGFloat(levelsList.count), height: scrollView.bounds.size.height)
        
        levelsList.enumerateObjects({ obj, idx, stop in
            self.addButton(withLevel: obj as! NSNumber, selectedLevel: selectedLevel, to: idx, mainColor: mainColor)
        })
        
    }
    
    

    func addButton(withLevel level: NSNumber?, selectedLevel: NSNumber?, to index: Int, mainColor: UIColor?) {
        let ctn = UIView(frame: CGRect(x: (contentView.bounds.size.width / 4) * CGFloat(index), y: 0, width: contentView.bounds.size.width / 4, height: scrollView.bounds.size.height))
        ctn.backgroundColor = UIColor.clear
        let xBtn: CGFloat = (ctn.frame.size.width - scrollView.frame.size.height) / 2
        let button = IntimateLevelButton(frame: CGRect(x: xBtn, y: 0, width: scrollView.bounds.size.height, height: scrollView.bounds.size.height))
        button.mainColor = mainColor
        button.tag = index + 1
        if Int(level ?? 0) > Int(_currentLevel!) {
            button.setDisabledUI()
        } else if Int(level ?? 0) <= Int(_currentLevel!) {
            button.setEnabledUI()
        }
        if button.tag == Int(selectedLevel ?? 0) {
            button.setSelectedUI()
        }
        if let aLevel = level {
            button.setTitle("\(aLevel)", for: .normal)
        }
        button.addTarget(self, action: #selector(self.selectLevel(_:)), for: .touchDown)
        ctn.addSubview(button)
        scrollView.addSubview(ctn)
        levelButtons?.add(button)
    }
    @objc func selectLevel(_ sender: IntimateLevelButton?) {
        sender?.setSelectedUI()
        /*if delegate?.responds(to: #selector(self.didSelectIntimateLevel(_:))) {
            delegate?.didSelectIntimateLevel(sender?.tag as! NSNumber)
        }
        levelButtons?.enumerateObjects({ btn, idx, stop in
            if btn.tag != sender?.tag && btn.tag ?? 0 <= _currentLevel {
                btn.setEnabledUI()
            }
        })*/
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    
}
