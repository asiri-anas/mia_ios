//
//  CalibrationHistoryManager.swift
//  mia
//
//  Created by ASIRI Anas on 19/09/2018.
//  Copyright © 2018 V-Labs. All rights reserved.
//
import CoreData
import Foundation
@objc class CalibrationHistoryManager : NSObject {
    
    @objc static let instance = CalibrationHistoryManager()
    
    @objc func create(withTonicityValue tonicity: NSNumber?) -> CalibrationHistory? {
        let cal = CalibrationHistory()
        cal.value = tonicity
        cal.createdAt = Date()
        CDManager.instance?.saveContext()
        return cal
    }
    @objc func fetchCalibrationHistory(withFiter filter: Filter?) -> [Any]? {
        let managedObjectContext = CDManager.instance!.managedObjectContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        let entity = NSEntityDescription.entity(forEntityName: "CalibrationHistory", in: managedObjectContext!)
        fetchRequest.entity = entity
        let sd = NSSortDescriptor(key: "createdAt", ascending: true)
        fetchRequest.sortDescriptors = [sd]
        if filter != nil {
            let predicate: NSPredicate? = IntimateManager.instance.generatePredicate(withFilters: [filter])
            fetchRequest.predicate = predicate
        }
        var error: Error?
        let result = try? CDManager.instance!.managedObjectContext?.fetch(fetchRequest)
        if error != nil {
            return nil
        }
        return result!
    }
}
