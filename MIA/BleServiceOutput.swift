//
//  BleServiceOutput.swift
//  mia
//
//  Created by Valentin Ferriere on 16/09/2017.
//  Copyright © 2017 V-Labs. All rights reserved.
//

import Foundation

@objc protocol BleServiceOutput: NSObjectProtocol {
    func onConnected()
    func onReady()
    func onDisconnected()
    func onBatteryLevel(level: Int)
    func onGettingPressureStarted()
    func onGettingPressureStopped()
    func onPressureDataReceived(data: Int)
    func onScanStarted()
    func onScanStopped()
}
