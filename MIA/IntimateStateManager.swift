//
//  IntimateStateManager.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 02/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import CoreData


class IntimateStateManager: NSObject {
    
    // MARK: - Singleton -
    override private init() {}
    @objc static let instance = IntimateStateManager()
    
    /// Returns the current IntimateState object.
    func fetchIntimateState() -> IntimateState? {
 
    var fetchRequest = NSFetchRequest<NSFetchRequestResult>()
    var entity = NSEntityDescription.entity(forEntityName: "IntimateState", in: (CDManager.instance!.managedObjectContext)!)
    fetchRequest.entity = entity
    fetchRequest.fetchLimit = 1
    var error: Error?
    var result = try? CDManager.instance!.managedObjectContext?.fetch(fetchRequest).last as? IntimateState
    if error != nil {
        return nil
    }
        return result!
        
    }
    /// Will create, save and return the initial IntimateState object. This will overwrite the previous one.
    func createInitialIntimateState(forProgramKey programKey: String?) -> IntimateState? {
        
    var intimateState = fetchIntimateState()
    if intimateState != nil {
        CDManager.instance!.managedObjectContext?.delete(intimateState!)
    }
    /*intimateState = IntimateState.create()
        var progTypes = IntimateManager.shared()?.fetchIntimateProgramTypes()
        (progTypes as! NSArray).enumerateObjects({(_type: IntimateProgramType?, _idx: Int, _stop: UnsafeMutablePointer<ObjCBool>) -> Void in
        if (_type?.label == programKey) {
            intimateState?.intimateProgramType = _type
            //_stop = true
        }
        } as! (Any, Int, UnsafeMutablePointer<ObjCBool>) -> Void)
        var intimateCoachManager = IntimateCoachManager.shared()
        var coach = intimateCoachManager!.fetchIntimateCoach()
        intimateState?.currentSession = 1
        //intimateState?.maxSession = IntimateProgramHandler.sharedManager().getSessionsCount(forProgramKey: programKey)
        intimateState?.programStart = Date()
        coach?.intimateState = intimateState
        CDManager.instance!.saveContext()
        let DEFAULT_INTIMATE_GOALS: NSNumber = 3
        IntimateManager.shared()?.updateIntimateGoals(withSessionsCount: DEFAULT_INTIMATE_GOALS)*/
    return intimateState
        
    }
    /// Will delete the IntimateState object from Core Data. This should be done at the end of a program.
    func deleteIntimateState() {
        
    var state = fetchIntimateState()
        CDManager.instance!.managedObjectContext?.delete(state!)
        CDManager.instance!.saveContext()
        
    }
    /// Will fetch and return an array of IntimateComfort entities.
    func fetchIntimateComforts() -> [Any]? {
        
        var fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        //var entity = NSEntityDescription.entity(forEntityName: "IntimateComfort", in: (CDManager.instance?.managedObjectContext)!)
    //fetchRequest.entity = entity
    var sd = NSSortDescriptor(key: "comfortID", ascending: true)
    fetchRequest.sortDescriptors = [sd]
    var error: Error?
        var result = try? CDManager.instance!.managedObjectContext?.fetch(fetchRequest)
    if error != nil {
        return nil
    }
        return result!
        
    }
    /// Will fetch and return an array of IntimateVaginalPain entities.
    func fetchIntimateVaginalPains() -> [Any]? {
        
        var fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        //var entity = NSEntityDescription.entity(forEntityName: "IntimateVaginalPain", in: (CDManager.instance?.managedObjectContext)!)
    //fetchRequest.entity = entity
    var sd = NSSortDescriptor(key: "painID", ascending: true)
    fetchRequest.sortDescriptors = [sd]
    var error: Error?
        var result = try? CDManager.instance!.managedObjectContext?.fetch(fetchRequest)
    if error != nil {
        return nil
    }
        return result!
        
    }
}
