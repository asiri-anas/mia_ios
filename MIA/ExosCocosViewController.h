//
//  ExosIntimateLockingViewController.h
//  mymercurochrome
//
//  Created by Rémi Caroff on 02/12/2015.
//  Copyright © 2015 V-Labs. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CocosHandler;
@class IntimateActivityBase;
@class IntimateActivityType;
@class IntimateExoViewModel;

@interface ExosCocosViewController : UIViewController
 
@property (nonatomic, strong) NSNumber *exoDuration;
@property (nonatomic) BOOL isFreeExo;
@property (nonatomic, strong) NSNumber *selectedLevel;
@property (nonatomic, strong) CocosHandler *cocosHandler;

@property (nonatomic, strong) IntimateActivityType *activityType;
@property (nonatomic, strong) IntimateExoViewModel *exoViewModel;

@end
