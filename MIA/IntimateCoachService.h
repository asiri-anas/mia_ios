//
//  IntimateCoachService.h
//  mymercurochrome
//
//  Created by Rémi Caroff on 09/12/2015.
//  Copyright © 2015 V-Labs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AlgoIntime.h"
@class IntimateActivityResult;
@class IntimateSession;
@class IntimateActivity;
@class IntimateCalibrationStructure;
@class IntimateState;

@interface IntimateCoachService : NSObject

/// Returns a singleton instance of IntimateCoachService.
+ (IntimateCoachService *)sharedService;


/// This method will return an IntimateCalibrationStruct value object for the given activitySteps objects.
- (IntimateCalibrationStructure *)getCalibrationForIntimateActivitySteps:(NSArray *)activitySteps;


/// This method returns the calibration value to record (aka tonicity).
- (NSNumber *)getTonicityForIntimateActivitySteps:(NSArray *)activitySteps;

/*!
 @brief This method will return an array of PressurePairRowData value objects. This array is used to construct a curve pattern in cocos2d.
 @param activityType The type of the wanted exercise.
 @param calibrationHistory The CalibrationHistory object created at the beginning of a session.
 @param level The user's level for this exercise.
 */
- (NSArray *)getActivitySkeletonForActivityType:(NSNumber*)activityType calibrationStructure:(IntimateCalibrationStructure *)calibrationStructure andLevel:(NSNumber *)level;


/*!
 @brief this method will return an IntimateActivityResult value object, that will permit to construct an IntimateActivity object.
 @param activitySteps An array of dictionaries which are from cocos at the end of an exercise.
 */
- (IntimateActivityResult *)getResultWithActivitySteps:(NSArray *)activitySteps;


/// Will calculate the leaf level based on the given IntimateState object.
- (NSNumber *)getLeafLevelForIntimateState:(IntimateState *)intimateState;


/// Will return an average of the hearts won in the intimateActivities of the given IntimateSession object.
- (NSNumber *)getHeartsForIntimateSession:(IntimateSession *)intimateSession;

@end
