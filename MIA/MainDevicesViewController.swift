//
//  MainDevicesViewController.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 02/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit

protocol DeviceCellDelegate: NSObjectProtocol {
    func didTapCustomButton()
}

class DeviceTableViewCell: UITableViewCell {
    @IBOutlet var gradientView: UIView!
    @IBOutlet var customLabel: UILabel!
    @IBOutlet var customImageView: UIImageView!
    @IBOutlet var subtitleLabel: UILabel!
    weak var delegate: DeviceCellDelegate?
    var selectedBG: UIView?
    func setContentWithDeviceType(_ deviceType: Int) {
    }
    func setContentWithDescriptionForDeviceType(_ deviceType: Int) {
    }
    @IBAction func selectProduct(_ sender: Any) {
    }
    func setContentWithTitle(_ title: String?, subtitle: String?, andImageName imageName: String?, withBlur blur: Bool) {
    }
}
