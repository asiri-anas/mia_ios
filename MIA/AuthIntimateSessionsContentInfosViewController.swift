//
//  AuthIntimateSessionsContentInfosViewController.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 14/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit


class AuthIntimateSessionsContentInfosViewController: AbstractViewController, UIScrollViewDelegate {
    

    @IBOutlet weak var widthView: NSLayoutConstraint!
    
    
    override func viewDidLoad() {
            
        super.viewDidLoad()
        
        setGradientBackground()
        
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        widthView.constant = screenWidth
        self.updateViewConstraints()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
        
    }
    
    
    
    
}
