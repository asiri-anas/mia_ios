//
//  MainIntimateAdvicesViewController.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 11/06/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation

class MainIntimateAdvicesViewController: AbstractAdvicesViewController, MenuProtocol {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        categoryID = Constants.ADVICE_CATEGORY_INTIMATE_ID 
        fetchAdvicesList()
        //[self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"nav_pink"] forBarMetrics:UIBarMetricsDefault];
        navigationItem.title = NSLocalizedString("advices_space", comment: "").uppercased()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        Singleton.instance.setVC(self)
        super.viewDidAppear(animated)
    }
    
    
    func notifyVC() {
        tabBarController?.selectedIndex = 0
    }
    
    
}
