//
//  MainIntimateProgressViewController.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 05/07/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit

class MainIntimateProgressViewController: AbstractViewController, BarButtonItemDelegate {
    
    
    
    @IBOutlet weak var barButtonSummaryDetail: UIButton!
    
    
    
    func didTapRightBarButtonItem() {
        //
    }
    
    func didTapLeftBarButtonItem() {
        //
        
}

    override func viewDidLoad() {
        
        super.viewDidLoad()
        setMenuButtonForNavigationController()
        
        setGradientBackground()
        navigationItem.title = "Ma Progression"
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.pinkMercu()]
        
        barButtonSummaryDetail.layer.borderWidth=1
        barButtonSummaryDetail.layer.borderColor=UIColor.pinkMercu()?.cgColor
        super.barButtonItemDelegate = self
}


    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //[self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"nav_pink"] forBarMetrics:UIBarMetricsDefault];
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setMenuButtonForNavigationController()
        
}
    
}
