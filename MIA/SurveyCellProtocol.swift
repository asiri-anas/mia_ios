//
//  SurveyCellProtocol.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 17/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit

protocol SurveyCellDelegate: NSObjectProtocol {
    func didUpdateValue(for widget: SurveyWidget?, at indexPath: IndexPath?)
}
protocol SurveyCellProtocol: NSObjectProtocol {
    func cellTapped()
    func setSurveyWidget(_ widget: SurveyWidget?, forIntimateStatusKey intimateStatusKey: String?, at indexPath: IndexPath?, delegate: (UIViewController & SurveyCellDelegate)?)
    //func init() -> (UITableViewCell & SurveyCellProtocol)?
}

