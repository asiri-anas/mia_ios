//
//  SurveyHeaderView.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 14/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit
class SurveyHeaderView: UIView {
    @IBOutlet var titleLabel: UILabel!
    class func create(withTitle title: String?) -> SurveyHeaderView? {
        
var view = Bundle.main.loadNibNamed("SurveyHeaderView", owner: nil, options: nil)?.last as? SurveyHeaderView
if (view is SurveyHeaderView) {
    view?.titleLabel.text = NSLocalizedString(title!, comment: "")
}
return view
        
    }
}
