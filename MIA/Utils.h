/*
 * Utils.h
 *
 *  Created on: Dec 15, 2015
 *      Author: kevin
 */

#ifndef UTILS_H_
#define UTILS_H_

#include "cocos2d.h"

using namespace cocos2d;

/**
 * Utils Class
 */
class Utils {
public:
	Utils();
	virtual ~Utils();

	/** Parse an hex string color to Color4B */
	static Color4B hexToColor4B(std::string hex);
};

#endif /* UTILS_H_ */
