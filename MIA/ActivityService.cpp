//
//  ActivityService.cpp
//  mymercurochrome
//
//  Created by Rémi Caroff on 16/12/2015.
//  Copyright © 2015 V-Labs. All rights reserved.
//

#include "ActivityService.h"
#include "ExerciseLayer.h"
#include "BreathLayer.h"
#include "CalibrationLayer.h"
#include "LeafService.h"
#include "Constants.h"
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include "../NDKHelper/NDKHelper.h"
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#include "NDKHelper.h"
#endif

USING_NS_CC;

using namespace std;


static ActivityService* s_activityService = nullptr;

ActivityService* ActivityService::getInstance()
{
    if (s_activityService == nullptr)
    {
    	s_activityService = new (std::nothrow) ActivityService;
    }

    return s_activityService;
}

void ActivityService::destroyInstance()
{
    LeafService::destroyInstance();
    if (s_activityService)
    {
        delete s_activityService;
        s_activityService = nullptr;
    }
}

ActivityService::~ActivityService()
{
	NDKHelper::removeSelectorsInGroup(ACTIVITY_SERVICE_SELECTORS);
}

bool ActivityService::init(Node* node)
{
	NDKHelper::removeSelectorsInGroup(ACTIVITY_SERVICE_SELECTORS);
	NDKHelper::addSelector(ACTIVITY_SERVICE_SELECTORS,
								SETUP_VIEW_EXERCISE,
								   CC_CALLBACK_2(ActivityService::setupViewExercise, this),
								   node);

	NDKHelper::addSelector(ACTIVITY_SERVICE_SELECTORS,
						   SETUP_VIEW_CALIBRATION,
						   CC_CALLBACK_2(ActivityService::setupViewCalibrationLayer, this),
						   node);

	NDKHelper::addSelector(ACTIVITY_SERVICE_SELECTORS,
						   SETUP_VIEW_BREATH,
						   CC_CALLBACK_2(ActivityService::setupViewBreathLayer, this),
						   node);

	NDKHelper::addSelector(ACTIVITY_SERVICE_SELECTORS,
								   END_SCENE,
								   CC_CALLBACK_2(ActivityService::endScene, this),
								   node);
    return true;
};

bool ActivityService::initLeafWithNode(Node* node)
{
	LeafService::getInstance()->init(node);
	init(node);
	return true;
}

void ActivityService::setupViewExercise(Node *sender, Value data)
{
    m_activitySteps.clear();
	if (!data.isNull() && data.getType() == Value::Type::MAP) {
		auto scene = ExerciseLayer::createScene(data);
		Director::getInstance()->replaceScene(TransitionSlideInR::create(0.3f, scene));
	}
}

void ActivityService::setupViewCalibrationLayer(Node *sender, Value data)
{
	if (!data.isNull() && data.getType() == Value::Type::MAP) {
		auto scene = CalibrationLayer::createScene(data);
		Director::getInstance()->replaceScene(TransitionSlideInR::create(0.3f, scene));
	}
}

void ActivityService::setupViewBreathLayer(Node *sender, Value data)
{
	if (!data.isNull() && data.getType() == Value::Type::MAP) {
		auto scene = BreathLayer::createScene(data);
		Director::getInstance()->replaceScene(TransitionSlideInR::create(0.3f, scene));
	}
}

void ActivityService::endScene(Node *sender, Value data)
{
	Director::getInstance()->end();
}

void ActivityService::setOriginalPattern(std::vector<Vec2> orginalPattern)
{
	m_originalPattern = orginalPattern;
	m_maxTimePattern = m_originalPattern[m_originalPattern.size() - 1].x;
}

float ActivityService::getTargetPressure(float time)
{
	int nbrPattern = time / m_maxTimePattern;
	float currentTime = time - nbrPattern * m_maxTimePattern - 0.001;
	Vec2 previousPts;
	Vec2 nextPts;
    
    log("time: %f, nbrPattern: %d, m_maxTimePattern: %f currentTime: %f", time, nbrPattern, m_maxTimePattern ,currentTime);
	for (int i = 1; i <= m_originalPattern.size(); ++i) {
		previousPts = m_originalPattern[i-1];
		nextPts = m_originalPattern[i];

		if (currentTime <= nextPts.x) {
			// Good points
			// determine if up down or same
			float diff = nextPts.y - previousPts.y;

			if (diff == 0) {
				return nextPts.y;
			} else if (diff > 0) {
				// up
				return ((currentTime - previousPts.x) * (nextPts.y -  previousPts.y)) / (nextPts.x - previousPts.x) + previousPts.y;
			} else {
				// down
				return ((nextPts.x - currentTime) * (previousPts.y - nextPts.y)) / (nextPts.x - previousPts.x) + nextPts.y;
			}
		}
	}
	return 0;
}

int ActivityService::getTextIndication(float time)
{
	int nbrPattern = time / m_maxTimePattern;
	float currentTime = time - nbrPattern * m_maxTimePattern;
	Vec2 previousPts;
	Vec2 nextPts;
	for (int i = 1; i <= m_originalPattern.size(); ++i) {
		previousPts = m_originalPattern[i-1];
		nextPts = m_originalPattern[i];
		if (currentTime <= nextPts.x) {
			// Good points
			// determine if up down or same
			float diff = nextPts.y - previousPts.y;

			if (diff == 0) {
				return 0;
			} else if (diff > 0) {
				// up
				return 1;
			} else {
				// down
				return -1;
			}
		}
	}
	return 0;
}

void ActivityService::addActivitySteps(activityStep* steps)
{
	ValueMap activitySteps;

	activitySteps[KEY_TIME] = steps->timestamp;
	activitySteps[KEY_PRESSURE] = steps->pressure;
	activitySteps[KEY_TARGET] = steps->target;

	Value value(activitySteps);
	m_activitySteps.push_back(value);
}

void ActivityService::sendActivitySteps()
{
	Value steps(m_activitySteps);
	ValueMap activityStepsList;
	activityStepsList[KEY_ACTIVITY_STEPS] = steps;
	Value valueToSend(activityStepsList);
	m_activitySteps.clear();
	LeafService::destroyInstance();
	sendMessageWithParams(ON_EXERCISE_FINISHED, valueToSend);
}
