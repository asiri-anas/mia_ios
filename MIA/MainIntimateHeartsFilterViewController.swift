//
//  MainIntimateHeartsFilterViewController.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 13/09/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit


class MainIntimateHeartsFilterViewController: AuthIntimateSliderViewController, BarButtonItemDelegate {
    func didTapLeftBarButtonItem() {
        
    }
    
    
    var selectedHearts: NSNumber?
    
    
    override func viewDidLoad() {
    super.viewDidLoad()
    setGradientBackground()
        navigationItem.title = NSLocalizedString("filter_by_hearts", comment: "").uppercased()
    setBackButtonForNavigationController()
    self.setRightBarButtonItemWith(UIImage(named: "validate_white"))
    if selectedHearts != nil {
        slider.setValue(selectedHearts as! Float, animated: false)
        let val = Int(roundf(slider.value))
        if val == 0 {
            cursorIV.image = nil
            cursorLabel.text = NSLocalizedString("no_filter", comment: "")
        } else {
            cursorLabel.text = String(format: "%i", val - 1)
            cursorIV.image = UIImage(named: String(format: "hearts_%i", val - 1))
        }
    } else {
        cursorIV.image = nil
        cursorLabel.text = NSLocalizedString("no_filter", comment: "")
    }
}
    
    override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    super.barButtonItemDelegate = self
}
    
    
func didTapRightBarButtonItem() {
    NotificationCenter.default.post(name: NSNotification.Name("intimate_hearts_filter"), object: nil, userInfo: ["hearts": Int(roundf(slider.value - 1))])
    navigationController?.popViewController(animated: true)
}
    
    override func sliderValueChanged(_ sender: Any?) {
    super.sliderValueChanged(sender)
    let val = Int(roundf(slider.value))
    if val == 0 {
        cursorLabel.text = NSLocalizedString("no_filter", comment: "")
        cursorIV.image = nil
    } else {
        cursorLabel.text = String(format: "%i", val - 1)
        cursorIV.image = UIImage(named: String(format: "hearts_%i", val - 1))
    }
}
    
    
}
