//
//  BleServiceInput.swift
//  mia
//
//  Created by Valentin Ferriere on 16/09/2017.
//  Copyright © 2017 V-Labs. All rights reserved.
//

import Foundation

@objc protocol BleServiceInput {
    func startScanning(names: [String])
    func stopScanning()
    func setAutoConnect(willAutoconnect: Bool)
    func connect()
    func disconnect()
    func isConnected() -> Bool
    func isScanning() -> Bool
    func isReady() -> Bool
    func getBattery()
    func startGettingPressure()
    func stopGettingPressure()
}
