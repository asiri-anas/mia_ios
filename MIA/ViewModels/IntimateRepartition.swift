//
//  IntimateRepartition.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 13/09/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation

class IntimateRepartition: NSObject {
    
    var maxScore: NSNumber?
    var score: NSNumber?
    var count: NSNumber?
    var intimateActivityType: IntimateActivityType?
    
    init(intimateActivity activity: IntimateActivity?) {
        
    super.init()
    count = 1
        intimateActivityType = activity?.intimateActivityType
        score = activity?.score
        maxScore = activity?.maxScore
        
    }
    
    
    func increment(score: NSNumber?, maxScore: NSNumber?) {
        

        self.score = Int(self.score!) + Int(score!) as NSNumber
        self.maxScore = Int(self.maxScore!) + Int(maxScore!) as NSNumber
        self.count = Int(self.count!) + 1 as NSNumber
        
    }
    
    
    
}
