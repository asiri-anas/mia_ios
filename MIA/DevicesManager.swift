//
//  DevicesManager.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 02/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import CoreData
import UIKit

//  The converted code is limited to 4 KB.
//  Upgrade your plan to remove this limitation.
//
/*!
 * @typedef devices
 * @brief Returns a constant that indicates the coach type the user owned.
 * @discussion
 
 * @field <b>hasCoachIntimate</b> The user has the IntimateCoach.
 
 */
enum devices : Int {
    ///The user has the IntimateCoach.
    case hasCoachIntimate
}

class DevicesManager: NSObject {
    /// Returns a singleton instance of DevicesManager.
    class func shared() -> DevicesManager? {
        
    var _sharedManager: DevicesManager? = nil
    var onceToken: Int = 0
    if (onceToken == 0) {
                /* TODO: move below code to a static variable initializer (dispatch_once is deprecated) */
        _sharedManager = DevicesManager()
    }
            onceToken = 1
    return _sharedManager
        }
    
        /// Returns an array of StressCoach/FitnessCoach objects the user added.
    func fetchUserDevices() -> [Any]? {
            
    var devices = [AnyHashable]()
    var predicate = NSPredicate(format: "user.userID == \(Registry.get("user_id"))")
        var intimateCoachED = NSEntityDescription.entity(forEntityName: "IntimateCoach", in: (CDManager.instance!.managedObjectContext)!)
        var intimateCoachFR = NSFetchRequest<NSFetchRequestResult>()
    intimateCoachFR.entity = intimateCoachED
    intimateCoachFR.predicate = predicate
    var error: Error?
        if let anError = try? (CDManager.instance!.managedObjectContext)!.fetch(intimateCoachFR) {
            devices.append(anError as! AnyHashable)
    }
    if error != nil {
        UIAlertView(title: NSLocalizedString("error", comment: ""), message: NSLocalizedString("error_fetch_context", comment: ""), delegate: nil, cancelButtonTitle: NSLocalizedString("ok", comment: ""), otherButtonTitles: "").show()
        return nil
    }
    return devices
            
        }
    
}
