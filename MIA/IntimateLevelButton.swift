//
//  IntimateLevelButton.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 16/07/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit

class IntimateLevelButton: UIButton {
    
    var mainColor: UIColor?
    
    func setDoneUI() {
        
        layer.borderWidth = 1.0
        layer.borderColor = mainColor?.cgColor
        tintColor = mainColor
        backgroundColor = UIColor.clear
        setTitle("", for: .normal)
        setImage(UIImage(named: "validate_pink"), for: .normal)
        if let aSize = UIFont(name: "SourceSansPro-Regular", size: 20) {
            titleLabel?.font = aSize
        }
        layer.cornerRadius = 3.0
        layer.masksToBounds = true
        contentEdgeInsets = UIEdgeInsetsMake(15, 15, 15, 15)
        
    }
    
    
    func setEnabledUI() {
        
        layer.borderWidth = 1.0
        layer.borderColor = mainColor?.cgColor
        setTitleColor(mainColor, for: .normal)
        backgroundColor = UIColor.clear
        if let aSize = UIFont(name: "SourceSansPro-Regular", size: 20) {
            titleLabel?.font = aSize
        }
        layer.cornerRadius = 3.0
        layer.masksToBounds = true
        self.isEnabled = true
        
    }
    
    
    func setDisabledUI() {
        
        layer.borderWidth = 1.0
        layer.borderColor = UIColor.colorWithHexString(stringToConvert: "#C8C7CC")?.cgColor
        setTitleColor(UIColor.colorWithHexString(stringToConvert: "#8F8F8F"), for: .normal)
        backgroundColor = UIColor.colorWithHexString(stringToConvert: "#F2F2F2")
        if let aSize = UIFont(name: "SourceSansPro-Regular", size: 20) {
            titleLabel?.font = aSize
        }
        layer.cornerRadius = 3.0
        layer.masksToBounds = true
        self.isEnabled = false
        
    }
    
    
    func setSelectedUI() {
        
        layer.borderWidth = 1.0
        layer.borderColor = mainColor?.cgColor
        backgroundColor = mainColor
        setTitleColor(UIColor.white, for: .normal)
        if let aSize = UIFont(name: "SourceSansPro-Regular", size: 20) {
            titleLabel?.font = aSize
        }
        layer.cornerRadius = 3.0
        layer.masksToBounds = true
        self.isEnabled = true
        
    }
    
    
    //- (void)drawRect:(CGRect)rect {
    //    [super drawRect:rect];
    //    [self.titleLabel setFont:[UIFont fontWithName:@"SourceSansPro-Regular" size:20]];
    //    [self.layer setCornerRadius:3.0f];
    //}
    
    
}

