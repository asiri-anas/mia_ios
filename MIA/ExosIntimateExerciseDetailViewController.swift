//
//  ExosIntimateExerciseDetailViewController.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 13/07/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit
import MessageUI


@objc class ExosIntimateExerciseDetailViewController: AbstractViewController, MFMailComposeViewControllerDelegate {
    
    /*
    var mailPicker: MFMailComposeViewController?
    @IBOutlet var chartView: LineChartView!*/
    
    
    var isFromListing = false
    var intimateActivity: IntimateActivity?
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var contentView: UIView!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var exoTypeLabel: UILabel!
    @IBOutlet var statusLabel: UILabel!
    @IBOutlet var levelLabel: UILabel!
    @IBOutlet var durationLabelCtn: UIView!
    @IBOutlet var scoreLabelCtn: UIView!
    @IBOutlet var heartsIV: UIImageView!
    @IBOutlet var infoButton: UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        super.barButtonItemDelegate = self as! BarButtonItemDelegate
        //[self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"nav_pink"] forBarMetrics:UIBarMetricsDefault];
        //self.rightBarButtonItemWithImage = UIImage(named: "share_picto")
        navigationItem.title = NSLocalizedString("exercise_details", comment: "").uppercased()
        //dateLabel.text = "\(DateHelper.serializeLongDateString(fromDate: intimateActivity.createdAt)), \(DateHelper.timeFormattedString(fromDate: intimateActivity.createdAt))".uppercased()
        exoTypeLabel.text = NSLocalizedString((intimateActivity?.intimateActivityType?.label)!, comment: "")
        levelLabel.text = "\(intimateActivity?.level)"
        heartsIV.image = UIImage(named: "hearts_\(intimateActivity?.hearts)")
        statusLabel.text = NSLocalizedString((intimateActivity?.intimateStatus?.name)!, comment: "")
        if isFromListing {
            setBackButtonForNavigationController()
        } else {
            navigationItem.setHidesBackButton(true, animated: true)
            setLeftCloseButtonForModalNavigationController()
        }
        fillGraph()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    
    
    func fillGraph() {
        let steps = NSKeyedUnarchiver.unarchiveObject(with: (intimateActivity?.datas!)!) as? [Any]
        var xValues = [AnyHashable](repeating: 0, count: steps?.count ?? 0)
        var yValuesUser = [AnyHashable](repeating: 0, count: steps?.count ?? 0)
        var yValuesTarget = [AnyHashable](repeating: 0, count: steps?.count ?? 0)
        (steps as NSArray?)?.enumerateObjects({ activityStep, idx, stop in
           // if let aTime = (activityStep as AnyObject).time {
           //     xValues.append("\(aTime)")
           // }
            //yValuesTarget.append(ChartDataEntry(x: activityStep?.time ?? 0.0, y: activityStep?.target ?? 0.0 + 5))
            //yValuesUser.append(ChartDataEntry(x: activityStep?.time ?? 0.0, y: activityStep?.pressure ?? 0.0 + 5))
        })
        /*chartView.backgroundColor = UIColor.white
        chartView.descriptionText = ""
        chartView.rightAxis.enabled = false
        chartView.leftAxis.enabled = true
        chartView.legend.enabled = false
        chartView.drawBordersEnabled = false
        chartView.drawGridBackgroundEnabled = false
        chartView.highlightPerTapEnabled = false
        let xAxis = chartView.xAxis as? ChartXAxis
        xAxis?.labelFont = UIFont(name: "SourceSansPro-Regular", size: 12)
        xAxis?.labelTextColor = UIColor.lightGray
        xAxis?.drawGridLinesEnabled = false
        xAxis?.labelPosition = XAxisLabelPositionBottom
        xAxis?.valueFormatter = IntimateExoDurationFormatter()
        xAxis?.axisLineDashLengths = [1.0]
        xAxis?.labelHeight = 100
        let yAxis: ChartYAxis? = chartView.leftAxis
        yAxis?.labelFont = UIFont(name: "SourceSansPro-Regular", size: 12)
        yAxis?.labelTextColor = UIColor.lightGray
        yAxis?.drawGridLinesEnabled = true
        yAxis?.drawAxisLineEnabled = false
        yAxis?.gridLineDashLengths = [1.0]
        yAxis?.drawTopYLabelEntryEnabled = true
        //yAxis.startAtZeroEnabled = NO;
        yAxis?.customAxisMin = -5
        yAxis?.drawLabelsEnabled = false
        let line1 = LineChartDataSet(values: yValuesUser)
        line1.colors = [UIColor.gray]
        line1.drawCirclesEnabled = false
        line1.drawCubicEnabled = false
        line1.drawValuesEnabled = false
        line1.highlightEnabled = false
        let line2 = LineChartDataSet(values: yValuesTarget)
        line2.colors = [UIColor(hexString: intimateActivity.intimateActivityType.colorHex)]
        line2.lineWidth = 0
        line2.drawCirclesEnabled = false
        line2.drawCubicEnabled = false
        line2.drawValuesEnabled = false
        line2.drawFilledEnabled = true
        line2.fillColor = UIColor(hexString: intimateActivity.intimateActivityType.colorHex)
        line2.fillAlpha = 0.5
        line2.highlightEnabled = false
        let lineData = LineChartData(dataSets: [line1, line2])
        lineData.highlightEnabled = false
        chartView.data = lineData
        chartView.fitScreen()
        chartView.setVisibleXRangeWithMinXRange(1, maxXRange: 20)*/
    }
    
    
    func didTapRightBarButtonItem() {
        /*mailPicker = MailComposerService.getMailController(forIntimateData: intimateActivity)
        mailPicker.mailComposeDelegate = self
        present(mailPicker, animated: true) {
            mailPicker.navigationBar.setBackgroundImage(UIImage(named: "nav_pink"), for: .default)
            mailPicker.navigationBar.barTintColor = UIColor.white
        }*/
    }
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        //mailPicker.dismiss(animated: true)
    }
    
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        //let minutes: Int = FREE_EXO_DURATION / 60
        //let durationStr = "#<B>%i# \(minutes)"
        //let durationLabel = TTTAttributedLabel()
        //durationLabel.setText(durationStr, inContainerView: durationLabelCtn, withBaseFont: UIFont(name: "SourceSansPro-Light", size: 14), baseColor: UIColor.lightGray, linkFont: UIFont(name: "SourceSansPro-Semibold", size: 17), linkColor: UIColor(hexString: "#1FA56C"), underlined: false, textAlignment: NSTextAlignment.left)
        let scoreStr = "#<B>\(intimateActivity?.score)#\n/\(intimateActivity?.maxScore) Pts"
        //let scoreLabel = TTTAttributedLabel()
        //scoreLabel.setText(scoreStr, inContainerView: scoreLabelCtn, withBaseFont: UIFont(name: "SourceSansPro-Light", size: 18), baseColor: UIColor.pinkMercu(), linkFont: UIFont(name: "SourceSansPro-Semibold", size: 23), linkColor: UIColor.pinkMercu(), underlined: false, textAlignment: NSTextAlignment.center)
    }
    
    
    
    @IBAction func infoButtonTapped(_ sender: Any) {
        
        UIAlertView(title: NSLocalizedString("points_info_title", comment: ""), message: NSLocalizedString("points_info_body", comment: ""), delegate: nil, cancelButtonTitle: NSLocalizedString("ok", comment: ""), otherButtonTitles: "").show()
        
    }
    
    
}
