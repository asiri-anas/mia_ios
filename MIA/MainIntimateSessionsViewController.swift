//
//  MainIntimateSessionsViewController.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 05/07/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit


class MainIntimateSessionsViewController: AbstractViewController, UITableViewDataSource, UITableViewDelegate, BarButtonItemDelegate, FilterCellDelegate, MenuProtocol {
    func didTapLeftBarButtonItem() {
        
    }
    
    
    @IBOutlet var tableView: UITableView!
    var dataSource: NSArray?
    @IBOutlet var filtersTableView: UITableView!
    @IBOutlet var heightFilterTableViewConstraint: NSLayoutConstraint!
    
    var selectedFilters: NSMutableArray?
    var filterTypes: NSMutableArray?
    var allDatas: NSArray?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setMenuButtonForNavigationController()
        //[self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"nav_pink"] forBarMetrics:UIBarMetricsDefault];
        navigationItem.title = NSLocalizedString("session_space", comment: "").uppercased()
        setGradientBackground()
        NotificationCenter.default.addObserver(self, selector: #selector(self.intimateDataSourceUpdated(_:)), name: NSNotification.Name("intimate_data_source_updated"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.filtersUpdated(_:)), name: NSNotification.Name("intimate_filters_updated"), object: nil)
        /*allDatas = IntimateManager.shared()?.fetchIntimateDataSource(withFilters: nil) as! NSArray
        if !(dataSource != nil) {
            dataSource = allDatas
            if (allDatas?.count)! > 0 {
                self.setRightBarButtonItemWith(UIImage.init(named: "loupe"))
            }
        } else {
            hideMenuButton()
            setLeftCloseButtonForModalNavigationController()
        }*/
        selectedFilters = NSMutableArray()
        filterTypes = NSMutableArray()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        Singleton.instance.setVC(self)
        super.viewDidAppear(animated)
        self.setPlayButtonWith(nil)
        super.barButtonItemDelegate = self
    }
    
    
    @objc func filtersUpdated(_ notification: Notification?) {
        selectedFilters = notification?.userInfo!["filters"] as! NSMutableArray
        filterTypes = notification?.userInfo!["types"] as! NSMutableArray
        filtersTableView.reloadData()
        UIView.animate(withDuration: 0.2, animations: {
            self.heightFilterTableViewConstraint.constant = self.filtersTableView.contentSize.height
            self.view.layoutIfNeeded()
        })
        dataSource = IntimateManager.instance.fetchIntimateDataSource(withFilters: selectedFilters as! [Any]) as! NSArray
        tableView.reloadData()
    }
    
    
    @objc func intimateDataSourceUpdated(_ notification: Notification?) {
        dataSource = IntimateManager.instance.fetchIntimateDataSource(withFilters: nil) as! NSArray
        tableView.reloadData()
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        let sb = UIStoryboard(name: "Exos", bundle: nil)
        if (dataSource![indexPath.row] is IntimateActivity) {
            let vc = sb.instantiateViewController(withIdentifier: "ExosIntimateExerciseDetailViewController") as? ExosIntimateExerciseDetailViewController
            vc?.isFromListing = true
            vc?.intimateActivity = dataSource?[indexPath.row] as! IntimateActivity
            if let aVc = vc {
                navigationController?.pushViewController(aVc, animated: true)
            }
        } else {
            let eisdvc = sb.instantiateViewController(withIdentifier: "ExosIntimateSessionDetailsViewController") as? ExosIntimateSessionDetailsViewController
            eisdvc?.isFromListing = true
            eisdvc?.intimateSession = dataSource?[indexPath.row] as! IntimateSession
            if let anEisdvc = eisdvc {
                navigationController?.pushViewController(anEisdvc, animated: true)
            }
        }
    }
    
    
    func didTapRightBarButtonItem() {
        performSegue(withIdentifier: "showFilters", sender: nil)
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == filtersTableView {
            return filterTypes!.count
        }
        if (dataSource?.count)! > 0 {
            return dataSource!.count
        } else {
            return 1
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if tableView == filtersTableView {
            return 0
        }
        return 50
    }
    
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 50))
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == filtersTableView {
            return 44
        }
        return 140
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == filtersTableView {
            let filterCellIdentifier = "filterCell"
            var filterCell = filtersTableView.dequeueReusableCell(withIdentifier: filterCellIdentifier, for: indexPath) as? FilterTableViewCell
            if filterCell == nil {
                filterCell = FilterTableViewCell()
            }
            filterCell?.setContentWithFilterType(filterTypes?[indexPath.row] as! NSNumber, atindexPath: indexPath, withdelegate: self)
            return filterCell!
        }
        let exoCellIdentifier = "exoCell"
        let phCellIdentifier = "phCell"
        if (dataSource?.count)! > 0 {
            var exoCell = self.tableView.dequeueReusableCell(withIdentifier: exoCellIdentifier, for: indexPath) as? IntimateExoTableViewCell
            if exoCell == nil {
                exoCell = IntimateExoTableViewCell()
            }
            //exoCell?.setContentWithIntimateActivity(dataSource[indexPath.row])
            return exoCell!
        } else {
            var phCell = self.tableView.dequeueReusableCell(withIdentifier: phCellIdentifier, for: indexPath) as? TitleTableViewCell
            if phCell == nil {
                phCell = TitleTableViewCell()
            }
            //phCell?.setContentWithTitle( = )NSLocalizedString("no_intimate_session_placeholder", comment: ""))
            return phCell!
        }
    }
    
    
    
    func didTapCloseButton(for cell: FilterTableViewCell?, at indexPath: IndexPath?) {
        var toRemove = [AnyHashable]()
        selectedFilters?.enumerateObjects({ filter, idx, stop in
            /*if (filter as! Filter).type ?? 0 == filterTypes![indexPath?.row ?? 0] {
                if let aFilter = filter {
                    toRemove.append(aFilter)
                }
            }*/
        })
        //selectedFilters = selectedFilters?.filter(using: { !toRemove.contains($0) })
        filterTypes?.remove(indexPath?.row ?? 0)
        filtersTableView.reloadData()
        UIView.animate(withDuration: 0.2, animations: {
            self.heightFilterTableViewConstraint.constant = self.filtersTableView.contentSize.height
            self.view.layoutIfNeeded()
        })
        dataSource = IntimateManager.instance.fetchIntimateDataSource(withFilters: selectedFilters as! [Any]) as! NSArray
        tableView.reloadData()
    }
    
    
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "showFilters") {
            let nc = segue.destination as? UINavigationController
            let mifvc = nc?.viewControllers[0] as? MainIntimateFiltersViewController
            mifvc?.selectedFilters = selectedFilters
            mifvc?.dataSource = allDatas
        }
    }
    
    func notifyVC() {
        tabBarController?.selectedIndex = 0
    }
    
}
