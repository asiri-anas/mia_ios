//
//  TutoSensorHowToUse.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 03/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit

class TutoSensorHowToUse: UIView {
    class func createView() -> TutoSensorHowToUse? {
        
    var view = Bundle.main.loadNibNamed("TutoSensorHowToUse", owner: nil, options: nil)?.last as? TutoSensorHowToUse
    if (view is TutoSensorHowToUse) {
    }
    return view
        
    }
}
