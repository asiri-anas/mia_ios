//
//  LocalizationHelper.cpp
//  mymercurochrome
//
//  Created by Rémi Caroff on 17/12/2015.
//  Copyright © 2015 V-Labs. All rights reserved.
//

#include "LocalizationHelper.h"

LocalizationHelper* LocalizationHelper::lm = nullptr;
LocalizationHelper* LocalizationHelper::getInstance()
{
    if (!lm)
    {
        lm = new LocalizationHelper();
    }
    return lm;
}

LocalizationHelper::LocalizationHelper()
{
}
LocalizationHelper ::~LocalizationHelper()
{
    if (lm)
    {
        delete lm;
    }
}

std::string LocalizationHelper::getLocalizedString(const std::string& key)
{
    
    NSString *str = [NSString stringWithUTF8String: key.c_str()];
    std::string s([NSLocalizedString(str, nil) UTF8String]);
    return s;
    
}