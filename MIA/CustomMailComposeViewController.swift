//
//  CustomMailComposeViewController.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 13/07/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit
import MessageUI



class CustomMailComposeViewController: MFMailComposeViewController {

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationBar.tintColor = UIColor.colorWithHexString(stringToConvert: "#D33777")
        if let aSize = UIFont(name: "SourceSansPro-Regular", size: 20) {
            navigationBar.titleTextAttributes = [NSAttributedStringKey.font: aSize, NSAttributedStringKey.foregroundColor: UIColor.darkGrayTextColorMercu()]
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /*
         #pragma mark - Navigation
     
         // In a storyboard-based application, you will often want to do a little preparation before navigation
         - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
         // Get the new view controller using [segue destinationViewController].
         // Pass the selected object to the new view controller.
         }
         */
}
