//
//  AutoLayoutMetrics.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 02/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import CoreGraphics
import UIKit

class AutoLayoutMetrics: NSObject {
    /*!
     @brief Returns a dictionary where values represent sizes of some native UI elements.
     @discussion
     These dictionary are useful to define autolayout visual format constraints. The keys available are listed below.
     @remark These values are based on the portrait mode.
     @param screenWidth The window's width.
     @param screenHeight The window's height.
     @param viewHeight The main controller's view height, minus the status bar and the navigation bar.
     @param keyboardHeight The height of the keyboard, without T9 top bar.
     */
    
    private var onceToken: Int = 0
    
    func getiPhone4Metrics() -> NSDictionary {
        return [
            "screenWidth" : 320,
            "screenHeight" : 480,
            "viewHeight" : 416,
            "keyboardHeight" : 224,
            "mainViewHeight" : 480 - 40
        ]
    }
    
    
    func getiPhone5Metrics() -> NSDictionary {
        return [
            "screenWidth" : 320,
            "screenHeight" : 568,
            "viewHeight" : 504,
            "keyboardHeight" : 224
        ]
    }
    
    func getiPhone6Metrics() -> NSDictionary {
        return [
            "screenWidth" : 375,
            "screenHeight" : 667,
            "viewHeight" : 603,
            "keyboardHeight" : 225
        ]
    }
    
    func getiPhone6PlusMetrics() -> NSDictionary {
        return [
            "screenWidth" : 414,
            "screenHeight" : 736,
            "viewHeight" : 672,
            "keyboardHeight" : 236
        ]
    }
    
    
    func getiPhoneXMetrics() -> NSDictionary {
        return [
            "screenWidth" : 375,
            "screenHeight" : 812,
            "viewHeight" : 748,
            "keyboardHeight" : 236
        ]
    }
    
    
    func get() -> NSDictionary {
        var _metrics: NSDictionary? = nil
        if (true) { // if (onceToken == 0) {
            /* TODO: move below code to a static variable initializer (dispatch_once is deprecated) */
        let screenBounds: CGRect = UIScreen.main.bounds
            //print(screenBounds.size.height)
        if screenBounds.size.height == 568 {
            _metrics = getiPhone5Metrics()
        } else if screenBounds.size.height == 667 {
            _metrics = getiPhone6Metrics()
        } else if screenBounds.size.height == 736 {
            _metrics = getiPhone6PlusMetrics()
        } else if screenBounds.size.height == 812 {
            _metrics = getiPhoneXMetrics()
        } else {
            _metrics = getiPhone4Metrics()
        }
    }
        onceToken = 1
        return _metrics!
    }
    
    
    
    
    
}
