//
//  DateHelper.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 14/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation


/*!
 @brief A useful helper to format dates.
 */
class DateHelper: NSObject {
    
    class func serializeDate(fromNSDate date: Date?) -> String? {
        var dateString: String? = nil
        if let aDate = date {
            dateString = "\(aDate)"
        }
        let df = DateFormatter()
        df.dateFormat = "dd/MM/yyyy"
        if let aDate = date {
            dateString = df.string(from: aDate)
        }
        return dateString
    }
    
    
    class func unserializeSlash(toNSDate dateString: String?) -> Date? {
        let df = DateFormatter()
        df.dateFormat = "dd/MM/yyyy"
        let date: Date? = df.date(from: dateString ?? "")
        return date
    }
    
    class func serializeLongDateString(from date: Date?) -> String? {
        let df = DateFormatter()
        df.dateFormat = "EEEE dd MMMM yyyy"
        if let aDate = date {
            return df.string(from: aDate)
        }
        return nil
    }
    
    
    
    class func getStartOf(_ date: Date?) -> Date? {
        if date == nil {
            return nil
        }
        let calendar = Calendar.current
        var dateComponents: DateComponents? = nil
        if let aDate = date {
            /*dateComponents = calendar.dateComponents(([.NSYearCalendarUnit, .NSMonthCalendarUnit, .NSDayCalendarUnit], from: aDate))*/
        }
        dateComponents?.hour = 0
        dateComponents?.minute = 0
        dateComponents?.second = 0
        if let aComponents = dateComponents {
            return calendar.date(from: aComponents)
        }
        return nil
    }
    
    
    class func getEndOf(_ date: Date?) -> Date? {
        if date == nil {
            return nil
        }
        let calendar = Calendar.current
        var dateComponents: DateComponents? = nil
        if let aDate = date {
            /*dateComponents = calendar.dateComponents([.NSYearCalendarUnit, .NSMonthCalendarUnit, .NSDayCalendarUnit], from: aDate)*/
        }
        dateComponents?.hour = 23
        dateComponents?.minute = 59
        dateComponents?.second = 59
        if let aComponents = dateComponents {
            return calendar.date(from: aComponents)
        }
        return nil
    }
    
    
    
}
