//
//  LocalizationHelper.hpp
//  mymercurochrome
//
//  Created by Rémi Caroff on 17/12/2015.
//  Copyright © 2015 V-Labs. All rights reserved.
//

#ifndef __Limball__LocalizationHelper__
#define __Limball__LocalizationHelper__

#include <iostream>
#include <string>
class LocalizationHelper
{
public:
    static LocalizationHelper* getInstance();
    std::string getLocalizedString(const std::string& key);
    
private:
    LocalizationHelper();
    ~LocalizationHelper();
    static LocalizationHelper* lm;
    
};

#endif