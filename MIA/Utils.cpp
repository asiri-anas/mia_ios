/*
 * Utils.cpp
 *
 *  Created on: Dec 15, 2015
 *      Author: kevin
 */

#include "Utils.h"

Utils::Utils() {
	// TODO Auto-generated constructor stub

}

Utils::~Utils() {
	// TODO Auto-generated destructor stub
}

Color4B Utils::hexToColor4B(std::string hex) {
	Color4B defaultColor(143,140,189,255);
	int size = hex.size();
	if (size == 0) {
		return defaultColor;
	}

	int *argb = new int[4];
	std::string str;

	// Delete hash
	if (hex[0] == '#') {
		hex.erase(0,1);
	}

	std::string finalHex;
	if (hex.size() == 6) {
		finalHex.append("ff");
		finalHex.append(hex);
	} else if (hex.size() == 8) {
		finalHex.append(hex);
	} else {
		return defaultColor;
	}

	for (int i = 0; i < 4; i++) {
		if (finalHex.size() == 8) str = finalHex.substr(i * 2, 2);
		else break;
		argb[i] = strtol(str.c_str(), NULL, 16);
	}

	Color4B layerColor(argb[1], argb[2], argb[3], argb[0]);

	return layerColor;
}
