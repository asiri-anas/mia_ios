//
//  ExosIntimateSessionDetailsViewController.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 21/06/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit

class ExosIntimateSessionDetailsViewController: AbstractViewController {
    
    
    @IBOutlet var repartitionContainer: UIView!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var programTypeLabel: UILabel!
    @IBOutlet var statusLabel: UILabel!
    @IBOutlet var sessionLevelLabel: UILabel!
    @IBOutlet var durationLabelCtn: UIView!
    @IBOutlet var infoButton: UIButton!
    @IBOutlet var pointsLabelCtn: UIView!
    @IBOutlet var heartsIV: UIImageView!
    @IBOutlet var legendLabel: UILabel!
    @IBOutlet var graphsTableView: UITableView!
    @IBOutlet var heightTableViewConstraint: NSLayoutConstraint!
    var intimateSession: IntimateSession?
    var isFromListing = false
    @IBAction func infoTapped(_ sender: Any) {
    }
    
    
    
}
