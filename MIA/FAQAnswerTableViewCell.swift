//
//  FAQAnswerTableViewCell.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 05/07/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit

class FAQAnswerTableViewCell: UITableViewCell {
    
    @IBOutlet var answerLabel: UILabel!
    
    @IBOutlet var textLabelHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var bubbleView: UIView!
    
    @IBOutlet var linkButton: UIButton!
    
    @IBOutlet var heightLinkContainerConstraint: NSLayoutConstraint!
    
    @IBAction func linkTapped(_ sender: Any) {
        
        if let aText = URL(string: linkButton.titleLabel?.text ?? "") {
            UIApplication.shared.openURL(aText)
        }
        
    }
    
    func setContentWithText(_ text: String?, andLink link: NSString?) {
        
        
        
        answerLabel.text = text
        textLabelHeightConstraint.constant = answerLabel.intrinsicContentSize.height
        if (link?.length)! > 0 {
            var buttonLabelText = NSAttributedString(string: link as! String, attributes: [NSAttributedStringKey.foregroundColor: UIColor.darkGrayTextColorMercu(), NSAttributedStringKey.font: UIFont(name: "SourceSansPro-Light", size: 14), NSAttributedStringKey.underlineStyle: NSUnderlineStyle.styleSingle])
            linkButton.setAttributedTitle(buttonLabelText, for: .normal)
            heightLinkContainerConstraint.constant = linkButton.intrinsicContentSize.height
            linkButton.isHidden = false
        } else {
            heightLinkContainerConstraint.constant = 0
            linkButton.isHidden = true
        }
        layoutIfNeeded()
        
    }
    
    

    override func setSelected(_ selected: Bool, animated: Bool) {
            super.setSelected(selected, animated: animated)
            // Configure the view for the selected state
        }
    override func draw(_ rect: CGRect) {
            super.draw(rect)
            bubbleView.layer.cornerRadius = 5.0
            bubbleView.layer.masksToBounds = true
        }
    override func layoutSubviews() {
            super.layoutSubviews()
            updateConstraintsIfNeeded()
            layoutIfNeeded()
        }
    
    
    
}
