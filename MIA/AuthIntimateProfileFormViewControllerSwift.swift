//
//  AuthIntimateProfileFormViewControllerSwift.swift
//  mia
//
//  Created by Xavier Bohin on 17/04/2018.
//  Copyright © 2018 V-Labs. All rights reserved.
//

import Foundation

@objc
class AuthIntimateProfileFormViewControllerSwift: AbstractViewController, UITextFieldDelegate, UIActionSheetDelegate, BarButtonItemDelegate, HyperLabelDelegate, TTTAttributedLabelDelegate {
    var isCguChecked = false
    var cguLabel: TTTAttributedLabel?
    var statusList = [Any]()
    var selectedStatus: IntimateStatus?
    var selectedBirthDate: Date?
    
    
func viewDidLoad() {
    super.viewDidLoad()
    navigationItem?.title = NSLocalizedString("my_profile", comment: "").uppercased()
    setGradientBackground()
    let gesture = UISwipeGestureRecognizer()
    gesture.direction = .down
    gesture.addTarget(self, action: #selector(self.dismissKeyboard))
    gesture.location(in: view)
    view.addGestureRecognizer(gesture)
    if user != nil {
        firstNameTF.text = user.firstName
        lastNameTF.text = user.lastName
        birthDateTF.text = DateHelper.serializeDate(fromNSDate: user.birthDate)
    }
    statusList = IntimateStatusManager.shared().fetchAllIntimateStatus()
    firstNameTF.setMercuStyle()
    lastNameTF.setMercuStyle()
    birthDateTF.setMercuStyle()
    statusTF.setMercuStyle()
    rightBarButtonItemWith = UIImage(named: "validate_white")
    setBackButtonForNavigationController()
    super.barButtonItemDelegate = self
    if user != nil {
        fillTextFields()
    }
    if editingMode {
        statusTF.isHidden = true
        statusPicto.isHidden = true
        statusChevron.isHidden = true
        checkboxButton.isHidden = true
        cguLabelCtn.isHidden = true
    } else {
        statusTF.isHidden = false
        statusPicto.isHidden = false
        statusChevron.isHidden = false
        checkboxButton.isHidden = false
        cguLabelCtn.isHidden = false
    }
    cguChecked = false
    checkboxButton.setImage(UIImage(named: "case_unmarked"), for: .normal)
    cguLabel = TTTAttributedLabel()
    cguLabel.delegate = self
}
    
func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    //[self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"nav_pink"] forBarMetrics:UIBarMetricsDefault];
    navigationController?.setNavigationBarHidden(false, animated: true)
}
func attributedLabel(_ label: TTTAttributedLabel?, didSelectLinkWith url: URL?) {
    if let anIndex = URL(string: "http://www.mymercurochrome.fr/\((NSLocale.current.localeIdentifier as? NSString)?.substring(to: 2) ?? "")/cgu") {
        UIApplication.shared.openURL(anIndex)
    }
}
func fillTextFields() {
    firstNameTF.text = user.firstName
    lastNameTF.text = user.lastName
    birthDateTF.text = DateHelper.serializeDate(fromNSDate: user.birthDate)
}
func didTapLeftBarButtonItem() {
    if navigationController?.viewControllers[0] == self {
        dismiss(animated: true) {() -> Void in }
    } else {
        if completionMode {
            let app = UIApplication.shared.delegate as? NativeAppDelegate
            app?.loadMainStoryboard(withCompletion: nil)
        } else {
            navigationController?.popViewController(animated: true)
        }
    }
}
    
    
func didTapRightBarButtonItem() {
    if !cguChecked && !editingMode {
        UIAlertView(title: NSLocalizedString("info", comment: ""), message: NSLocalizedString("accept_cgu_error", comment: ""), delegate: nil, cancelButtonTitle: NSLocalizedString("ok", comment: ""), otherButtonTitles: "").show()
        return
    }
    var isTFEmpty = false
    var textfields: [Any]
    if editingMode {
        textfields = [firstNameTF, lastNameTF, birthDateTF]
    } else {
        textfields = [firstNameTF, lastNameTF, birthDateTF, statusTF]
    }
    (textfields as NSArray).enumerateObjects({(_ obj: UITextField?, _ idx: Int, _ stop: UnsafeMutablePointer<ObjCBool>) -> Void in
        if obj?.text?.replacingOccurrences(of: " ", with: "").count == 0 {
            isTFEmpty = true
        }
    })
    if isTFEmpty {
        UIAlertView(title: NSLocalizedString("info", comment: ""), message: NSLocalizedString("textfields_empty", comment: ""), delegate: nil, cancelButtonTitle: NSLocalizedString("ok", comment: ""), otherButtonTitles: "").show()
    } else {
        let intimateCoach = IntimateCoachManager.shared().createInitialIntimateCoach()
        if user == nil {
            user = User.create()
            let userObjectID: NSManagedObjectID? = user.objectID
            let user_PK = Int(((userObjectID?.uriRepresentation().lastPathComponent as? NSString)?.substring(from: 1) ?? "")) ?? 0
            user.userID = user_PK
            DebugLog("USER ID : %@", user.userID)
            user.gender = "F"
        }
        if !editingMode {
            user.intimateCoach = intimateCoach
        }
        user.firstName = firstNameTF.text
        user.lastName = lastNameTF.text
        user.birthDate = (selectedBirthDate == nil) ? DateHelper.unserializeSlash(toNSDate: birthDateTF.text) : selectedBirthDate
        CDManager.shared().saveContext()
        if editingMode {
            NotificationCenter.default.post(name: NSNotification.Name("update_intimate_user"), object: nil, userInfo: ["user": user])
            dismiss(animated: true) {() -> Void in }
        } else {
            Registry.set("user_id", withValue: user.userID)
            NotificationCenter.default.post(name: NSNotification.Name("coach_created"), object: nil, userInfo: ["intimate_coach": intimateCoach])
            performSegue(withIdentifier: "showSpecialist", sender: nil)
        }
    }
}
    
    
func dismissKeyboard() {
    view.endEditing(true)
}
func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    if textField == firstNameTF {
        lastNameTF.becomeFirstResponder()
    }
    if textField == lastNameTF {
        birthDateTF.becomeFirstResponder()
    }
    return false
}
func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
    if textField == birthDateTF {
        ActionSheetDatePicker.show(withTitle: NSLocalizedString("birth_date", comment: ""), datePickerMode: .date, selectedDate: ((user.birthDate == nil) ? Date() : user.birthDate), doneBlock: {(_ picker: ActionSheetDatePicker?, _ selectedDate: Any?, _ origin: Any?) -> Void in
            selectedBirthDate = selectedDate
            birthDateTF.text = DateHelper.serializeDate(fromNSDate: selectedDate)
        }, cancelBlock: nil, origin: view)
        dismissKeyboard()
        return false
    }
    if textField == statusTF {
        let `as` = UIActionSheet(title: NSLocalizedString("your_status", comment: ""), delegate: self, cancelButtonTitle: NSLocalizedString("cancel", comment: ""), destructiveButtonTitle: "", otherButtonTitles: "")
        statusList.enumerateObjects({(_ status: IntimateStatus?, _ idx: Int, _ stop: UnsafeMutablePointer<ObjCBool>) -> Void in
            `as`.addButton(withTitle: NSLocalizedString(status?.name, comment: ""))
        })
        `as`.show(in: view)
        dismissKeyboard()
        return false
    }
    return true
}
    
    
    
func actionSheet(_ actionSheet: UIActionSheet, clickedButtonAt buttonIndex: Int) {
    if buttonIndex != 0 {
        selectedStatus = statusList[buttonIndex - 1]
        statusTF.text = NSLocalizedString(selectedStatus.name, comment: "")
    }
}
func hyperLabel(_ label: FRHyperLabel?, didSelectLinkAt index: Int) {
    DebugLog("selected hyperlink index : %u", index)
}
@IBAction func checkboxTapped(_ sender: Any) {
    cguChecked = !cguChecked
    if cguChecked {
        checkboxButton.setImage(UIImage(named: "case_marked"), for: .normal)
    } else {
        checkboxButton.setImage(UIImage(named: "case_unmarked"), for: .normal)
    }
}
func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    cguLabel.setText(NSLocalizedString("accept_cgu", comment: ""), inContainerView: cguLabelCtn, withBaseFont: UIFont(name: "SourceSansPro-Regular", size: 14), baseColor: UIColor.darkGrayTextColorMercu(), linkFont: UIFont(name: "SourceSansPro-Regular", size: 14), linkColor: UIColor.darkGrayTextColorMercu(), underlined: true, textAlignment: .left)
}
    
    
    
    
// MARK: - Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if (segue.identifier == "showSpecialist") {
        let aisvc = segue.destination as? AuthIntimateSpecialistViewController
        aisvc?.user = user
        aisvc?.intimateStatus = selectedStatus
        aisvc?.editingMode = false
    }
}
    
}
