//
//  ExosIntimateSessionFinishViewController.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 13/07/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit


class ExosIntimateSessionFinishViewController: AbstractViewController {
    
    @IBOutlet var scoreLabel: UILabel!
    @IBOutlet var maxScoreLabel: UILabel!
    @IBOutlet var heartsIV: UIImageView!
    @IBOutlet var showDetailButton: UIButton!
    @IBOutlet var levelsLabel: UILabel!
    var activitiesLevelUp: [Any] = []
    @IBOutlet var scoreView: UIView!
    var intimateSession: IntimateSession?
    var isFromListing = false
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scoreLabel.text = "\(intimateSession?.score)"
        maxScoreLabel.text = "/\(intimateSession?.maxScore)"
        heartsIV.image = UIImage(named: "hearts_\(intimateSession?.hearts)")
        scoreView.layer.cornerRadius = scoreView.frame.size.height / 2
        setLevelsLabelText()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIAlertView(title: NSLocalizedString("cleanup_alert_title", comment: ""), message: NSLocalizedString("cleanup_alert_message", comment: ""), delegate: self as! UIAlertViewDelegate, cancelButtonTitle: NSLocalizedString("ok", comment: ""), otherButtonTitles: NSLocalizedString("help", comment: "")).show()
    }
    
    
 
    
    func setLevelsLabelText() {
        if activitiesLevelUp.count == 0 {
            let random = arc4random_uniform(4)
            let messageKey = String(format: "sessions_end_message_%i", random)
            levelsLabel.text = NSLocalizedString(messageKey, comment: "")
        } else {
            var finalStr = NSMutableAttributedString()
            var str1: NSAttributedString? = nil
            if let aSize = UIFont(name: "SourceSansPro-Regular", size: 14.0) {
                str1 = NSAttributedString(string: NSLocalizedString("congrats_you_reached", comment: "").uppercased(), attributes: [NSAttributedStringKey.font: aSize, NSAttributedStringKey.foregroundColor: UIColor.darkGrayTextColorMercu()])
            }
            if let aStr1 = str1 {
                finalStr.append(aStr1)
            }
           
            /*
            activitiesLevelUp.enumerateObjects({ activity, idx, stop in
                var lvlStr: NSAttributedString? = nil
                if let aSize = UIFont(name: "SourceSansPro-Semibold", size: 14.0) {
                    lvlStr = NSAttributedString(string: String(format: NSLocalizedString("the_level_and_type", comment: ""), activity?.intimateActivityType.programLevelReached, NSLocalizedString(activity?.intimateActivityType.label, comment: "")).uppercased(), attributes: [NSAttributedStringKey.font: aSize, NSAttributedStringKey.foregroundColor: UIColor.pinkMercu()])
                }
                if let aStr = lvlStr {
                    finalStr.append(aStr)
                }
            })
            levelsLabel.attributedText = finalStr*/
        }
        view.layoutIfNeeded()
    }
    
    
    
    @IBAction func showDetails(_ sender: Any) {
        
        performSegue(withIdentifier: "showSessionDetails", sender: nil)
        //[self dismissViewControllerAnimated:YES completion:nil];
        
    }
    
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "showSessionDetails") {
            let eisdvc = segue.destination as? ExosIntimateSessionDetailsViewController
            eisdvc?.intimateSession = intimateSession
            eisdvc?.isFromListing = isFromListing
        }
        }
    
    
}

