//
//  InfosViewController.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 25/06/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit

class InfosViewController: AbstractViewController, BarButtonItemDelegate {
    func didTapRightBarButtonItem() {
        //
    }
    
    func didTapLeftBarButtonItem() {
        //
        
    }
    

    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var button3: UIButton!
    @IBOutlet weak var button2: UIButton!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        setMenuButtonForNavigationController()
        
        setGradientBackground()
        navigationItem.title = "Infos"
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.pinkMercu()]
        
        setButtons(button: button)
        setButtons(button: button2)
        setButtons(button: button3)

        super.barButtonItemDelegate = self
    }
    
    
    func setButtons(button: UIButton )
    {
        
        button.backgroundColor = UIColor.white
        button.layer.cornerRadius = 30
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.pinkMercu()?.cgColor
        
    }
    

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //[self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"nav_pink"] forBarMetrics:UIBarMetricsDefault];
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setMenuButtonForNavigationController()

    }
   
    

    @IBAction func tutosTapped(_ sender: Any) {
        
        performSegue(withIdentifier: "showTutoList", sender: nil)
        
        
    }
    
    
    
    
    @IBAction func faqTapped(_ sender: Any) {
        
        
        
    }
    
    
    
}
