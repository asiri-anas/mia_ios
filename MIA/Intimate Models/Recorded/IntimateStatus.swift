//
//  IntimateStatus.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 02/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import CoreData

/// The user's status.
class IntimateStatus: NSManagedObject {
    /// The localized key of the name of the user's status.
    var name = ""
    /// The unique identifier of the IntimateStatus object.
    var statusID: NSNumber?
    class func create(withKey key: String?) -> IntimateStatus? {
        var status = NSEntityDescription.insertNewObject(forEntityName: "IntimateStatus", into: (CDManager.instance!.managedObjectContext)!) as! IntimateStatus
        status.name = key!
        return status
    }
}
