//
//  IntimateState.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 02/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import CoreData


/// This model is the representation state of the current state of the user's progression.
public class IntimateState: NSManagedObject {
    /// The current session reached in the current program.
    @NSManaged var currentSession: NSNumber?
    @NSManaged var maxSession: NSNumber?
    /// The date of the start of the program.
    
    @NSManaged var programStart: Date?
    @NSManaged public var bodyHeight: String
    @NSManaged public var bodyWeight: String
    @NSManaged public var deliveryWay: String?
    @NSManaged public var educatedLately: Bool
    @NSManaged public var hasChild: Bool
    @NSManaged public var hasNeither: Bool
    @NSManaged public var hasDelivered: Bool
    @NSManaged public var hasDoneDiscovery: Bool
    @NSManaged public var hasLackSensation: Bool
    @NSManaged public var hasLeak: Bool
    @NSManaged public var hasMenopause: Bool
    @NSManaged public var isPregnant: Bool
    @NSManaged public var lastDelivery: Date?
    @NSManaged public var leakFrequency: String?
    @NSManaged public var leakQuantity: String?
    @NSManaged public var sensationPrecision: String?
    @NSManaged public var uncomfortSensationLevel: String?
    @NSManaged public var uncomfortLeakLevel: String?
    
    /// The IntimateProgramType object representing the type of the current program.
    @NSManaged var intimateProgramType: IntimateProgramType?
    /// The current intimate status of the user.
    @NSManaged var intimateStatus: IntimateStatus?
    /// The relation to the IntimateCoach.
    @NSManaged var intimateCoach: IntimateCoach?
    /// The selected IntimateComfort entity.
    @NSManaged var intimateComfort: IntimateComfort?
    /// The selected IntimateVaginalPain entity.
    @NSManaged var intimateVaginalPain: IntimateVaginalPain?
    /// Will instantiate and insert new IntiamteState object in Core Data. This should be done only once for this model.
    

    @nonobjc public class func fetchRequest() -> NSFetchRequest<IntimateState> {
        return NSFetchRequest<IntimateState>(entityName: "IntimateState")
    }
    
    
    class func create() -> IntimateState? {
        
        var state = NSEntityDescription.insertNewObject(forEntityName: "IntimateState", into: (CDManager.instance!.managedObjectContext)!) as? IntimateState
    return state
    }
}
