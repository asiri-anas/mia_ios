//
//  IntimateCoach.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 02/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import CoreData

/// The model corresponding to the intimate coach.
public class IntimateCoach: NSManagedObject {
    
    
    /// The ID of the intimate coach.
    @NSManaged var intimateCoachID : String?
    
    /// The date of creation.
    @NSManaged var createdAt: Date?
    
    /// The date of the last IntimateActivity done.
    @NSManaged var lastActivity: Date?
    
    /// The date of the last synchro with the server.
    @NSManaged var lastSync: Date?
    
    /// The User owning the IntimateCoach.
    @NSManaged var user: User?
    
    //The specialiste of the User
    @NSManaged var intimateSpecialist: IntimateSpecialist?
    
    /// The IntimateStateObject that represents the current state of user's intimate program.
    @NSManaged var intimateState: IntimateState?
    
    /// The last IntimateGoals object recorded.
    @NSManaged var intimateGoal: IntimateGoal?
    
    @NSManaged var intimateSessions: NSSet?
    
    /// This method will insert a new IntimateCoach entity in core data.
    class func create() -> IntimateCoach? {
        let intimateCoach = NSEntityDescription.insertNewObject(forEntityName: "IntimateCoach", into: (CDManager.instance!.managedObjectContext)!) as! IntimateCoach
        return intimateCoach
    }
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<IntimateCoach> {
        return NSFetchRequest<IntimateCoach>(entityName: "IntimateCoach")
    }
    
    public class func intimateCoachFetchRequest() -> NSFetchRequest<IntimateCoach> {
        let fetchRequest:NSFetchRequest<IntimateCoach> = IntimateCoach.fetchRequest()
        fetchRequest.fetchLimit = 1
        
        let sortDescriptor = NSSortDescriptor(key: "createdAt", ascending: false)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        return fetchRequest
    }
    
    public class func retrieveLastCoach() -> IntimateCoach? {
        var intimate: [IntimateCoach] = []
        do {
            intimate = try CDManager.instance!.managedObjectContext!.fetch(IntimateCoach.intimateCoachFetchRequest())
            print("COACH", "fetch intimateCoach")
            
        } catch {
            print("Failed")
        }
        if(intimate.count > 0) {
            return intimate[0]
        }
        else {
            return nil
        }
    }
}
