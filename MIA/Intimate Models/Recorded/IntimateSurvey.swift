//
//  IntimateSurvey.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 17/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import CoreData


/// A key-value pair object that corresponds to a survey's answer.
class IntimateSurvey {
    /// The key of the "question"
    var key = ""
    /// The value of the answer.
    var value = ""
    /// The corresponding IntimateStatus object.
    var intimateStatus: IntimateStatus?
    /// Will instantiate and insert a new IntimateSurvey in Core Data.
    class func create() -> IntimateSurvey? {
        
        var survey = NSEntityDescription.insertNewObject(forEntityName: "IntimateSurvey", into: (CDManager.instance!.managedObjectContext)!) as! IntimateSurvey
        return survey
        
    }
    
    
func getDictionaryRepresentation() -> [AnyHashable: Any]? {
    return ["key": key, "value": value, "type": (key == "baby_date") ? "datetime" : "string"]
}
    
    
}
