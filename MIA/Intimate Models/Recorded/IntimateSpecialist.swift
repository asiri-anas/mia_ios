//
//  IntimateSpecialist.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 02/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import CoreData


public class IntimateSpecialist: NSManagedObject {
    
    /// The unique identifier of IntimateSpecialist object.
    @NSManaged public var specialistID: String?
    /// The kind of medecine of the specialist.
    @NSManaged public var type: String?
    /// The name of the specialist.
    @NSManaged public var name: String?
    /// The email address of the specialist.
    @NSManaged public var email: String?
    /// The phone number of the specialist.
    @NSManaged public var phone: String?
    /// The user attached to this specialist.
    @NSManaged public var user: User?
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<IntimateSpecialist> {
        return NSFetchRequest<IntimateSpecialist>(entityName: "IntimateSpecialist")
    }
    
    @nonobjc public func generateUniqueID() -> String? {
        let uuidRef = CFUUIDCreate(nil)
        let uuidStringRef = CFUUIDCreateString(nil, uuidRef)
        return uuidStringRef as! String
    }
    
}
