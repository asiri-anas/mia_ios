//
//  IntimateProgramType.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 02/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import CoreData
/// The model corresponding to the type of a program.
public class IntimateProgramType: NSManagedObject {
    /// The key to localize corresponding to the program's name.
    @NSManaged var label: String?
    /// The ID of the IntimateProgramType object.
    @NSManaged var typeID: NSNumber?
    /*!
     @brief Will insert a new IntimateProgramType entity in Core Data.
     @param name The key to localize.
     */
    class func create(withName name: String?) -> IntimateProgramType? {
        
        var program = NSEntityDescription.insertNewObject(forEntityName: "IntimateProgramType", into: (CDManager.instance!.managedObjectContext)!) as! IntimateProgramType
        switch name {
        case NSLocalizedString("prog_perineal_discovery",  comment:""):
            program.label = "prog_perineal_discovery"
        case NSLocalizedString("prog_perineal_pain",  comment:""):
            program.label = "prog_perineal_pain"
        case NSLocalizedString("prog_urinary_discomfort",  comment:""):
            program.label = "prog_urinary_discomfort"
        case NSLocalizedString("prog_lack_of_sexual_pleasure",  comment:""):
            program.label = "prog_lack_of_sexual_pleasure"
        case NSLocalizedString("prog_post_childbirth",  comment:""):
            program.label = "prog_post_childbirth"
        default:
            program.label = "prog_perineal_pain"
        }
        return program
    }
    
}
