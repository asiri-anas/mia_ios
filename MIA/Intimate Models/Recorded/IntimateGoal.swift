//
//  IntimateGoal.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 02/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import CoreData

public class IntimateGoal: NSManagedObject {

    /// The number of sessions to reach in a week.
    @NSManaged var weeklySessionsCount: NSNumber?
    @NSManaged var goalPerWeek: NSNumber?
    @NSManaged var createdAt: Date?
    @NSManaged var intimateCoach: IntimateCoach?
    
    class func create() -> IntimateGoal? {
 
        var goal = NSEntityDescription.insertNewObject(forEntityName: "IntimateGoal", into: (CDManager.instance!.managedObjectContext)!) as! IntimateGoal
        return goal
        
    }
    
}
