//
//  IntimateActivityType.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 03/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import CoreData

/// The object corresponding to a type of an IntimateActivity object.
@objc public class IntimateActivityType: NSManagedObject {
    /// The ID of this IntimateActivityType object.
    @NSManaged var activityTypeID: String?
    /// A number corresponding to the type of an exercise, defined in activityName enum in Constants.h
    @NSManaged var type: NSNumber?
    /*!
     @brief The level of this IntimateActivityType in a program context.
     @discussion When a level is passed in a program context, the freeLevelReached will be incremented too.
     */
    @NSManaged var programLevelReached: NSNumber?
    /*!
     @brief The level of this IntimateActivityType in free exercise context.
     @discussion In opposition of the programLevelReached property, the freeLevelReached will not increment the programLevelReached.
     */
    @NSManaged var freeLevelReached: NSNumber?
    /// The localization key of the activity type's name.
    @NSManaged var label: String?
    /// The color string in hexadecimal format.
    @NSManaged var colorHex: String?
    /*!
     @brief Will instantiate and insert a new IntimateActivityType object in Core Data.
     @param label The localization key corresponding to the name of an exercise type.
     */
    class func create(withLabel label: String?) -> IntimateActivityType? {
        
        var activityType = NSEntityDescription.insertNewObject(forEntityName: "IntimateActivityType", into: (CDManager.instance!.managedObjectContext)!) as! IntimateActivityType
        activityType.activityTypeID = (CDManager.instance!.generateUniqueID())!
        activityType.programLevelReached = 1
        activityType.freeLevelReached = 1
        activityType.label = label!
        return activityType
        
    }
}
