//
//  IntimateVaginalPain.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 02/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import CoreData

/// Represents the current vaginal pain of the user.
class IntimateVaginalPain: NSManagedObject {
    /// The IntimateVaginalPain object ID.
    var painID: NSNumber?
    /// The localization key corresponding to the name of the vaginal pain.
    var label = ""
    /*!
     @brief Will instantiate and insert new IntimateVaginalPain entity in Core Data.
     @param label The localization key of the name of vaginal pain.
     */
    class func create(withLabel label: String?) -> IntimateVaginalPain? {
        
        var pain = NSEntityDescription.insertNewObject(forEntityName: "IntimateVaginalPain", into: (CDManager.instance!.managedObjectContext)!) as! IntimateVaginalPain
        pain.label = label!
    return pain
        
    }
}
