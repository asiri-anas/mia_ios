//
//  IntimateSession.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 03/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import CoreData

/// This object corresponds to a session of a program.
class IntimateSession: NSManagedObject, IntimateDataSourceProtocol, SynchroProtocol {
    /// The ID of the IntimateSession object.
    @NSManaged var sessionID: NSString?
    /// The creation date of the IntimateSession object.
    @NSManaged var createdAt: Date?
    /// The date of the end of the IntimateSession object.
    @NSManaged var finishedAt: Date?
    /// The score reached for this session.
    @NSManaged var score: NSNumber?
    /// The max score to reach for this session.
    @NSManaged var maxScore: NSNumber?
    /// The position of the session in her program.
    @NSManaged var level: NSNumber?
    /// The count of sessions in the program.
    @NSManaged var maxLevel: NSNumber?
    /// The duration of the session, in seconds.
    @NSManaged var duration: NSNumber?
    /// The average value of all hearts won from the intimateActivities.
    @NSManaged var hearts: NSNumber?
    /// An array of IntimateActivity objects, corresponds to the activities to make in this session.
    @NSManaged var intimateActivities: NSArray?
    /// The program type of this session.
    @NSManaged var intimateProgramType: IntimateProgramType?
    /// The IntimateStatus which this session has been made with.
    @NSManaged var intimateStatus: IntimateStatus?
    /// A relation to the IntimateCoach object.
    @NSManaged var intimateCoach: IntimateCoach?
    /// The IntimateVaginalPain selected by the user, in case of intimateProgramType.label != prog_perineal_pain.
    @NSManaged var intimateComfort: IntimateComfort?
    /// The IntimateVaginalPain selected by the user, in case of intimateProgramType.label == prog_perineal_pain.
    @NSManaged var intimateVaginalPain: IntimateVaginalPain?
    
    /// Will instantiate and insert a new IntimateSession object in Core Data.
    class func create() -> IntimateSession? {
        var session = NSEntityDescription.insertNewObject(forEntityName: "IntimateSession", into: (CDManager.instance!.managedObjectContext)!) as! IntimateSession
        session.sessionID = (CDManager.instance!.generateUniqueID()) as! NSString
        return session
        
    }
    
    
    func getCreationDate() -> Date? {
        return createdAt
    }
    
    func getDictionaryRepresentation() -> [AnyHashable: Any]? {
        var dic = [AnyHashable: Any](minimumCapacity: 11)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dic["created_at"] = dateFormatter.string(from: createdAt!)
        dic["score"] = score
        dic["max_score"] = maxScore
        dic["level"] = level
        dic["max_level"] = maxLevel
        dic["duration"] = duration
        dic["hearts"] = hearts
        dic["program_type"] = intimateProgramType?.label
        if (intimateProgramType?.label == "prog_perineal_pain") {
            dic["program_reason"] = intimateVaginalPain?.label
        } else {
            dic["program_reason"] = intimateComfort?.label
        }
        dic["status"] = intimateStatus?.name
        var actDics = [AnyHashable]() /* TODO: .reserveCapacity(intimateActivities.count) */
        intimateActivities?.enumerateObjects({(_ activity: IntimateActivity?, _ idx: Int, _ stop: UnsafeMutablePointer<ObjCBool>) -> Void in
            
            if let aRepresentation = activity?.getDictionaryRepresentation() {
                actDics.append(aRepresentation as! AnyHashable)
            }
            } as! (Any, Int, UnsafeMutablePointer<ObjCBool>) -> Void)
        dic["activities"] = actDics
        return dic
    }
    
    
}
