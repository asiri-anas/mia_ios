//
//  CalibrationHistory.swift
//  mia
//
//  Created by ASIRI Anas on 17/09/2018.
//  Copyright © 2018 V-Labs. All rights reserved.
//

import Foundation
import CoreData

/// Represents the result of a calibration session.
class CalibrationHistory: NSManagedObject, GraphValuesExtractorProtocol {

    /// The value returned by the AlgoIntime, to be display.
    @NSManaged var value: NSNumber?
    /// The maximum value recorded during calibration. This value is a transient property (not recorded).
    @NSManaged var maxValue: NSNumber?
    /// The minimum value recorded during calibration. This value is a transient property (not recorded).
     @NSManaged var minValue: NSNumber?
    /// The object creation date.
     @NSManaged var createdAt: Date?

    convenience init() {
        self.init()
        let calibration = NSEntityDescription.insertNewObject(forEntityName: "CalibrationHistory", into: (CDManager.instance!.managedObjectContext)!) as! CalibrationHistory
    }
    func getDate() -> Date? {
        return createdAt
    }
    func getValueFor(_ type: NSNumber) -> Any? {
        return value
    }
    func getIntimateCalibrationStructure() -> IntimateCalibrationStructure? {
        return IntimateCalibrationStructure(minValue: minValue, maxValue: maxValue, calibrationValue: value)
    }
}
