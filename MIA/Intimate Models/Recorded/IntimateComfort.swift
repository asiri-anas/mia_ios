//
//  IntimateComfort.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 02/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import CoreData
/// Represents the vaginal comfort of the user.
class IntimateComfort: NSManagedObject {
    /// The ID of the IntimateComfort object.
    var comfortID: NSNumber?
    /// The localization key of the name of the comfort.
    var label = ""
    /*!
     @brief Will instantiate and insert new IntimateComfort entity in Core Data.
     @param label The localization key of the name of comfort.
     */
    class func create(withLabel label: String?) -> IntimateComfort? {
        
        var comfort = NSEntityDescription.insertNewObject(forEntityName: "IntimateComfort", into: (CDManager.instance!.managedObjectContext)!) as? IntimateComfort
        comfort?.label = label!
    return comfort
        
    }
}
