//
//  IntimateActivity.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 03/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import CoreData

/// Model that corresponds to an exercise.
public class IntimateActivity: NSManagedObject, IntimateDataSourceProtocol, SynchroProtocol {
    /// The ID of IntimateActivity object.
    @NSManaged var activityID: String?
    /// A serialize array of ActivityStep objects.
    @NSManaged var datas: Data?
    // [activityStep]
    /// The level this IntimateActivity made with.
    @NSManaged var level: NSNumber?
    /// The number of hearts gotten from AlgoIntime.
    @NSManaged var hearts: NSNumber?
    /// The score reached.
    @NSManaged var score: NSNumber?
    /// The score to reach.
    @NSManaged var maxScore: NSNumber?
    /// The creation date.
    @NSManaged var createdAt: Date?
    /// The IntimateActivityType object corresponding to the type of this exercise.
    @NSManaged var intimateActivityType: IntimateActivityType?
    /// The IntimateStatus object.
    @NSManaged var intimateStatus: IntimateStatus?
    /// The IntimateSession object where this activity has been done.
    @NSManaged var intimateSession: IntimateSession?
    
    /// Will instantiate and insert a new IntimateActivity in Core Data.
    class func create() -> IntimateActivity? {
        var activity = NSEntityDescription.insertNewObject(forEntityName: "IntimateActivity", into: (CDManager.instance!.managedObjectContext)!) as! IntimateActivity
        activity.activityID = (CDManager.instance!.generateUniqueID())!
        return activity
    }
    
    func getCreationDate() -> Date? {
        return createdAt
    }
    
    func getDictionaryRepresentation() -> [AnyHashable: Any]? {
        var dic = [AnyHashable: Any](minimumCapacity: 8)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dic["created_at"] = dateFormatter.string(from: createdAt!)
        dic["level"] = level
        dic["hearts"] = hearts
        dic["score"] = score
        dic["max_score"] = maxScore
        dic["status"] = intimateStatus?.name
        dic["label"] = intimateActivityType?.label
        var datasStr = String()
        let datas = NSKeyedUnarchiver.unarchiveObject(with: self.datas!) as? [Any]
        (datas as NSArray?)?.enumerateObjects({(_ step: IntimateActivityStep?, _ idx: Int, _ stop: UnsafeMutablePointer<ObjCBool>) -> Void in
            if let aTime = step?.time, let aPressure = step?.pressure, let aTarget = step?.target {
                datasStr += "\(aTime),\(aPressure),\(aTarget);"
            }
            } as! (Any, Int, UnsafeMutablePointer<ObjCBool>) -> Void)
        dic["datas"] = datasStr
        return dic
    }
    
    
    
}
