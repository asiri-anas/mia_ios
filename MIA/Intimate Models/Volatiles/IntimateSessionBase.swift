//
//  IntimateSessionBase.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 04/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation

/// The base of IntimateSession model, created from IntimatePrograms plist file.
public class IntimateSessionBase: NSObject {
    /// The total duration of the session.
     var totalDuration: NSNumber?
    /// The score to reach for this session.
     var maxScore: NSNumber?
    /// An array of IntimateActivityBase objects.
     var activities: [IntimateActivityBase]?
    /*!
     @brief Will init the IntimateSessionBase object.
     @param sessionDic The dictionary representing a session from the IntimatePrograms plist file.
     @see IntimateProgramParser.h
     */
    init(dictionary sessionDic: [String:Any]?) {
        
    super.init()
        maxScore = sessionDic?["max_score"] as! NSNumber
        totalDuration = sessionDic?["duration"] as! NSNumber
        activities = [IntimateActivityBase]()
        
        var act = sessionDic!["activities"] as? [[String:Any]]
        for item in act! {
            var activity = IntimateActivityBase(activityDic: item)
            activities?.append(activity)
        }
        
    }
}
