//
//  IntimateExoViewModel.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 03/07/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit


/// This object is a view model that contains all needed characteristics to set a view based on its type.
public class IntimateExoViewModel: NSObject {
    /// The hexadecimal string color of the exercise.
    @NSManaged var exoColorString: String?
    /// The localized string label of the exercise.
    @NSManaged var exoLabel: String?
    /// The type of exercise defined in Constant.h
    @NSManaged var exoType: NSNumber?
    /// The picto of the exercise.
    @NSManaged var picto: UIImage?
    /// The image for buttons backgrounds.
    @NSManaged var squareImage: UIImage?
    /// The IntimateActivityType object of the exercise.
    @NSManaged var activityType: IntimateActivityType?

    /// The intilizer based on an IntimateActivityType object.
    init(activityType: IntimateActivityType?) {
        
        super.init()
        
        self.activityType = activityType
        exoLabel = NSLocalizedString((activityType?.label)!, comment: "")
        switch activityType?.type {
        case 0:
            exoColorString = "#1ebdbc"
            picto = UIImage(named: "exo_intime_renforcement_profond")
            squareImage = UIImage(named: "exo_intime_renforcement_profond_button")
            exoType = 0
            
        case 1:
            exoColorString = "#f48787"
            picto = UIImage(named: "exo_intime_verrouillage")
            squareImage = UIImage(named: "exo_intime_verrouillage_button")
            exoType = 1
            
        case 2:
            exoColorString = "#8183b7"
            picto = UIImage(named: "exo_intime_relachement")
            squareImage = UIImage(named: "exo_intime_relachement")
            exoType = 2
            
        case 3:
            exoColorString = "#c26aa3"
            picto = UIImage(named: "exo_intime_renforcement_superficiel")
            squareImage = UIImage(named: "exo_intime_renforcement_superficiel_button")
            exoType = 3
        default:
            print("activity type is bad in intimateexoviewmodel")
        }
        
    }
    

    /// The initializer based on activityName enum value.
    init(activityName: Int?) {
        
        super.init()
        
        exoType = activityName as! NSNumber
        
        switch activityName {
        case 0:
            exoColorString = "#1ebdbc"
            picto = UIImage(named: "exo_intime_renforcement_profond")
            squareImage = UIImage(named: "exo_intime_renforcement_profond_button")
            exoType = 0
            exoLabel = "Endurance"
            
        case 1:
            exoColorString = "#f48787"
            picto = UIImage(named: "exo_intime_verrouillage")
            squareImage = UIImage(named: "exo_intime_verrouillage_button")
            exoType = 1
            exoLabel = "Resistance"
        case 2:
            exoColorString = "#8183b7"
            picto = UIImage(named: "exo_intime_relachement")
            squareImage = UIImage(named: "exo_intime_relachement_button")
            exoType = 2
            exoLabel = NSLocalizedString("slackening", comment: "")
        case 3:
            exoColorString = "#c26aa3"
            picto = UIImage(named: "exo_intime_renforcement_superficiel")
            squareImage = UIImage(named: "exo_intime_renforcement_superficiel_button")
            exoType = 3
            exoLabel = NSLocalizedString("latching", comment: "")
        case 4:
            picto = UIImage(named: "ma_progression_pink")
            exoLabel = NSLocalizedString("breath_exercise", comment: "")
            exoType = 4
        case 5:
            picto = UIImage(named: "ma_progression_pink")
            exoLabel = NSLocalizedString("exo_mesurer_tonicite_perineale", comment: "")
            exoType = 5
        default:
            print("default")
        }
    }
    
    @objc func getActivityType() -> IntimateActivityType? {
        return activityType
    }
    
}
