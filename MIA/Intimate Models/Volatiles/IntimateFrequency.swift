//
//  IntimateFrequency.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 04/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation

/// The object that indicates the advised exercises frequency.
class IntimateFrequency: NSObject {
    /// The duration of each session to make.
    var time: NSNumber?
    /// The number of times that is advised by week.
    var repeatByWeek: NSNumber?
    /*!
     @brief The initializer of IntimateFrquency object.
     @param freqDic A dictionary representation from the IntimatePrograms plist file.
     @see IntimateProgramParser.h
     */
    init(dictionary freqDic: NSDictionary?) {
        
        super.init()
        
        time = freqDic?["time"] as! NSNumber
        repeatByWeek = freqDic?["repeat_by_week"] as! NSNumber
        
        print("LOG")
        print("intimateFrequency.time")
        print(time)
        
        
    }
}
