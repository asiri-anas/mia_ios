//
//  SurveyValues.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 17/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation

//  The converted code is limited to 4 KB.
//  Upgrade your plan to remove this limitation.
//
/// A collection of IntimateSurvey objects.
class SurveyValues: NSObject {
    /// An array of IntimateSurvey objects.
    var surveyValues: NSArray = []
    /*!
     @brief A getter for the wanted IntimateSurvey object.
     @param key the key of the wanted IntimateSurvey.
     */
    func getSurveyForKey(_ key: String?) -> IntimateSurvey? {
        
        var selected: IntimateSurvey?
        /*surveyValues.enumerateObjects({(_ survey: IntimateSurvey?, _ idx: Int, _ stop: UnsafeMutablePointer<ObjCBool>?) -> Void in
            if (survey?.key == key) {
                selected = survey
                stop = true
            }
        })*/
        return selected
        
    }
}
