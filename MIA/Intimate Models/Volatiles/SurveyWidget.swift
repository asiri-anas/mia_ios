//
//  SurveyWidget.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 14/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation

/// This object corresponds to a dynamic field in the intimate survey.
class SurveyWidget: NSObject {
    /// The name that identifies the field.
    var name = ""
    /// The value that identifies the field.
    var value = ""
    /// Optional image name for text field
    var picto = ""
    /// The type of selection : 0 = date ; 1 = string selection
    var type: NSNumber?
    /// The selectable values if type = 1. These values correspond are localizable keys.
    var values = [Any]()
    /*!
     @brief The initializer.
     @param widgetDic A dictionary representation of a widget.
     @param surveyValues The SurveyValues object to know if this widget should be filled.
     */
    init(dictionary widgetDic: [AnyHashable: Any]?, andSurveyValues surveyValues: SurveyValues?) {
  
        
let SURVEY_WIDGET_TYPE_DATE = 0
        super.init()
        name = widgetDic!["name"] as! String
        type = widgetDic?["type"] as! NSNumber
        picto = widgetDic!["picto"] as! String
        value = (surveyValues?.getSurveyForKey(name)?.value)!
        
    }
}
