//
//  IntimateProgram.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 04/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation

class IntimateProgram: NSObject {
    var name: NSString?
    var sessions: [IntimateSessionBase]?
    var maxLevel: NSNumber?
    var programType: IntimateProgramType?
    var frequency: IntimateFrequency?
    
    // We init the program containing the sessions and activities
    init(dictionary progDic: NSDictionary?) {
        super.init()
        //maxLevel and name
        maxLevel = progDic!["max_level"] as? NSNumber
        name = progDic!["name"] as? NSString
        
        //Loop to get all sessions pattern
        sessions = [IntimateSessionBase]()
        let sessDic = progDic!["sessions"] as? [[String:Any]]
        for item in sessDic! {
            var session = IntimateSessionBase(dictionary: item)
            sessions?.append(session)
        }
        
        var myDic: NSDictionary?
        myDic = progDic!["frequency"] as? NSDictionary
        frequency = IntimateFrequency(dictionary: myDic)

    }
    
    
    
}
