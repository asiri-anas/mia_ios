//
//  File.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 03/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation


/// This object is the representation of a 250ms step of an exercise.
class IntimateActivityStep: NSObject, NSCoding {
    
    /// The instant time.
    @NSManaged var time: NSNumber?
    /// The instant target to reach at the instant time.
    @NSManaged var target: NSNumber?
    /// The pressure level at the instant time.
    @NSManaged var pressure: NSNumber?
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(pressure, forKey: "pressure")
        aCoder.encode(time, forKey: "time")
        aCoder.encode(target, forKey: "target")
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init()
        pressure = aDecoder.decodeObject(forKey: "pressure") as! NSNumber
        time = aDecoder.decodeObject(forKey: "time") as! NSNumber
        target = aDecoder.decodeObject(forKey: "target") as! NSNumber
    }
    
    
    /*!
     @brief The initializer of IntimateActivityStep object.
     @param stepDic A dictionary representing the IntimateActivityStep, got from cocos view.
     */
    
    func initWithDictionary(_stepDic: [AnyHashable: Any]?) -> IntimateActivityStep? {
        pressure = _stepDic?["pressure"] as! NSNumber
        time = _stepDic?["time"] as! NSNumber
        target = _stepDic?["target"] as! NSNumber
        return self
        
    }
    
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encode(pressure, forKey: "pressure")
        aCoder.encode(time, forKey: "time")
        aCoder.encode(target, forKey: "target")
    }
    
    
    func initWithCoder(aDecoder: NSCoder) -> IntimateActivityStep {
        pressure = aDecoder.decodeObject(forKey: "pressure") as! NSNumber
        time = aDecoder.decodeObject(forKey: "time") as! NSNumber
        target = aDecoder.decodeObject(forKey: "target") as! NSNumber
        return self
    }
    
}
