//
//  IntimateActivityResult.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 03/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation

/// A value object corresponding to the result of an exercise from AlgoIntime.
class IntimateActivityResult: NSObject {
    /// The score made by the user.
    @NSManaged var score: NSNumber?
    /// The number of hearts.
    @NSManaged var hearts: NSNumber?
    /// The serialized array of IntimateActivitySteps objects.
    @NSManaged var activitySteps: Data?
    // [IntimateActivityStep]
    /// A boolean that indicates if the user has reached the next level.
    @NSManaged var isLevelUp: NSNumber?
    @NSManaged var maxScore: NSNumber?


    /// The initializer.
    override init() {
    super.init()
    }
    
}
