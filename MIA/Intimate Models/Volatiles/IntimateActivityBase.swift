//
//  IntimateActivityBase.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 04/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation

/// The base of IntimateActivity model, created from IntimatePrograms plist file.
class IntimateActivityBase: NSObject {
    /*!
     @brief The type of the activity
     @see activityName in Constants.h
     */
    @NSManaged var type: NSNumber?
    /// The duration of the exercise, in seconds.
    @NSManaged var duration: NSNumber?
    /// The score to reach for this exercise.
    @NSManaged var maxScore: NSNumber?
    /// the localization key of the activity name.
    @NSManaged var key: NSString?
    
    /*!
     @brief Will init the IntimateActivityBase object.
     @param activityDic A dictionary that corresponds to an activity from the IntimatePrograms plist file.
     @see IntimateProgramParser.h
     */
    init(activityDic: [String:Any]?) {
        
        super.init()
        duration = activityDic?["duration"] as? NSNumber
        type = activityDic?["type"] as? NSNumber
        maxScore = activityDic?["score"] as? NSNumber
        
        switch type {
        case 0:
            key = "deep_strengthening_flat"
        case 1:
            key = "latching_with_text"
        case 2:
            key = "superficial_with_text"
        case 3:
            key = "superficial_strengthening"
        case 4:
            key = "latching"
        case 5:
            key = "slackening"
        case 6:
            key = "latching_strengthening"
        default:
            key = "null"
        }
        
    }
}
