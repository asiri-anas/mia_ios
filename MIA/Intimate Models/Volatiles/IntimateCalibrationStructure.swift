//
//  IntimateCalibrationStructure.m
//  mymercurochrome
// IntimateCalibrationStructure is a value object representing the AlgoIntime::calibration struct.
//  Created by Anas Asiri.
//  Copyright © 2016 V-Labs. All rights reserved.
//
class IntimateCalibrationStructure: NSObject {
    
    /// The minimum value recorded during the calibration.
    @NSManaged var minValue: NSNumber?
    /// The maximum value recorded during the calibration.
    @NSManaged var maxValue: NSNumber?
    /// The value to record.
    @NSManaged var calibrationValue: NSNumber?

    @objc init(minValue: NSNumber?, maxValue: NSNumber?, calibrationValue: NSNumber?) {
        super.init()
        self.minValue = minValue
        self.maxValue = maxValue
        self.calibrationValue = calibrationValue
    
    }
}
