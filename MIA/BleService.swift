//
//  BleService.swift
//  mia
//
//  Created by Valentin Ferriere on 13/09/2017.
//  Copyright © 2017 V-Labs. All rights reserved.
//

import Foundation
import RxBluetoothKit
import RxSwift
import CoreBluetooth

@objc class BleService: NSObject, BleServiceInput
{
    // MARK: - Private variables -
    private let disposeBag = DisposeBag()
    private let manager = CentralManager(queue: .main)
    private var scanningDisposable: Disposable?
    private var scannedPeripheral: ScannedPeripheral!
    private var autoConnect = false
    private var scanning = false
    private var connected = false
    private var ready = false
    private var batteryCharacteristic: Characteristic!
    private var pressureCharacteristic: Characteristic!
    private var pressureDisposable: Disposable?
    // MARK: - Public variables -
    var bleOutput: BleServiceOutput!
    
    // MARK: - Singleton -
    override private init() {}
    @objc static let instance = BleService()
    
    @objc func setBleOutput (bleOutput: BleServiceOutput){
        self.bleOutput = bleOutput
    }
    
    // MARK: - public methods -
    // MARK: Scan
    func startScanning(names: [String]) {
        if(self.scanningDisposable != nil) { // safety check
            print("nooooo")
            return
        }
        
        let scanOpts = [CBCentralManagerScanOptionAllowDuplicatesKey: NSNumber(value: true)];
        
        self.scanningDisposable = manager.observeState()
            .filter { $0 == .poweredOn }
            .timeout(3.0, scheduler: MainScheduler.instance)
            .take(1)
            .flatMap { _ in self.manager.scanForPeripherals(withServices: nil, options: scanOpts) }
            .do(onSubscribe: {
                self.scanning = true
                print("BLE", "Scan started")
                
                if(self.bleOutput != nil) {
                    if(self.bleOutput.responds(to: #selector(BleServiceOutput.onScanStarted))) {
                        self.bleOutput.onScanStarted()
                    }
                }
            })
            .subscribe(onNext: { device in
                let n = device.peripheral.name ?? "unknown"
                if(names.contains(n)) {
                    print(n)
                    self.scannedPeripheral = device
                    self.stopScanning() // prevent another connection
                    
                    if(self.autoConnect && !self.isConnected()) {
                        print("BLE", "try to connect")
                        self.connect()
                        self.monitorConnection(for: device.peripheral)
                    }
                }
            }, onError: { error in
                print(error)
            })
    }
    
    func stopScanning() {
        self.scanningDisposable?.dispose()
        self.scanningDisposable = nil
        self.scanning = false
        
        if(self.bleOutput != nil) {
            if(self.bleOutput.responds(to: #selector(BleServiceOutput.onScanStopped))) {
                self.bleOutput.onScanStopped()
            }
        }
        print("BLE", "Scan stopped")
    }
    
    // MARK: Connection
    func setAutoConnect(willAutoconnect: Bool) {
        self.autoConnect = willAutoconnect
    }
    
    func connect() {
        print("BLE", "Connection")
        
        self.manager.establishConnection(self.scannedPeripheral.peripheral)
            .subscribe(onNext: { device in
                self.connected = true
                self.monitorDisconnection(for: device)
                self.discoverServices(for: device)
                print("Connected")
            }, onError: { error in
                print(error)
            })
            .disposed(by: disposeBag)
    }
    
    func disconnect() {
        print("BLE", "Disconnection")
        
        /*self.manager.cancelPeripheralConnection(self.scannedPeripheral.peripheral)
            .subscribe(onNext: { device in
                print("Device disconnected")
                self.reset() // safety check
            }, onError: { error in
                print(error)
            })
            .addDisposableTo(disposeBag)*/
    }
    
    func isScanning() -> Bool {
        return self.scanning
    }
    
    func isConnected() -> Bool {
        return self.connected
    }
    
    func isReady() -> Bool {
        return self.ready
    }
    
    // MARK: Datas
    func getBattery() {
        /*batteryCharacteristic.readValue()
            .subscribe({ c in
                print("value battery : \(c.value?.hexadecimalString ?? "no value")")
                if(self.bleOutput != nil) {
                    if(self.bleOutput.responds(to: #selector(BleServiceOutput.onBatteryLevel(level:)))) {
                        self.bleOutput.onBatteryLevel(level: Int((c.value?.hexadecimalString)!, radix: 16)!)
                    }
                }
            })
            .addDisposableTo(self.disposeBag)*/
    }
    
    func startGettingPressure() {
        let interval = Observable<Int>.interval(BleConstant.acquisitionFrequency, scheduler: MainScheduler.instance)
        
        pressureDisposable = pressureCharacteristic.observeValueUpdateAndSetNotification()
            .sample(interval)
            .subscribe(onNext: { (c) in
                print("BLE", "pression : \(c.value?.hexadecimalString ?? "no value")")
                if(self.bleOutput != nil) {
                    if(self.bleOutput.responds(to: #selector(BleServiceOutput.onPressureDataReceived(data:)))) {
                        self.bleOutput.onPressureDataReceived(data: self.parsePressure(data: c.value!))
                    }
                }
            }, onError: { (error) in
                print(error)
            })
        pressureDisposable!.disposed(by: self.disposeBag)
    }
    
    func stopGettingPressure() {
        if(pressureCharacteristic == nil) {
            return
        }
        pressureDisposable?.dispose()
    }
    
    // MARK: - Private methods -
    // MARK: Connection
    private func monitorConnection(for peripheral: Peripheral) {
        self.manager.observeConnect(for: peripheral)
            .subscribe(onNext: { device in
                self.stopScanning()
            })
            .disposed(by: disposeBag)
    }
    
    private func monitorDisconnection(for peripheral: Peripheral) {
        self.manager.observeDisconnect(for: peripheral)
            .subscribe(onNext: { device in
                self.reset()
                
                if(self.bleOutput != nil) {
                    if(self.bleOutput.responds(to: #selector(BleServiceOutput.onDisconnected))) {
                        self.bleOutput.onDisconnected()
                    }
                }
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "device_status_disconnect"), object: nil)
                print("Disconnected")
            })
            .disposed(by: disposeBag)
    }
    
    // MARK: Services & Characteristics
    private func discoverServices(for peripheral: Peripheral) {
        peripheral.discoverServices(nil).asObservable()
            .subscribe(onNext: { services in
                for service in services {
                    self.discoverCharacteristics(for: service)
                }
            }, onCompleted: {
                print("service discovering completed")
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "device_status_connect"), object: nil)
            })
            .addDisposableTo(disposeBag)
    }
    
    private func discoverCharacteristics(for service: Service) {
        service.discoverCharacteristics(nil).asObservable()
            .subscribe(onNext: { characteristics in
                for characteristic in characteristics {
                    if(String(describing: characteristic.uuid) == "Battery Level") {
                        self.batteryCharacteristic = characteristic
                    }
                    
                    if(String(describing: characteristic.uuid) == "AA41") {
                        self.pressureCharacteristic = characteristic
                    }
                }
            }, onCompleted: {
                print("characteristics discovering completed")
                
                if(self.batteryCharacteristic != nil && self.pressureCharacteristic != nil) {
                    self.ready = true
                    
                    if(self.bleOutput != nil) {
                        if(self.bleOutput.responds(to: #selector(BleServiceOutput.onConnected))) {
                            self.bleOutput.onConnected()
                        }
                    }
                }
            })
            .addDisposableTo(self.disposeBag)
    }
    
    private func parsePressure(data: Data) -> Int {
        return data.withUnsafeBytes { (pointer: UnsafePointer<UInt8>) -> Int in
            print("BLE", "pointer values : \(pointer[0]) \(pointer[1]) \(pointer[2]) \(pointer[3])")
            
            let p1 = Data(bytes: [pointer[0], pointer[1]])
            let pressureSensor1 = UInt16(bigEndian: p1.withUnsafeBytes { $0.pointee })
            
            let p2 = Data(bytes: [pointer[2], pointer[3]])
            let pressureSensor2 = UInt16(bigEndian: p2.withUnsafeBytes { $0.pointee })
            
            print("BLE", "pressure datas : \(pressureSensor1) \(pressureSensor2)")
            
            return (Int(pressureSensor1)+Int(pressureSensor2))/2/3*8190/100000
        }
    }
    
    private func reset() {
        self.ready = false
        self.connected = false
        self.scannedPeripheral = nil
        self.batteryCharacteristic = nil
        self.pressureCharacteristic = nil
    }
}



