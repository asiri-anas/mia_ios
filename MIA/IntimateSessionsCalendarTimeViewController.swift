//
//  IntimateSessionsCalendarTimeViewController.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 20/07/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit


class IntimateSessionsCalendarTimeViewController: AbstractViewController, UIPickerViewDelegate, BarButtonItemDelegate, CloseModalDelegate {
    func didTapLeftBarButtonItem() {
        //
    }
    
    
    @IBOutlet var dateLabel: UILabel!
    
    @IBOutlet var datePicker: UIDatePicker!
    
    var selectedDate: Date?
    var futureSession: FutureIntimateSession?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setRightBarButtonItemWith(UIImage(named: "validate_white"))
        setLeftCloseButtonForModalNavigationController()
        navigationItem.title = NSLocalizedString("schedule_your_session", comment: "").uppercased()
        datePicker.date = selectedDate!
        dateLabel.text = DateHelper.serializeLongDateString(from: selectedDate)
        //[self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"nav_pink"] forBarMetrics:UIBarMetricsDefault];
        //if selectedDate.isToday {
            datePicker.minimumDate = Date()
        
        //}
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        super.barButtonItemDelegate = self
        super.closeDelegate = self
    }
    func didTapRightBarButtonItem() {
        buildDate()
        /*if futureSession != nil {
            IntimateSessionsScheduler.shared().updateFutureIntimateSession(futureSession, withDate: selectedDate)
        } else {
            IntimateSessionsScheduler.shared().createFutureIntimateSession(forDate: selectedDate)
        }
        IntimateSessionsScheduler.shared().scheduleNextSession()*/
        NotificationCenter.default.post(name: NSNotification.Name("update_calendar_selection"), object: nil, userInfo: ["date": selectedDate, "deselect": false])
        dismiss(animated: true)
    }
    
    
    
    func didCloseModalViewController() {
        NotificationCenter.default.post(name: NSNotification.Name("update_calendar_selection"), object: nil, userInfo: ["date": selectedDate, "deselect": futureSession == nil])
        dismiss(animated: true)
    }
    func buildDate() {
        let cal = Calendar.current
        let pickerComps: DateComponents? = cal.dateComponents([.hour, .minute], from: datePicker.date)
        var selectedComps: DateComponents? = cal.dateComponents([.day, .month, .year, .timeZone, .hour, .minute], from: selectedDate!)
        selectedComps?.hour = pickerComps?.hour ?? 0
        selectedComps?.minute = pickerComps?.minute ?? 0
        if let aComps = selectedComps {
            selectedDate = cal.date(from: aComps)
        }
    }
    /*
         #pragma mark - Navigation
     
         // In a storyboard-based application, you will often want to do a little preparation before navigation
         - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
         // Get the new view controller using [segue destinationViewController].
         // Pass the selected object to the new view controller.
         }
         */


}
