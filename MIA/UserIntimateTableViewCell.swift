//
//  UserIntimateTableViewCell.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 29/06/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit
protocol IntimateCellDelegate: NSObjectProtocol {
    func didTapSpecialistPhone()
    func didTapSpecialistEmail()
}
class UserIntimateTableViewCell: UITableViewCell, UITextViewDelegate {
    
@IBOutlet var heightSpecialistTextViewConstraint: NSLayoutConstraint!
    
    
    @IBOutlet var pictoIV: UIImageView!
    @IBOutlet var titleLabel: UILabel!

    
    @IBOutlet weak var detailLabel: UILabel!
    
    @IBOutlet var rightIV: UIImageView!
    weak var delegate: IntimateCellDelegate?
    @IBOutlet var specialistLabelCtn: UIView!
    @IBOutlet var specialistTextView: UITextView!
    
    
    
    func setContentWith(_ user: User?, andIntimateState intimateState: IntimateState?, at indexPath: IndexPath?) {
        
        //intimatespecialist now in intimateCoach not in user
        var specialistKey = (user?.intimateSpecialist?.type == nil) ? "my_specialist" : "my_\(user?.intimateSpecialist?.type)"
        
        
        switch (indexPath?.row) {
        case 0:
            pictoIV.image = UIImage(named: "airbee_picto")
            titleLabel.text = NSLocalizedString("my_motivation", comment: "")
            detailLabel.text = NSLocalizedString((intimateState?.intimateProgramType?.label)!, comment: "")
            break;
        case 1:
            pictoIV.image = UIImage(named: "stethoscope")
            titleLabel.text = NSLocalizedString(specialistKey, comment: "")
            //setSpecialist(user?.intimateSpecialist)
            fillSpecialist()
            break;
        default :
            print("defaut")
 
    }
    }
    
    
    override func awakeFromNib() {
    // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    // Configure the view for the selected state
    }
    
    
    func fillSpecialist()
    {
        do {
            var intimate: [IntimateCoach] = []
            intimate = try CDManager.instance!.managedObjectContext!.fetch(IntimateCoach.intimateCoachFetchRequest())
            print("profil", "fetch intimateCoach")
            for data in intimate {
                print("profil","user fetched")
                if  data.intimateSpecialist != nil {
                    print("profil","specialist fetched")
                    var specialist = data.intimateSpecialist
                    
                    specialistTextView.delegate = self
                    var paragraph = NSMutableParagraphStyle()
                    paragraph.alignment = .left
                    var finalStr = NSMutableAttributedString()
                    if specialist == nil {
                        specialistLabelCtn.isHidden = true
                    } else {
                        specialistLabelCtn.isHidden = false
                    }
                    var nameStr = NSMutableAttributedString(string: (specialist?.name)!)
                    
                    
                    var emailStr = NSMutableAttributedString(string: (specialist?.email)!)
                    
                    
                    var phoneStr = NSMutableAttributedString(string: (specialist?.phone)!)
                    
                    
                    finalStr.append(nameStr)
                    if specialist?.name != nil {
                        finalStr.append(NSAttributedString(string: "\n"))
                        finalStr.append(emailStr)
                        finalStr.append(NSAttributedString(string: "\n"))
                        finalStr.append(phoneStr)
                    }
                    finalStr.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraph, range: NSRange(location: 0, length: finalStr.length))
                    specialistTextView.attributedText = finalStr
                    print("content size height : %f", specialistTextView.sizeThatFits(specialistTextView.frame.size).height)
                    heightSpecialistTextViewConstraint.constant = specialistTextView.sizeThatFits(specialistTextView.frame.size).height
                    contentView.layoutIfNeeded()
                    
                    titleLabel.text = specialist?.type
                }
            }
        } catch {
            print("Failed")
        }
    }
    
    
    func setSpecialist(_ specialist: IntimateSpecialist?) {
    
            specialistTextView.delegate = self
            var paragraph = NSMutableParagraphStyle()
            paragraph.alignment = .left
            var finalStr = NSMutableAttributedString()
            if specialist == nil {
                specialistLabelCtn.isHidden = true
            } else {
                specialistLabelCtn.isHidden = false
            }
            var nameStr = NSMutableAttributedString(string: (!((specialist?.name) != nil)) != nil ? "" : specialist?.name ?? "")
            nameStr.addAttributes([NSAttributedStringKey.foregroundColor: UIColor.darkGrayTextColorMercu(), NSAttributedStringKey.font: UIFont(name: "SourceSansPro-Semibold", size: 15)], range: NSRange(location: 0, length: nameStr.length))
            var emailStr = NSMutableAttributedString(string: (!((specialist?.email) != nil)) != nil ? "" : specialist?.email ?? "")
            emailStr.addAttributes([NSAttributedStringKey.font: UIFont(name: "SourceSansPro-Regular", size: 15), NSAttributedStringKey.foregroundColor: UIColor.colorWithHexString(stringToConvert: "#40A0DE"), NSAttributedStringKey.link: "action://email", NSAttributedStringKey.underlineStyle: NSUnderlineStyle.styleSingle], range: NSRange(location: 0, length: emailStr.length))
            var phoneStr = NSMutableAttributedString(string: (!((specialist?.phone) != nil)) != nil ? "" : specialist?.phone ?? "")
            phoneStr.addAttributes([NSAttributedStringKey.font: UIFont(name: "SourceSansPro-Regular", size: 15), NSAttributedStringKey.foregroundColor: UIColor.colorWithHexString(stringToConvert: "#40A0DE"), NSAttributedStringKey.link: "action://phone", NSAttributedStringKey.underlineStyle: NSUnderlineStyle.styleSingle], range: NSRange(location: 0, length: phoneStr.length))
            finalStr.append(nameStr)
            if specialist?.name != nil {
                finalStr.append(NSAttributedString(string: "\n"))
                finalStr.append(emailStr)
                finalStr.append(NSAttributedString(string: "\n"))
                finalStr.append(phoneStr)
            }
            finalStr.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraph, range: NSRange(location: 0, length: finalStr.length))
            specialistTextView.attributedText = finalStr
            print("content size height : %f", specialistTextView.sizeThatFits(specialistTextView.frame.size).height)
            heightSpecialistTextViewConstraint.constant = specialistTextView.sizeThatFits(specialistTextView.frame.size).height
            contentView.layoutIfNeeded()
    }
    
    
func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
    if URL.scheme?.hasPrefix("action") ?? false {
        if URL.host?.hasPrefix("email") ?? false {
            /*if delegate?.responds(to: #selector(self.didTapSpecialistEmail)) {
                delegate?.didTapSpecialistEmail()
            }*/
        }
        if URL.host?.hasPrefix("phone") ?? false {
            /*if delegate?.responds(to: #selector(self.didTapSpecialistPhone)) {
                delegate?.didTapSpecialistPhone()
            }*/
        }
    }
    return false
}
    
    
    
    
}
