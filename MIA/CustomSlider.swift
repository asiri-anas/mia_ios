//
//  CustomSlider.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 15/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit

class CustomSlider: UISlider {
    
    override func trackRect(forBounds bounds: CGRect) -> CGRect {
        super.trackRect(forBounds: bounds)
        return CGRect(x: bounds.origin.x, y: bounds.origin.y, width: bounds.size.width, height: 20)
    }
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        let size = CGSize(width: 20, height: 20)
        UIGraphicsBeginImageContextWithOptions(size, false, UIScreen.main.scale)
        UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: size.width, height: 20), cornerRadius: 10).addClip()
        minimumTrackTintColor?.setFill()
        UIRectFill(CGRect(x: 0, y: 0, width: size.width, height: size.height))
        let img: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        setMinimumTrackImage(img?.resizableImage(withCapInsets: UIEdgeInsetsMake(0, 10, 0, 10), resizingMode: .stretch), for: .normal)
    }
    
}
