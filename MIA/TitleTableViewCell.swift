//
//  TitleTableViewCell.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 02/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit


class TitleTableViewCell: UITableViewCell {
    
    @IBOutlet var customLabel: UILabel!
    
override func awakeFromNib() {
    // Initialization code
}
    
func setContentWithTitle(_ title: String?) {
    customLabel.text = title
}
    
    func setSeparatorInset(_ separatorInset: UIEdgeInsets) {
    self.separatorInset = UIEdgeInsetsMake(0, contentView.frame.size.width, 0, 0)
}
    
override func setSelected(_ selected: Bool, animated: Bool) {
    let selectedBG = UIView()
    selectedBG.backgroundColor = UIColor.clear
    selectedBackgroundView = selectedBG
}
    
}
