//
//  ExosIntimateExoListViewController.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 02/07/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit

class ExosIntimateExoListViewController: AbstractViewController, UITableViewDataSource, UITableViewDelegate {
    
    
    var activityTypes: NSArray?
    var viewModels: NSMutableArray?
    
    @IBOutlet weak var selectExoTableVIew: UITableView!
    
    var exosListViewController: ExosListViewController?
    
    override func viewDidLoad() {
    super.viewDidLoad()

       
        activityTypes = (IntimateManager.instance.fetchIntimateActivityTypes(withFilters: nil))!
        activityTypes?.enumerateObjects({ activityType, idx, stop in
        print("activityType: ")
        print(activityType)
            viewModels?.add(IntimateExoViewModel(activityType: IntimateActivityType.create(withLabel: activityType as! String)))
        })
        viewModels?.add(IntimateExoViewModel(activityName: 4))
        //4 is equal to breath 5 is calibration
        
        
    
        
    setGradientBackground()
    
        let exoSB = UIStoryboard(name: "Exos", bundle: nil)
        exosListViewController = exoSB.instantiateViewController(withIdentifier: "ExosListViewController") as! ExosListViewController
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        setBackButtonForNavigationController()
        
    }
    
    
    
    @IBAction func selectExoBackButtonTapped(_ sender: Any) {
        
        //back
        
        
    }
    
    
    
    
    
    
    
    
func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    print(indexPath.row)
    //print(viewModels![indexPath.row])
    performSegue(withIdentifier: "verrouillage", sender: nil)//viewModels?[indexPath.row])
}
    
    
func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 4
}
    
    
    
func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
    return UIView()
}
    
    
    
func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    // Remove seperator inset
    if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
        cell.separatorInset = .zero
    }
    // Prevent the cell from inheriting the Table View's margin settings
    if cell.responds(to: #selector(setter: UITableViewCell.preservesSuperviewLayoutMargins)) {
        cell.preservesSuperviewLayoutMargins = false
    }
    // Explictly set your cell's layout margins
    if cell.responds(to: #selector(setter: UITableViewCell.layoutMargins)) {
        cell.layoutMargins = .zero
    }
}
    
    
func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cellIdentifier = "ExoCell"
    var cell = self.selectExoTableVIew.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? ExoTableViewCell
    if cell == nil {
        cell = ExoTableViewCell()
    }
    cell?.delegate = self
    cell?.tableView = tableView
    cell?.indexPath = indexPath
    print(IntimateExoViewModel(activityName: indexPath.row))
    //cell?.setContentForIntimateActivityType(viewModels?[indexPath.row])
    cell?.setContentForIntimateActivityType(IntimateExoViewModel(activityName: indexPath.row))
    if let aCell = cell {
        return aCell
    }
    return UITableViewCell()
}
    
// MARK: - Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "verrouillage") {//}&& (sender is IntimateExoViewModel) {
        let eilpvc = segue.destination as? ExosIntimateParamsViewController
        eilpvc?.exoViewModel = sender as? IntimateExoViewModel
        eilpvc?.isFreeExo = true
    }
}
    
    
    
    
    
    
    
    
}
