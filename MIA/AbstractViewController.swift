//
//  AbstractViewController.swift
//  mia
//
//  Created by Xavier Bohin on 16/04/2018.
//  Copyright © 2018 V-Labs. All rights reserved.
//

import Foundation
import iOS_Slide_Menu



protocol BarButtonItemDelegate: NSObjectProtocol {
    /// Triggered when a custom left bar button item is tapped.
    func didTapLeftBarButtonItem()
    /// Triggered when a custom right bar button item is tapped.
    func didTapRightBarButtonItem()
}
protocol PlayButtonDelegate: NSObjectProtocol {
    /// Triggered when the launcher button is tapped.
    func didTapPlayButton()
}
protocol CloseModalDelegate: NSObjectProtocol {
    /// Triggered when the navigation bar close button is tapped.
    func didCloseModalViewController()
}


@objc class AbstractViewController: UIViewController, SlideNavigationControllerDelegate {
    

weak var barButtonItemDelegate: BarButtonItemDelegate?
weak var playButtonDelegate: PlayButtonDelegate?
weak var closeDelegate: CloseModalDelegate?
    
    
let IS_IPHONE_4 = UIScreen.main.bounds.size.height == 480.0
let IS_IPHONE_5 = UIScreen.main.bounds.size.height == 568.0
let IS_IPHONE_6 = UIScreen.main.bounds.size.height == 667.0
let IS_IPHONE_6_PLUS = UIScreen.main.bounds.size.height == 736.0

    

/// A reference to a menu button.
@IBOutlet var closeButton: UIButton!
@IBOutlet weak var menuButton: UIButton!
    
/// A reference to a back button.
@IBOutlet var backButton: UIButton!
    
/// A reference to the exercise launcher button.
var playButton: UIButton?


    override func viewDidLoad() {
        super.viewDidLoad()
        setNeedsStatusBarAppearanceUpdate()
        navigationController?.navigationBar.tintColor = UIColor.pinkMercu()
        //menuButton.setImage(menuButton.imageView?.image?.withRenderingMode(.alwaysTemplate), for: .normal)
        //backButton.setImage(UIImage(named: "chevron_gauche_blanc")?.withRenderingMode(.alwaysTemplate), for: .normal)
        //backButton.setImage(#imageLiteral(resourceName: "chevron_gauche_blanc"), for: .normal)
        //navigationController?.navigationItem.backBarButtonItem?.title = ""
    }
    
    func setGradientBackground() {
        GradientHelper.setGradientOnLayer(view.layer, from: UIColor.white, to: UIColor.colorWithHexString(stringToConvert: "#F3DCD7"))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //NotificationCenter.default.addObserver(self, selector: #selector(self.showExos), name: "show_exos" as? NSNotification.Name, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: "show_exos" as? NSNotification.Name, object: nil)
    }
    
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool {
        return true
    }
    
    @IBAction func showMenu(_ sender: Any) {
        SlideNavigationController.sharedInstance().toggleLeftMenu()
    }
    
    @IBAction func close(_ sender: Any) {
        dismiss(animated: true) {() -> Void in }
    }
    
    @IBAction func back(_ sender: Any) {
        
            navigationController?.popViewController(animated: true)
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func setMercuNavigationBar() {
        if IS_IPHONE_6 {
            navigationController?.navigationBar.setBackgroundImage(UIImage(named: "nav_mercu_ip6_v3"), for: .default)
        } else {
            navigationController?.navigationBar.setBackgroundImage(UIImage(named: "nav_mercu_ip5_v3"), for: .default)
        }
    }
    
    func setRightCloseButtonForModalNavigationController() {
        let closeButton = UIButton(type: .custom)
        closeButton.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        closeButton.setImage(UIImage(named: "close")?.withRenderingMode(.alwaysTemplate), for: .normal)
        closeButton.addTarget(self, action: #selector(self.closeNavigationController), for: .touchUpInside)
        let closeItem = UIBarButtonItem(customView: closeButton)
        navigationItem.rightBarButtonItem = closeItem
    }
    
func setRightBarButtonItemWith(_ image: UIImage?) {
    let btn = UIButton(type: .custom)
    btn.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
    btn.setImage(image?.withRenderingMode(.alwaysTemplate), for: .normal)
    btn.addTarget(self, action: #selector(self.rightTapped), for: .touchUpInside)
    btn.contentEdgeInsets = UIEdgeInsetsMake(3, 6, 3, 0)
    navigationItem.rightBarButtonItem = UIBarButtonItem(customView: btn)
    }
    
    
    
    func setLeftCloseButtonForModalNavigationController() {
        let closeButton = UIButton(type: .custom)
        closeButton.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        closeButton.setImage(UIImage(named: "close")?.withRenderingMode(.alwaysTemplate), for: .normal)
        closeButton.addTarget(self, action: #selector(self.closeNavigationController), for: .touchUpInside)
        let closeItem = UIBarButtonItem(customView: closeButton)
        navigationItem.leftBarButtonItem = closeItem
    }
    
    @objc func closeNavigationController() {
        //if closeDelegate?.responds(to: #selector(self.didCloseModalViewController)) {
        if (true) {
            closeDelegate?.didCloseModalViewController()
        }
        dismiss(animated: true) {() -> Void in }
    }
    
    
    func setLeftBarButtonItemWithTitle(_ title: String?) {
        let btn = UIButton(type: .custom)
        btn.frame = CGRect(x: 0, y: 0, width: 80, height: 25)
        btn.setTitle(title, for: .normal)
        btn.setTitleColor(UIColor.white, for: .normal)
        if let aSize = UIFont(name: "SourceSansPro-Regular", size: 17) {
            btn.titleLabel?.font = aSize
        }
        btn.addTarget(self, action: #selector(self.leftTapped), for: .touchUpInside)
        btn.titleLabel?.textAlignment = .left
        btn.contentHorizontalAlignment = .left
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: btn)
    }
    
    func setRightBarButtonItemWithTitle(_ title: String?) {
        let btn = UIButton(type: .custom)
        btn.frame = CGRect(x: 0, y: 0, width: 80, height: 25)
        btn.setTitle(title, for: .normal)
        btn.setTitleColor(UIColor.white, for: .normal)
        if let aSize = UIFont(name: "SourceSansPro-Regular", size: 17) {
            btn.titleLabel?.font = aSize
        }
        btn.addTarget(self, action: #selector(self.rightTapped), for: .touchUpInside)
        btn.titleLabel?.textAlignment = .right
        btn.contentHorizontalAlignment = .right
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: btn)
    }
    
    
    
    
    func setMenuButtonForNavigationController() {
        let menuBtn = UIButton(type: .custom)
        menuBtn.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        menuBtn.setImage(UIImage(named: "menu_button")?.withRenderingMode(.alwaysTemplate), for: .normal)
        menuBtn.imageView?.tintColor = UIColor.pinkMercu()
        menuBtn.addTarget(self, action: Selector("showMenu:"), for: .touchUpInside)
        let `left` = UIBarButtonItem(customView: menuBtn)
        `left`.tag = Constants.MENU_BUTTON_TAG
        navigationItem.leftBarButtonItem = `left`
    }
    
    func hideMenuButton() {
        if navigationItem.leftBarButtonItem?.tag == Constants.MENU_BUTTON_TAG {
            navigationItem.leftBarButtonItem = nil
        }
    }
    
    func setBackButtonForNavigationController() {
        navigationItem.hidesBackButton = true
        navigationController?.navigationBar.barTintColor = UIColor.white
        let backButton = UIButton(type: .custom)
        backButton.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        backButton.setImage(UIImage(named: "chevron_gauche_blanc")?.withRenderingMode(.alwaysTemplate), for: .normal)
        backButton.imageView?.tintColor = UIColor.pinkMercu()
        backButton.addTarget(self, action: #selector(self.pop), for: .touchUpInside)
        let backItem = UIBarButtonItem(customView: backButton)
        navigationItem.leftBarButtonItem = backItem
    }
    
    
    @objc func pop() {
        //if (barButtonItemDelegate?.responds(to: #selector(self.leftTapped)))! {
        //    barButtonItemDelegate?.didTapLeftBarButtonItem()
        //} else {
            navigationController?.popViewController(animated: true)
        //}
    }
    
    @objc func rightTapped() {
        if (barButtonItemDelegate?.responds(to: #selector(self.rightTapped)))! {
            barButtonItemDelegate?.didTapRightBarButtonItem()
        }
    }
    
    @objc func leftTapped() {
        if (barButtonItemDelegate?.responds(to: #selector(self.leftTapped)))! {
            barButtonItemDelegate?.didTapLeftBarButtonItem()
        }
    }
    
    
    func setPlayButtonWith(_ delegate: PlayButtonDelegate?) {
        playButtonDelegate = delegate
        let button = UIButton(type: .custom)
        button.tag = Constants.PLAY_BUTTON_TAG
        button.frame = CGRect(x: view.frame.size.width / 2 - 25, y: view.frame.size.height - 70, width: 50, height: 50)
        button.setBackgroundImage(UIImage(named: "play_exo_button"), for: .normal)
        button.addTarget(self, action: #selector(self.playButtonTapped), for: .touchUpInside)
        /*view.subviews.enumerateObjects({(_ obj: Any?, _ idx: Int, _ stop: UnsafeMutablePointer<ObjCBool>?) -> Void in
            if (obj is UIButton) {
                let btn = obj as? UIButton
                if btn?.tag == PLAY_BUTTON_TAG {
                    btn?.removeFromSuperview()
                    stop = true
                }
            }
        })*/
        view.addSubview(button)
        playButton = button
    }
    
    func setCloseExoButton() {
        let button = UIButton(type: .custom)
        button.frame = CGRect(x: view.frame.size.width / 2 - 25, y: view.frame.size.height - 70, width: 50, height: 50)
        button.setBackgroundImage(UIImage(named: "close_red"), for: .normal)
        button.addTarget(self, action: Selector("dismissModalViewControllerAnimated:"), for: .touchUpInside)
        view.addSubview(button)
    }
    
   
    
    @objc func playButtonTapped() {
            showExos()
    }
    
    func enableSwipeMenu(_ enabled: Bool) {
        
        var app = UIApplication.shared.delegate as? NativeAppDelegate
        if (app?.window?.rootViewController is SlideNavigationController)
        {
            var nc = app?.window?.rootViewController as? SlideNavigationController
            nc?.enableSwipeGesture = enabled
        }
        
    }
    
    @objc func showExos() {
        let app = UIApplication.shared.delegate as? NativeAppDelegate
        let aController = app?.loadExosStoryBoard()
        aController?.popToRootViewController(animated: false)
        present(aController!, animated: true) {() -> Void in}
        
    }
    
    
    
}

