import Foundation
import UIKit
import RxBluetoothKit

class ExosIntimateParamsViewController: AbstractViewController, BleServiceOutput, UITableViewDataSource, UITableViewDelegate, IntimateLevelCellDelegate, UIAlertViewDelegate {

    
    var activityType: IntimateActivityType?
    var exoViewModel: IntimateExoViewModel?
    
    
    @IBOutlet weak var bluetoothIV: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    
    var isFreeExo = false
    
    private var batteryLevel: NSNumber?
    private var isAirbeeReady = false
    private var selectedLevel: NSNumber?
    private var sessionBase: IntimateSessionBase?
    private var activitiesBases = NSMutableArray()
    private var intimateState: IntimateState?
    private var mainColor: UIColor?
    
    override func viewDidLoad() {
        print("exoViewModel?.activityType: ", exoViewModel?.activityType)
        activityType = exoViewModel?.activityType
        bluetoothIV.image = bluetoothIV!.image!.withRenderingMode(.alwaysTemplate)
        if !isFreeExo {
            intimateState = (IntimateCoach.retrieveLastCoach())?.intimateState
            print(intimateState)
            sessionBase = IntimateProgramHandler.sharedManager()?.getIntimateSessionBase(at: Int((intimateState?.currentSession)!) , for: intimateState?.intimateProgramType)
            
            
            if let aSession = intimateState?.currentSession, let aSession1 = intimateState?.maxSession {
                titleLabel.text = "\(NSLocalizedString("session", comment: "")) \(aSession)/\(aSession1)"
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: true)
        print("Try ble")
        BleService.instance.bleOutput = self
        BleService.instance.setAutoConnect(willAutoconnect: true)
        BleService.instance.startScanning(names: BleConstant.names)
        print("scanning BLE")
        if !BleService.instance.isConnected() {
            print("Disconnected ble")
            bluetoothIV.alpha = 0.2
            tableView.reloadData()
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //shouldShowTutos()
        
    }
    
    func shouldShowTutos() {
        if !Registry.has(Constants.CONNECTION_TUTO_READ) {
            
            let tutosVC = storyboard?.instantiateViewController(withIdentifier: "ExosIntimateSensorTutosViewController") as? ExosIntimateSensorTutosViewController
            tutosVC?.sensorTutoType = .SENSOR_TUTO_TYPE_ALL
            let transition = CATransition()
            transition.duration = 0.3
            transition.type = kCATransitionFade
            view.window?.layer.add(transition, forKey: kCATransition)
            if let aVC = tutosVC {
                present(aVC, animated: false)
            }
        }
    }
    
    func onConnected() {
        UIView.animate(withDuration: 0.2, animations: {
            self.bluetoothIV.alpha = 1
        }) { finished in
            UIView.animate(withDuration: 0.2, animations: {
                self.bluetoothIV.alpha = 0.2
            }) { finished in
                UIView.animate(withDuration: 0.2, animations: {
                    self.bluetoothIV.alpha = 1
                }) { finished in
                    UIView.animate(withDuration: 0.2, animations: {
                        self.bluetoothIV.alpha = 0.2
                    }) { finished in
                        UIView.animate(withDuration: 0.2, animations: {
                            self.bluetoothIV.alpha = 1
                        })
                    }
                }
            }
        }
        BleService.instance.getBattery()
    }
    
    @IBAction func start(_ sender: Any) {

        print("exoViewModel?.exoType: ", exoViewModel?.exoType)
        
        if exoViewModel?.exoType != 4 {//Constants.BREATH {
            if BleService.instance.isReady() {
                    print("The exercise can start")
                    if (Registry.get("has_vaginal_ring") != nil) == true {
                        let alert = UIAlertView(title: NSLocalizedString("vaginal_ring_exo_alert_title", comment: ""), message: NSLocalizedString("vaginal_ring_exo_alert_message", comment: ""), delegate: self, cancelButtonTitle: NSLocalizedString("ok", comment: ""), otherButtonTitles: "")
                        alert.tag = ALERT_VAGINAL_RING_TAG
                        alert.show()
                    } else {
                        performSegue(withIdentifier: "startLocking", sender: nil)
                    }
            } else {
                let connectAlert = UIAlertView(title: NSLocalizedString("airbee_not_connected_alert_title", comment: ""), message: NSLocalizedString("airbee_not_connected_alert_message", comment: ""), delegate: self, cancelButtonTitle: NSLocalizedString("ok", comment: ""), otherButtonTitles: NSLocalizedString("help", comment: ""))
                connectAlert.tag = ALERT_CONNECT_TAG
                connectAlert.show()
            }
        }
    }
    
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
        if alertView.tag == ALERT_VAGINAL_RING_TAG {
            performSegue(withIdentifier: "startLocking", sender: nil)
        }
        if alertView.tag == ALERT_CONNECT_TAG && buttonIndex == 1 {
            let tutoVC = storyboard?.instantiateViewController(withIdentifier: "ExosIntimateSensorTutosViewController") as? ExosIntimateSensorTutosViewController
            tutoVC?.sensorTutoType = .SENSOR_TUTO_TYPE_CONNECT
            if let aVC = tutoVC {
                navigationController?.present(aVC, animated: true)
            }
        }
        
    }
    /*
     @IBAction func knowMore(_ sender: Any) {
        if isFreeExo {
            performSegue(withIdentifier: "showInfos", sender: nil)
        } else {
            let sb = UIStoryboard(name: "Auth", bundle: nil)
            let dest = sb.instantiateViewController(withIdentifier: "AuthIntimateSessionsContentInfosViewController") as? AuthIntimateSessionsContentInfosViewController
            var activityTypes = [AnyHashable](repeating: 0, count: sessionBase?.activities.count ?? 0)
            for (idx, activityBase) in sessionBase?.activities.enumerated() {
                let filter = Filter(attribute: "type", relation: "==", value: activityBase?.type)
                let type = IntimateManager.shared().fetchIntimateActivityTypes(withFilters: [filter]).last as? IntimateActivityType
                if let aType = type {
                    if !activityTypes.contains(aType) && type != nil {
                        if let aType = type {
                            activityTypes.append(aType)
                        }
                    }
                }
            }
            dest?.activityTypes = activityTypes
            if let aDest = dest {
                navigationController?.pushViewController(aDest, animated: true)
            }
        }
    }*/

    

    @IBAction override func back(_ sender: Any) {
        
        navigationController?.popViewController(animated: true)
        
    }
    
    
    
    @IBAction func cleanTuto(_ sender: Any) {
        
        let tutosVC = self.storyboard?.instantiateViewController(withIdentifier: "ExosIntimateSensorTutosViewController") as? ExosIntimateSensorTutosViewController
        
            tutosVC?.sensorTutoType = .SENSOR_TUTO_TYPE_HOW_TO_CLEAN
       
        if let aVC = tutosVC {
            present(aVC, animated: true)
        }
        
    }
    
    
    
    func onDisconnected() {
        batteryLevel = nil
        tableView.reloadData()
        UIView.animate(withDuration: 0.2, animations: {
            self.bluetoothIV.alpha = 0.2
        })
        BleService.instance.startScanning(names: BleConstant.names)
    }
    
    func onBatteryLevel(level: Int) {
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if isFreeExo {
            if exoViewModel?.exoType == Constants.BREATH || exoViewModel?.exoType == Constants.CALIBRATION {
                return 70
            }
            if indexPath.row == 0 {
                return 130
            } else {
                return 70
            }
        } else {
            if indexPath.row == 0 {
                return UITableViewAutomaticDimension
            } else {
                return 70
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if exoViewModel?.exoType == Constants.BREATH || exoViewModel?.exoType == Constants.CALIBRATION {
            return 2
        }
        return 3
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func didSelectIntimateLevel(_ selectedLevel: NSNumber?) {
        self.selectedLevel = selectedLevel
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let levelCellIdentifier = "levelCell"
        let durationCellIdentifier = "durationCell"

    
         if !isFreeExo {
            
        if indexPath.row == 0 {
            
            var levelCell = self.tableView.dequeueReusableCell(withIdentifier: levelCellIdentifier, for: indexPath) as? IntimateLevelParamTableViewCell
            if levelCell == nil {
                levelCell = IntimateLevelParamTableViewCell()
            }
            //[levelCell setContentWithLevels:@[@1,@2,@3,@4,@5,@6,@7,@8,@9,@10] currentLevel:_activityType.freeLevelReached selectedLevel:_selectedLevel];
            levelCell?.setContentWithLevels([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], currentLevel: 10, selectedLevel: selectedLevel, mainColor: UIColor.pinkMercu())
            levelCell?.delegate = self
            if let aCell = levelCell {
                return aCell
            }
            return levelCell!
            
        }
            
        else if indexPath.row == 1 {
                var durationCell = self.tableView.dequeueReusableCell(withIdentifier: durationCellIdentifier, for: indexPath) as? SessionParamTableViewCell
                if durationCell == nil {
                    durationCell = SessionParamTableViewCell()
                }
                var minutes = Int32(sessionBase?.totalDuration ?? 0 )
                minutes = minutes/60
                var duration = String(format: NSLocalizedString("minutes_long_variable", comment: ""), minutes, "s")
                durationCell?.setContentWithTitle(NSLocalizedString("exercise_duration", comment: ""), image: UIImage(named: "duree_seance_picto"), detailText: duration)
                if let aCell = durationCell {
                    return aCell
                }
                return durationCell!
            }
        
        }
        if exoViewModel?.exoType == 4 || exoViewModel?.exoType == 5 {
            if indexPath.row == 0 {
                var durationCell = self.tableView.dequeueReusableCell(withIdentifier: durationCellIdentifier, for: indexPath) as? SessionParamTableViewCell
                if durationCell == nil {
                    durationCell = SessionParamTableViewCell()
                }
                var duration: String
                if 4 == exoViewModel?.exoType {
                    duration = String(format: NSLocalizedString("n_seconds", comment: ""), 20)
                } else if exoViewModel?.exoType == 5 {
                    duration = String(format: NSLocalizedString("n_seconds", comment: ""), 30)
                } else {
                    if isFreeExo {
                        duration = String(format: NSLocalizedString("minutes_long_variable", comment: ""), 2, "s")
                    } else {
                        duration = String(format: NSLocalizedString("minutes_long_variable", comment: ""), 1, "")
                    }
                }
                durationCell?.setContentWithTitle(NSLocalizedString("exercise_duration", comment: ""), image: UIImage(named: "duree_seance_picto"), detailText: duration)
                if let aCell = durationCell {
                    return aCell
                }
                return durationCell!
            }
        } else {
            if indexPath.row == 0 {
                var levelCell = self.tableView.dequeueReusableCell(withIdentifier: levelCellIdentifier, for: indexPath) as? IntimateLevelParamTableViewCell
                if levelCell == nil {
                    levelCell = IntimateLevelParamTableViewCell()
                }
                //[levelCell setContentWithLevels:@[@1,@2,@3,@4,@5,@6,@7,@8,@9,@10] currentLevel:_activityType.freeLevelReached selectedLevel:_selectedLevel];
                levelCell?.setContentWithLevels([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], currentLevel: 10, selectedLevel: selectedLevel, mainColor: UIColor.pinkMercu())
                levelCell?.delegate = self
                if let aCell = levelCell {
                    return aCell
                }
                return levelCell!
            } else if indexPath.row == 1 {
                var durationCell = self.tableView.dequeueReusableCell(withIdentifier: durationCellIdentifier, for: indexPath) as? SessionParamTableViewCell
                if durationCell == nil {
                    durationCell = SessionParamTableViewCell()
                }
                var duration: String
                if isFreeExo {
                    duration = String(format: NSLocalizedString("minutes_long_variable", comment: ""), 2, "s")
                } else {
                    duration = String(format: NSLocalizedString("minutes_long_variable", comment: ""), 1, "")
                }
                durationCell?.setContentWithTitle(NSLocalizedString("exercise_duration", comment: ""), image: UIImage(named: "duree_seance_picto"), detailText: duration)
                if let aCell = durationCell {
                    return aCell
                }
                return durationCell!
            }
        }
        return UITableViewCell()
    }
    
    override func viewWillLayoutSubviews() {
        
        
    }
    
    override func viewDidLayoutSubviews() {
        /*let lbl = TTTAttributedLabel()
        lbl.setText(NSLocalizedString("verrouillage_exo_description", comment: ""), inContainerView: textContainer, withBaseFont: UIFont(name: "SourceSansPro-It", size: 15), baseColor: UIColor.pinkMercu(), linkFont: UIFont(name: "SourceSansPro-Bold", size: 15), linkColor: UIColor.pinkMercu(), underlined: false, textAlignment: .center)*/
    }
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "startLocking") {
            let eilvc = segue.destination as? ExosCocosViewController
            //eilvc?.activityType = activityType
            if isFreeExo {
                if exoViewModel?.exoType == Constants.BREATH {
                    eilvc?.exoDuration = Constants.INTIMATE_BREATH_DURATION
                } else if exoViewModel?.exoType == Constants.CALIBRATION {
                    eilvc?.exoDuration = Constants.CALIBRATION_DURATION
                } else {
                    eilvc?.exoDuration = Constants.FREE_EXO_DURATION
                }
            } else {
                eilvc?.exoDuration = Constants.PROGRAM_EXO_DURATION
            }
            eilvc?.isFreeExo = isFreeExo
            eilvc?.selectedLevel = selectedLevel
            eilvc?.exoViewModel = exoViewModel
        }
        if (segue.identifier == "showInfos") {
            let eiivc = segue.destination as? ExosIntimateInfosViewController
            eiivc?.exoViewModel = exoViewModel
        }
    }
    
    func onReady() {
        
    }
    
    func onGettingPressureStarted() {
        
    }
    
    func onGettingPressureStopped() {
        
    }
    
    func onPressureDataReceived(data: Int) {
        
    }
    
    func onScanStarted() {
        
    }
    
    func onScanStopped() {
        
    }
    
}

let ALERT_CONNECT_TAG = 404
let ALERT_VAGINAL_RING_TAG = 111
let ALERT_BATTERY_TAG = 666
