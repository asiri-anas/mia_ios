//
//  AuthIntimateSurveyViewControllerSwift.swift
//  mia
//
//  Created by Xavier Bohin on 17/04/2018.
//  Copyright © 2018 V-Labs. All rights reserved.
//

import Foundation
import UIKit

class AuthIntimateSurveyViewController: AbstractViewController, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate {
    
    
    var intimateState: IntimateState?
    var intimateCoach: IntimateCoach?

    
    @IBOutlet weak var surveyView: UIScrollView!
    
    @IBOutlet weak var checkBoxChild: UIButton!
    @IBOutlet weak var checkBoxDelivery: UIButton!
    @IBOutlet weak var checkBoxMenopause: UIButton!
    @IBOutlet weak var checkBoxNeither: UIButton!
    var isChildChecked = false
    var isDeliveryChecked = false
    var isMenopauseChecked = false
    var isNeitherChecked = false

    @IBOutlet weak var DeliveryView: UIView!
    
    @IBOutlet weak var spinnerDeliveryDate: UIDatePicker!
    @IBOutlet weak var spinnerDeliveryWAy: UIPickerView!
    
    @IBOutlet weak var checkBoxLeak: UIButton!
    var isLeakChecked = false
    
    @IBOutlet weak var LeakView: UIView!
    
    
    @IBOutlet weak var spinnerFrequency: UIPickerView!
    @IBOutlet weak var spinnerQuantity: UIPickerView!
    
    @IBOutlet weak var checkBoxDiffFeel: UIButton!
    var isDiffFeelChecked = false
    @IBOutlet weak var spinnerDiffFeel: UIPickerView!
    
    @IBOutlet weak var DiffFeelView: UIView!
    
    @IBOutlet weak var textFieldHeight: UITextField!
    @IBOutlet weak var textFieldWeight: UITextField!
    
    @IBOutlet weak var checkBoxRecentPerine: UIButton!
    var isRecentPerineChecked = false
    
    @IBOutlet weak var validateButton: UIButton!
    
    @IBOutlet weak var scrollViewWidth: NSLayoutConstraint!
    
    var isEditingMode = false
    
    var pickerSex = ["Choisir", "Oui", "Non"]
    var pickerWay = ["Choisir", "Voie basse", "Césarienne"]
    var pickerFrequency = ["Choisir", "Jamais", "Une fois par semaine au maximum", "Deux à trois fois par semaine", "Environ une fois par jour", "Très fréquemment"]
    var pickerQuality = ["Choisir", "Aucune", "Une petite quantité", "Une quantité moyenne", "Une grande quantité"]
    var pickerDiffFeel = ["Choisir", "Douleurs", "Manque de sensations"]
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //   navigationItem.title = NSLocalizedString((intimateStatus?.name)!, comment: "").uppercased()
        setGradientBackground()
        //GradientHelper.setGradientOnLayer(surveyView.layer, from: UIColor.white, to: UIColor.colorWithHexString(stringToConvert: "#F3DCD7"))
        
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        scrollViewWidth.constant = screenWidth
        self.updateViewConstraints()
        
        DeliveryView.isHidden = true
        LeakView.isHidden = true
        DiffFeelView.isHidden = true
        
        
        if intimateState != nil {
            
            textFieldHeight.text = intimateState?.bodyHeight
            textFieldWeight.text = intimateState?.bodyWeight
            spinnerDeliveryWAy.selectedRow(inComponent: pickerStringToRow(string: intimateState?.deliveryWay, picker: pickerWay))
            isRecentPerineChecked = (intimateState?.educatedLately)!
            isChildChecked = (intimateState?.hasChild)!
            isNeitherChecked = (intimateState?.hasNeither)!
            isDeliveryChecked = (intimateState?.hasDelivered)!
            isDiffFeelChecked = (intimateState?.hasLackSensation)!
            isLeakChecked = (intimateState?.hasLeak)!
            isMenopauseChecked = (intimateState?.hasMenopause)!
            spinnerDeliveryDate.date = (intimateState?.lastDelivery)!
            spinnerFrequency.selectRow(pickerStringToRow(string: intimateState?.leakFrequency, picker: pickerFrequency), inComponent: 0, animated: false)
            spinnerQuantity.selectRow(pickerStringToRow(string: intimateState?.leakQuantity, picker: pickerQuality), inComponent: 0, animated: false)
            spinnerDiffFeel.selectRow(pickerStringToRow(string: intimateState?.sensationPrecision, picker: pickerDiffFeel), inComponent: 0, animated: false)
            
            
        }
        
        
        textFieldWeight.setMercuStyle()
        textFieldHeight.setMercuStyle()
        
        spinnerDeliveryDate.maximumDate = Date()
        preFillDiagnostic()
        setCheckBoxes()
        
        
        
    }
    
    
    
    func setCheckBoxes() {
     
        if isDeliveryChecked {
            isDeliveryChecked = !isDeliveryChecked
            tappedDelivery(self)
        }
        if isChildChecked {
            isChildChecked = !isChildChecked
            tappedChild(self)
        }
        if isMenopauseChecked {
            isMenopauseChecked = !isMenopauseChecked
            tappedMenopause(self)
        }
        if isNeitherChecked {
            isNeitherChecked = !isNeitherChecked
            tappedNone(self)
        }
        if isLeakChecked {
            isLeakChecked = !isLeakChecked
            tappedLeak(self)
        }
        if isDiffFeelChecked {
            isDiffFeelChecked = !isDiffFeelChecked
            tappedDiff(self)
        }
        if isRecentPerineChecked {
            isRecentPerineChecked = !isRecentPerineChecked
            tappedRecent(self)
        }
        
    }
    
    
    
    
    func pickerStringToRow (string: String?, picker: [String] ) -> Int
    {
        var count = 0
        for i in picker
        {
            if i == string
            {
                return count
            }
            count = count + 1
        }
        return 0
    }
    
    
    
    func preFillDiagnostic() {
        do {
            var results = try CDManager.instance!.managedObjectContext!.fetch(IntimateCoach.intimateCoachFetchRequest())
            print("AUTH", "fetch intimateCoach")
            for singleData in results {
                if singleData.intimateState != nil {
                    print("AUTH", "intimateState fetched:", singleData.intimateState!.bodyHeight)
                    textFieldWeight.text = singleData.intimateState!.bodyWeight
                    textFieldHeight.text = singleData.intimateState!.bodyHeight
                    spinnerDeliveryWAy.selectRow(pickerStringToRow(string: singleData.intimateState!.deliveryWay, picker: pickerWay), inComponent: 0, animated: true)
                    isRecentPerineChecked = (singleData.intimateState!.educatedLately)
                    isChildChecked = (singleData.intimateState!.hasChild)
                    isNeitherChecked = (singleData.intimateState!.hasNeither)
                    isDeliveryChecked = (singleData.intimateState!.hasDelivered)
                    print("lololololol")
                    print(singleData.intimateState!.hasDelivered)
                    isDiffFeelChecked = (singleData.intimateState!.hasLackSensation)
                    isLeakChecked = (singleData.intimateState!.hasLeak)
                    isMenopauseChecked = (singleData.intimateState!.hasMenopause)
                    spinnerDeliveryDate.date = (singleData.intimateState!.lastDelivery)!
                    spinnerFrequency.selectRow(pickerStringToRow(string: singleData.intimateState!.leakFrequency, picker: pickerFrequency), inComponent: 0, animated: true)
                    spinnerQuantity.selectRow(pickerStringToRow(string: singleData.intimateState!.leakQuantity, picker: pickerQuality), inComponent: 0, animated: true)
                    spinnerDiffFeel.selectRow(pickerStringToRow(string: singleData.intimateState!.sensationPrecision, picker: pickerDiffFeel), inComponent: 0, animated: true)
                }
                intimateCoach = singleData
            }
        } catch {
            print("Failed")
        }
    }
    
    
    
    
    
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var pickerLabel: UILabel? = (view as? UILabel)
        if pickerLabel == nil {
            pickerLabel = UILabel()
            pickerLabel?.font = UIFont(name: "System", size: 17)
            pickerLabel?.textAlignment = .center
        }
        
        switch pickerView {
        case spinnerDeliveryWAy:
            pickerLabel?.text = pickerWay[row]
        case spinnerFrequency:
            pickerLabel?.text = pickerFrequency[row]
        case spinnerQuantity:
            pickerLabel?.text = pickerQuality[row]
        case spinnerDiffFeel:
            pickerLabel?.text = pickerDiffFeel[row]
        default:
            pickerLabel?.text = pickerSex[row]
        }
        
        return pickerLabel!
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView {
        case spinnerDeliveryWAy:
            return pickerWay.count
        case spinnerFrequency:
            return pickerFrequency.count
        case spinnerQuantity:
            return pickerQuality.count
        case spinnerDiffFeel:
            return pickerDiffFeel.count
        default:
            return 1
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView {
        case spinnerDeliveryWAy:
            return pickerWay[row]
        case spinnerFrequency:
            return pickerFrequency[row]
        case spinnerQuantity:
            return pickerQuality[row]
        case spinnerDiffFeel:
            return pickerDiffFeel[row]
        default:
            return "choisir"
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIView.animate(withDuration: 0.3, animations: {() -> Void in
            self.validateButton.alpha = 1
        })
    }

    
    
    
    func didTapLeftBarButtonItem() {
        if isEditingMode {
            dismiss(animated: true) {() -> Void in }
        } else {
            //        NativeAppDelegate *app = (NativeAppDelegate *)[[UIApplication sharedApplication] delegate];
            //        [app loadMainStoryboardWithCompletion:nil];
            navigationController?.popViewController(animated: true)
        }
    }
    
    
    @IBAction func tappedChild(_ sender: Any) {
        isChildChecked = !isChildChecked
        if isChildChecked {
            checkBoxChild.setImage(#imageLiteral(resourceName: "case_marked"), for: .normal)
            
            if ( isNeitherChecked )
            {
                tappedNone(checkBoxNeither)
            }
            
        } else {
            checkBoxChild.setImage(#imageLiteral(resourceName: "case_unmarked"), for: .normal)
        }
    }
    
    @IBAction func tappedDelivery(_ sender: Any) {
        isDeliveryChecked = !isDeliveryChecked
        if isDeliveryChecked {
            checkBoxDelivery.setImage(#imageLiteral(resourceName: "case_marked"), for: .normal)
            DeliveryView.isHidden = false
            
            if ( isNeitherChecked )
            {
                tappedNone(checkBoxNeither)
            }
            
        } else {
            checkBoxDelivery.setImage(#imageLiteral(resourceName: "case_unmarked"), for: .normal)
            DeliveryView.isHidden = true
        }
    }
    
    @IBAction func tappedMenopause(_ sender: Any) {
        isMenopauseChecked = !isMenopauseChecked
        if isMenopauseChecked {
            checkBoxMenopause.setImage(#imageLiteral(resourceName: "case_marked"), for: .normal)
            
            if ( isNeitherChecked )
            {
                tappedNone(checkBoxNeither)
            }
            
        } else {
            checkBoxMenopause.setImage(#imageLiteral(resourceName: "case_unmarked"), for: .normal)
        }
    }
    
    
    @IBAction func tappedNone(_ sender: Any) {
        isNeitherChecked = !isNeitherChecked
        if isNeitherChecked {
            checkBoxNeither.setImage(#imageLiteral(resourceName: "case_marked"), for: .normal)
            
            if ( isMenopauseChecked )
            {
                tappedMenopause(checkBoxMenopause)
            }
            
            if ( isDeliveryChecked )
            {
                tappedDelivery(checkBoxDelivery)
            }
            
            if ( isChildChecked )
            {
                tappedChild(checkBoxChild)
            }
            
            
        } else {
            checkBoxNeither.setImage(#imageLiteral(resourceName: "case_unmarked"), for: .normal)
        }
    }
    
    
    
    
    @IBAction func tappedLeak(_ sender: Any) {
        isLeakChecked = !isLeakChecked
        if isLeakChecked {
            checkBoxLeak.setImage(#imageLiteral(resourceName: "case_marked"), for: .normal)
            LeakView.isHidden = false
        } else {
            checkBoxLeak.setImage(#imageLiteral(resourceName: "case_unmarked"), for: .normal)
            LeakView.isHidden = true
        }
    }
    
    
    @IBAction func tappedDiff(_ sender: Any) {
        isDiffFeelChecked = !isDiffFeelChecked
        if isDiffFeelChecked {
            checkBoxDiffFeel.setImage(#imageLiteral(resourceName: "case_marked"), for: .normal)
            DiffFeelView.isHidden = false
        } else {
            checkBoxDiffFeel.setImage(#imageLiteral(resourceName: "case_unmarked"), for: .normal)
            DiffFeelView.isHidden = true
        }
    }
    
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == textFieldHeight {
            textFieldWeight.becomeFirstResponder()
        }
        if textField == textFieldWeight {
            view.endEditing(true)
        }
        return false
    }
    
    
    
    @IBAction func tappedRecent(_ sender: Any) {
        isRecentPerineChecked = !isRecentPerineChecked
        if isRecentPerineChecked {
            checkBoxRecentPerine.setImage(#imageLiteral(resourceName: "case_marked"), for: .normal)
        } else {
            checkBoxRecentPerine.setImage(#imageLiteral(resourceName: "case_unmarked"), for: .normal)
        }
    }
    
    
    func programSelection () -> Int
    {
        var programNum = 1
        
        if (isLeakChecked) {
            programNum = 2
        }
        else if (isDiffFeelChecked) {
            programNum = 3
        }
        else if (isDeliveryChecked) {
            var different = spinnerDeliveryDate.date.timeIntervalSinceNow
            //print("accouche")
            //print (different)
             if (different > -31593781) { //this value corespond to 1 year. It's negative(see timeIntervalSinceNow)
             programNum = 4
             }
            
        }
        
        return programNum;
    }
    
    func programKey(int: Int) -> String
    {
        switch int {
        case 1:
            return "post accouchement"
        case 2:
            return "incomfort urinaire"
        case 3:
            return "Sensations perturbées"
        case 4:
            return "prendre soin de mon périnée"
        default:
            return "incomfort urinaire"
        }
    }
    
    
    
    
    @IBAction func clickValid(_ sender: Any) {
        var validString = isAllValid()
        
        if ( validString != "")
        {
            let alert = UIAlertController(title: "Veuillez remplir la partie:", message: validString, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true)
            return
        }
        
        intimateCoach = initStateAndSave()
        
        
        performSegue(withIdentifier: "showSelector", sender: nil)
    }
    
    
    func initStateAndSave() -> IntimateCoach? {
        if intimateCoach == nil {
            intimateCoach = IntimateCoachManager.instance.createInitialIntimateCoach()
            print("AUTH", "intimateCoach created")
        }
        intimateState = intimateCoach?.intimateState
        if intimateState == nil {
            intimateState = IntimateState.create()
            print("AUTH", "intimateState created")
        }
        
        intimateState?.bodyWeight = textFieldWeight.text!
        intimateState?.bodyHeight = textFieldHeight.text!
        intimateState?.deliveryWay = pickerWay[spinnerDeliveryWAy.selectedRow(inComponent: 0)]
        intimateState?.educatedLately = isRecentPerineChecked
        intimateState?.hasChild = isChildChecked
        intimateState?.hasNeither = isNeitherChecked
        intimateState?.hasDelivered = isDeliveryChecked
        print(isDeliveryChecked)
        intimateState?.hasLackSensation = isDiffFeelChecked
        intimateState?.hasLeak = isLeakChecked
        intimateState?.hasMenopause = isMenopauseChecked
        intimateState?.lastDelivery = spinnerDeliveryDate.date
        intimateState?.leakFrequency = pickerFrequency[spinnerFrequency.selectedRow(inComponent: 0)]
        intimateState?.leakQuantity = pickerQuality[spinnerQuantity.selectedRow(inComponent: 0)]
        intimateState?.sensationPrecision = pickerDiffFeel[spinnerDiffFeel.selectedRow(inComponent: 0)]
        
        intimateCoach?.intimateState = intimateState
        print("AUTH", "profil survey program type", intimateCoach?.intimateState?.intimateProgramType)
        //print("Intimate to send: ", intimateCoach)
        CDManager.instance!.saveContext()
        
        return intimateCoach
    }
    
    
    
    
    func isStatusValid () -> Bool
    {
        var valid = false
        valid = isChildChecked || isDeliveryChecked || isMenopauseChecked || isNeitherChecked
        return valid
        
    }
    func isDeliveryValid () -> Bool
    {
        var valid = false
        if ( spinnerDeliveryWAy.selectedRow(inComponent: 0) != 0) {
            valid = true
        }
        return valid
    }
    func isComfortValid () -> Bool
    {
        var valid1 = false
        var valid2 = false
        if ( spinnerFrequency.selectedRow(inComponent: 0) != 0) {
            valid1 = true
        }
        if ( spinnerQuantity.selectedRow(inComponent: 0) != 0) {
            valid2 = true
        }
        
        return valid1 && valid2
    }
    func isPainValid () -> Bool
    {
        var valid = false
        if ( spinnerDiffFeel.selectedRow(inComponent: 0) != 0) {
            valid = true
        }
        return valid
    }
    func isMyBodyValid () -> Bool
    {
        var valid = true
        var textfields: NSArray
        
        textfields = [textFieldWeight, textFieldHeight]
        
        textfields.enumerateObjects({ obj, idx, stop in
            if (obj as AnyObject).text?.replacingOccurrences(of: " ", with: "").count == 0 {
                valid = false
            }
        })
        return valid
    }
    
    func isAllValid () -> String
    {
        var validString = ""
        if !isStatusValid() { validString = "Mon statut intime" }
        
        if ( isDeliveryChecked ) {
        if !isDeliveryValid() { validString = "Mon accouchement" }
        }
        
        if ( isLeakChecked) {
        if !isComfortValid() { validString = "Ma Gêne" }
        }
        
        if ( isDiffFeelChecked) {
        if !isPainValid() { validString = "Ma Gêne" }
        }
        
        if !isMyBodyValid() { validString = "Connaissance de mon corps" }
        return validString
    }
    
    
    
    
    
// MARK: - Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if (segue.identifier == "showSelector") {
        let airvc = segue.destination as? AuthProgramSelector

        airvc?.programId = programSelection ()
        airvc?.programDiscover = !isRecentPerineChecked
        }
    }
    
    
}


