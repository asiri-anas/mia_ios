//
//  Token.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 02/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation


class Token: NSObject, NSCoding {
    
    
    
    var accessToken = ""
    var expiresIn: NSNumber?
    var tokenType = ""
    /*!
     @param tokenDic Dictionary with keys corresponding to the remote API
     @param tokenDic["access_token"] The token to use to authenticate user's requests
     @param tokenDic["expires_in"] Time interval until token expires
     @param tokenDic["token_type"] Type of the authorization token
     */

    init(dictionary tokenDic: NSDictionary) {
        super.init()
        accessToken = tokenDic["access_token"] as! String
        expiresIn = tokenDic["expires_in"] as! NSNumber
        tokenType = tokenDic["token_type"] as! String
}
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(accessToken, forKey: "accessToken")
        aCoder.encode(expiresIn, forKey: "expiresIn")
        aCoder.encode(tokenType, forKey: "tokenType")
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init()
        accessToken = aDecoder.decodeObject(forKey: "accessToken") as! String
        expiresIn = aDecoder.decodeObject(forKey: "expiresIn") as! NSNumber
        tokenType = aDecoder.decodeObject(forKey: "tokenType") as! String
    }

    
}
