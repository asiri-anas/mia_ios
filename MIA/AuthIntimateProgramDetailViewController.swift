//
//  AuthIntimateProgramDetailViewControllerSwift.swift
//  mia
//
//  Created by Xavier Bohin on 18/04/2018.
//  Copyright © 2018 V-Labs. All rights reserved.
//

import Foundation
import UIKit


class AuthIntimateProgramDetailViewController: AbstractViewController {
    
    var program: IntimateProgram?
    var intimateActivityTypes = [AnyHashable]()
    
    var stringTitle = ""
    
    var intimateState: IntimateState?
    
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var contentView: UIView!
    @IBOutlet var topView: UIView!
    @IBOutlet var midView: UIView!
    @IBOutlet var bottomView: UIView!
    @IBOutlet var programNameLabel: UILabel!
    @IBOutlet var sessionsNumberLabel: UILabel!
    @IBOutlet var averageDurationLabel: UILabel!
    @IBOutlet var typesNumberLabel: UILabel!
    @IBOutlet var typesLabel: UILabel!
    @IBOutlet var infoButton: UIButton!
    @IBOutlet var startButton: UIButton!
    @IBOutlet var skipButton: UIButton!
    @IBOutlet var heightTypesLabelConstraint: NSLayoutConstraint!
    var isEditingMode = false
    
    
    let DEEP_STRENGTHENING_FLAT: NSNumber = 0
    let DEEP_STRENGTHENING_SQUARE: NSNumber = 1
    let DEEP_STRENGTHENING_SAW: NSNumber = 2
    let SUPERFICIAL_STRENGTHENING: NSNumber = 3
    let LATCHING: NSNumber = 4
    let SLACKENING: NSNumber = 5
    let BREATH: NSNumber = 6
    let CALIBRATION: NSNumber = 7
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setGradientBackground()
        
        programNameLabel.text = "Programme " + stringTitle
        
        /*Registry.set("is_first_connection", withValue: (0))
         navigationItem.title = NSLocalizedString("your_program", comment: "").uppercased()
         navigationItem.hidesBackButton = true
         intimateActivityTypes = [AnyHashable]()
         
         program = IntimateProgramHandler.sharedManager()?.getIntimateProgram(forKey: stringTitle)
         
         let progLocKey = "\(program?.name)_program"
         programNameLabel.text = NSLocalizedString(progLocKey, comment: "")
         let sessionNumberString = String(format: NSLocalizedString("n_sessions", comment: ""), intimateState!.maxSession!, ((intimateState!.maxSession?.intValue)! > 1) ? "s" : "")
         sessionsNumberLabel.setText("\(sessionNumberString) - \(String(format: NSLocalizedString("adviced_frequency", comment: ""), (program!.frequency?.repeatByWeek)!))", highlightColor: UIColor.darkGrayTextColorMercu()!, highlightFont: nil)
         if isEditingMode {
         startButton.isHidden = true
         skipButton.setTitle(NSLocalizedString("done", comment: "").uppercased(), for: .normal)
         } else {
         startButton.isHidden = false
         skipButton.setTitle(NSLocalizedString("skip", comment: "").uppercased(), for: .normal)
         }
         let activities = IntimateProgramHandler.sharedManager()?.getIntimateActivityBase(forProgramKey: intimateState?.intimateProgramType?.label)
         var typesStr = String()
         var hasDeep = false
         var typesCount: Int = 0
         (activities as! NSArray).enumerateObjects({(_ obj: IntimateActivityBase?, _ idx: Int, _ stop: UnsafeMutablePointer<ObjCBool>) -> Void in
         let filter = Filter(_attribute: "type", _relation: "==", _value: obj?.type)
         let type = IntimateManager.shared()?.fetchIntimateActivityTypes(withFilters: [filter])?.last as? IntimateActivityType
         if let aType = type {
         if !self.intimateActivityTypes.contains(aType) && type != nil {
         if let aType = type {
         self.intimateActivityTypes.append(aType)
         }
         }
         }
         var str = "\(NSLocalizedString((type?.label)!, comment: "")) \n"
         
         
         if obj!.type == self.DEEP_STRENGTHENING_FLAT || obj?.type == self.DEEP_STRENGTHENING_SQUARE || obj?.type == self.DEEP_STRENGTHENING_SAW {
         if !hasDeep {
         typesCount += 1
         str = "\(NSLocalizedString("deep_strengthening_flat", comment: "")) \n"
         typesStr += str
         hasDeep = true
         }
         } else {
         typesStr += str
         typesCount += 1
         }
         
         
         } as! (Any, Int, UnsafeMutablePointer<ObjCBool>) -> Void)
         typesNumberLabel.text = String(format: NSLocalizedString("n_exercice_types", comment: ""), typesCount, ((activities?.count)! > 1) ? "s" : "")
         typesLabel.text = typesStr
         averageDurationLabel.text = String(format: NSLocalizedString("average_duration_n_min", comment: ""), (program?.frequency?.time)!)*/
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
        
    }
    
    @IBAction func start(_ sender: Any) {
        let app = UIApplication.shared.delegate as? NativeAppDelegate
        app?.loadMainStoryboardToStartIntimateProgram()
    }
    
    @IBAction func skip(_ sender: Any) {
        
        let app = UIApplication.shared.delegate as? NativeAppDelegate
        app?.loadMainStoryboard()
        
    }
    
    @IBAction func showInfos(_ sender: Any) {
        performSegue(withIdentifier: "showContentInfos", sender: nil)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        heightTypesLabelConstraint.constant = typesLabel.intrinsicContentSize.height
    }
    
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "showContentInfos") {
            let dest = segue.destination as? AuthIntimateSessionsContentInfosViewController
        }
    }
    
    
    
    
    
}

