//
//  MenuTableViewCell.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 02/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit


class MenuTableViewCell: UITableViewCell {
    
    @IBOutlet var customLabel: UILabel!
    @IBOutlet var customImageView: UIImageView!
    
    var selectedBG: UIView?
    let SPACE_INTIMATE = 5
    let SPACE_EXOS = 6
    

    override func awakeFromNib() {
        selectedBG = UIView()
    }
    
    func setContentWithText(_ text: String?, andImageName imageName: String?, withSeparator separator: Bool) {
        customLabel?.text = text
        customImageView.image = UIImage(named: imageName ?? "")
        if separator {
            separatorInset = UIEdgeInsetsMake(0, 0, 0, 0)
        } else {
            separatorInset = UIEdgeInsetsMake(0, bounds.size.width, 0, 0)
        }
    }
    
    func setContentForSpace(_ space: Int) {
        if space == SPACE_EXOS {
            customLabel.text = NSLocalizedString("start_an_exercice", comment: "")
            customImageView.image = UIImage(named: "play_exo_button")
        }
        if space == SPACE_INTIMATE {
            customLabel.text = NSLocalizedString("my_intimate_space", comment: "")
            customImageView.image = UIImage(named: "ma_progression_pink")
        }
        separatorInset = UIEdgeInsetsMake(0, bounds.size.width, 0, 0)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: true)
        selectedBG?.backgroundColor = UIColor.grayForGradientMercu()
        selectedBackgroundView = selectedBG
    }
    
    
}
