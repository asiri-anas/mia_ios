//
//  CalibrationLayer.cpp
//  mymercurochrome
//
//  Created by Rémi Caroff on 16/12/2015.
//  Copyright © 2015 V-Labs. All rights reserved.
//

#include "CalibrationLayer.h"
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include "../NDKHelper/NDKHelper.h"
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#include "NDKHelper.h"
#endif
#include "Constants.h"
#include "ActivityService.h"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#include "LocalizationHelper.h"
#endif

#include <string>
USING_NS_CC;

using namespace std;

Value c_data;

CalibrationLayer::~CalibrationLayer()
{
	_eventDispatcher->removeEventListener(_customlistener);
};

Scene* CalibrationLayer::createScene(Value data)
{
    c_data = data;
    
    auto scene = Scene::create();
    auto layer = CalibrationLayer::create();
    
    scene->addChild(layer);
    
    return scene;
}

bool CalibrationLayer::init()
{
    
    if (!LayerGradient::initWithColor(Color4B(246,134,141,255), Color4B(246,134,141,255))) {
        return false;
    }

    _visibleSize = Director::getInstance()->getWinSize();
    _originDirector = Director::getInstance()->getVisibleOrigin();
    


    unserializeData();
    _currentTimeStamp = 0;
    
    ActivityService::getInstance()->initLeafWithNode(this);
    AbstractLayer::initWithNameAndDuration("", _duration);
    
    calculMaxPressureDraw();
	calculMaxDraw();

	_parallaxNode = ParallaxNode::create();
    
    
    Size parallaxSize = Size(_visibleSize.width*(_duration/TIME_BY_SCREEN), _visibleSize.height);
    
    _parallaxNode->setContentSize(parallaxSize);
    this->addChild(_parallaxNode);
    
    _leafSprite = Sprite::create("drawable/feuille_big_white.png");
    _leafSprite->setScale(0.3, 0.3);
    _leafSprite->setPosition(Vec2(_visibleSize.width/ 2, _newOrigin.y));
    
    this->addChild(_leafSprite);
    
    _lastPoint = _leafSprite->getPosition();
    
    auto minLine = DrawNode::create();
    Color4B whiteAlpha(255,255,255,175);
    Color4F white(whiteAlpha);
    minLine->drawLine(Vec2(0, _visibleSize.height / 3), Vec2(_visibleSize.width+_originDirector.x, _visibleSize.height / 3), white);
    this->addChild(minLine);
    
    _customlistener = EventListenerCustom::create(KEY_POST_PRESSURE_EVENT, [=](EventCustom* event) {
        int* pressurePtr = static_cast<int*>(event->getUserData());
        int pressure = *((int*)pressurePtr);
        Vector<FiniteTimeAction *> fta;
        float height = getHeightLeaf(pressure, _maxPressure);
        Vec2 pos = Vec2(_leafSprite->getPosition().x, height);
        auto moveAction = MoveTo::create(0.2, pos);
        fta.pushBack(moveAction );
        
        if (!AbstractLayer::isInCountdown() && !AbstractLayer::isPaused()){
			ActivityService::activityStep steps;
			steps.pressure = pressure;
			steps.timestamp = _currentTimeStamp;
			steps.target = 0;
			ActivityService::getInstance()->addActivitySteps(&steps);

			_currentTimeStamp += STEP_FREQUENCY;
		}
        //log("leaf position : %f %f", pos.x, pos.y);
        if (!AbstractLayer::isPaused()) {
			auto seq = Sequence::create(fta);
			_leafSprite->runAction(seq);
        }
        
    });
    _eventDispatcher->addEventListenerWithFixedPriority(_customlistener, 1);

	_isContract = true;
    _actionLabel = Label::createWithTTF("Serrez", "fonts/SourceSansPro-Regular.otf", 20);
	#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	_actionLabel->setPosition(Vec2(_visibleSize.width/2, _visibleSize.height-33));
	#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	_actionLabel->setPosition(Vec2(_visibleSize.width/2, _visibleSize.height - _actionLabel->getContentSize().height / 2 - 10));
	#endif


	this->addChild(_actionLabel);

    if (_lastCalibrationValue != 0) {
		auto dotsLayer = Layer::create();
		dotsLayer->setContentSize(Director::getInstance()->getWinSize());

		this->addChild(dotsLayer);

		float height = getHeightLeaf(_lastCalibrationValue, _maxPressure) ;
		float dotSize = 7;
		int dashSpace = 10;
		auto dotNode = DrawNode::create();
		for (int i = 0; i < dotsLayer->getContentSize().width; i++) {
			if (i % dashSpace == 0) {
				dotNode->drawPoint(Vec2((float)i, height), dotSize, Color4F::WHITE);
			}
		}

		dotsLayer->addChild(dotNode);


		TTFConfig ttfConfig("fonts/SourceSansPro-Regular.otf", 15);
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	auto lastValueLabel = Label::createWithTTF(ttfConfig, LocalizationHelper::getInstance()->getLocalizedString("last_calibration_value"));
	lastValueLabel->setPosition(Vec2(_visibleSize.width - 25, height + lastValueLabel->getContentSize().height));
	lastValueLabel->setAnchorPoint(Vec2(1,0.5f));
	lastValueLabel->setTextColor(Color4B::WHITE);
	dotsLayer->addChild(lastValueLabel);
#endif

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
		std::stringstream ss;
		ss << _lastCalibrationValue;
		Label* lastValueLabel = Label::createWithTTF(ttfConfig, "Niveau précédent", TextHAlignment::RIGHT);
		lastValueLabel->setPosition(Vec2(_visibleSize.width - 25, height + lastValueLabel->getContentSize().height));
		lastValueLabel->setAnchorPoint(Vec2(1,0.5f));
		lastValueLabel->setTextColor(Color4B::WHITE);
		dotsLayer->addChild(lastValueLabel);
#endif

    }
    startTimerCalibration();

    Value nullValue;
    sendMessageWithParams(ON_EXERCISE_INITIALIZED, nullValue);
    
    return true;
};

void CalibrationLayer::updateTitle(float dt)
{
	if (_isContract)
		_actionLabel->setString("Relâcher");
	else
		_actionLabel->setString("Serrez");
	_isContract = !_isContract;
}

void CalibrationLayer::unserializeData() {
    if (!c_data.isNull() && c_data.getType() == Value::Type::MAP) {
        ValueMap valueMap = c_data.asValueMap();
        _duration = valueMap[KEY_DURATION].asInt();
        _maxPressure = valueMap[KEY_MAX_PRESSURE_CALIBRATION].asFloat();
        _lastCalibrationValue = valueMap[KEY_LAST_PRESSURE_MAX].asInt();
    } else {
    }
}

