//
//  AbstractViewController.swift
//  mia
//
//  Created by Xavier Bohin on 17/04/2018.
//  Copyright © 2018 V-Labs. All rights reserved.
//

import Foundation

@objc
class AbstractViewControllerSwift: UIViewController, SlideNavigationControllerDelegate {
    func viewDidLoad() {
        super.viewDidLoad()
        setNeedsStatusBarAppearanceUpdate()
        navigationController?.navigationBar.tintColor = UIColor.pinkMercu()
        menuButton.setImage(menuButton.imageView?.image?.withRenderingMode(.alwaysTemplate), for: .normal)
        backButton.setImage(UIImage(named: "chevron_gauche_blanc")?.withRenderingMode(.alwaysTemplate), for: .normal)
    }
    func setGradientBackground() {
        GradientHelper.setGradientOnLayer(view.layer, from: UIColor.white, to: UIColor(hexString: "#F3DCD7"))
    }
    func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(self.showExos), name: "show_exos" as? NSNotification.Name, object: nil)
    }
    func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: "show_exos" as? NSNotification.Name, object: nil)
    }
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool {
        return true
    }


@IBAction func showMenu(_ sender: Any) {
    SlideNavigationController.sharedInstance().toggleLeftMenu()
}
@IBAction func close(_ sender: Any) {
    dismiss(animated: true) {() -> Void in }
}
@IBAction func back(_ sender: Any) {
    if barButtonItemDelegate.responds(to: #selector(self.didTapLeftBarButtonItem)) {
        barButtonItemDelegate.didTapLeftBarButtonItem()
    } else {
        navigationController?.popViewController(animated: true)
    }
}
var preferredStatusBarStyle: UIStatusBarStyle {
    return .lightContent
}
func setMercuNavigationBar() {
    if IS_IPHONE_6 {
        navigationController?.navigationBar.setBackgroundImage(UIImage(named: "nav_mercu_ip6_v3"), for: .default)
    } else {
        navigationController?.navigationBar.setBackgroundImage(UIImage(named: "nav_mercu_ip5_v3"), for: .default)
    }
}

func setRightCloseButtonForModalNavigationController() {
    let closeButton = UIButton(type: .custom)
    closeButton.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
    closeButton.setImage(UIImage(named: "close")?.withRenderingMode(.alwaysTemplate), for: .normal)
    closeButton.addTarget(self, action: #selector(self.closeNavigationController), for: .touchUpInside)
    let closeItem = UIBarButtonItem(customView: closeButton)
    navigationItem?.rightBarButtonItem = closeItem
}
func setLeftCloseButtonForModalNavigationController() {
    let closeButton = UIButton(type: .custom)
    closeButton.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
    closeButton.setImage(UIImage(named: "close")?.withRenderingMode(.alwaysTemplate), for: .normal)
    closeButton.addTarget(self, action: #selector(self.closeNavigationController), for: .touchUpInside)
    let closeItem = UIBarButtonItem(customView: closeButton)
    navigationItem?.leftBarButtonItem = closeItem
}
@objc func closeNavigationController() {
    if closeDelegate.responds(to: #selector(self.didCloseModalViewController)) {
        closeDelegate.didCloseModalViewController()
    }
    dismiss(animated: true) {() -> Void in }
}


func setLeftBarButtonItemWithTitle(_ title: String?) {
    let btn = UIButton(type: .custom)
    btn.frame = CGRect(x: 0, y: 0, width: 80, height: 25)
    btn.setTitle(title, for: .normal)
    btn.setTitleColor(UIColor.white, for: .normal)
    if let aSize = UIFont(name: "SourceSansPro-Regular", size: 17) {
        btn.titleLabel?.font = aSize
    }
    btn.addTarget(self, action: #selector(self.leftTapped), for: .touchUpInside)
    btn.titleLabel?.textAlignment = .left
    btn.contentHorizontalAlignment = .left
    navigationItem?.leftBarButtonItem = UIBarButtonItem(customView: btn)
}
func setRightBarButtonItemWithTitle(_ title: String?) {
    let btn = UIButton(type: .custom)
    btn.frame = CGRect(x: 0, y: 0, width: 80, height: 25)
    btn.setTitle(title, for: .normal)
    btn.setTitleColor(UIColor.white, for: .normal)
    if let aSize = UIFont(name: "SourceSansPro-Regular", size: 17) {
        btn.titleLabel?.font = aSize
    }
    btn.addTarget(self, action: #selector(self.rightTapped), for: .touchUpInside)
    btn.titleLabel?.textAlignment = .right
    btn.contentHorizontalAlignment = .right
    navigationItem?.rightBarButtonItem = UIBarButtonItem(customView: btn)
}
func setRightBarButtonItemWith(_ image: UIImage?) {
    let btn = UIButton(type: .custom)
    btn.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
    btn.setImage(image?.withRenderingMode(.alwaysTemplate), for: .normal)
    btn.addTarget(self, action: #selector(self.rightTapped), for: .touchUpInside)
    btn.contentEdgeInsets = UIEdgeInsetsMake(3, 6, 3, 0)
    navigationItem?.rightBarButtonItem = UIBarButtonItem(customView: btn)
}


func setMenuButtonForNavigationController() {
    let menuBtn = UIButton(type: .custom)
    menuBtn.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
    menuBtn.setImage(UIImage(named: "menu_button")?.withRenderingMode(.alwaysTemplate), for: .normal)
    menuBtn.imageView?.tintColor = UIColor.pinkMercu()
    menuBtn.addTarget(self, action: Selector("showMenu:"), for: .touchUpInside)
    let `left` = UIBarButtonItem(customView: menuBtn)
    `left`.tag = MENU_BUTTON_TAG
    navigationItem?.leftBarButtonItem = `left`
}
func hideMenuButton() {
    if navigationItem?.leftBarButtonItem?.tag == MENU_BUTTON_TAG {
        navigationItem?.leftBarButtonItem = nil
    }
}
func setBackButtonForNavigationController() {
    navigationItem?.hidesBackButton = true
    navigationController?.navigationBar.barTintColor = UIColor.white
    let backButton = UIButton(type: .custom)
    backButton.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
    backButton.setImage(UIImage(named: "chevron_gauche_blanc")?.withRenderingMode(.alwaysTemplate), for: .normal)
    backButton.imageView?.tintColor = UIColor.pinkMercu()
    backButton.addTarget(self, action: #selector(self.popController), for: .touchUpInside)
    let backItem = UIBarButtonItem(customView: backButton)
    navigationItem?.leftBarButtonItem = backItem
}

    
func pop() {
    if barButtonItemDelegate.responds(to: #selector(self.didTapLeftBarButtonItem)) {
        barButtonItemDelegate.didTapLeftBarButtonItem()
    } else {
        navigationController?.popViewController(animated: true)
    }
}
func rightTapped() {
    if barButtonItemDelegate.responds(to: #selector(self.didTapRightBarButtonItem)) {
        barButtonItemDelegate.didTapRightBarButtonItem()
    }
}
func leftTapped() {
    if barButtonItemDelegate.responds(to: #selector(self.didTapLeftBarButtonItem)) {
        barButtonItemDelegate.didTapLeftBarButtonItem()
    }
}
    
    
func setPlayButtonWith(_ delegate: PlayButtonDelegate?) {
    playButtonDelegate = delegate
    let button = UIButton(type: .custom)
    button.tag = PLAY_BUTTON_TAG
    button.frame = CGRect(x: view.frame.size.width / 2 - 25, y: view.frame.size.height - 70, width: 50, height: 50)
    button.setBackgroundImage(UIImage(named: "play_exo_button"), for: .normal)
    button.addTarget(self, action: #selector(self.playButtonTapped), for: .touchUpInside)
    view.subviews.enumerateObjects({(_ obj: Any?, _ idx: Int, _ stop: UnsafeMutablePointer<ObjCBool>?) -> Void in
        if (obj is UIButton) {
            let btn = obj as? UIButton
            if btn?.tag == PLAY_BUTTON_TAG {
                btn?.removeFromSuperview()
                stop = true
            }
        }
    })
    view.addSubview(button)
    playButton = button
}
func setCloseExoButton() {
    let button = UIButton(type: .custom)
    button.frame = CGRect(x: view.frame.size.width / 2 - 25, y: view.frame.size.height - 70, width: 50, height: 50)
    button.setBackgroundImage(UIImage(named: "close_red"), for: .normal)
    button.addTarget(self, action: Selector("dismissModalViewControllerAnimated:"), for: .touchUpInside)
    view.addSubview(button)
}

    
func playButtonTapped() {
    if playButtonDelegate.responds(to: #selector(self.didTapPlayButton)) {
        playButtonDelegate.didTapPlayButton()
    } else {
        showExos()
    }
    //    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Exos" bundle:nil];
    //    ExosIntimateSessionFinishViewController *vc = [sb instantiateViewControllerWithIdentifier:@"ExosIntimateSessionFinishViewController"];
    //    [self.navigationController presentViewController:vc animated:YES completion:nil];
}
func enableSwipeMenu(_ enabled: Bool) {
    let app = UIApplication.shared.delegate as? NativeAppDelegate
    if (app?.window.rootViewController is SlideNavigationController) {
        let nc = app?.window.rootViewController as? SlideNavigationController
        nc?.enableSwipeGesture = enabled
    }
}
func showExos() {
    let app = UIApplication.shared.delegate as? NativeAppDelegate
    app?.exosNavController.popToRootViewController(animated: false)
    if let aController = app?.exosNavController {
        present(aController, animated: true) {() -> Void in }
    }
}
    
    


}
