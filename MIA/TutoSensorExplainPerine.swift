//
//  TutoSensorExplainPerine.swift
//  mia
//
//  Created by Xavier Bohin on 19/04/2018.
//  Copyright © 2018 V-Labs. All rights reserved.
//

import Foundation
import UIKit

class TutoSensorExplainPerine: UIView {
    class func createView() -> TutoSensorExplainPerine? {
        
        var view = Bundle.main.loadNibNamed("TutoSensorExplainPerine", owner: nil, options: nil)?.last as? TutoSensorExplainPerine
        if (view is TutoSensorExplainPerine) {
        }
        return view
        
    }
}
