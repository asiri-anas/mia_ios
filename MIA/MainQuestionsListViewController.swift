//
//  MainQuestionsListViewController.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 02/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit
import EMAccordionTableViewController
import iOS_Slide_Menu


class MainQuestionsListViewController: AbstractViewController, FAQSectionViewDelegate, EMAccordionTableDelegate, ValidatorDelegate {
    
    @IBOutlet var tableView: UITableView!
    
    var categoryID: NSNumber?
    
    @IBOutlet var phLabel: UILabel!
    
    var accordionTVC: EMAccordionTableViewController?
    var questionsList: QuestionsList?
    var sections: NSMutableArray?
    var sectionsViews: NSMutableArray?
    var isSlideStack = false
    var idToFetch: NSNumber?

    override func viewDidLoad() {
        super.viewDidLoad()
        setGradientBackground()
        isSlideStack = navigationController is SlideNavigationController
        navigationItem.title = NSLocalizedString("faq_coach_intime", comment: "").uppercased()
        //[self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"nav_pink"] forBarMetrics:UIBarMetricsDefault];
        idToFetch = 7
        tableView.sectionHeaderHeight = 70
        accordionTVC = EMAccordionTableViewController(table: tableView, with: EMAnimationType.none)
        accordionTVC?.setDelegate(self)
        fetchDatas()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
            setBackButtonForNavigationController()
        
    }
    
    
    override func slideNavigationControllerShouldDisplayLeftMenu() -> Bool {
        return isSlideStack
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIHelper.hideHUD()
    }
    
    
    
    
    // MARK: - Validator delegate
    func showAlert(withTitle title: String?, message: String?) {
        UIAlertView(title: title!, message: message!, delegate: nil, cancelButtonTitle: NSLocalizedString("ok", comment: ""), otherButtonTitles: "").show()
    }
    
    func didTokenRefresh() {
        fetchDatas()
    }
    
    func fetchDatas() {
        tableView.alpha = 0
        phLabel.alpha = 0
        UIHelper.showHUD(withTitle: "", color: UIColor.redMercu()!)
        
        if (ReachabilityManager.instance.isReachable())! {
            MercuClient.instance.getQuestionsForCategoryID(idToFetch, withCompletion: { list, error in
                UIHelper.hideHUD()
                UIView.animate(withDuration: 0.3, animations: {
                    self.tableView.alpha = 1
                })
                print(error?.statusCode)
                if error?.statusCode == nil {
                    print("no error m8, all gud")
                    self.questionsList = list
                    self.reloadDataSource()
                } else {
                    Validator.validateError(error, with: self)
                }
            })
        } else {
            UIHelper.hideHUD()
            UIView.animate(withDuration: 0.3, animations: {
                self.phLabel.alpha = 1
            })
        }
    }
    
    
    func reloadDataSource() {
        sections = NSMutableArray()
        sectionsViews = NSMutableArray()
        
        questionsList?.questionsArray?.enumerateObjects({ question, idx, stop in
            var section = EMAccordionSection()
            section.items = [question]
            accordionTVC?.add(section, initiallyOpened: false)
            sectionsViews?.add(FAQSectionView.createView(withTitle: (question as AnyObject).title, at: idx, delegate: self))
            sections?.add(section)
        })
        //accordionTVC?.defaultOpenedSection = (questionsList?.questionsArray?.count)!
        accordionTVC?.tableView.reloadData()
    }
    
    
    
    
    
    // MARK: - UITableView delegate
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return sectionsViews![section] as? UIView
    }
    
    func didTap(_ sectionView: FAQSectionView?, at index: Int) {
        accordionTVC?.openTheSection(sectionView)
    }
    
    
    // MARK: - EMAccordionTableDelegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      //  if SYSTEM_VERSION_LESS_THAN("8.0") {
      //      return heightForBasicCell(at: indexPath)
      //  } else {
            return UITableViewAutomaticDimension
      //  }
    }
    
    
    func heightForBasicCell(at indexPath: IndexPath?) -> CGFloat {
        var sizingCell: FAQAnswerTableViewCell? = nil
        var onceToken: Int = 0
        if (onceToken == 0) {
            sizingCell = self.tableView.dequeueReusableCell(withIdentifier: "answerCell") as? FAQAnswerTableViewCell
        }
        onceToken = 1
        configureCell(sizingCell, forRowAt: indexPath)
        return calculateHeight(forConfiguredSizingCell: sizingCell)
    }
    
    func configureCell(_ cell: FAQAnswerTableViewCell?, forRowAt indexPath: IndexPath?) {
        
        var question: Question? = questionsList?.questionsArray![(indexPath?.section)!] as! Question
        cell?.setContentWithText(question?.body as! String, andLink: question?.url as! NSString)
        
    }
    
    func calculateHeight(forConfiguredSizingCell sizingCell: UITableViewCell?) -> CGFloat {
        sizingCell?.setNeedsLayout()
        sizingCell?.layoutIfNeeded()
        let size: CGSize? = sizingCell?.contentView.systemLayoutSizeFitting(UILayoutFittingCompressedSize)
        return (size?.height)! + 1.0
        // Add 1.0f for the cell separator height
    }
    
    
    func basicCell(at indexPath: IndexPath?) -> FAQAnswerTableViewCell? {
        var cell: FAQAnswerTableViewCell? = nil
        if let aPath = indexPath {
            cell = tableView.dequeueReusableCell(withIdentifier: "answerCell", for: aPath) as? FAQAnswerTableViewCell
        }
        configureCell(cell, forRowAt: indexPath)
        return cell
    }
    
  
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
    func latestSectionViewOpened(_ sectionView: Any?, withValue value: Bool) {
        let section = sectionView as? FAQSectionView
        section?.selected = value
        if let aSection = section {
            sectionsViews![(section?.index)!] = aSection
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //    static NSString *cellIdentifier = @"answerCell";
        //    FAQAnswerTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        //    if (!cell) {
        //        cell = [[FAQAnswerTableViewCell alloc] init];
        //    }
        //
        //    EMAccordionSection *section = _sections[indexPath.section];
        //    Question *question = section.items[indexPath.row];
        //
        //    [cell setContentWithText:question.body];
        //
        //    return cell;
        return basicCell(at: indexPath)!
    }
    /*
         #pragma mark - Navigation
     
         // In a storyboard-based application, you will often want to do a little preparation before navigation
         - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
         // Get the new view controller using [segue destinationViewController].
         // Pass the selected object to the new view controller.
         }
         */
    // Add ineed of new _categoryID faq to display
    // switch ([_categoryID intValue]) {
    //   case COACH_INTIMATE_TYPE:
    //    break;
    // }
    
    
    
}
