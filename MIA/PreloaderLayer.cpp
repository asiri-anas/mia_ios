//
// Created by kevin on 11/26/15.
//

#include "PreloaderLayer.h"
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include "../NDKHelper/NDKHelper.h"
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#include "NDKHelper.h"
#endif
#include "ActivityService.h"
#include "Constants.h"

USING_NS_CC;

Scene* PreloaderLayer::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();

    // 'layer' is an autorelease object
    auto layer = PreloaderLayer::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

bool PreloaderLayer::init()
{
	if (!LayerGradient::initWithColor(Color4B(253,190,188,255), Color4B(255,255,255,255))) {
	        return false;
	}

	ActivityService::getInstance()->init(this);

    Size visibleSize = Director::getInstance()->getVisibleSize();
    Size winSize = Director::getInstance()->getWinSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

	Value nullValue;
	sendMessageWithParams(ON_PRELOADER_INITIALIZED, nullValue);
    return true;
}
