//
//  AuthFCViewController.m
//  mercu
//
//  Created by Rémi Caroff on 02/07/2015.
//  Copyright (c) 2015 V-Labs. All rights reserved.
//

#import "AuthFCViewController.h"

@interface AuthFCViewController ()

@end

@implementation AuthFCViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"nav_red"] forBarMetrics:UIBarMetricsDefault];
    [self setRightCloseButtonForModalNavigationController];
    [self.navigationItem setTitle:NSLocalizedString(@"fc_max", nil)];
    
    [_textView setText:NSLocalizedString(@"fc_max_text", nil)];
    [_textView setTextContainerInset:UIEdgeInsetsMake(0, 16, 0, 16)];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
