//
// Created by kevin on 11/26/15.
//

#include "Constants.h"
#include "ExerciseLayer.h"
#include "PreloaderLayer.h"
#include "LeafService.h"
#include "Utils.h"
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include "../NDKHelper/NDKHelper.h"
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#include "NDKHelper.h"
#endif
#include <vector>

USING_NS_CC;

using namespace std;

Value m_data;

Scene* ExerciseLayer::createScene(Value data)
{
	m_data = data;
    // 'scene' is an autorelease object
    auto scene = Scene::create();

    // 'layer' is an autorelease object
    auto layer = ExerciseLayer::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

ExerciseLayer::~ExerciseLayer() {
	_eventDispatcher->removeEventListener(_customlistener);
}

bool ExerciseLayer::init()
{
    if (!LayerGradient::initWithColor(Color4B(253,190,188,255), Color4B(255,255,255,255))) {
        return false;
    }

    _visibleSize = Director::getInstance()->getWinSize();
    _originDirector = Director::getInstance()->getVisibleOrigin();

    unserializeData();


    this->setContentSize(Size(_visibleSize.width * (_duration / TIME_BY_SCREEN),this->getContentSize().height));

    _currentTimeStamp = 0.5;


    ActivityService::getInstance()->initLeafWithNode(this);
    ActivityService::getInstance()->setOriginalPattern(_originalPatternPoint);

	AbstractLayer::initWithNameAndDuration(_nameExercise, _duration);

	calculMaxPressureDraw();
    calculMaxDraw();



	convertPointsToView();
	extendsPatterns();
	Color4B colorLayer = Utils::hexToColor4B(_hexColorString);

	auto layerColor = LayerColor::create(colorLayer, _visibleSize.width, _newOrigin.y);

	this->addChild(layerColor, 1);

   actionLabel = Label::createWithTTF("Maintenez", "fonts/SourceSansPro-Regular.otf", 20);
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	actionLabel->setPosition(Vec2(_visibleSize.width/2, _visibleSize.height-33));
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	actionLabel->setPosition(Vec2(_visibleSize.width/2, _visibleSize.height - actionLabel->getContentSize().height / 2 - 10));
#endif

	this->addChild(actionLabel);

	backNode = Node::create();
	_parallaxNode = ParallaxNode::create();
	this->addChild(_parallaxNode, 1);
	//backNode->setPosition(_visibleSize.width, _newOrigin.y);

	Vec2 speed = Vec2(1,1);
	drawGraph(speed, colorLayer);
	_parallaxNode->setPosition(_parallaxNode->getPosition() +  Vec2(1,0) * (TIME_BY_SCREEN - 1) * _ratioWidth);
	_parallaxNode->setContentSize(Size(_visibleSize.width * (_duration / TIME_BY_SCREEN),_parallaxNode->getContentSize().height));

	_leafSprite = Sprite::create("drawable/feuille_big_white.png");
	_leafSprite->setPosition(Vec2(_visibleSize.width/ 2, _newOrigin.y));
	_leafSprite->setScale(0.3);
	this->addChild(_leafSprite, 2);
    
    
    
	int maxPressure = _maxPressure * 1.2;
	_customlistener = EventListenerCustom::create(KEY_POST_PRESSURE_EVENT, [=](EventCustom* event) {
		int* pressurePtr = static_cast<int*>(event->getUserData());
		int pressure = *((int*)pressurePtr);
		Vector<FiniteTimeAction *> fta;
		float height = getHeightLeaf(pressure, _maxPressure, _minPressure);
		Vec2 pos = Vec2(_leafSprite->getPosition().x, height);
		auto moveAction = MoveTo::create(0.25, pos);
		fta.pushBack(moveAction);
		log("pressure: %d", pressure);

		if (!AbstractLayer::isInCountdown() && !AbstractLayer::isPaused()){
			ActivityService::activityStep steps;
			if (pressure >= maxPressure)
				steps.pressure = maxPressure;
			else if (pressure <= _minPressure)
				steps.pressure = _minPressure;
			else
				steps.pressure = pressure;
			steps.timestamp = _currentTimeStamp;
			double targetMesured = ActivityService::getInstance()->getTargetPressure(_currentTimeStamp);
			steps.target = targetMesured; //* m_ratio;
			ActivityService::getInstance()->addActivitySteps(&steps);
			int direction =ActivityService::getInstance()->getTextIndication(_currentTimeStamp);
			if (direction == 0) {
				actionLabel->setString("Maintenez");
			} else if (direction > 0) {
				actionLabel->setString("Contractez");
			} else {
				actionLabel->setString("Relâchez");
			}

			_currentTimeStamp += STEP_FREQUENCY;
		}

		if (!AbstractLayer::isPaused()) {
			auto seq = Sequence::create(fta);
            _leafSprite->stopAllActions();
			_leafSprite->runAction(seq);
		}

	});
	 _eventDispatcher->addEventListenerWithFixedPriority(_customlistener, 1);

	startTimer();

	Value nullValue;
	sendMessageWithParams(ON_EXERCISE_INITIALIZED, nullValue);
    return true;
}

void ExerciseLayer::pauseEventListener()
{
	_eventDispatcher->pauseEventListenersForTarget(this, true);
}

void ExerciseLayer::resumeEventListener()
{
	_eventDispatcher->resumeEventListenersForTarget(this, true);
}

void ExerciseLayer::drawGraph(Vec2 speed, Color4B color) {
	auto backGroundNode = DrawNode::create();
	Color4F graphColor(color);

	node = DrawNode::create();

	std::vector<Vec2> simplified;
	Vec2 lastVec(-1, -1);
	for (int i = 0; i < _scaleVectorExtended.size(); ++i) {
		if (_scaleVectorExtended[i] != lastVec) {
			lastVec = _scaleVectorExtended[i];
			simplified.push_back(lastVec);
		}
	}
	/*nodeBase = DrawNode::create();
	nodeBase->setLineWidth(10);
	for (int var = 1; var < simplified.size(); ++var) {
		Vec2 p0 = simplified[var - 1];
		Vec2 p1 = simplified[var];
		nodeBase->drawLine(p0, p1, Color4F::GREEN);
	}
	nodeBase->setName("nodeBase");
*/
	Vec2 pOldEnd(Vec2::ZERO);
	for (int i = 2; i < simplified.size(); ++i) {
		Vec2 p0 = simplified[i - 2];
		Vec2 p1 = simplified[i - 1];
		Vec2 p2 = simplified[i];
		float valueY = calculYvalue(p0, p1, true);
		Vec2 pStart((4 * (p1.x - p0.x)) / 5 + p0.x, valueY);

		valueY = calculYvalue(p1, p2, false);
		Vec2 pEnd((p2.x - p1.x) / 5 + p1.x, valueY);

		if (pOldEnd == Vec2::ZERO) {
			pOldEnd = pEnd;
			//node->drawQuadBezier(p0, p0, pStart, 100, graphColor);
			drawQuadBezier(p0, p0, pStart, 100, graphColor);
		} else {
			//node->drawQuadBezier(pOldEnd, pOldEnd, pStart, 100, graphColor);
			drawQuadBezier(pOldEnd, pOldEnd, pStart, 100, graphColor);
			pOldEnd = pEnd;
		}
		//node->drawQuadBezier(pStart, p1, pEnd, 100, graphColor);
		drawQuadBezier(pStart, p1, pEnd, 100, graphColor);
	}
	//backNode->addChild(node, 0, 100);
	//backNode->addChild(nodeBase, 0, 200);
	_parallaxNode->addChild(node, 0, speed, Vec2(0, _newOrigin.y));
	//_parallaxNode->addChild(nodeBase, 0, speed, Vec2(0, _newOrigin.y));
}

float ExerciseLayer::calculYvalue(Vec2 p0, Vec2 p1, bool isFirstPair) {
	float numberOfSeg = (float) VALUE_NUMBER_QUAD;
	float valueY = (p1.y - p0.y) / numberOfSeg;

	if (valueY == 0) {
		valueY += p0.y;
	} else if (valueY > 0) {
		if (isFirstPair) {
			float numberToAdd = (float) valueY * VALUE_RATIO_QUAD;
			valueY = numberToAdd;
		}
		valueY += p0.y;
	} else {
		valueY *= -1;
		if (!isFirstPair) {
			float numberToAdd = (float) valueY * VALUE_RATIO_QUAD;
			valueY = numberToAdd;
		}
		valueY += p1.y;
	}
	return valueY;
}

void ExerciseLayer::convertPointsToView() {

	_ratioWidth = _visibleSize.width / TIME_BY_SCREEN;
	_ratioHeight =  (m_maxHeightGraphToDraw - _newOrigin.y) / (_maxPressure - _minPressure);

	log("_maxPressure: %f, _minPressure: %f, _ratioHeight: %f",_maxPressure,  _minPressure, _ratioHeight);
	_scalePatternVector.reserve(_originalPatternPoint.size());
	for (int i = 0; i < _originalPatternPoint.size(); i++) {
		Vec2 pt = _originalPatternPoint[i];
		pt.x = pt.x * _ratioWidth;
		log("pt.y: %f", pt.y);
		pt.y = pt.y * _ratioHeight - _minPressure * _ratioHeight;
		log("pt.y ratio: %f", pt.y);
		_scalePatternVector.push_back(pt);
	}
}

void ExerciseLayer::unserializeData() {
	if (!m_data.isNull() && m_data.getType() == Value::Type::MAP) {
		ValueMap valueMap = m_data.asValueMap();
		ValueVector valueVector = valueMap[KEY_DATAS].asValueVector();
		_maxPressure = 0;
		_minPressure = -1;
		size_t size = valueVector.size();
		for (int i = 0; i < size; i++) {
			ValueVector pairData = valueVector[i].asValueVector();
			for (int j = 0; j < pairData.size(); j++) {
				ValueMap rawData = pairData[j].asValueMap();
				float time = rawData[KEY_TIME].asFloat();
				float pressure = rawData[KEY_PRESSURE].asFloat();
				if (pressure > _maxPressure) {
					_maxPressure = pressure;
				}
				if (_minPressure == -1) {
					_minPressure = pressure;
				} else if (pressure < _minPressure) {
					_minPressure = pressure;
				}
				Vec2 vec(time, pressure);
				_originalPatternPoint.push_back(vec);

			}
		}
		log("minPressure %f: ", _minPressure);
		_nameExercise = valueMap[KEY_LABEL].asString();
		_exerciseNumber = valueMap[KEY_EXERCISE_NUMBER].asString();
		_timePattern = _originalPatternPoint[_originalPatternPoint.size() - 1].x;
		_duration = valueMap[KEY_DURATION].asInt();
		_hexColorString = valueMap[KEY_COLOR].asString();
	} else {
	}
}

void ExerciseLayer::extendsPatterns() {
	int numberOfRepeat = _duration / _timePattern + 10;

	_originalVectorExtended.reserve(numberOfRepeat * _originalPatternPoint.size());
	std::vector<Vec2> tmpVectorOriginal = _originalPatternPoint;
	std::vector<Vec2> tmpVectorScaled = _scalePatternVector;
	float maxValue = _scalePatternVector[_scalePatternVector.size() - 1].x;
	for (int i = 0; i < numberOfRepeat; i++) {
		for (int j = 0; j < tmpVectorOriginal.size(); j++) {
			tmpVectorOriginal[j].x = _originalPatternPoint[j].x + i*_timePattern;
		}
		_originalVectorExtended.insert(_originalVectorExtended.end(), tmpVectorOriginal.begin(), tmpVectorOriginal.end());
		for (int j = 0; j < tmpVectorScaled.size(); j++) {
			tmpVectorScaled[j].x = _scalePatternVector[j].x + i*maxValue;
		}
		_scaleVectorExtended.insert(_scaleVectorExtended.end(), tmpVectorScaled.begin(), tmpVectorScaled.end());
	}
}

void ExerciseLayer::drawQuadBezier(const Vec2 &origin, const Vec2 &control, const Vec2 &destination, unsigned int segments, const Color4F &color)
{
    Vec2* vertices = new (std::nothrow) Vec2[segments + 1];
    if( ! vertices )
        return;

    float t = 0.0f;
    for(unsigned int i = 0; i < segments; i++)
    {
        vertices[i].x = powf(1 - t, 2) * origin.x + 2.0f * (1 - t) * t * control.x + t * t * destination.x;
        vertices[i].y = powf(1 - t, 2) * origin.y + 2.0f * (1 - t) * t * control.y + t * t * destination.y;
        t += 1.0f / segments;
    }
    vertices[segments].x = destination.x;
    vertices[segments].y = destination.y;

    for (int i = 1; i <= segments; ++i) {

		Vec2 polygon[4];
    	Vec2 p0 = vertices[i - 1];
		Vec2 p1 = vertices[i];
		polygon[0] = Vec2(p0.x, 0);
		polygon[1] = Vec2(p0.x, p0.y);
		polygon[2] = Vec2(p1.x, p1.y);
		polygon[3] = Vec2(p1.x, 0);
		//node->drawLine(Vec2(p0.x, 0), Vec2(p0.x, _visibleSize.height), Color4F::GREEN);

		//log("p0x: %f, p1x: %f, diff: %f", p0.x, p1.x, p1.x - p0.x);

		//node->drawSolidPoly(polygon, 4, color);
		/*
		node->drawTriangle(polygon[0], polygon[1], polygon[3], color);

		node->drawTriangle(polygon[1], polygon[2], polygon[3], color);
		*/
		node->drawPolygon(polygon, 4, color, 0, color);
    }


    //node->drawPoly(vertices, segments+1, false, color);


    CC_SAFE_DELETE_ARRAY(vertices);
}
