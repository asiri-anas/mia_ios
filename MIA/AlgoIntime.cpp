//#include <android/log.h>
#define LOG_TAG "AlgoIntime"
//#define  ALOG(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
//
//  AlgoIntime.cpp
//  AlgoIntime
//
//  Created by ARIAS Johann on 25/11/2015.
//  Copyright Ã‚Â© 2015 UrgoTech. All rights reserved.
//

#include "AlgoIntime.h"
#include <vector>
#include <math.h>
#include <algorithm>

std::vector<AlgoIntime::pairRawData> AlgoIntime::getActivitySkeleton(AlgoIntime::activityDescription activityDesc, AlgoIntime::calibration calibration, AlgoIntime::calibration breathCalibration){
    
    // variables for all exercices
    int restPressure = breathCalibration.calibrationValue;
    double plateauPressure(10.);
    AlgoIntime::pairRawData temp;
    double maxPressure = calibration.maxPressure;
    static double ecart = maxPressure - restPressure;
    if(ecart < 50.) ecart = 50.;
    //ALOG("Maybe one day it will : %f", ecart);
    double puissance(0.);
    
    //DEEP_STRENGTHENING Variables
    //duration variables for DS
    double restDurationDS(5.);
    double transitionDurationDS(2.);
    
    //Pressure variables for DS
    int maxLevel(10);
    double minPlateauPressure(10.);
    double maxPlateauPressure(25.);
    int pressureLevels = floor(maxLevel/3);
    
    //DEEP_STRENGTHENING_BIS Variables
    //duration variables for DSBis
    double restDurationDSBis(5.);
    double transitionDurationDSBis(2.);
    double stepDurationDSBis(2.);
    double maxPlateauDurationDSBis(8.);
    double minPlateauDurationDSBis(4.);
    double stepPressure;
    
    //pressure variables for DSBis
    double minPlateauPressureDSBis(10.);
    double maxPlateauPressureDSBis(20.);
    
    //SUPERFICIAL_STRENGHTENING
    
    double stepNumberSS(10);
    
    //duration variables for SS
    double restDurationSS(3.);
    double stepDurationSS(1.5);
    
    //pressure variables for SS
    double stepPressureSS;
    double minStepPressureSS(10.);
    double maxStepPressureSS(25);
    
    //RELAXATION
    int numberOfStepsR(3);
    //duration variables for relaxation
    double restDurationR(2.);
    double transitionDurationR(2.5);
    double stepDurationR(2.);
    
    //pressure variables
    double maxPlateauPressureR(20.);
    double minPlateauPressureR(10.);
    
    //DEEP_STRENGHTENING_SAW
    
    const int numberOfTeeth(4);
    
    //duration parameters
    const double restDurationDSS(5.);
    const double transitionDurationDSS(2.);
    const double teethDuration(1.);
    
    //pressure parameters
    const double minTeethPressure(10);
    const double maxTeethPressure(25);
    double teethPressure(10.);
    
    //LATCHING
    
    //duration parameters
    const double restDurationLatching(8.);
    double halfPeakDurationLatching(2.);
    
    //pressure parameters
    const double minPeakPressureL(20.);
    const double maxPeakPressureL(30.);
    double peakPressure(20);
    
    std::vector<AlgoIntime::pairRawData> points;
    
    //ALOG("baseline %d", restPressure);
    
    switch(activityDesc.name)
    {
        case DEEP_STRENGTHENING_FLAT: {
            
            temp.startData.time = 0;
            temp.startData.pressure = restPressure;
            temp.endData.time = restDurationDS;
            temp.endData.pressure = restPressure;
            
            points.push_back(temp);
            
            temp.startData.time = temp.endData.time;
            temp.startData.pressure = temp.endData.pressure;
            
            temp.endData.time = temp.startData.time + transitionDurationDS;
            
            if (activityDesc.lvl > 8) {
                puissance = ecart * 0.7;
            } else if (activityDesc.lvl > 4) {
                puissance = ecart * 0.6;
            } else {
                puissance = ecart * 0.5;
            }
            
            
            plateauPressure = restPressure + puissance;//restPressure + minPlateauPressure + (ceil(double(activityDesc.lvl)/3)-1)*(maxPlateauPressure-minPlateauPressure)/double(pressureLevels);
            
            temp.endData.pressure = plateauPressure;
            
            points.push_back(temp);
            
            temp.startData.time = temp.endData.time;
            temp.startData.pressure = temp.endData.pressure;
            int plateauTime = 0;
            
            if (activityDesc.lvl % 4 == 1) {
                plateauTime = 6;
            } else {
                if (activityDesc.lvl % 4 == 0) {
                    plateauTime = 20;
                } else {
                    plateauTime = restDurationDS * (activityDesc.lvl % 4);
                }
            }
            
            temp.endData.time = temp.startData.time + plateauTime;
            /*if (activityDesc.lvl <= 5) {
             temp.endData.time = temp.startData.time + (restDurationDS * activityDesc.lvl) + restDurationDS;
             } else {
             if (activityDesc.lvl % 5 == 0) {
             temp.endData.time = temp.startData.time + (restDurationDS * 5) + restDurationDS;
             } else {
             temp.endData.time = temp.startData.time + (restDurationDS * (activityDesc.lvl % 5)) + restDurationDS;
             }
             }
             
             /*if(activityDesc.lvl % 3 == 0){
             temp.endData.time = temp.startData.time + 8;
             }
             else if(activityDesc.lvl % 3 == 1){
             temp.endData.time = temp.startData.time + 4;
             }
             else{
             temp.endData.time = temp.startData.time + 6;
             }*/
            
            temp.endData.pressure = plateauPressure;
            
            points.push_back(temp);
            
            temp.startData.time = temp.endData.time;
            temp.startData.pressure = temp.endData.pressure;
            temp.endData.time = temp.startData.time + transitionDurationDS;
            temp.endData.pressure = restPressure;
            
            points.push_back(temp);
            
            temp.startData.time = temp.endData.time;
            temp.startData.pressure = temp.endData.pressure;
            temp.endData.time = temp.startData.time + plateauTime - restDurationDS;
            /*if (activityDesc.lvl <= 5) {
             temp.endData.time = temp.startData.time + activityDesc.lvl * restDurationDS;
             } else {
             if (activityDesc.lvl % 5 == 0) {
             temp.endData.time = temp.startData.time + (restDurationDS * 5) + 4;
             } else {
             temp.endData.time = temp.startData.time + (restDurationDS * (activityDesc.lvl % 5)) + 4;
             }
             }*/
            temp.endData.pressure = restPressure;
            
            points.push_back(temp);
            
            break;
        }
        case SUPERFICIAL_STRENGTHENING:
        case SUPERFICIAL_WITH_TEXT:{
            
            temp.startData.time = 0;
            temp.startData.pressure = restPressure;
            temp.endData.time = temp.startData.time + restDurationSS;
            temp.endData.pressure = temp.startData.pressure;
            
            points.push_back(temp);
            
            /*if(activityDesc.lvl%2 == 1){
             stepDurationSS = 1.;
             }
             else{
             stepDurationSS = 0.75;
             }*/
            
            puissance = ecart * 0.75;
            
            if (activityDesc.lvl < 3) {
                stepNumberSS = 3;
            } else if(activityDesc.lvl == 3){
                stepNumberSS = 5;
            }else{
                stepNumberSS = 8;
            }
            
            pressureLevels = (double(maxLevel)/ 2.)-1.;
            stepPressureSS = restPressure + puissance;//restPressure + minStepPressureSS + (ceil(double(activityDesc.lvl)/2)-1)*(maxStepPressureSS-minStepPressureSS)/double(pressureLevels);
            
            
            for(int i = 0; i < stepNumberSS; i++){
                
                temp.startData.time = temp.endData.time;
                temp.startData.pressure = temp.endData.pressure;
                
                temp.endData.time = temp.startData.time + stepDurationSS;
                temp.endData.pressure = stepPressureSS;
                
                points.push_back(temp);
                
                temp.startData.time = temp.endData.time;
                temp.startData.pressure = temp.endData.pressure;
                
                temp.endData.time = temp.startData.time + stepDurationSS;
                temp.endData.pressure = restPressure;
                
                points.push_back(temp);
                
                if(i < (stepNumberSS - 1)){
                    temp.startData.time = temp.endData.time;
                    temp.startData.pressure = temp.endData.pressure;
                    if(activityDesc.lvl == 1){
                        temp.endData.time = temp.startData.time + 2;
                    }else{
                        temp.endData.time = temp.startData.time + 1;
                    }
                    temp.endData.pressure = temp.startData.pressure;
                    
                    points.push_back(temp);
                }
            }
            
            int restBetweenPeaks = 13;
            
            if(activityDesc.lvl == 1){
                restBetweenPeaks = 13;
            }else if(activityDesc.lvl == 2){
                restBetweenPeaks = 11;
            }else if(activityDesc.lvl == 3){
                restBetweenPeaks = 17;
            }else if(activityDesc.lvl == 4){
                restBetweenPeaks = 26;
            }
            
            temp.startData.time = temp.endData.time;
            temp.startData.pressure = temp.endData.pressure;
            temp.endData.time = temp.startData.time + restBetweenPeaks - restDurationSS;
            temp.endData.pressure = temp.startData.pressure;
            
            points.push_back(temp);
            
            break;
        }
            
        case LATCHING_STRENGTHENING : {
            temp.startData.time = 0;
            temp.startData.pressure = restPressure;
            temp.endData.time = restDurationDSS;
            temp.endData.pressure = restPressure;
            
            points.push_back(temp);
            
            temp.startData.time = temp.endData.time;
            temp.startData.pressure = temp.endData.pressure;
            
            peakPressure = restPressure + ecart;
            
            temp.endData.time = temp.startData.time + halfPeakDurationLatching/4.;
            temp.endData.pressure = restPressure + (peakPressure - restPressure)/6;
            
            points.push_back(temp);
            //definition of the 3 points used for parabola
            double x1 = temp.endData.time;
            double y1 = temp.endData.pressure;
            
            double x2 = x1 + halfPeakDurationLatching;
            double y2 = peakPressure;
            
            double x3 = x2 + halfPeakDurationLatching;
            double y3 = temp.endData.pressure;
            
            //calculation of the parabolas parameters (ax^2+bx/c)
            double b = ((y2-y1) * x1*x1 + y3 * (x2*x2 - x1*x1) - (y2-y1) * x3*x3 - y1 * (x2*x2 - x1*x1)) / ((x2-x1) * x1*x1 - x1 * (x2*x2 - x1*x1) + x3 * (x2*x2 - x1*x1) - (x2 - x1) * x3*x3);
            
            double a = (-b * (x2-x1) + (y2-y1)) / (x2*x2 - x1*x1);
            
            double c = y3 - a * x3*x3 - b * x3;
            
            double y;
            for(double x=x1;x<x3;x=x+0.1){
                
                y = a*x*x + b*x + c;
                
                temp.startData.time = temp.endData.time;
                temp.startData.pressure = temp.endData.pressure;
                
                temp.endData.time = x;
                temp.endData.pressure = y;
                
                points.push_back(temp);
                
            }
            
            temp.startData.time = temp.endData.time;
            temp.startData.pressure = temp.endData.pressure;
            
            temp.endData.time = x3;
            temp.endData.pressure = y3;
            
            points.push_back(temp);
            
            temp.startData.time = temp.endData.time;
            temp.startData.pressure = temp.endData.pressure;
            
            temp.endData.time = temp.startData.time + halfPeakDurationLatching/4.;
            temp.endData.pressure = restPressure;
            
            points.push_back(temp);
            
            temp.startData.time = temp.endData.time;
            temp.startData.pressure = temp.endData.pressure;
            temp.endData.time = temp.startData.time + 5;
            temp.endData.pressure = temp.startData.pressure;
            
            points.push_back(temp);
            
            temp.startData.time = temp.endData.time;
            temp.startData.pressure = temp.endData.pressure;
            temp.endData.time = temp.startData.time + 2.5;
            temp.endData.pressure = peakPressure;
            
            points.push_back(temp);
            
            temp.startData.time = temp.endData.time;
            temp.startData.pressure = temp.endData.pressure;
            temp.endData.time = temp.startData.time + 2 * activityDesc.lvl;
            temp.endData.pressure = temp.startData.pressure;
            
            points.push_back(temp);
            
            temp.startData.time = temp.endData.time;
            temp.startData.pressure = temp.endData.pressure;
            temp.endData.time = temp.startData.time + 2.5;
            temp.endData.pressure = restPressure;
            
            points.push_back(temp);
            
            temp.startData.time = temp.endData.time;
            temp.startData.pressure = temp.endData.pressure;
            temp.endData.time = temp.startData.time + 2 * activityDesc.lvl;
            temp.endData.pressure = temp.startData.pressure;
            
            points.push_back(temp);
            
            break;
        }
            
            
        case LATCHING:
        case LATCHING_WITH_TEXT : {
            
            
            temp.startData.time = 0;
            temp.startData.pressure = restPressure;
            temp.endData.time = restDurationDSS;
            temp.endData.pressure = restPressure;
            
            points.push_back(temp);
            
            
            temp.startData.time = temp.endData.time;
            temp.startData.pressure = temp.endData.pressure;
            
            
            //Shen: 100% de la puissance
            peakPressure = restPressure + ecart;//restPressure + (maxPeakPressureL-minPeakPressureL)/(maxLevel-1)*activityDesc.lvl + minPeakPressureL - (maxPeakPressureL-minPeakPressureL)/(maxLevel-1);
            
            
            if (activityDesc.lvl == 1) {
                temp.endData.time = temp.startData.time + halfPeakDurationLatching/4.;
                temp.endData.pressure = restPressure + (peakPressure - restPressure)/6;
                
                points.push_back(temp);
                //definition of the 3 points used for parabola
                double x1 = temp.endData.time;
                double y1 = temp.endData.pressure;
                
                double x2 = x1 + halfPeakDurationLatching;
                double y2 = peakPressure;
                
                double x3 = x2 + halfPeakDurationLatching;
                double y3 = temp.endData.pressure;
                
                //calculation of the parabolas parameters (ax^2+bx/c)
                double b = ((y2-y1) * x1*x1 + y3 * (x2*x2 - x1*x1) - (y2-y1) * x3*x3 - y1 * (x2*x2 - x1*x1)) / ((x2-x1) * x1*x1 - x1 * (x2*x2 - x1*x1) + x3 * (x2*x2 - x1*x1) - (x2 - x1) * x3*x3);
                
                double a = (-b * (x2-x1) + (y2-y1)) / (x2*x2 - x1*x1);
                
                double c = y3 - a * x3*x3 - b * x3;
                
                double y;
                for(double x=x1;x<x3;x=x+0.1){
                    
                    y = a*x*x + b*x + c;
                    
                    temp.startData.time = temp.endData.time;
                    temp.startData.pressure = temp.endData.pressure;
                    
                    temp.endData.time = x;
                    temp.endData.pressure = y;
                    
                    points.push_back(temp);
                    
                }
                
                
                temp.startData.time = temp.endData.time;
                temp.startData.pressure = temp.endData.pressure;
                
                temp.endData.time = x3;
                temp.endData.pressure = y3;
                
                points.push_back(temp);
                
                temp.startData.time = temp.endData.time;
                temp.startData.pressure = temp.endData.pressure;
                
                temp.endData.time = temp.startData.time + halfPeakDurationLatching/4.;
                temp.endData.pressure = restPressure;
                
                points.push_back(temp);
            } else {
                temp.endData.time = temp.startData.time + 2.5;
                temp.endData.pressure = peakPressure;
                
                points.push_back(temp);
                
                temp.startData.time = temp.endData.time;
                temp.startData.pressure = temp.endData.pressure;
                temp.endData.time = temp.startData.time + 2 * (activityDesc.lvl - 1);
                temp.endData.pressure = peakPressure;
                
                points.push_back(temp);
                
                temp.startData.time = temp.endData.time;
                temp.startData.pressure = temp.endData.pressure;
                
                temp.endData.time = temp.startData.time + 2.5;
                temp.endData.pressure = restPressure;
                
                points.push_back(temp);
            }
            
            temp.startData.time = temp.endData.time;
            temp.startData.pressure = temp.endData.pressure;
            temp.endData.time = temp.startData.time + 2 * (activityDesc.lvl - 1);
            temp.endData.pressure = temp.startData.pressure;
            
            points.push_back(temp);
            
            break;
        }
            
        case SLACKENING: {
            
            temp.startData.time = 0;
            temp.startData.pressure = restPressure * 0.95;
            temp.endData.time = restDurationR;
            temp.endData.pressure = temp.startData.pressure;
            
            points.push_back(temp);
            
            temp.startData.time = temp.endData.time;
            temp.startData.pressure = temp.endData.pressure;
            temp.endData.time = temp.startData.time + transitionDurationR;
            
            plateauPressure = restPressure + ecart;
            
            temp.endData.pressure = plateauPressure;
            
            points.push_back(temp);
            
            temp.startData.time = temp.endData.time;
            temp.startData.pressure = temp.endData.pressure;
            temp.endData.time = temp.startData.time + stepDurationR;
            temp.endData.pressure = plateauPressure;
            
            points.push_back(temp);
            
            int slackeningDuration = 0;
            if (activityDesc.lvl == 1) {
                slackeningDuration = 10;
            }
            if (activityDesc.lvl == 2) {
                slackeningDuration = 7;
            }
            if (activityDesc.lvl == 3) {
                slackeningDuration = 4;
            }
            if (activityDesc.lvl == 4) {
                slackeningDuration = 2;
            }
            if (activityDesc.lvl == 5) {
                slackeningDuration = 2;
            }
            
            temp.startData.time = temp.endData.time;
            temp.startData.pressure = temp.endData.pressure;
            temp.endData.time = temp.startData.time + slackeningDuration;
            temp.endData.pressure = restPressure * 0.95;
            
            points.push_back(temp);
            
            temp.startData.time = temp.endData.time;
            temp.startData.pressure = temp.endData.pressure;
            temp.endData.time = temp.startData.time + slackeningDuration - restDurationR;
            temp.endData.pressure = temp.startData.pressure;
            
            points.push_back(temp);
            
            break;
        }
            
        case BREATH: {
            
            break;
        }
            
        case CALIBRATION: {
            
            break;
        }
        case MESSAGE: {
            
            break;
        }
    }
    
    for(int i=0; i<points.size() ;i++){
        points[i].startData.pressure = ((points[i].startData.pressure - restPressure)) + restPressure;
        points[i].endData.pressure = ((points[i].endData.pressure - restPressure)) + restPressure;
    }
    return points;
}

AlgoIntime::activityResult AlgoIntime::getResult(std::vector<activityStep> activity){
    
    AlgoIntime::activityResult results;
    double score(0);
    int percentage(0);
    int hearts(0);
    bool levelUp;
    int maxScore(0);
    double prct_score(0);
    int default_pressure(activity[0].target);
    double note(0);
    
    std::vector<int> scores;
    double Itarget = 0;
    double Ipressure = 0;
    double baseline = activity[0].target;
    
    for (int i = 0; i < activity.size(); i++) {
        if (activity[i].target > baseline) {
            while(activity[i].target > baseline) {
                Itarget += activity[i].target - baseline;
                Ipressure += std::max(0.0, activity[i].pressure - baseline);
                i++;
            }
            scores.push_back(std::max(0.0, (Itarget - fabs(Itarget - Ipressure)) / Itarget) * 100);
            //ALOG("Scores : %f", std::max(0.0, (Itarget - fabs(Itarget - Ipressure)) / Itarget) * 100);
            Itarget = 0;
            Ipressure = 0;
        }
    }
    
    int total = 0;
    
    for (int i = 0; i < scores.size(); i++) {
        total += scores[i];
    }
    
    //ALOG("Here, %d", total);
    
    /*std::vector<double> tab_coeff;
     
     //Test de la derniÃƒÂ¨re valeur timestamp dans activity
     //Si supÃƒÂ©rieur Ãƒ  1m30,
     //Exercice de 2 minutes, sinon exercice de 1 minute.
     
     if(activity[activity.size()-1].timestamp > 90){
     maxScore = 200;
     }else{
     maxScore = 100;
     }
     
     //Boucle de calcul des coefficients de tangentes de la courbe de target pour modif de la mÃƒÂ©thode de calcul des scores
     //calcul du minimum de la courbe target ÃƒÂ©galement
     
     for(int j=0; j<activity.size(); j++){
     if(activity[j].target < default_pressure)
     default_pressure = activity[j].target;
     
     if(j != activity.size()){
     //TEST DIVISION PAR 0
     //unreachable condition
     if(activity[j+1].timestamp - activity[j].timestamp != 0)
     tab_coeff.push_back((activity[j+1].target - activity[j].target) / (activity[j+1].timestamp - activity[j].timestamp));
     else
     tab_coeff.push_back(0);
     }else{
     tab_coeff.push_back(tab_coeff[j-1]);
     }
     }
     
     //boucle de calcul du score
     //intÃƒÂ©gration de la zone de tolÃƒÂ©rance (tangente et intensitÃƒÂ© de target)
     //Calcul de la note max atteignable pour la courbe donnÃƒÂ©e, simultanÃƒÂ©ment.
     //i.e. note pondÃƒÂ©rÃƒÂ©e par les valeurs neutres de la courbe target
     for(int i=0; i<activity.size(); i++){
     if(activity[i].target == default_pressure){
     note = note + 0.01;
     }else{
     note++;
     }
     
     if(fabs(activity[i].pressure - activity[i].target) < 1 + ((activity[i].target - default_pressure) * 0.05) + (0.5 * fabs(tab_coeff[i]))){
     if(activity[i].target != default_pressure)
     score++;
     else
     score=score+0.01;
     }
     }
     
     //    cout << "NEW SCORE ENGRANGE (SUR " << note << ") : " << score << endl;
     // TEST DIVISION PAR 0
     // unreachable condition
     if(note != 0)
     prct_score = double(score) / double(note);
     else
     prct_score = 0;
     
     score = ceil(prct_score * maxScore);
     percentage = ceil(prct_score*100);
     */
    
    //    cout << "NEW SCORE CALC : " << score << endl;
    //    cout << "NEW PERCENTAGE CALC : " << percentage << endl;
    percentage = ceil(total / scores.size());
    
    if(percentage > 90){
        hearts = 6;
    }
    else if(percentage > 75){
        hearts = 5;
    }
    else if(percentage > 60){
        hearts = 4;
    }
    else if(percentage > 45){
        hearts = 3;
    }
    else if(percentage > 30){
        hearts = 2;
    }
    else if(percentage > 15){
        hearts = 1;
    }
    else {
        hearts = 0;
    }
    
    if(percentage > 80){
        levelUp = true;
    }
    else{
        levelUp = false;
    }
    
    score = percentage;
    maxScore = 100;
    
    results.score = score;
    results.percentage = percentage;
    results.hearts = hearts;
    results.levelUp = levelUp;
    results.maxScore = maxScore;
    
    return results;
    
}

int AlgoIntime::getLeafLevel(int sessionsDone, int totalSessions){
    
    int leafLevel(0);
    double temp;
    
    if (totalSessions > 0){
        temp = 100*sessionsDone/totalSessions;
    }
    else{
        temp = 0;
    }
    
    if (sessionsDone == 0){
        
        leafLevel = 0;
    }
    else if (temp < 25){
        
        leafLevel = 1;
    }
    else if (temp < 50){
        
        leafLevel = 2;
    }
    else if (temp < 75){
        
        leafLevel = 3;
    }
    else if (temp >= 75){
        
        leafLevel = 4;
    }
    
    return leafLevel;
}

int AlgoIntime::getSessionHearts(std::vector<int> hearts){
    
    int total = 0;
    int sessionHearts(0);
    
    if (hearts.size() > 0){
        
        for(int i=0 ; i < hearts.size() ; i++){
            total = total + hearts[i];
        }
        
        sessionHearts = ceil(double(total)/double(hearts.size()));
        
    }
    return sessionHearts;
}

AlgoIntime::calibration AlgoIntime::getCalibration(std::vector<int> pressures){
    
    AlgoIntime::calibration calibrationPressures;
    int min = pressures[0];
    int max = pressures[0];
    int somme = 0;
    int moyenne = 0;
    
    for(int i =1;i<pressures.size();i++){
        
        if(pressures[i] < min){
            min = pressures[i];
        }
        
        if(pressures[i] > max){
            max = pressures[i];
        }
        somme = somme + pressures[i];
    }
    
    if(pressures.size() != 0){
        moyenne = floor(somme/pressures.size());
    }
    
    calibrationPressures.minPressure = min;
    calibrationPressures.maxPressure = max;
    calibrationPressures.calibrationValue = floor(moyenne)+1;
    
    return calibrationPressures;
}

int AlgoIntime::getTonicity(std::vector<int> pressures){
    //__android_log_print(ANDROID_LOG_ERROR, "ALGO - Nb values for tonicity:", "%d", pressures.size());
    if(pressures.size() > 59){
        std::sort(pressures.begin(), pressures.end());
        return pressures.at(static_cast<int>((float)pressures.size() * (float)0.75) - 1);
    }
    return 0;
    
}

int AlgoIntime::getBaseline(std::vector<int> pressures){
    //__android_log_print(ANDROID_LOG_ERROR, "ALGO - Nb values for baseline:", "%d", pressures.size());
    if(pressures.size() > 59){
        std::sort(pressures.begin(), pressures.end());
        return pressures.at((static_cast<int>((float)pressures.size() * (float)0.25) - 1));
    }
    return 0;
}
