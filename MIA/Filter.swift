//
//  Filter.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 04/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation


class Filter: NSObject, NSCoding {
    
    /// The property name of the managedObject we want to filter on
    var attribute = ""
    /// The value of the attribute This can be a NSNumber, NSString, NSDate, or a NSArray of two objects.
    var value: Any?
    /// a string representing the comparator available in NSPredicate formatting : ">=", "<", "=="...
    /// If the value is a NSArray, it should be "BETWEEN"
    var relation = ""
    /*!
     @brief The type of the filter :
     @param FILTER_TYPE_DATE value : 1
     @param FILTER_TYPE_SPORT value : 2
     @param FILTER_TYPE_FOOD value : 3
     @param FILTER_TYPE_ABSORBED_CALORIES value : 4
     @param FILTER_TYPE_BURNT_CALORIES value : 5
     @param FILTER_TYPE_STRESS_FACTOR value : 6
     @param FILTER_TYPE_CONSISTENCY value : 7
     */
    var type: NSNumber?
    /*!
     @brief Constructs Filter object.
     @param attribute The property name of the managedObject we want to filter on
     @param relation a string representing the comparator available in NSPredicate formatting : ">=", "<", "=="... If the value is a NSArray, it should be "BETWEEN".
     @param value The value of the attribute This can be a NSNumber, NSString, NSDate, or a NSArray of two objects.
     */
    init (_attribute: String?, _relation: String?, _value: Any?) {
        
        attribute = _attribute!
        relation = _relation!
        value = _value
        
    }
    
    
    func encode (with aCoder: NSCoder) {
        aCoder.encode(attribute, forKey: "attribute")
        aCoder.encode(value, forKey: "value")
        aCoder.encode(relation, forKey: "relation")
    }
    
    required init?(coder aDecoder: NSCoder) {
        attribute = aDecoder.decodeObject(forKey: "attribute") as! String
        value = aDecoder.decodeObject(forKey: "value")
        relation = aDecoder.decodeObject(forKey: "relation") as! String
    }
    
    
}
