//
//  MainIntimateWeeklyGoalViewController.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 22/06/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit

class MainIntimateWeeklyGoalViewController: AbstractViewController, BarButtonItemDelegate {
    func didTapLeftBarButtonItem() {
        leftTapped()
    }
    
    
    @IBOutlet var slider: CustomSlider!
    @IBOutlet var countLabel: UILabel!
    var intimateCoach: IntimateCoach?
    
    @IBAction func sliderValueChanged(_ sender: Any) {
        
        var value = Float((Int((slider.value + 0.5) / 1) * 1))
        slider.setValue(value, animated: true)
        countLabel.text = String(format: NSLocalizedString("n_sessions_a_week", comment: ""), value, (value > 1) ? "s" : "")
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLeftCloseButtonForModalNavigationController()
        setRightBarButtonItemWith(UIImage(named: "validate_white"))
        navigationItem.title = NSLocalizedString("sessions_a_week", comment: "").uppercased()
        super.barButtonItemDelegate = self
        

        do {
            var results = try CDManager.instance!.managedObjectContext!.fetch(IntimateCoach.intimateCoachFetchRequest())
            print("goals", "fetch intimateCoach")
            for singleData in results {
                if singleData.intimateState != nil {
                    print("goals", "intimateState fetched:", singleData.intimateState!.intimateProgramType)
                }
                intimateCoach = singleData
            }
        } catch {
            print("Failed")
        }
        
        
        if(intimateCoach?.intimateGoal?.goalPerWeek == nil)
        {
            //intimateCoach?.intimateGoal?.goalPerWeek = 3
            //print(intimateCoach?.intimateGoal)
        }
        //slider.setValue(Float((intimateCoach?.intimateGoal?.goalPerWeek)!) , animated: false)
        slider.setValue(Float(3) , animated: false)
        
        //countLabel.text = String(format: NSLocalizedString("n_sessions_a_week", comment: ""), (intimateCoach?.intimateGoal!.goalPerWeek!)!, (Int((intimateCoach?.intimateGoal?.goalPerWeek)!) > 1) ? "s" : "")
 
        
        
        
    }
    
    
    func didTapRightBarButtonItem() {
        //IntimateManager.shared()?.updateIntimateGoals(withSessionsCount: slider.value as NSNumber)
        //intimateCoach?.intimateGoal?.goalPerWeek = slider.value as NSNumber
         
         //print("goals", "slider", intimateCoach?.intimateGoal?.goalPerWeek)
         //CDManager.instance!.saveContext()
        
        dismiss(animated: true)
        
        
    }
    
    
    /*
     #pragma mark - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
     // Get the new view controller using [segue destinationViewController].
     // Pass the selected object to the new view controller.
     }
     */
    
    
}
