//
//  GraphValuesExtractorProtocol.swift
//  mia
//
//  Created by ASIRI Anas on 17/09/2018.
//  Copyright © 2018 V-Labs. All rights reserved.
//

import Foundation
/*!
 @brief This protocol must be implemented by any NSManagedObject that will be used as graph data source.
 @discussion
 • @c -(NSDate*)getDate;
 • @c -(id)getValueForType:(graphType)type;
 @see GraphManager.h
 */
protocol GraphValuesExtractorProtocol: NSObjectProtocol {
    /// Should return the date used to build graph x axis.
    func getDate() -> Date?
    /// Return here the value you want to display for the returned date and for the graphType passed as parameter.
    func getValueFor(_ type: NSNumber) -> Any?
}
