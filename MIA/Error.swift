//
//  Error.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 23/08/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
class Error: NSObject {
    ///The HTTP status code of error
    var statusCode: NSNumber?
    ///The message delivered by the server
    var message: String?
    /*!
     @param message The message delivered by the server
     @param statusCode The HTTP status code of error
     */
    init(message: String?, andStatusCode statusCode: NSNumber?) {
        
        super.init()
        self.message = message
        self.statusCode = statusCode
        
        
    }
}
