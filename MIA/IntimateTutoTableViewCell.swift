//
//  IntimateTutoTableViewCell.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 25/06/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit

class IntimateTutoTableViewCell: UITableViewCell {
    

    @IBOutlet weak var titleLabel: UILabel!
    
    
    override func awakeFromNib() {
    // Initialization code
}
    override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    // Configure the view for the selected state
}
    
func setContentWith(_ titleString: String?) {

    titleLabel.text = titleString
    titleLabel.textColor = UIColor.lightGray
    
}
    
}
