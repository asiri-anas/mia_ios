//
//  MainIntimateFiltersViewController.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 13/09/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit



class MainIntimateFiltersViewController: AbstractViewController, BarButtonItemDelegate {
    func didTapRightBarButtonItem() {
        
    }
    
    
    
    @IBOutlet var datesButton: FilterButton!
    @IBOutlet var exosButton: FilterButton!
    @IBOutlet var programButton: FilterButton!
    @IBOutlet var heartsButton: FilterButton!
    var dataSource: NSArray?
    var selectedFilters: NSMutableArray?
  
    
    var allDates: NSMutableArray?
    var selectedDates: NSMutableArray?
    var selectedExoTypes: NSMutableArray?
    var selectedProgramTypes: NSMutableArray?
    var selectedHearts: NSNumber?
    var filterTypes: NSMutableArray?
    
 
    override func viewDidLoad() {
    super.viewDidLoad()
    setGradientBackground()
    selectedDates = NSMutableArray()
    allDates = NSMutableArray()
        filterTypes = [AnyHashable](repeating: 0, count: 4) as! NSMutableArray
        navigationItem.title = NSLocalizedString("filter_my_sessions", comment: "").uppercased()
    setBackButtonForNavigationController()
    super.barButtonItemDelegate = self
    NotificationCenter.default.addObserver(self, selector: #selector(self.dateFilterSelected(_:)), name: NSNotification.Name("intimate_date_filter"), object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(self.exosFilterSelected(_:)), name: NSNotification.Name("intimate_exos_filter"), object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(self.programFilterSelected(_:)), name: NSNotification.Name("intimate_program_filter"), object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(self.heartsFilterSelected(_:)), name: NSNotification.Name("intimate_hearts_filter"), object: nil)
        dataSource?.enumerateObjects({ obj, idx, stop in
            if let aDate = (obj as! IntimateDataSourceProtocol).getCreationDate() {
                allDates?.insert(aDate, at: 0)
        }
    })
        /*selectedFilters?.enumerateObjects({ filter, idx, stop in
            if (filter as! Filter).type ?? 0 == Constants.FILTER_TYPE_DATE {
                if let anObject = (filter as AnyObject).value.last {
                selectedDates.append(anObject)
            }
        }
    })*/
    updateButtonsUIAndDataSource()
}
    
    
func updateButtonsUIAndDataSource() {
    var hasDates = false
    var hasExos = false
    var hasProgram = false
    var hasHearts = false
    selectedFilters?.enumerateObjects({ filter, idx, stop in
        switch (filter as! Filter).type ?? 0 {
        case 0://FILTER_TYPE_DATE:
                hasDates = true
                if let aValue = (filter as! Filter).value {
                    selectedDates?.add(aValue)
                }
        case 1://FILTER_TYPE_INTIMATE_EXO:
                hasExos = true
                if let aValue = (filter as! Filter).value {
                    selectedExoTypes?.add(aValue)
                }
        case 2://FILTER_TYPE_INTIMATE_PROGRAM:
                hasProgram = true
                if let aValue = (filter as! Filter).value {
                    selectedProgramTypes?.add(aValue)
                }
        case 3://FILTER_TYPE_INTIMATE_HEARTS:
                hasHearts = true
                selectedHearts = (filter as! Filter).value as! NSNumber
            default:
                break
        }
    })
    if hasDates {
        filterTypes?.add(Constants.FILTER_TYPE_DATE)
        datesButton.isSelected = true
    }
    if hasExos {
        filterTypes?.add(Constants.FILTER_TYPE_INTIMATE_EXO)
        exosButton.isSelected = true
    }
    if hasProgram {
        filterTypes?.add(Constants.FILTER_TYPE_INTIMATE_PROGRAM)
        programButton.isSelected = true
    }
    if hasHearts {
        filterTypes?.add(Constants.FILTER_TYPE_INTIMATE_HEARTS)
        heartsButton.isSelected = true
    }
    datesButton.setImageWithName("calendar_picto")
}
   
    @objc func dateFilterSelected(_ notification: Notification?) {
        selectedDates = notification?.userInfo!["selected_dates"] as! NSMutableArray
        if selectedDates?.count == 0 {
        removeFilterFromDataSource(forType: Constants.FILTER_TYPE_DATE)
            datesButton.isSelected = false
    } else {
            datesButton.isSelected = true
        addFilter(forType: Constants.FILTER_TYPE_DATE)
    }
    datesButton.setImageWithName("calendar_picto")
}
    @objc func exosFilterSelected(_ notification: Notification?) {
        selectedExoTypes = notification?.userInfo!["selected_type"] as! NSMutableArray
        if selectedExoTypes?.count == 0 {
        removeFilterFromDataSource(forType: Constants.FILTER_TYPE_INTIMATE_EXO)
        selectedExoTypes = nil
            exosButton.isSelected = false
    } else {
        addFilter(forType: Constants.FILTER_TYPE_INTIMATE_EXO)
            exosButton.isSelected = true
    }
}
    
  
    @objc func programFilterSelected(_ notification: Notification?) {
        selectedProgramTypes = notification?.userInfo!["selected_program_type"] as! NSMutableArray
        if selectedProgramTypes?.count == 0 {
        removeFilterFromDataSource(forType: Constants.FILTER_TYPE_INTIMATE_PROGRAM)
        selectedProgramTypes = nil
            programButton.isSelected = false
    } else {
        addFilter(forType: Constants.FILTER_TYPE_INTIMATE_PROGRAM)
            programButton.isSelected = true
    }
}
    @objc func heartsFilterSelected(_ notification: Notification?) {
        selectedHearts = notification?.userInfo!["hearts"] as! NSNumber
    if selectedHearts == 0 {
        removeFilterFromDataSource(forType: Constants.FILTER_TYPE_INTIMATE_HEARTS)
        heartsButton.isSelected = false
    } else {
        addFilter(forType: Constants.FILTER_TYPE_INTIMATE_HEARTS)
        heartsButton.isSelected = true
    }
}
   
func removeFilterFromDataSource(forType filterType: Int) {
    if (filterTypes?.contains(filterType))! {
        filterTypes = filterTypes?.filter({ ($0) as AnyObject !== (filterType) as AnyObject }) as! NSMutableArray
    }
    var toRemove = [AnyHashable]()
    selectedFilters?.enumerateObjects({ filter, idx, stop in
        /*if (filter as! Filter).type ?? 0 == filterType {
            if let aFilter = filter {
                toRemove.append(aFilter)
            }
        }*/
    })
    //selectedFilters = selectedFilters?.filter(using: { !toRemove.contains($0) })
}
    
 
func addFilter(forType filterType: Int) {
    removeFilterFromDataSource(forType: filterType)
    filterTypes?.add(filterType)
    if filterType == Constants.FILTER_TYPE_DATE {
        selectedDates?.enumerateObjects({ date, idx, stop in
            //let dateFilter = Filter(_attribute: "createdAt", _relation: "BETWEEN", _value: [DateHelper.getStartOf(date), DateHelper.getEndOf(date)])
            //dateFilter.type = Constants.FILTER_TYPE_DATE
            //selectedFilters.append(dateFilter)
        })
    }
    if filterType == Constants.FILTER_TYPE_INTIMATE_EXO {
        selectedExoTypes?.enumerateObjects({ activityType, idx, stop in
            let typeFilter = Filter(_attribute: "intimateActivityType.type", _relation: "==", _value: (activityType as! IntimateActivityType
                ).type)
            typeFilter.type = Constants.FILTER_TYPE_INTIMATE_EXO as NSNumber
            selectedFilters?.add(typeFilter)
        })
    }
    if filterType == Constants.FILTER_TYPE_INTIMATE_PROGRAM {
        selectedProgramTypes?.enumerateObjects({ programType, idx, stop in
            let progFilter = Filter(_attribute: "intimateProgramType.typeID", _relation: "==", _value: (programType as! IntimateProgramType).typeID)
            progFilter.type = Constants.FILTER_TYPE_INTIMATE_PROGRAM as NSNumber
            selectedFilters?.add(progFilter)
        })
    }
    if filterType == Constants.FILTER_TYPE_INTIMATE_HEARTS {
        if selectedHearts != nil {
            let heartsFilter = Filter(_attribute: "hearts", _relation: "==", _value: selectedHearts)
            heartsFilter.type = Constants.FILTER_TYPE_INTIMATE_HEARTS as NSNumber
            selectedFilters?.add(heartsFilter)
        }
    }
}
    
    
    @IBAction func datesButtonTapped(_ sender: Any) {
        
            performSegue(withIdentifier: "showDateFilter", sender: nil)
    }
    
    
    @IBAction func exosButtonTapped(_ sender: Any) {
 
            performSegue(withIdentifier: "showExosFilters", sender: nil)
    }
    
    
    @IBAction func programButtonTapped(_ sender: Any) {
 
            performSegue(withIdentifier: "showProgramFilter", sender: nil)
    }
    
    
    @IBAction func heartsButtonTapped(_ sender: Any) {

            performSegue(withIdentifier: "showHeartsFilter", sender: nil)
    }
    
    
    func didTapLeftBarButtonItem() {
        NotificationCenter.default.post(name: NSNotification.Name("intimate_filters_updated"), object: nil, userInfo: ["filters": selectedFilters, "types": filterTypes])
        dismiss(animated: true) {
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name("intimate_date_filter"), object: nil)
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name("intimate_exos_filter"), object: nil)
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name("intimate_program_filter"), object: nil)
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name("intimate_hearts_filter"), object: nil)
        }
    }
    
    
  
// MARK: - Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if (segue.identifier == "showDateFilter") {
        let midfvc = segue.destination as? MainIntimateDatesFilterViewController
        midfvc?.selectedDates = selectedDates
        midfvc?.allDates = allDates
    }
    if (segue.identifier == "showExosFilters") {
        let miefvc = segue.destination as? MainIntimateExosFilterViewController
        miefvc?.dataSource = dataSource
        miefvc?.selectedTypes = selectedExoTypes
    }
    if (segue.identifier == "showProgramFilter") {
        let mipfvc = segue.destination as? MainIntimateProgramFilterViewController
        mipfvc?.dataSource = dataSource
        mipfvc?.selectedProgramTypes = selectedProgramTypes
    }
    if (segue.identifier == "showHeartsFilter") {
        let mihfvc = segue.destination as? MainIntimateHeartsFilterViewController
        mihfvc?.selectedHearts = selectedHearts
    }
}
    
    
    
    
}
