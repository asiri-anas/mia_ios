//
//  Constants.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 02/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation

struct Constants {
    
    static let  COACH_INTIMATE_TYPE = 7
    static let  SPACE_INTIMATE = 5
    static let  SPACE_EXOS = 6
    static let  MENU_BUTTON_TAG = 5
    static let  DAY_ID_SUNDAY = 1
    static let  DAY_ID_MONDAY = 2
    static let  DAY_ID_TUESDAY = 3
    static let  DAY_ID_WEDNESDAY = 4
    static let  DAY_ID_THURSDAY = 5
    static let  DAY_ID_FRIDAY = 6
    static let  DAY_ID_SATURDAY = 7
    static let  BREATH_ACQUISITION_FREQUENCY = 2
    static let  BREATH_COUNT_DOWN = 3
    static let  PLAY_BUTTON_TAG = 84765
    static let  BT_FITNESS_SERVICE = "0000FED0-494C-4F47-4943-544543480000"
    static let  BT_FITNESS_SETTINGS_CHARACTERISTIC = "0000FED2-494C-4F47-4943-544543480000"
    static let  BT_FITNESS_DATA_CHARACTERISTIC = "0000FED1-494C-4F47-4943-544543480000"
    static let  CLIENT_ID = "1_1s8t0y2m5g4k4cwcg4080kw8s4o4ow0koo4og4g08wks44ogk0"
    static let  CLIENT_SECRET = "23irfm179casc4w0osc4kw8k0s0k008o0c4w8w08ccgsg0gsg4"
    static let  ADVICE_CATEGORY_FAQ_ID = 4
    static let  ADVICE_CATEGORY_INTIMATE_ID = 7
    static let  FILTER_BY_DAY = 1
    static let  FILTER_BY_WEEK = 2
    static let  FILTER_BY_MONTH = 3
    static let  FILTER_TYPE_DATE = 1
    static let  FILTER_TYPE_INTIMATE_EXO = 8
    static let  FILTER_TYPE_INTIMATE_PROGRAM = 9
    static let  FILTER_TYPE_INTIMATE_HEARTS = 10
    
    /*!
     @typedef enum graphType
     
     @brief  A struct about the Graph type.
     
     @discussion
     All types of graphs that are currently available.
     
     @param vrc Used to type the VRC graph in ExosBreathDetailsViewController.
     @param consistencyScoreEvolution  Used to type the Consistency Score Evolution graph in ExosBreathDetailsViewController.
     @param consistencyStackedBar Used to type the Consistency Evolution stacked bar graph in ZenConsistencyEvolutionWidget.
     @param consistencyLine Used to type the Consistency Evolution line graph in ZenConsistencyEvolutionWidget.
     
     @param stepsNumber Used to type the steps number line graph in FitnessStepsGraphWidget.
     @param activityDuration Used to type the bar graph in FitnessActivityLevelWidget.
     @param absorbedCalories Used to type the line graph in FitnessCaloriesEvolutionWidget.
     @param burntCalories Used to type the line graph in FitnessCaloriesEvolutionWidget.
     
     @param nightGraph Used to type the horizontal stacked bar in SleepNightGraphWidget.
     @param nightBarGraph Used to type the multiple bar graph in SleepBarGraphWidget.
     @param nightDurationsGraph Used to type the bar graph in SleepNightsDurationWidget.
     @param nightDurationsGoalsGraph Used to type the line graph in SleepNightsDurationWidget.
     @param timeToFallAsleepGraph Used to type the bar graph in SleepTimeToFallAsleepGraphWidget.
     @param timeToFallAsleepGoalsGraph Used to type the line graph in SleepTimeToFallAsleepGraphWidget.
     @param awakeningsNumberGraph Used to type the line graph representing the evolution of awakenings in SleepAwakeningsEvolutionGraphWidget.
     @param awakeningsNumberGoalsGraph Used to type the line graph representing the awakenings goals in SleepAwakeningsEvolutionGraphWidget.
     */
    
    static let intimateEvolution: NSNumber =  0
    
    static let DEEP_STRENGTHENING_FLAT: NSNumber = 0;
    static let LATCHING_WITH_TEXT: NSNumber = 1;
    static let SUPERFICIAL_WITH_TEXT: NSNumber = 2;
    static let SUPERFICIAL_STRENGTHENING: NSNumber = 3;
    static let LATCHING: NSNumber = 4;
    static let SLACKENING: NSNumber = 5;
    static let LATCHING_STRENGTHENING: NSNumber = 6;
    static let BREATH: NSNumber = 111;
    static let CALIBRATION: NSNumber = 222;
    static let MESSAGE: NSNumber = 333;
    
    static let SENSOR_TUTO_TYPE_CONNECT: NSNumber =  0
    static let SENSOR_TUTO_TYPE_RELOAD: NSNumber =  1
    static let SENSOR_TUTO_TYPE_INSERT: NSNumber =  2
    static let SENSOR_TUTO_TYPE_HOW_TO_USE: NSNumber =  3
    static let SENSOR_TUTO_TYPE_ALL: NSNumber =  4
    
    /***** Cocos constants *****/
    static let SETUP_VIEW_EXERCISE = "setupViewExercise"
    static let SETUP_VIEW_CALIBRATION = "setupViewCalibration"
    static let SETUP_VIEW_BREATH = "setupViewBreath"
    static let POST_PRESSURE = "postPressure"
    static let ON_PRELOADER_INITIALIZED = "onPreloaderInitialized"
    static let ON_EXERCISE_INITIALIZED = "onExerciseInitialized"
    static let ON_EXERCISE_FINISHED = "onExerciseFinished"
    static let ON_BACK_PRESSED = "onBackPressed"
    static let END_SCENE = "endScene"
    static let TOGGLE_PAUSE = "togglePause"
    static let KEY_COLOR = "color"
    static let KEY_DATAS = "datas"
    static let KEY_DURATION = "duration"
    static let KEY_LAST_PRESSURE_MAX = "lastPressureMax"
    static let KEY_TIME = "time"
    static let KEY_PRESSURE = "pressure"
    static let KEY_TARGET = "target"
    static let KEY_LABEL = "label"
    static let KEY_EXERCISE_NUMBER = "exerciseNumber"
    static let KEY_ACTIVITY_STEPS = "activitySteps"
    static let KEY_POST_PRESSURE_EVENT = "postPressureEvent"
    static let KEY_MAX_PRESSURE_CALIBRATION = "maxPressureCalibration"
    static let PRELOADER_LAYER_SELECTORS = "PreloaderLayerSelectors"
    static let EXERCISE_LAYER_SELECTORS = "ExerciseLayerSelectors"
    static let ABSTRACT_LAYER_SELECTORS = "AbstractLayerSelectors"
    static let ACTIVITY_SERVICE_SELECTORS = "ActivityServiceSelectors"
    static let LEAF_SERVICE_SELECTORS = "LeafServiceSelectors"
    static let TIME_BY_SCREEN = 10
    static let MAX_PRESSURE_AIRBEE = 600
    static let PERCENT_PRESSURE_ABOVE = 0.2
    static let COUNTDOWN = 5
    static let OFFSET_AIRBEE = 0
    static let MAX_PRESSURE_ALGO = 40
    static let VALUE_RATIO_QUAD = 4
    static let VALUE_NUMBER_QUAD = 5
    static let STEP_FREQUENCY = 0.13
    static let DURATION_INSPIRATION_BREATH = 4
    static let DURATION_EXPIRATION_BREATH = 6
    static let DURATION_CYCLE_EXERCICE_CALIBRATION = 5
    static let BREATH_EXERCISE_TYPE = 1
    /***** end *****/
    static let MAX_CALIBRATION = 100
    static let FREE_EXO_DURATION : NSNumber =  120
    static let PROGRAM_EXO_DURATION : NSNumber =  60
    static let INTIMATE_BREATH_DURATION : NSNumber =  20
    static let CALIBRATION_DURATION : NSNumber =  30
    static let SURVEY_WIDGET_TYPE_DATE : NSNumber =  0
    static let SURVEY_WIDGET_TYPE_SELECT : NSNumber =  1
    static let SURVEY_WIDGET_TYPE_SLIDER : NSNumber =  2
    static let DEFAULT_INTIMATE_GOALS: NSNumber =  3
    static let FLURRY_API_KEY = "RZ7WH69XXZ785RBGBPC5"
    static let CONNECTION_TUTO_READ = "connectionTutoRead"
    /***** Kegel *****/
    static let DEVICE_STATUS_CONNECT = "device_status_connect"
    static let DEVICE_STATUS_DISCONNECT = "device_status_disconnect"
    static let DEVICE_BATTERY_UPDATED = "device_battery_updated"
    static let KEY_MODEL_NUMBER = "modelNumber"
    static let KEY_MODEL_TYPE = "modelType"
    
    
    enum SensorTutoType : Int {
        case sensor_TUTO_TYPE_CONNECT
        case sensor_TUTO_TYPE_RELOAD
        case sensor_TUTO_TYPE_INSERT
        case sensor_TUTO_TYPE_HOW_TO_USE
        case sensor_TUTO_TYPE_ALL
    }
    
    @objc enum activityName: Int {
        case DEEP_STRENGTHENING_FLAT    // 0
        case LATCHING_WITH_TEXT        // 1
        case SUPERFICIAL_WITH_TEXT      // 2
        case SUPERFICIAL_STRENGTHENING  // 3
        case LATCHING                   // 4
        case  SLACKENING                // 5
        case LATCHING_STRENGTHENING     // 6
        case BREATH                     // 111
        case CALIBRATION                // 222
        case MESSAGE                    // 333
    };
    
    

    
}


