//
//  IntimateSessionsScheduler.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 20/07/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation

/// This manager is responsible of the CRUD of FutureIntimateSession objects, and the associated notifications workflow.
class IntimateSessionsScheduler: NSObject {
    
    
    /// A singleton instance of IntimateSessionsScheduler object.
    class func sharedManager() -> IntimateSessionsScheduler? {
        
         var _sharedManager: IntimateSessionsScheduler? = nil
         var onceToken: Int = 0
        if (onceToken == 0) {
            _sharedManager = IntimateSessionsScheduler()
        }
            onceToken = 1
        return _sharedManager
        
    }
    
    
    /// Will create and return a new instance of FutureIntimateSession object, based on the given date.
    func createFutureIntimateSession(for date: Date?) -> FutureIntimateSession? {
        
        var sess = FutureIntimateSession.create()
        sess?.fireDate = date
        CDManager.instance?.saveContext()
        return sess
        
    }
    
    /*
    /// Will fetch and return an array of id<IntimateDataSourceProtocol> object, based on the given IntimateState object.
    func fetchDataSource(for intimateState: IntimateState?) -> NSArray {
        
        var filter1 = Filter(_attribute: "intimateProgramType.typeID", _relation: "==", _value: intimateState?.intimateProgramType?.typeID)
        var filter2 = Filter(_attribute: "finishedAt", _relation: ">", _value: intimateState?.programStart)
        var dataSource = NSMutableArray()
        var sessions = IntimateManager.shared()?.fetchIntimateSessions(withFilters: [filter1, filter2], withCount: 0)
        var futureSessions = fetchFutureIntimateSessions()
        dataSource.add(futureSessions)
        (futureSessions as! NSArray).enumerateObjects({ futureSession, idx1, stop1 in
            var startDate = DateHelper.getStartOf((futureSession as AnyObject).fireDate)
            var endDate = DateHelper.getEndOf((futureSession as AnyObject).fireDate)
            var sessionToAdd: IntimateSession? = nil
            (sessions as! NSArray).enumerateObjects({ session, idx2, stop2 in
                if DateHelper.isDate(session?.finishedAt, inRangeFirstDate: startDate, last: endDate) {
                    if let aSession = session {
                        dataSource[idx1] = aSession
                    }
                    stop2 = true
                } else {
                    if idx2 + 1 == sessions?.count {
                        sessionToAdd = session as! IntimateSession
                    }
                }
            })
            if sessionToAdd != nil {
                if let anAdd = sessionToAdd {
                    if !dataSource.contains(anAdd) {
                        if let anAdd = sessionToAdd {
                            dataSource.add(anAdd)
                        }
                    }
                }
            }
        })
        var ordered = (dataSource as NSArray).sortedArray(comparator: { obj1, obj2 in
            return (obj1 as AnyObject).getCreationDate().compare(obj2?.getCreationDate()) == .orderedDescending
            })
        return ordered
        
    }
    
    
    /// Will fetch and returns all FutureIntimateSession objects.
    func fetchFutureIntimateSessions() -> [Any]? {
        
        var fetchRequest = NSFetchRequest()
        var entity = NSEntityDescription.entity(forEntityName: "FutureIntimateSession", in: CDManager.shared().managedObjectContext)
        fetchRequest.entity = entity
        var predicate = NSPredicate(format: "fireDate > %@", Date())
        fetchRequest.predicate = predicate
        var sd = NSSortDescriptor(key: "fireDate", ascending: false)
        fetchRequest.sortDescriptors = [sd]
        var error: Error?
        var result = try? CDManager.shared().managedObjectContext.fetch(fetchRequest)
        if error != nil {
            return nil
        }
        return result
        
    }
    
    
    /// Will schedule the next notification, basing on the created FutureIntimateSession objects.
    func scheduleNextSession() {
        
        UIApplication.shared.cancelAllLocalNotifications()
        var futSess = fetchFutureIntimateSessions().reverseObjectEnumerator().allObjects
        // Ordre croissant par date
        (futSess as NSArray).enumerateObjects({ fs, idx, stop in
            if self.schedule(fs) {
                stop = true
            }
        })
        
    }
    
    
    /*!
     @brief Will update the given FutureIntimateSession object with the given date.
     @param session The FutureIntimateSession object to update.
     @param date The date that wil replace the FutureIntimateSession fireDate property.
     */
    func update(_ session: FutureIntimateSession?, with date: Date?) -> FutureIntimateSession? {
        
        session.fireDate = date
        CDManager.shared().saveContext()
        return session
        
    }
    
    
    
func schedule(_ futureSession: FutureIntimateSession?) -> Bool {
    DebugLog("FUTURE INTIMATE SESSION : %@", futureSession?.fireDate)
    var result = false
    if futureSession != nil {
        let cal = Calendar.current
        var comps: DateComponents? = nil
        if let aDate = futureSession?.fireDate {
            comps = cal.components([.day, .month, .year, .hour, .minute, .timeZone], from: aDate)
        }
        comps?.hour = 9
        comps?.minute = 0
        var dailyFireDate: Date? = nil
        if let aComps = comps {
            dailyFireDate = cal.date(from: aComps)
        }
        let notif = UILocalNotification()
        notif.soundName = "default"
        notif.timeZone = NSTimeZone.local
        // Cas particulier où la session est programmée entre 9:00 et 9:30, ou que la séance est avant 9:00 (on ne programme pas la notification de rappel de 9:00).
        if (futureSession?.fireDate.hour == 9 && futureSession?.fireDate.minute ?? 0 >= 0 && futureSession?.fireDate.minute ?? 0 <= 30) || futureSession?.fireDate.hour ?? 0 < 9 {
            DebugLog("CAS PARTICULIER")
            notif.alertBody = NSLocalizedString("intimate_notification_body_15_min", comment: "")
            notif.fireDate = futureSession?.fireDate.date(byAddingMinutes: -15)
            result = true
        } else {
            // Si la prochaine session est aujourd'hui
            if futureSession?.fireDate.isToday != nil {
                DebugLog("IS TODAY")
                // Avec les 15 minutes de moins, sommes-nous toujours au delà de l'interval de 30min à partir de maintenant ?
                if futureSession?.fireDate.date(byAddingMinutes: -15).compare(Date().addingMinutes(30)) == .orderedDescending {
                    DebugLog("CONDITION 30 MIN OK")
                    notif.alertBody = NSLocalizedString("intimate_notification_body_15_min", comment: "")
                    notif.fireDate = futureSession?.fireDate.date(byAddingMinutes: -15)
                    result = true
                } else {
                    DebugLog("CONDITION 30 MIN NOT OK")
                }
            } else {
                DebugLog("NOT TODAY")
                notif.alertBody = String(format: NSLocalizedString("intimate_notification_body_9h", comment: ""), DateHelper.timeFormattedString(fromDate: futureSession?.fireDate))
                notif.fireDate = dailyFireDate
                DebugLog("DAILY FIRE DATE : %@", dailyFireDate)
                result = true
            }
        }
        if result {
            DebugLog("NEXT SCHEDULED FIREDATE : %@", notif.fireDate)
            UIApplication.shared.scheduleLocalNotification(notif)
        }
    }
    return result
}
    */
    
    
}
