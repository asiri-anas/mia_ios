//
//  SessionParamTableViewCell.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 17/07/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit

protocol ScrollCellDelegate: NSObjectProtocol {
    
    func scrollView(_ scrollView: UIScrollView?, didScrollTo index: Int)
    
}

class SessionParamTableViewCell: UITableViewCell {
    
    @IBOutlet var timeScrollView: UIScrollView!
    
    @IBOutlet var customLabel: UILabel!
    
    @IBOutlet var customImageView: UIImageView!
    
    @IBOutlet var detailLabel: UILabel!
    
    var currentIndex: Int = 0
    
    weak var delegate: ScrollCellDelegate?
    
    func setContentWithTitle(_ title: String?, image: UIImage?, labels: NSArray) {
        
        customImageView.image = image
        customLabel.text = title
        
        labels.enumerateObjects({ text, idx, stop in
            var label = UILabel(frame: CGRect(x: CGFloat(timeScrollView.frame.size.width * CGFloat(idx)), y: 0, width: timeScrollView.frame.size.width, height: timeScrollView.frame.size.height))
            label.text = text as! String
            if let aSize = UIFont(name: "SourceSansPro-Regular", size: 14) {
                label.font = aSize
            }
            label.textAlignment = .center
            label.textColor = UIColor.darkGrayTextColorMercu()
            timeScrollView.addSubview(label)
        })
        
    }
    
    
    func setContentWithTitle(_ title: String?, image: UIImage?, detailText: String?) {
     
    customLabel.text = title
    detailLabel.text = detailText
    customImageView.image = image
    customLabel.sizeToFit()
    
    layoutIfNeeded()
        
    }
    
}
