//
//  ExosIntimateLockingViewController.m
//  mymercurochromeË
//
//  Created by Rémi Caroff on 02/12/2015.
//  Copyright © 2015 V-Labs. All rights reserved.
//

#import <AFNetworking/AFHTTPRequestOperationManager.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <EMAccordionTableViewController/EMAccordionTableViewController.h>
#import <FSCalendar/FSCalendar.h>
#import <iOS_Slide_Menu/SlideNavigationController.h>
#import <TTTAttributedLabel/TTTAttributedLabel.h>
#import "platform/ios/CCEAGLView-ios.h"
#import "cocos2d.h"
#import "ExosCocosViewController.h"
#import "IntimateActivityHandler.h"
#import "IOSNDKHelper.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "mia-Swift.h"

@class CocosHandler;

@interface ExosCocosViewController () <IntimateActivityHandlerDelegate>

@property (nonatomic, strong) CCEAGLView *eaglView;
@property (nonatomic, strong) IntimateActivityHandler *activity;
@property (nonatomic, strong) IntimateActivityBase *nextActivity;
@property (nonatomic, strong) IntimateCalibrationStructure *currentCalibrationStructure;
@property (weak, nonatomic) IBOutlet UILabel *counter_label;


@property (nonatomic, strong) NSMutableArray *activitiesLevelUp;

@property (nonatomic) BOOL isBacking;
@property (nonatomic) BOOL alreadyEnded;
@property (nonatomic) BOOL isBreathDone;
@property (nonatomic) NSTimer *counterTimer;
@property (nonatomic) NSInteger counter;
@property (nonatomic) NSInteger prog_type;

/// Set to NO when the connection is interrupted
@property (nonatomic) BOOL shouldLoadView;

@end



@implementation ExosCocosViewController

static AppDelegate s_sharedApplication;


- (void)viewDidLoad
{
    [super viewDidLoad];
    _activitiesLevelUp = [NSMutableArray new];
    _shouldLoadView = YES;
    
}

- (IBAction)back:(id)sender {
    
}


- (void)viewWillAppear:(BOOL)animated
{
    
    [self hideCounter:YES];
    _counter =  3;
    [super viewWillAppear:animated];
    
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(shouldPop:) name:@"should_pop" object:nil];
    
    if (_shouldLoadView) {
            [IOSNDKHelper setNDKReceiver:self];
            cocos2d::Application *app = cocos2d::Application::getInstance();
    
            app->initGLContextAttrs();
            cocos2d::GLViewImpl::convertAttrs();
    
            cocos2d::Director *dir = cocos2d::Director::getInstance();
    
            _eaglView = [CCEAGLView viewWithFrame:self.view.frame
                                      pixelFormat:(__bridge NSString *)cocos2d::GLViewImpl::_pixelFormat
                                      depthFormat:cocos2d::GLViewImpl::_depthFormat
                               preserveBackbuffer:NO
                                       sharegroup:nil
                                    multiSampling:NO
                                  numberOfSamples:0];
    
    
            //[self.view insertSubview:_eaglView belowSubview:self.backButton];
    
            cocos2d::GLView *glView = cocos2d::GLViewImpl::createWithEAGLView((__bridge void*)_eaglView);
    
            dir->setOpenGLView(glView);
            app->run();
    }
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    _isBacking = NO;
    _alreadyEnded = NO;
    _isBreathDone = NO;
}

- (void)onPreloaderInitialized:(NSObject *)param
{
    DebugLog(@"PRELOADER INITIALIZED");
    if (!_isFreeExo) {
        _cocosHandler = [CocosHandler init];
        [_cocosHandler startNewSessionForCurrentState];
        _nextActivity = [_cocosHandler getNextActivity];
        [self runNewActivityWithDuration:_nextActivity.duration];
    } else {
        [self runNewActivityWithDuration:_exoDuration];
    }
    
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
}

- (void)shouldPop:(NSNotification *)notification
{
    //[self back:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"should_pop" object:nil];
}
/*
- (void)back:(id)sender
{
    _isBacking = YES;
    /*DebugLog(@"activity handler delegate : %@", _activity.delegate);
    DebugLog(@"activity type : %@", _activity.activityType.label);
    
    [[BleService instance] setBleOutput:nil];
    [[BleService instance] stopGettingPressure];
    
    if (!_isFreeExo) {
        [[IntimateProgramHandler sharedManager] clean];
        if (_activity != nil) {
            [_activity stopExercise];
            [self end];
        }
        
        if ([_exoViewModel.exoType intValue] == BREATH || _activity.delegate != self) {
            [self end];
        }
        
    } else {
        if ([_exoViewModel.exoType intValue] == BREATH) {
            [self end];
        } else {
            [_activity stopExercise];
        }
    }
    [super back:nil];
}*/

- (void)end
{
    if (!_alreadyEnded) {
        cocos2d::Director *dir = cocos2d::Director::getInstance();
        dir->pause();
        dir->stopAnimation();
        dir->end();
        _eaglView = nil;
        _alreadyEnded = YES;
    }
}



- (void)timerCount{
    if (  _counter >= 0){
        [_counter_label setText: [NSString stringWithFormat: @"%ld", (long)_counter]];
        
        _counterTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                         target:self
                                                       selector:@selector(timerCount)
                                                       userInfo:nil
                                                        repeats:NO];
        _counter--;
    }else{
        //[self hideCounter:YES];
        if ( _prog_type == 1){
            //[_activity startBreath];
        }else{
            //[_activity startCalibration];
        }
    }
    
    
}

-(void)startProgram{
    //[_activity startCalibration];
}

- (void)runNewActivityWithDuration:(NSNumber *)duration
{
    if (!_isFreeExo) {
        _exoViewModel = [IntimateExoViewModel init];
        _exoViewModel.exoType= _nextActivity.type;
        _selectedLevel = _exoViewModel.activityType.programLevelReached;
    }
    
    if ([_exoViewModel.exoType intValue] == CALIBRATION) {
        _activity = [[IntimateActivityHandler alloc] initForCalibrationWithDuration:duration];
        [_activity setDelegate:self];
        [self hideCounter:NO];
        _prog_type = 0;
        [self timerCount];
        
        
    } else if ([_exoViewModel.exoType intValue] == BREATH) {
        _activity = [[IntimateActivityHandler alloc] initForBreathWithDuration:duration withCalibration:(_currentCalibrationStructure == nil)];
        [_activity setDelegate:self];
        [self hideCounter:NO];
        _prog_type = 1;
        [self timerCount];
        
    } else {
        if (_isBreathDone || !_isFreeExo) {
            _activity = [[IntimateActivityHandler alloc] initWithViewModel:_exoViewModel andSelectedLevel:_selectedLevel];
            _activity.exoDuration = duration;
            [_activity setDelegate:self];
            [_activity startExerciseWithCalibrationStructure:_currentCalibrationStructure];
        } else {
            _activity = [[IntimateActivityHandler alloc] initForBreathWithDuration:@(INTIMATE_BREATH_DURATION) withCalibration:(_currentCalibrationStructure == nil)];
            [_activity setDelegate:self];
            [self hideCounter:NO];
            _prog_type = 1;
            [self timerCount];
            
        }
        
    }
}


- (void)didFinishIntimateActivity:(IntimateActivity *)activity levelUp:(BOOL)levelUp
{
    if (_isFreeExo) {
        [_activity stopExercise];
        
        [_cocosHandler performSelectorInBackground:@selector(synchronizeIntimateDatas) withObject:nil];
        
        [self performSegueWithIdentifier:@"showExoDetail" sender:activity];
    } else {
        IntimateActivity *updatedActivity = [_cocosHandler save:activity levelUp:levelUp];
        if (levelUp) {
            __block BOOL shouldReplace = NO;
            __block NSUInteger indexToReplace = 0;
            [_activitiesLevelUp enumerateObjectsUsingBlock:^(IntimateActivity *act, NSUInteger idx, BOOL * _Nonnull stop) {
                if ([act.intimateActivityType.activityTypeID intValue] == [activity.intimateActivityType.activityTypeID intValue]) {
                    shouldReplace = YES;
                    indexToReplace = idx;
                    *stop = YES;
                }
            }];
            
            if (shouldReplace) {
                [_activitiesLevelUp replaceObjectAtIndex:indexToReplace withObject:updatedActivity];
            } else {
                [_activitiesLevelUp addObject:updatedActivity];
            }
            
        }
        
        [self shouldStartNewActivity];
    }
}

- (void)didFinishCalibration
{
    if (_isFreeExo) {
        [_activity stopExercise];
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        [self shouldStartNewActivity];
    }
}



- (void)didFinishBreath:(IntimateCalibrationStructure *)calibrationStructure
{
    if (_isFreeExo) {
        if ([_exoViewModel.activityType.type intValue] == BREATH) {
            [self didExerciseStop];
            [self dismissViewControllerAnimated:YES completion:nil];
        } else {
            _isBreathDone = YES;
            if (!_currentCalibrationStructure) {
                _currentCalibrationStructure = calibrationStructure;
            }
            
            [self runNewActivityWithDuration:_exoDuration];
        }
        
    } else {
        if (!_currentCalibrationStructure) {
            _currentCalibrationStructure = calibrationStructure;
        }
        [self shouldStartNewActivity];
    }
}


- (void)shouldStartNewActivity
{
    _nextActivity = [_cocosHandler getNextActivity];
    if (_nextActivity != nil) {
        [self runNewActivityWithDuration:_nextActivity.duration];
    } else {
        // La session est terminée, on passe au résultat en passant l' IntimateSession en sender.
        [_activity stopExercise];
        IntimateSession *session = [_cocosHandler terminateSession];
        [self performSegueWithIdentifier:@"showFinish" sender:session];
        [_cocosHandler clean];
    }
}

- (void)didExerciseStop
{
    [self end];
    if (!_isBacking) {
        [_activity disconnectAirbee];
    }
    
    _activity = nil;
}

- (void)didConnectionInterrupted
{
    _shouldLoadView = NO;
    [_activity pauseExercise];
    [self performSegueWithIdentifier:@"showDisconnect" sender:nil];
    
}

- (void)hideCounter:(BOOL)value{
    [_counter_label setHidden: value];
    _counter_label.hidden = value;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    /*if ([[segue identifier] isEqualToString:@"showExoDetail"]) {
        ExosIntimateExerciseDetailViewController *eiedvc = [segue destinationViewController];
        eiedvc.intimateActivity = (IntimateActivity *)sender;
        eiedvc.isFromListing = NO;
    }
    
    if ([[segue identifier] isEqualToString:@"showFinish"]) {
        ExosIntimateSessionFinishViewController *eisfvc = [segue destinationViewController];
        eisfvc.intimateSession = (IntimateSession *)sender;
        eisfvc.isFromListing = NO;
        eisfvc.activitiesLevelUp = _activitiesLevelUp;
    }*/
}



@end
