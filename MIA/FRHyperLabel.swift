//
//  FRHyperLabel.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 14/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit

protocol HyperLabelDelegate: NSObjectProtocol {
    func hyperLabel(_ label: FRHyperLabel?, didSelectLinkAt index: Int)
}

class FRHyperLabel: UILabel {
    
    var handlerDictionary = [AnyHashable: Any]()
    var layoutManager: NSLayoutManager?
    var textContainer: NSTextContainer?
    var backupAttributedText: NSAttributedString?
    var indexesDictionary = [AnyHashable: Any]()
    var rangeForString = [AnyHashable: Any]()
    var fullString = ""
    
    var highLightAnimationTime: CGFloat = 0.15
    var FRHyperLabelLinkColorDefault: UIColor?
    var FRHyperLabelLinkColorHighlight: UIColor?
    
    
    var linkAttributeDefault = [AnyHashable: Any]()
    var linkAttributeHighlight = [AnyHashable: Any]()
    var delegate: HyperLabelDelegate?
    
    
    //TODO
    func setLinkFor(_ range: NSRange, withAttributes attributes: [AnyHashable: Any]?, andLinkHandler handler: @escaping (_ label: FRHyperLabel?, _ selectedRange: NSRange) -> Void) {
    }
    func setLinkFor(_ range: NSRange, withLinkHandler handler: @escaping (_ label: FRHyperLabel?, _ selectedRange: NSRange) -> Void) {
    }
    func setLinkForSubstring(_ substring: String?, withAttribute attribute: [AnyHashable: Any]?, andLinkHandler handler: @escaping (_ label: FRHyperLabel?, _ substring: String?) -> Void) {
    }
    func setLinkForSubstring(_ substring: String?, withLinkHandler handler: @escaping (_ label: FRHyperLabel?, _ substring: String?) -> Void) {
    }
    func setLinksForSubstrings(_ substrings: [Any]?, withLinkHandler handler: @escaping (_ label: FRHyperLabel?, _ substring: String?) -> Void) {
    }
    func clearActionDictionary() {
    }
    // Custom specific method
    func setText(_ string: String?, withWidgets widgets: [Any]?) {
    }
    func insertText(_ string: String?, at index: Int) {
    }
}
