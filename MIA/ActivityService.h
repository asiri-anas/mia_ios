//
//  ActivityService.cpp


#ifndef ActivityService_h
#define ActivityService_h

#include "cocos2d.h"

using namespace cocos2d;

/*
 * This class handle all messages from NDK Helper and dispatch to correct view
 */
class ActivityService {
public:
	/** Get an instance of ActivityService */
	static ActivityService* getInstance();

	/** Destroy ActivityService */
	static void destroyInstance();

	virtual ~ActivityService();

	/** Init ActivityService with the current node (ie: ExerciseLayer, BreathLayer, PreloaderLayer, etc.) */
    bool init(Node*);

    /** Init ActivityService and LeafService with the current node (ie: ExerciseLayer, BreathLayer, PreloaderLayer, etc.) */
    bool initLeafWithNode(Node* node);

    /** This structure is used to save data for a current time stamp with the pressure from Airbee and the expected value */
    struct activityStep {
		double timestamp;
		double target;
		double pressure;
	};

    /** Add in vector of activityStep the current steps */
    void addActivitySteps(ActivityService::activityStep* steps);

    /** Send the vector to Native via NDKHelper */
    void sendActivitySteps();

    /** Set to ActivityService the current pattern */
    void setOriginalPattern(std::vector<Vec2> originalPattern);

    /** Return the pressure expected for the given time */
    float getTargetPressure(float time);

    /** Return the text indication like contract, release, hold */
    int getTextIndication(float time);

private:

    /** NDK Helper method to start ExerciseLayer */
    void setupViewExercise(Node *sender, Value data);

    /** NDK Helper method to start CalibrationLayer */
    void setupViewCalibrationLayer(Node *sender, Value data);

    /** NDK Helper method to start BreathLayer */
    void setupViewBreathLayer(Node *sender, Value data);

    /** NDK Helper method to end scene */
    void endScene(Node *sender, Value data);


	std::vector<Vec2> m_originalPattern;
	float m_maxTimePattern;

    ValueVector m_activitySteps;

};

#endif /* ActivityService_h */
