//
//  ExoTableViewCell.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 02/07/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit


class ExoTableViewCell: UITableViewCell {

    //var customLabel: TTTAttributedLabel?
    
    @IBOutlet weak var launchExoCellButton: UIButton!
    
    @IBOutlet weak var selectExoCellButton: UIButton!
    
    @IBOutlet weak var launchExoLabelContainer: UIView!
    
    
    @IBOutlet weak var launchExoLabel: UILabel!
    @IBOutlet weak var launchExoCellPicto: UIImageView!
    
    
    @IBOutlet weak var selectExoTitle: UILabel!
    
    @IBOutlet weak var selectExoSubtitle: UILabel!
    
    
    
    var indexPath: IndexPath?
    var tableView: UITableView?
    weak var delegate: UITableViewDelegate?
    
    func setContentWithImageName(_ imageName: String?, text: String?, linkColor: UIColor?) {
        
        launchExoCellPicto.image = UIImage(named: imageName!)
        launchExoLabel.text = text
        //launchExoLabel.textColor = linkColor
        
    }
    
    func addTextToLabel(text: String?, linkColor: UIColor?) {
        
        launchExoLabel.text = launchExoLabel.text! + text!
        
    }
    
    
    func setContentWithImageName(_ imageName: String?, title: String?, subtitle: String?, color: UIColor?) {
  
        
        launchExoCellPicto.image = UIImage(named: imageName!)
        selectExoTitle.text = title?.uppercased()
        selectExoSubtitle.text = subtitle?.uppercased()
        selectExoSubtitle.textColor = color
        
    }
    
    
    func setContentForIntimateActivityType(_ exoModel: IntimateExoViewModel?) {
        

        launchExoCellPicto.image = exoModel?.picto
        selectExoSubtitle.textColor = UIColor.colorWithHexString(stringToConvert: exoModel?.exoColorString as! NSString)
        selectExoTitle.text = exoModel?.exoLabel?.uppercased()
        if exoModel?.exoType != 4 { //BREATH
            var myString = "Niveau 1"
            print(exoModel?.activityType?.freeLevelReached)
            //var num = exoModel?.activityType?.freeLevelReached as! Int
            //var numString = String(num)
            //myString.append(numString)
            selectExoSubtitle.text = myString.uppercased()
        } else {
            selectExoSubtitle.text = ""
        }
        
    }
    

    
    @IBAction func launchExoCellTapped(_ sender: Any) {
        
        delegate?.tableView!(tableView!, didSelectRowAt: indexPath!)
    }
    
    
    
    
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        contentView.updateConstraints()
        //launchExoLabelContainer.updateConstraints()
    }
    
    
    
}
