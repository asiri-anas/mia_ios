//
//  FilterTableViewCell.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 13/09/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit

protocol FilterCellDelegate: NSObjectProtocol {
    
    func didTapCloseButton(for cell: FilterTableViewCell?, at indexPath: IndexPath?)
    
}


class FilterTableViewCell: UITableViewCell {
    
    
    @IBOutlet var closeButton: UIButton!
    @IBOutlet var titleLabel: UILabel!
    var indexPath: IndexPath?
    weak var delegate: FilterCellDelegate?
    
    
    override func awakeFromNib() {
        // Initialization code
    }
    
    
@IBAction func close(_ sender: Any) {
    if (true){ //delegate?.responds(to: #selector(self.didTapCloseButton(forCell:atIndexPath:))) {
        delegate?.didTapCloseButton(for: self, at: indexPath)
    }
}
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    
    
    func setContentWithFilterType(_ filterType: NSNumber?, atindexPath: IndexPath?, withdelegate: FilterCellDelegate?) {
        
        indexPath = atindexPath
        delegate = withdelegate
        switch filterType {
        case 0://FILTER_TYPE_DATE:
                titleLabel.text = NSLocalizedString("filter_date", comment: "")
        case 1://FILTER_TYPE_INTIMATE_EXO:
                titleLabel.text = NSLocalizedString("exercise_type", comment: "")
        case 2://FILTER_TYPE_INTIMATE_PROGRAM:
                titleLabel.text = NSLocalizedString("program_type", comment: "")
        case 3://FILTER_TYPE_INTIMATE_HEARTS:
                titleLabel.text = NSLocalizedString("hearts_number", comment: "")
            default:
                break
        }
        titleLabel.layer.cornerRadius = titleLabel.frame.size.height / 2
        titleLabel.layer.masksToBounds = true
        
    }
    
    
    
    
}
