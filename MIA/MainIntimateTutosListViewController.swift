//
//  MainIntimateTutosListViewController.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 02/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit


class MainIntimateTutosListViewController: AbstractViewController, UITableViewDataSource, UITableViewDelegate, BarButtonItemDelegate {
    func didTapLeftBarButtonItem() {
        //( ͡° ͜ʖ ͡°)
    }
    
    func didTapRightBarButtonItem() {
        //( ͡° ͜ʖ ͡°)
    }
    
    
var titles = [String]()
var introTutosVC: AuthTutoViewController?
var authTutoNC: UINavigationController?
var exosSB: UIStoryboard?
    
@IBOutlet weak var tutosTableView: UITableView!
    
    
    override func viewDidLoad() {
        
    super.viewDidLoad()
        
        setGradientBackground()
        navigationItem.title = NSLocalizedString("tutos", comment: "").uppercased()
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.pinkMercu()]
        
        
        super.barButtonItemDelegate = self
        
    titles = ["Tutoriels introductifs", "Comment connecter urgo mia ?", "Comment insérer urgo mia ?", "Comment m'entrainer ?", "Nettoyer et entretenir urgo mia", "En quoi consistent les exercices ?", "Qu'est-ce que mon périnée ?"]
        
    exosSB = UIStoryboard(name: "Exos", bundle: nil)
        
}
    
    
    override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    navigationController?.setNavigationBarHidden(false, animated: true)
    
}
    
    override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)

        setBackButtonForNavigationController()

    }
    
    
func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
    if indexPath.row == 0 {
        let authSB = UIStoryboard(name: "Auth", bundle: nil)
        let authTutoNC = authSB.instantiateViewController(withIdentifier: "TutoNavigationController") as? UINavigationController
        let vc = authTutoNC?.viewControllers[0] as? AuthTutoViewController
        vc?.deviceType = Constants.COACH_INTIMATE_TYPE
        if let aNC = authTutoNC {
            present(aNC, animated: true)
        }
        return
    }
    let tutosVC = exosSB?.instantiateViewController(withIdentifier: "ExosIntimateSensorTutosViewController") as? ExosIntimateSensorTutosViewController
    switch indexPath.row {
        case 1:
            tutosVC?.sensorTutoType = .SENSOR_TUTO_TYPE_CONNECT
        case 2:
            tutosVC?.sensorTutoType = .SENSOR_TUTO_TYPE_INSERT
        case 3:
            tutosVC?.sensorTutoType = .SENSOR_TUTO_TYPE_HOW_TO_USE
        case 4:
            tutosVC?.sensorTutoType = .SENSOR_TUTO_TYPE_HOW_TO_CLEAN
        case 5:
        tutosVC?.sensorTutoType = .SENSOR_EXPLAIN_EXERCICE
        case 6:
        tutosVC?.sensorTutoType = .SENSOR_EXPLAIN_PERINE
    default:
        tutosVC?.sensorTutoType = .SENSOR_TUTO_TYPE_CONNECT
    }
    
    if let aVC = tutosVC {
        present(aVC, animated: true)
    }
    
}

    
func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return titles.count
}
    
    
    
func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cellIdentifer = "tutoCell"
    var cell = tutosTableView.dequeueReusableCell(withIdentifier: cellIdentifer, for: indexPath) as? IntimateTutoTableViewCell
    if cell == nil {
        cell = IntimateTutoTableViewCell()
    }
    cell?.setContentWith(titles[indexPath.row])
    if let aCell = cell {
        return aCell
    }
    return UITableViewCell()
}
    
    
}
