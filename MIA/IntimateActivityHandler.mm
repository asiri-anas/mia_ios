//
//  IntimateActivity.m
//  mymercurochrome
//
//  Created by Rémi Caroff on 07/12/2015.
//  Copyright © 2015 V-Labs. All rights reserved.
//

#import <AFNetworking/AFHTTPRequestOperationManager.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <EMAccordionTableViewController/EMAccordionTableViewController.h>
#import <FSCalendar/FSCalendar.h>
#import <iOS_Slide_Menu/SlideNavigationController.h>
#import <TTTAttributedLabel/TTTAttributedLabel.h>
#import "IntimateActivityHandler.h"
#import "IOSNDKHelper.h"
#import "IntimateCoachService.h"
#import "mia-Swift.h"
#import "Constants.h"


@interface IntimateActivityHandler () <BleServiceOutput>

@property (nonatomic, strong) IntimateExoViewModel *exoViewModel;
@property (nonatomic, strong) CalibrationHistory *calibrationHistory;

@property (nonatomic) BOOL shouldCalibrate;
@property (nonatomic) BOOL isDisconnectVolunteer;

@end

@implementation IntimateActivityHandler

- (IntimateActivityHandler *)initWithViewModel:(IntimateExoViewModel *)viewModel andSelectedLevel:(NSNumber *)selectedLevel
{
    if (self = [super init]) {
        self.exoViewModel = viewModel;
        NSNumber* an = (NSNumber*)[viewModel getActivityType].type;
        self.name = an;
        self.activityLevel = selectedLevel;
        self.activityType = [viewModel getActivityType];
        [IOSNDKHelper setNDKReceiver:self];
        [[BleService instance] setBleOutputWithBleOutput:self];
        _isDisconnectVolunteer = NO;
        
    }
    
    return self;
}

- (IntimateActivityHandler *)initForCalibrationWithDuration:(NSNumber *)duration
{
    if (self = [super init]) {
        self.exoDuration  = duration;
        self.name = @(CALIBRATION);
        [IOSNDKHelper setNDKReceiver:self];
        [[BleService instance] setBleOutputWithBleOutput:self];
        _isDisconnectVolunteer = NO;
    }
    
    return self;
}

- (IntimateActivityHandler *)initForBreathWithDuration:(NSNumber *)duration withCalibration:(BOOL)calibration
{
    if (self = [super init]) {
        self.exoDuration = duration;
        self.name = @(BREATH);
        [IOSNDKHelper setNDKReceiver:self];
        [[BleService instance] setBleOutputWithBleOutput:self];
        self.shouldCalibrate = calibration;
        _isDisconnectVolunteer = NO;
    }
    
    return self;
}

- (IntimateActivityHandler *)initWithActivityBase:(IntimateActivityBase *)activityBase
{
    if (self = [super init]) {
        
    }
    
    return self;
}

- (void)startCalibration
{
    [[BleService instance] setBleOutputWithBleOutput:self];
    self.calibrationHistory = [[[CalibrationHistoryManager instance] fetchCalibrationHistoryWithFiter:nil] lastObject];
    
    if (_calibrationHistory != nil) {
        [IOSNDKHelper sendMessage:@SETUP_VIEW_CALIBRATION withParameters:@{@KEY_LAST_PRESSURE_MAX:_calibrationHistory.value, @KEY_DURATION:self.exoDuration, @KEY_MAX_PRESSURE_CALIBRATION: @MAX_CALIBRATION}];
    } else {
        [IOSNDKHelper sendMessage:@SETUP_VIEW_CALIBRATION withParameters:@{@KEY_LAST_PRESSURE_MAX:@0, @KEY_DURATION:self.exoDuration, @KEY_MAX_PRESSURE_CALIBRATION: @MAX_CALIBRATION}];
    }
}

- (void)pauseExercise
{
    [IOSNDKHelper sendMessage:@TOGGLE_PAUSE withParameters:nil];
}

- (void)startBreath
{
    PressureActivityParameter *pap = [PressureActivityParameter alloc];
    pap.duration = self.exoDuration;
    pap.datas = @[];
    pap.colorString = @"";
    
    [IOSNDKHelper sendMessage:@SETUP_VIEW_BREATH withParameters:[pap serialize]];
}


- (void)startExerciseWithCalibrationStructure:(IntimateCalibrationStructure *)calibrationStructure
{
    [[BleService instance] setBleOutputWithBleOutput:self];

    NSArray *datas = [[IntimateCoachService sharedService] getActivitySkeletonForActivityType:_name calibrationStructure:calibrationStructure andLevel:_activityLevel];
    PressureActivityParameter *pap = [PressureActivityParameter init];
    pap.datas = datas;
    pap.colorString = _exoViewModel.exoColorString;
    pap.duration = _exoDuration;
    pap.label = NSLocalizedString(_activityType.label, nil);
    [IOSNDKHelper sendMessage:@SETUP_VIEW_EXERCISE withParameters:[pap serialize]];
}


- (void)stopExercise
{
    [[BleService instance] stopGettingPressure];
    [self airbeeDidStopExercise];
}

- (void)airbeeDidStopExercise
{
    if ([_delegate respondsToSelector:@selector(didExerciseStop)]) {
        [_delegate didExerciseStop];
    }
}

- (void)disconnectAirbee
{
    _isDisconnectVolunteer = YES;
    [[BleService instance] disconnect];
}

- (void)onExerciseInitialized:(NSObject *)paramObject
{
    DebugLog(@"EXERCISE INITIALIZED");
    [[BleService instance] startGettingPressure];
}

- (void)onExerciseFinished:(NSObject *)paramOject
{
    NSDictionary *obj = (NSDictionary *)paramOject;
    NSArray *steps = obj[@"activitySteps"];
    
    if (self.name == @(CALIBRATION)) {
        
        // On appelle l'algo en lui passant les activitySteps, puis on crée un CalibrationHistory avec les données reçues de l'algo.

        [[CalibrationHistoryManager instance] createWithTonicityValue:[[IntimateCoachService sharedService] getTonicityForIntimateActivitySteps:steps]];
        
        [[CDManager instance] saveContext];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"calibration_history_updated" object:nil];
        
        [[BleService instance] stopGettingPressure];
        
        if ([_delegate respondsToSelector:@selector(didFinishCalibration)]) {
            [_delegate didFinishCalibration];
        }
        
    } else if (self.name == @(BREATH)) {
        
        IntimateCalibrationStructure *structure = nil;
        if (self.shouldCalibrate) {
            
            DebugLog(@"CALIBRATED");
            structure = [[IntimateCoachService sharedService] getCalibrationForIntimateActivitySteps:steps];
        }
        
        [[BleService instance] stopGettingPressure];
        
        if ([_delegate respondsToSelector:@selector(didFinishBreath:)]) {
            [_delegate didFinishBreath:structure];
        }
    
    } else {
        IntimateActivityResult *result = [[IntimateCoachService sharedService] getResultWithActivitySteps:steps];
        IntimateManager *intimateManager = [IntimateManager instance];
        IntimateActivity *intimateActivity = [intimateManager createIntimateActivityWith:result forType:_activityType andLevel:_activityLevel];
        if (result.isLevelUp == [NSNumber numberWithInt:1]) {
            if (_isFreeExo || [_activityType.programLevelReached intValue] >= [_activityType.freeLevelReached intValue]) {
                _activityType.freeLevelReached = @([_activityType.freeLevelReached intValue] + 1);
                [[CDManager instance] saveContext];
            }
        }
        
        [[BleService instance] stopGettingPressure];
        
        if ([_delegate respondsToSelector:@selector(didFinishIntimateActivity:levelUp:)]) {
            [_delegate didFinishIntimateActivity:intimateActivity levelUp:result.isLevelUp == [NSNumber numberWithInt:1]];
        }
    }
}

- (void)onPressureDataReceivedWithData:(NSInteger)data
{
    DebugLog(@"Native pressure level : %@", [NSNumber numberWithInteger:data]);
    [IOSNDKHelper sendMessage:@POST_PRESSURE withParameters:@{@KEY_PRESSURE:[NSNumber numberWithInteger:data]}];
}


- (void)onDisconnected
{
    [[BleService instance] stopScanning];
    
    if (!_isDisconnectVolunteer) {
        [[BleService instance] startScanningWithNames:[[BleConstant alloc] init].sensorNames];
        if ([_delegate respondsToSelector:@selector(didConnectionInterrupted)]) {
            [_delegate didConnectionInterrupted];
        }
    } else {
        [[BleService instance] setBleOutputWithBleOutput:self];
    }
}

@end
