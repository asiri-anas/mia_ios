//
//  AuthStartViewControllerSwift.swift
//  mia
//
//  Created by Xavier Bohin on 16/04/2018.
//  Copyright © 2018 V-Labs. All rights reserved.
//

import Foundation
import UIKit


class AuthStartViewController: AbstractViewController {
    
    let COACH_INTIMATE_TYPE = 7
    
    override func viewDidLoad() {
        //super.viewDidLoad()
        //Registry.set("is_first_connection", withValue: true)
    }
    
    @IBAction func start(_ sender: Any) {
        performSegue(withIdentifier: "showTuto", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "showTuto") {
            let atvc = segue.destination as? AuthTutoViewController
            atvc?.deviceType = COACH_INTIMATE_TYPE
        }
    }
    
}
