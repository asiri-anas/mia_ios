//
//  TutoWhyPractice.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 14/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit
class TutoWhyPractice: UIView {
    class func createView() -> TutoWhyPractice? {
        
        var view = Bundle.main.loadNibNamed("TutoWhyPractice", owner: nil, options: nil)?.last as? TutoWhyPractice
        if (view is TutoWhyPractice) {
        }
        return view
        
    }
}
