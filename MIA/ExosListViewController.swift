//
//  ExosListViewController.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 02/07/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit

class ExosListViewController: AbstractViewController, UITableViewDataSource, UITableViewDelegate {
    
    
    @IBOutlet weak var launchExoTableView: UITableView!
    
    var intimateCoach: IntimateCoach?
    var intimateProfileCompleted = false
    var intimateProgramFinished = false
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setCloseExoButton()
        setGradientBackground()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        intimateProgramFinished = false
        intimateCoach = IntimateCoach.retrieveLastCoach()
        
        navigationController?.setNavigationBarHidden(true, animated: true)
        launchExoTableView.reloadData()
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        switch indexPath.row {
        case 0:
            performSegue(withIdentifier: "program", sender: nil)
        case 1:
            performSegue(withIdentifier: "launchIntimateExercise", sender: nil)
        default:
            performSegue(withIdentifier: "calibration", sender: nil)
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 70))
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 70
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 2
    }
    
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        // Remove seperator inset
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
            cell.separatorInset = .zero
        }
        // Prevent the cell from inheriting the Table View's margin settings
        if cell.responds(to: #selector(setter: UITableViewCell.preservesSuperviewLayoutMargins)) {
            cell.preservesSuperviewLayoutMargins = false
        }
        // Explictly set your cell's layout margins
        if cell.responds(to: #selector(setter: UITableViewCell.layoutMargins)) {
            cell.layoutMargins = .zero
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "ExoCell"
        var cell = self.launchExoTableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? ExoTableViewCell
        if cell == nil {
            cell = ExoTableViewCell()
        }
        cell?.delegate = self
        cell?.tableView = tableView
        cell?.indexPath = indexPath
        switch indexPath.row {
        case 0:
            cell?.setContentWithImageName("ma_progression_pink", text: NSLocalizedString("start_new_program", comment: ""), linkColor: UIColor.pinkMercu())
            //ajouter le nom du programme  -------------v
            cell?.addTextToLabel(text: NSLocalizedString((intimateCoach?.intimateState?.intimateProgramType?.label)!, comment: ""), linkColor: UIColor.pinkMercu())
            
            
        case 1:
            
            cell?.setContentWithImageName("ma_progression_pink", text: NSLocalizedString("exo_perineal", comment: ""), linkColor: UIColor.darkGrayTextColorMercu())
            
        default:
            cell?.setContentWithImageName("ma_progression_pink", text: NSLocalizedString("exo_mesurer_tonicite_perineale", comment: ""), linkColor: UIColor.darkGrayTextColorMercu())
        }
        
        if let aCell = cell {
            return aCell
        }
        
        return cell!
    }
    
    
    
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "calibration") {
            let vm = IntimateExoViewModel(activityName: 5)
            let eipvc = segue.destination as? ExosIntimateParamsViewController
            eipvc?.isFreeExo = true
            eipvc?.exoViewModel = vm
        }
        if (segue.identifier == "program") {
            let eipvc = segue.destination as? ExosIntimateParamsViewController
            eipvc?.isFreeExo = false
        }
    }
    
    
    
    
    
    
}
