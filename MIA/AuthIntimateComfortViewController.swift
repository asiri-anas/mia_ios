//
//  AuthIntimateComfortViewController.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 14/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit

class AuthIntimateComfortViewController: AuthIntimateSliderViewController {
    
    var intimateState: IntimateState?
    var intimateCoach: IntimateCoach?
    
    var isEditingMode = false
    var isFromSurvey = false
    
    var intimateComforts = [Any]()
    var selectedComfort: Int = 0
    
    let keyLoc = ["Aucune gêne", "Gêne légère", "Gêne modérée", "Gêne importante", "Gêne intense",]
    let imgName = ["curseur_femme_0", "curseur_femme_1", "curseur_femme_2", "curseur_femme_3", "curseur_femme_4"]
    
    override func viewDidLoad() {
    isEditing = isEditingMode && !isFromSurvey
    super.viewDidLoad()
    //setGradientBackground()
    //super.barButtonItemDelegate = self as! BarButtonItemDelegate
    //intimateComforts = (IntimateStateManager.shared()?.fetchIntimateComforts()!)!
        
    if intimateState != nil {
        selectedComfort = stringToInt(string: (intimateState?.uncomfortLeakLevel)!)
        cursorLabel.text = intimateState?.uncomfortLeakLevel
        cursorIV.image = UIImage(named: imgName[selectedComfort])
        slider.setValue(Float(selectedComfort), animated: false)
        
    } else {
        cursorLabel.text = keyLoc[0]
        cursorIV.image = UIImage(named: imgName[0])
        slider.setValue(0, animated: false)
    }
        
    preFillSlider()
        
}
    
    
    func stringToInt(string: String) -> Int
    {
        var count = 0
        for i in keyLoc
        {
            if ( i == string )
            {
                return count
            }
            count = count + 1
        }
        return count
    }
    
    
    func preFillSlider() {
        do {
            var results = try CDManager.instance!.managedObjectContext!.fetch(IntimateCoach.intimateCoachFetchRequest())
            print("AUTH", "fetch intimateCoach")
            for singleData in results {
                if singleData.intimateState != nil {
                    print("AUTH", "intimateState fetched:", singleData.intimateState!.uncomfortLeakLevel)
                    selectedComfort = stringToInt(string: singleData.intimateState!.uncomfortLeakLevel!)
                    slider.setValue(Float(selectedComfort), animated: false)
                    cursorLabel.text = singleData.intimateState!.uncomfortLeakLevel!
                    cursorIV.image = UIImage(named: imgName[selectedComfort])
                    
                }
                intimateCoach = singleData
            }
        } catch {
            print("Failed")
        }
    }
    

    override func sliderValueChanged(_ sender: Any?) {
    super.sliderValueChanged(sender)
    let val = Int(roundf(slider.value))
        //selectedComfort = intimateComforts[val] as! IntimateComfort
    cursorLabel.text = keyLoc[val]
        //let imgName = "curseur_femme_\(Int((selectedComfort?.comfortID)!) - 1)"
    cursorIV.image = UIImage(named: imgName[val])
        
        intimateCoach = initStateAndSave()
    }
    
    
    func initStateAndSave() -> IntimateCoach? {
        if intimateCoach == nil {
            intimateCoach = IntimateCoachManager.instance.createInitialIntimateCoach()
            print("AUTH", "intimateCoach created")
        }
        intimateState = intimateCoach?.intimateState
        if intimateState == nil {
            intimateState = IntimateState.create()
            print("AUTH", "intimateState created")
        }
        
        intimateState?.uncomfortLeakLevel = keyLoc[Int(roundf(slider.value))]
        
        intimateCoach?.intimateState = intimateState
        print("AUTH", "profil survey leak comfort", intimateCoach?.intimateState?.uncomfortLeakLevel)
        //print("Intimate to send: ", intimateCoach)
        CDManager.instance!.saveContext()
        
        return intimateCoach
    }
    
    

}
