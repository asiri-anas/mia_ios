//
//  MainIntimateSessionsCalendarViewController.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 23/07/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import FSCalendar


class MainIntimateSessionsCalendarViewController: AbstractViewController, FSCalendarDataSource, FSCalendarDelegate, BarButtonItemDelegate, FSCalendarDelegateAppearance {
    func didTapLeftBarButtonItem() {
        //
    }
    
    func didTapRightBarButtonItem() {
        //
    }
    
    
    @IBOutlet var calendar: FSCalendar!
    var intimateState: IntimateState?
    var intimateSessions: NSArray = []
    
    var futureSessions: NSArray = []
    var dataSource: NSArray = []
    var dataSourceDic: NSMutableDictionary = [:]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        reloadSelection()
        setBackButtonForNavigationController()
        navigationItem.title = NSLocalizedString("schedule_session", comment: "").uppercased()
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateCalendarSelection(_:)), name: NSNotification.Name("update_calendar_selection"), object: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    @objc func updateCalendarSelection(_ notification: Notification?) {
        let selectedDate = notification?.userInfo!["date"] as? Date
        let shouldDeselect: Bool = notification?.userInfo!["deselect"] as! Int != 0
        if shouldDeselect {
            calendar.deselect(selectedDate)
        }
        reloadSelection()
    }
    
    func reloadSelection() {/*
        dataSourceDic = NSMutableDictionary()
        dataSource = IntimateSessionsScheduler.shared().fetchSchedulerDataSource(forIntimateState: intimateState)
        dataSource.enumerateObjects({ obj, idx, stop in
            if obj.responds(to: #selector(IntimateDataSourceProtocol.getCreationDate)) ?? false {
                calendar?.selectDate(obj.getCreationDate())
                let key = DateHelper.getStartOfDate(obj?.getCreationDate())
                dataSourceDic[key] = obj
            }
        })
    */}
    func minimumDate(for calendar: FSCalendar?) -> Date? {
        if intimateSessions.count > 0 {
            let session = intimateSessions.lastObject as? IntimateSession
            return session?.createdAt
        }
        return Date()
    }
    
    func calendar(_ calendar: FSCalendar?, appearance: FSCalendarAppearance?, selectionColorFor date: Date?) -> UIColor? {
        if date?.compare(Date()) == .orderedAscending {
            if let aDate = date {
                if (dataSourceDic[aDate] is IntimateSession) {
                    return UIColor.colorWithHexString(stringToConvert: "#1FA56C")
                } else {
                    return UIColor.clear
                }
            }
        }
        return UIColor.pinkMercu()
    }
    
    
    func calendar(_ calendar: FSCalendar?, shouldSelect date: Date?) -> Bool {
        if date?.compare(Date()) == .orderedAscending {
            return false
        }
        return true
    }
    func calendar(_ calendar: FSCalendar?, shouldDeselect date: NSDate?) -> Bool {
        var data: IntimateDataSourceProtocol? = nil
        if let aDate = date {
            data = dataSourceDic[aDate] as! IntimateDataSourceProtocol
        }
        if data != nil {
            if (data is FutureIntimateSession) {
                performSegue(withIdentifier: "showTime", sender: data)
            }
            if (data is IntimateSession) {
                performSegue(withIdentifier: "showList", sender: getSessionsFor(date))
            }
            return false
        }
        return true
    }
    
    
    
    func getSessionsFor(_ date: NSDate?) -> NSArray? {
        var filter = Filter(_attribute: "createdAt", _relation: "BETWEEN", _value: [Date(), Date()])
        return IntimateManager.instance.fetchIntimateSessions(withFilters: [filter], withCount: 0)
    }
    
    
    
    func calendar(_ calendar: FSCalendar?, didSelect date: Date?) {
        performSegue(withIdentifier: "showTime", sender: date)
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "showTime") {
            let nc = segue.destination as? UINavigationController
            let vc = nc?.viewControllers[0] as? IntimateSessionsCalendarTimeViewController
            if (sender is Date) {
                vc?.selectedDate = sender as? Date
            } else {
                let fs = sender as? FutureIntimateSession
                vc?.futureSession = fs
                vc?.selectedDate = fs?.fireDate
            }
        }
        if (segue.identifier == "showList") && (sender is [Any]) {
            let nc = segue.destination as? UINavigationController
            let misvc = nc?.viewControllers[0] as? MainIntimateSessionsViewController
            //misvc?.dataSource = sender as? [Any]
        }
    }
    
    
    
    
}
