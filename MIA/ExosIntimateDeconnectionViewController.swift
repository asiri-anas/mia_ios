//
//  ExosIntimateDeconnectionViewController.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 13/07/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit

class ExosIntimateDeconnectionViewController: AbstractViewController {
    
    @IBOutlet var headerLabel: UILabel!
    @IBOutlet var btIV: UIImageView!
    @IBOutlet var bodyLabel: UILabel!
    @IBOutlet var returnButton: UIButton!
    
    var isConnected = false
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        returnButton.setTitle(NSLocalizedString("back_to_parameters", comment: ""), for: .normal)
        //[self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"nav_pink"] forBarMetrics:UIBarMetricsDefault];
        navigationItem.title = NSLocalizedString("mia_synchro", comment: "").uppercased()
        headerLabel.text = NSLocalizedString("sensor_disconnect_alert_title", comment: "").uppercased()
        bodyLabel.text = NSLocalizedString("sensor_disconnect_alert_body", comment: "")
        isConnected = false
    }
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(self.sensorConnected(_:)), name: NSNotification.Name(rawValue: Constants.DEVICE_STATUS_CONNECT), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.sensorDisconnected(_:)), name: NSNotification.Name(rawValue: Constants.DEVICE_STATUS_DISCONNECT), object: nil)
        //BleService.instance().startScanning(withNames: BleConstant.names())
    }
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: Constants.DEVICE_STATUS_CONNECT), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: Constants.DEVICE_STATUS_DISCONNECT), object: nil)
    }
    
    
    
    @objc func sensorConnected(_ notification: Notification?) {
        isConnected = true
        headerLabel.text = NSLocalizedString("sensor_connect_alert_title", comment: "").uppercased()
        UIView.animate(withDuration: 0.2, animations: {
            self.btIV.alpha = 1
        }) { finished in
            UIView.animate(withDuration: 0.2, animations: {
                self.btIV.alpha = 0.2
            }) { finished in
                UIView.animate(withDuration: 0.2, animations: {
                    self.btIV.alpha = 1
                }) { finished in
                    UIView.animate(withDuration: 0.2, animations: {
                        self.btIV.alpha = 0.2
                    }) { finished in
                        UIView.animate(withDuration: 0.2, animations: {
                            self.btIV.alpha = 1
                        })
                    }
                }
            }
        }
        returnButton.setTitle(NSLocalizedString("back_to_training", comment: ""), for: .normal)
    }
    @objc func sensorDisconnected(_ notification: Notification?) {
        isConnected = false
        UIView.animate(withDuration: 0.2, animations: {
            self.btIV.alpha = 0.2
        })
        returnButton.setTitle(NSLocalizedString("back_to_parameters", comment: ""), for: .normal)
    }
    
    
    @IBAction func getBackToTraining(_ sender: Any) {
        
        /*if isConnected {
            BleService.instance().startGettingPressure()
            dismiss(animated: true)
        } else {
            dismiss(animated: true) {
                NotificationCenter.default.post(name: NSNotification.Name("should_pop"), object: nil)
            }
        }*/
        
    }
    
    
}
