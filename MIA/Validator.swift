//
//  Validator.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 23/08/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit

@objc protocol ValidatorDelegate: NSObjectProtocol {
    /// This method is triggered when there's a 401 status code (unauthorized), and a new access token is set in HTTP request's header.
    func didTokenRefresh()
    /// Delegate method that will return a title and a message to let the delegate manage an UIAlertView or something else.
    @objc optional func showAlert(withTitle title: String?, message: String?)
}

class Validator: NSObject {
    /*!
     @brief This method will map an Error object depending on his statusCode, and returns the contextualized message to display to the delegate.
     @param errorObject Raised by MercuClient. This method should be implemented in a callback of a MercuClient's method.
     */
    class func validateError(_ errorObject: Error?, with validatorDelegate: ValidatorDelegate?) {
        
        if (errorObject?.statusCode == 500) {
            UIAlertView(title: NSLocalizedString("error", comment: ""), message: NSLocalizedString("error_500_message", comment: ""), delegate: nil, cancelButtonTitle: NSLocalizedString("ok", comment: ""), otherButtonTitles: "").show()
        }
        if (errorObject?.statusCode == 401) {
            MercuClient.instance.getAccessToken(withCompletion: { error in
                if error?.message == "" {
                    validatorDelegate?.didTokenRefresh()
                } else {
                    Validator.validateError(error, with: nil)
                }
            })
        }
        
    }
}
