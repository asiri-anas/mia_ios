//
//  PreloaderViewController.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 25/06/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit
import iOS_Slide_Menu

class PreloaderViewController: AbstractViewController {
    
    var intimateState: IntimateState?
    var intimateCoach: IntimateCoach?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("Preloader appear")
        let app = UIApplication.shared.delegate as? NativeAppDelegate
        super.viewDidAppear(animated)
        
        getIntimateCoachFromBdd()
        
        //If there isn't a program, it means it's the first time
        if (intimateCoach?.intimateState?.intimateProgramType == nil) {
            //Launch Auth storyboard
            app?.loadAuthStoryBoard()
            intimateCoach = initIntimateCoach()
        }
        //If user already choose a program (so she went throught the auth
        else {
            let sb = UIStoryboard(name: "Intimate", bundle: nil)
            let snc = sb.instantiateInitialViewController() as? SlideNavigationController
            if let aView = snc?.view {
                UIView.transition(from: (app?.window?.rootViewController?.view)!, to: aView, duration: 0.5, options: .transitionCrossDissolve) { finished in
                    app?.window?.rootViewController = snc
                }
            }
            //Init lateral menu
            let mainSB = UIStoryboard(name: "Intimate", bundle: nil)
            let smvc = mainSB.instantiateViewController(withIdentifier: "SlideMenuViewController") as? SlideMenuViewController
            SlideNavigationController.sharedInstance().leftMenu = smvc
        }
    }
    
    
    func getIntimateCoachFromBdd() {
        do {
            var results = try CDManager.instance!.managedObjectContext!.fetch(IntimateCoach.intimateCoachFetchRequest())
            print("AUTH", "fetch intimateCoach")
            for singleData in results {
                if singleData.intimateState != nil {
                    print("AUTH", "intimateState fetched:", singleData.intimateState!.bodyHeight)
                }
                intimateCoach = singleData
            }
        } catch {
            print("Failed")
        }
    }
    
    /*
     ** Init the data where we put the user informations
     */
    func initIntimateCoach() -> IntimateCoach? {
        if intimateCoach == nil {
            intimateCoach = IntimateCoachManager.instance.createInitialIntimateCoach()
            print("AUTH", "intimateCoach created")
        }
        intimateCoach?.intimateState?.uncomfortLeakLevel = "Aucune gêne"
        intimateCoach?.intimateState?.uncomfortSensationLevel = "Aucune gêne"
        print("AUTH", "preloaded", intimateCoach?.intimateState?.uncomfortLeakLevel)
        //print("Intimate to send: ", intimateCoach)
        CDManager.instance!.saveContext()
        print("AUTH", "preloaded", intimateCoach?.intimateState?.uncomfortLeakLevel)
        
        return intimateCoach
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     #pragma mark - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
     // Get the new view controller using [segue destinationViewController].
     // Pass the selected object to the new view controller.
     }
     */
    
}
