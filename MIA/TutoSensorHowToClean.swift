//
//  TutoSensorHowToClean.swift
//  mia
//
//  Created by Xavier Bohin on 19/04/2018.
//  Copyright © 2018 V-Labs. All rights reserved.
//

import Foundation
import UIKit

class TutoSensorHowToClean: UIView {
    class func createView() -> TutoSensorHowToClean? {
        
        var view = Bundle.main.loadNibNamed("TutoSensorHowToClean", owner: nil, options: nil)?.last as? TutoSensorHowToClean
        if (view is TutoSensorHowToClean) {
        }
        return view
        
    }
}
