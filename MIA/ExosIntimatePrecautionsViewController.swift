//
//  ExosIntimatePrecautionsViewController.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 13/07/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit


class ExosIntimatePrecautionsViewController: AbstractViewController {
    
    @IBOutlet var useConditionTitleLabel: UILabel!
    @IBOutlet var useConditionBodyLabel: UILabel!
    @IBOutlet var hygieneConditionTitleLabel: UILabel!
    @IBOutlet var hygieneConditionBodyLabel: UILabel!
    @IBOutlet var tutoButton: UIButton!
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = NSLocalizedString("use_precautions", comment: "").uppercased()
        //[self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"nav_pink"] forBarMetrics:UIBarMetricsDefault];
        setBackButtonForNavigationController()
        setGradientBackground()
        var paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center
        let useConditionAttr: NSAttributedString? = NSMutableAttributedString(string: NSLocalizedString("use_condition", comment: ""), attributes: [NSAttributedStringKey.font: useConditionTitleLabel.font, NSAttributedStringKey.foregroundColor: useConditionTitleLabel.textColor, NSAttributedStringKey.underlineStyle: NSUnderlineStyle.styleSingle.rawValue, NSAttributedStringKey.paragraphStyle: paragraphStyle])
        let hygieneConditionAttr: NSAttributedString? = NSMutableAttributedString(string: NSLocalizedString("hygiene_condition", comment: ""), attributes: [NSAttributedStringKey.font: hygieneConditionTitleLabel.font, NSAttributedStringKey.foregroundColor: hygieneConditionTitleLabel.textColor, NSAttributedStringKey.underlineStyle: NSUnderlineStyle.styleSingle.rawValue, NSAttributedStringKey.paragraphStyle: paragraphStyle])
        useConditionTitleLabel.attributedText = useConditionAttr
        useConditionBodyLabel.text = NSLocalizedString("use_condition_body", comment: "")
        hygieneConditionTitleLabel.attributedText = hygieneConditionAttr
        hygieneConditionBodyLabel.text = NSLocalizedString("hygiene_condition_body", comment: "")
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    
    @IBAction func tutoButtonTapped(_ sender: Any) {
        

        var tutosVC = storyboard?.instantiateViewController(withIdentifier: "ExosIntimateSensorTutosViewController") as? ExosIntimateSensorTutosViewController
        //tutosVC?.sensorTutoType = 4//SENSOR_TUTO_TYPE_ALL
        if let aVC = tutosVC {
            present(aVC, animated: true)
        }
        
    }
    
    
    /*
     #pragma mark - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
     // Get the new view controller using [segue destinationViewController].
     // Pass the selected object to the new view controller.
     }
     */
    
    
}
