//
//  MainUserProfileViewController.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 02/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit

class MainUserProfileViewController: AbstractViewController, UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate, UIActionSheetDelegate, IntimateCellDelegate {
    
    //MFMailComposeViewControllerDelegate,
    
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var birthDateLabel: UILabel!
    @IBOutlet var editButton: UIButton!
    @IBOutlet var tableView: UITableView!
    var user: User?
    
    var intimateState: IntimateState?
    //var mailPicker: CustomMailComposeViewController?
    var programsList = NSArray()
    
    
    @IBAction func editProfile(_ sender: Any) {
        
        var sb = UIStoryboard(name: "Auth", bundle: nil)
        var nc = sb.instantiateViewController(withIdentifier: "IntimateProfileFormNavigationController") as? UINavigationController
        var aipfvc = nc?.viewControllers[0] as? AuthIntimateProfileFormViewController
        aipfvc?.user = user
        aipfvc?.isEditingMode = true
        if let aNc = nc {
            present(aNc, animated: true)
        }
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setGradientBackground()
        editButton.contentHorizontalAlignment = .right
        editButton.contentMode = .scaleAspectFit
        editButton.setImage(editButton.imageView?.image?.withRenderingMode(.alwaysTemplate), for: .normal)
        nameLabel.text = "\(user?.firstName ?? "") \(user?.lastName ?? "")"
        birthDateLabel.text = DateHelper.serializeDate(fromNSDate: user?.birthDate)
        
        intimateState = IntimateStateManager.instance.fetchIntimateState()
        
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationItem.title = "mon profil".uppercased()
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.pinkMercu()]
        
        setMenuButtonForNavigationController()
        
        programsList = ["Prendre soin de mon périneé", "Inconfort urinaire", "Manque de sensations", "Post-accouchement"]

NotificationCenter.default.addObserver(self, selector: #selector(self.intimateUserUpdated(_:)), name: NSNotification.Name("update_intimate_state"), object: nil)

NotificationCenter.default.addObserver(self, selector: #selector(self.intimateSpecialistUpdated(_:)), name: NSNotification.Name("update_intimate_specialist"), object: nil)

        
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    enableSwipeMenu(true)
        


    }
    
    override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
        
        setMenuButtonForNavigationController()
        
        setPlayButtonWith(nil)
    }
    

    
    @objc func intimateUserUpdated(_ notification: Notification?) {
        if notification?.userInfo!["intimateState"] != nil {
            intimateState = notification?.userInfo!["intimateState"] as? IntimateState
        
    }
        user = UserManager.sharedManager()?.fetchUser()
        nameLabel.text = "\(user?.firstName ?? "") \(user?.lastName ?? "")"
        birthDateLabel.text = DateHelper.serializeDate(fromNSDate: user?.birthDate)
        tableView.reloadData()
}
    
    
    @objc func intimateSpecialistUpdated(_ notification: Notification?) {
        user = notification?.userInfo!["user"] as! User
        tableView.reloadData()
}
    
    
    
    
func didFinishEditingUserProfile(_ user: User?) {
    self.user = user
    NotificationCenter.default.post(name: NSNotification.Name("user_updated"), object: nil, userInfo: ["user": user])
    nameLabel.text = "\(self.user?.firstName ?? "") \(self.user?.lastName ?? "")"
    birthDateLabel.text = DateHelper.serializeDate(fromNSDate: self.user?.birthDate)
    tableView.reloadData()
}

    
    
    
func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 2
}
    
    
func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
    let ctn = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 0.5))
    let separator = UIView(frame: CGRect(x: 60, y: 0, width: view.frame.size.width, height: 0.5))
    separator.backgroundColor = UIColor.lightGray
    ctn.alpha = 0.7
    ctn.addSubview(separator)
    if section != 1 {
        return ctn
    }
    return UIView()
}
    
func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
    return 70
}
    
func numberOfSections(in tableView: UITableView) -> Int {
    return 1
}
    
func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return UITableViewAutomaticDimension
}
    
func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
    return 70
}
    
    
func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
    var alert: UIAlertView?
    if indexPath.row == 0 {
        alert = UIAlertView(title: NSLocalizedString("warning", comment: ""), message: NSLocalizedString("warning_intimate_program_update", comment: ""), delegate: self, cancelButtonTitle: NSLocalizedString("cancel", comment: ""), otherButtonTitles: NSLocalizedString("ok", comment: ""))
        alert?.tag = 1
        print("cell1")
        alert?.show()
    }
    
    if indexPath.row == 1 {
        
            let sb = UIStoryboard(name: "Auth", bundle: nil)
            let nc = sb.instantiateViewController(withIdentifier: "SpecialistNavigationController") as? UINavigationController
            let aisvc = nc?.viewControllers[0] as? AuthIntimateSpecialistViewController
            aisvc?.isEditingMode = true
            aisvc?.user = user
            if let aNc = nc {
                present(aNc, animated: true)
            }
        alert?.show()
    }
    
    
}
    

    
func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let detailLabelIdentifier = "intimateLabelDetail"
    let detailAttributeIdentifier = "intimateAttributedDetail"
    var cell: UserIntimateTableViewCell?
    switch indexPath.row {
        case 0:
            cell = getIntimateCell(withIdentifier: detailLabelIdentifier, at: indexPath)
        case 1:
            cell = getIntimateCell(withIdentifier: detailAttributeIdentifier, at: indexPath)
    default:
            cell = getIntimateCell(withIdentifier: detailLabelIdentifier, at: indexPath)
    }
    if let aCell = cell {
        return aCell
    }
    return UITableViewCell()
}
    
    
    
    
func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
    if alertView.tag == 1337 && buttonIndex == 1 {
        if let aPhone = URL(string: "tel:\(user?.intimateSpecialist?.phone)") {
            UIApplication.shared.openURL(aPhone)
        }
    }
    if alertView.tag == 1 && buttonIndex == 1 {
        print("program")
        let `as` = UIActionSheet(title: NSLocalizedString("my_motivation", comment: ""), delegate: self, cancelButtonTitle: NSLocalizedString("cancel", comment: ""), destructiveButtonTitle: nil, otherButtonTitles: "Découvrir mon périnée")
        for i in programsList
        {
            `as`.addButton(withTitle: i as! String)
        }
        `as`.tag = 2
        `as`.show(in: view)
    }
}
    
    
func actionSheet(_ actionSheet: UIActionSheet, clickedButtonAt buttonIndex: Int) {
    if buttonIndex == 0 {
        return
    }
    let sb = UIStoryboard(name: "Auth", bundle: nil)
    if actionSheet.tag == 1 {
        // program
        let programType: IntimateProgramType? = programsList[buttonIndex - 1] as! IntimateProgramType
        intimateState = IntimateStateManager.instance.createInitialIntimateState(forProgramKey: programType?.label)
        
        
        CDManager.instance?.saveContext()
       
        tableView.reloadData()
    }
    }
    
  
    
func didTapSpecialistPhone() {
    /*let alert = UIAlertView(title: NSLocalizedString("call_specialist", comment: ""), message: user.intimateSpecialist.phone, delegate: self, cancelButtonTitle: NSLocalizedString("no", comment: ""), otherButtonTitles: NSLocalizedString("yes", comment: ""))
    alert.tag = 1337
    alert.show()*/
}
    
func didTapSpecialistEmail() {
   /* mailPicker = CustomMailComposeViewController()
    mailPicker.mailComposeDelegate = self
    mailPicker.setToRecipients([user.intimateSpecialist.email])
    present(mailPicker, animated: true)*/
}
    /*
func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
    mailPicker.dismiss(animated: true)
}*/
    
func getIntimateCell(withIdentifier cellIdentifier: String?, at indexPath: IndexPath?) -> UserIntimateTableViewCell? {
    var cell: UserIntimateTableViewCell? = nil
    if let aPath = indexPath {
        cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier ?? "", for: aPath) as? UserIntimateTableViewCell
    }
    if cell == nil {
        cell = UserIntimateTableViewCell()
    }
    
    cell?.setContentWith(user, andIntimateState: intimateState, at: indexPath)
    cell?.delegate = self
    return cell
}
    override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
}
    
    
    
    
    
}
