//
//  BreathLayer.cpp
//  mymercurochrome
//
//  Created by Rémi Caroff on 16/12/2015.
//  Copyright © 2015 V-Labs. All rights reserved.
//

#include "Constants.h"
#include "BreathLayer.h"
#include "ActivityService.h"
#include "LeafService.h"
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include "../NDKHelper/NDKHelper.h"
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#include "NDKHelper.h"
#endif

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#include "LocalizationHelper.h"
#endif

USING_NS_CC;

using namespace std;

Value m_dataBreath;

BreathLayer::~BreathLayer()
{
	_eventDispatcher->removeEventListener(_customlistener);
    _eventDispatcher->removeEventListener(_pauseListener);
    _eventDispatcher->removeEventListener(_resumeListener);
};

Scene* BreathLayer::createScene(Value data)
{
	m_dataBreath = data;
    auto scene = Scene::create();
    auto layer = BreathLayer::create();
    
    scene->addChild(layer);
    
    return scene;
}


bool BreathLayer::init()
{

    if (!LayerGradient::initWithColor(Color4B(244,199,199,255), Color4B(255,255,255,255))) {
        return false;
    }
    
    auto gradientLayer = LayerGradient::create(Color4B(255,255,255,0), Color4B(244,199,199,255));
    this->addChild(gradientLayer);
    unserializeData();
    AbstractLayer::initWithNameAndDuration("", _duration, BREATH_EXERCISE_TYPE);
    
    ActivityService::getInstance()->initLeafWithNode(this);

    
    Size visibleSize = Director::getInstance()->getWinSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    auto leafSprite = Sprite::create("drawable/feuille_big_white.png");
    leafSprite->setScale(0.75, 0.75);

    
    actionLabel = Label::createWithTTF("inspirez", "fonts/SourceSansPro-Regular.otf", 20);
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    actionLabel->setPosition(Vec2(visibleSize.width/2, visibleSize.height-33));
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    actionLabel->setPosition(Vec2(visibleSize.width/2, visibleSize.height - actionLabel->getContentSize().height / 2 - 10));
#endif
    
    leafSprite->setPosition(Vec2(visibleSize.width/2, visibleSize.height/2+25));

    
    this->addChild(actionLabel);
    this->addChild(leafSprite);

    
    auto bigger = ScaleBy::create(DURATION_INSPIRATION_BREATH, 2);
    auto smaller = ScaleBy::create(DURATION_EXPIRATION_BREATH, 0.5);
    
    _pauseListener = EventListenerCustom::create("pause_breath", [=](EventCustom* event) {
        leafSprite->pause();
    });
    
    _resumeListener = EventListenerCustom::create("resume_breath", [=](EventCustom* event) {
        leafSprite->resume();
    });
                                                  
                                                  


    _customlistener = EventListenerCustom::create(KEY_POST_PRESSURE_EVENT, [=](EventCustom* event) {
		int* pressurePtr = static_cast<int*>(event->getUserData());
		int pressure = *((int*)pressurePtr);

		ActivityService::activityStep steps;
		steps.pressure = pressure;
		steps.timestamp = 0;
		steps.target = 0;
		ActivityService::getInstance()->addActivitySteps(&steps);
	});
    _eventDispatcher->addEventListenerWithFixedPriority(_customlistener, 1);
    _eventDispatcher->addEventListenerWithFixedPriority(_pauseListener, 1);
    _eventDispatcher->addEventListenerWithFixedPriority(_resumeListener, 1);

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    auto callBackBigger = CallFunc::create([&]() {
        actionLabel->setString(LocalizationHelper::getInstance()->getLocalizedString("inspirez"));
    });
    
    auto callBackSmaller= CallFunc::create([&]() {
        actionLabel->setString(LocalizationHelper::getInstance()->getLocalizedString("expirez"));
    });
    auto seq = Sequence::create(callBackBigger, bigger, callBackSmaller, smaller, NULL);
	leafSprite->runAction(RepeatForever::create(seq));
    
#endif
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    auto callBackBigger = CallFunc::create([&]() {
		actionLabel->setString("inspirez");
	});

	auto callBackSmaller= CallFunc::create([&]() {
		actionLabel->setString("expirez");
	});
	auto seq = Sequence::create(callBackBigger, bigger, callBackSmaller, smaller, NULL);
	leafSprite->runAction(RepeatForever::create(seq));
#endif
    startTimerBreath();
    Value nullValue;
    sendMessageWithParams(ON_EXERCISE_INITIALIZED, nullValue);
    return true;
};

void BreathLayer::unserializeData() {
	if (!m_dataBreath.isNull() && m_dataBreath.getType() == Value::Type::MAP) {
		ValueMap valueMap = m_dataBreath.asValueMap();
		_duration = valueMap[KEY_DURATION].asInt();
	} else {
	}
}

