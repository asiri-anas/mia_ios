//
// Created by kevin on 11/26/15.
//

#ifndef MERCU_PRELOADERLAYER_H
#define MERCU_PRELOADERLAYER_H

#include "cocos2d.h"
#include "AbstractLayer.h"

using namespace cocos2d;

/**
 * Class start when the view is launched and initialized the NDK Helper
 */
class PreloaderLayer : public AbstractLayer {

public:
	/** Create a Preloader Scene */
    static Scene* createScene();

    /** Init the view */
    virtual bool init();

    /** Macro definition to initialize the view */
    CREATE_FUNC(PreloaderLayer);
};


#endif //MERCU_PRELOADERLAYER_H
