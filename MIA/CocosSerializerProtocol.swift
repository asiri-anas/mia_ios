//
//  CocosSerializerProtocol.swift
//  mia
//
//  Created by ASIRI Anas on 19/09/2018.
//  Copyright © 2018 V-Labs. All rights reserved.
//

import Foundation
@objc protocol CocosSerializerProtocol: NSObjectProtocol {
    @objc optional func serialize() -> [AnyHashable : Any]?
    @objc init(dictionary dic: [AnyHashable : Any])
}
