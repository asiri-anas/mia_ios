//
//  AuthIntimateSliderViewController.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 14/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit

class AuthIntimateSliderViewController: AbstractViewController {
    
    
    @IBOutlet weak var slider: CustomSlider!
    @IBOutlet var cursorIV: UIImageView!
    @IBOutlet var cursorLabel: UILabel!
    @IBOutlet var cursorCtn: UIView!
    //var isEditing = false
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = NSLocalizedString("perineal_comfort", comment: "").uppercased()
        if isEditing {
            setLeftCloseButtonForModalNavigationController()
        } else {
            setBackButtonForNavigationController()
        }
        //self.rightBarButtonItemWithImage = UIImage(named: "validate_white")
        //[_cursorCtn setAlpha:0];
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //[self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"nav_pink"] forBarMetrics:UIBarMetricsDefault];
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        cursorCtn.alpha = 1
    }
    
    @IBAction func sliderValueChanged(_ sender: Any) {
        let value = Float((Int((slider.value + 0.5) / 1) * 1))
        slider.setValue(value, animated: true)
        cursorCtn.center = CGPoint(x: xPositionFromSliderValue(), y: slider.center.y - cursorCtn.frame.size.height / 2 - 30)
    }
    
    
    func xPositionFromSliderValue() -> CGFloat {
        var sliderRange: CGFloat = slider.frame.size.width - (slider.currentThumbImage?.size.width ?? 0.0)
        var sliderOrigin: CGFloat = slider.frame.origin.x + ((slider.currentThumbImage?.size.width ?? 0.0) / 2.0)
        let a = slider.value - (slider.minimumValue ?? 0)
        let b = (slider.maximumValue ?? 0) - (slider.minimumValue ?? 0)
        let c = CGFloat(a / b)
        var sliderValueToPixels = (c * sliderRange) + sliderOrigin
        return sliderValueToPixels
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        cursorCtn.center = CGPoint(x: xPositionFromSliderValue(), y: slider.center.y - cursorCtn.frame.size.height / 2 - 30)
    }
    /*
         #pragma mark - Navigation
     
         // In a storyboard-based application, you will often want to do a little preparation before navigation
         - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
         // Get the new view controller using [segue destinationViewController].
         // Pass the selected object to the new view controller.
         }
         */
    
}
