//
//  UIHelper.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 12/06/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit
import SVProgressHUD

class UIHelper: NSObject {
    /*!
     @brief Displays a SVProgressHUD on the center of the view
     @param title The text that will be displayed below the hud.
     @param color Default : black
     */
    class func showHUD(withTitle title: NSString, color: UIColor) {
        
    SVProgressHUD.setBackgroundColor(UIColor.clear)
    SVProgressHUD.setForegroundColor(color)
    if title.length > 0 {
        SVProgressHUD.setStatus(title as String!)
        SVProgressHUD.setFont(UIFont(name: "SourceSansPro-Regular", size: 12))
    }
    SVProgressHUD.show()
        
    }
    
    
    /// Hides the SVProgressHUD.
    class func hideHUD() {
        
    SVProgressHUD.dismiss()
        
    }
    
}
