//
//  ContextTableViewCell.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 13/09/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit


class ContextTableViewCell: UITableViewCell {
    
    @IBOutlet var titleLabel: UILabel!
    
    
    override func awakeFromNib() {
        // Initialization code
    }
    
    func setContentWithTitle(_ title: String?) {
  
            titleLabel.text = title
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    
}
