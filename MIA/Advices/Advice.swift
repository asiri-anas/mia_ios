//
//  Advice.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 05/07/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation



class Advice: NSObject {
    /// Returns the title of the Advice.
    var title: NSString?
    /// Returns the body of the Advice.
    var body: NSString?
    /// Returns a HTML content of the detail message.
    var detail: NSString?
    /// Returns the source URL specified in the back office.
    var sourceURL: NSString?
    /// Returns the absolute URL of the uploaded untreated image.
    var imageDefaultURL: NSString?
    /// Returns the absolute URL of the uploaded blured image.
    var imageBlurURL: NSString?
    /// Returns the date of Advice first creation in the back office.
    var createadAt: NSDate?
    /// Returns the date of the last Advice update in the back office.
    var updatedAt: NSDate?
    /*!
     @brief Returns an instance of Advice object.
     @param adviceDic["title"] Set the title property
     @param adviceDic["body"] Set the body property
     @param adviceDic["detail"] Set the detail property
     @param adviceDic["source_url"] Set the sourceURL property
     @param adviceDic["image_default_url"] Set the imageDefaultURL property
     @param adviceDic["image_blur_url"] Set the imageBlurURL property
     @param adviceDic["created_at"] Set the createadAt property
     @param adviceDic["updated_at"] Set the updatedAt property
     */
    init(dictionary adviceDic: NSDictionary) {
        
        super.init()
        title = adviceDic["title"] as! NSString
        body = adviceDic["body"] as! NSString
        detail = adviceDic["detail"] as! NSString
        sourceURL = adviceDic["source_url"] as! NSString
        imageDefaultURL = adviceDic["image_default_url"] as! NSString
        imageBlurURL = adviceDic["image_blur_url"] as! NSString
        createadAt = adviceDic["created_at"] as! NSDate
        updatedAt = adviceDic["updated_at"] as! NSDate
        
    }
}
