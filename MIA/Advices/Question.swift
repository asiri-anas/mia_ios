//
//  Question.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 05/07/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation


class Question: NSObject {
    var createdAt: NSDate?
    var updatedAt: NSDate?
    var publishedAt: NSDate?
    var title: NSString?
    var body: NSString?
    var url: NSString?
    
    
    init(dictinonary dic: NSDictionary?) {
        

        super.init()
        createdAt = dic?["created_at"] as! NSDate
        updatedAt = dic?["updated_at"] as! NSDate
        publishedAt = dic?["published_at"] as! NSDate
        title = dic?["title"] as! NSString
        body = dic?["body"] as! NSString
        url = dic?["url"] as! NSString

        
    }
    
    
    
}
