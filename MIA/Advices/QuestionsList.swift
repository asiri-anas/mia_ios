//
//  QuestionsList.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 05/07/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation

class QuestionsList: NSObject {
    
    var questionsArray: NSMutableArray?
    
    init(dictionary dic: NSDictionary?) {
        
        super.init()
        questionsArray = NSMutableArray()
        var arr: NSArray?
        arr = dic?["questions"] as? NSArray
        arr?.enumerateObjects({ obj, idx, stop in
            self.questionsArray!.add(Question(dictinonary: obj as! NSDictionary))
        })

        
    }
    
    
    
}
