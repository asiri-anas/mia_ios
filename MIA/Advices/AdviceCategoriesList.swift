//
//  AdviceCategoriesList.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 18/06/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation

class AdviceCategoriesList: NSObject {
    /// An array of AdviceCategory objects.
    var categoriesArray = NSMutableArray()
    /*!
     @brief Returns an instance of AdviceCategoriesList object.
     @param listDic["advice_categories"] An array of AdviceCategory dictionary representation.
     */
    init(dictionary listDic: NSDictionary?) {
        
        super.init()
        categoriesArray = NSMutableArray()
        var categories: NSArray
        categories = listDic!["advice_categories"] as! NSArray
        (categories).enumerateObjects({ obj, idx, stop in
            categoriesArray.add(obj)
        })
        
            }
    
}
