//
//  AdvicesList.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 11/06/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation

public class AdvicesList: NSObject {
    var advicesArray = NSMutableArray()
    /*!
     @brief Returns a list of Advice objects.
     @param adviceDic["advices"] An array of dictionary representations of an Advice object.
     */
    init(dictionary adviceDic: NSDictionary?) {
        
        super.init()
        var advices = adviceDic!["advices"]
        advicesArray = NSMutableArray()
        (advices as! NSArray).enumerateObjects({ obj, idx, stop in
            advicesArray.add(obj)
        })
        
    }
}

