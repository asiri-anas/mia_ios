//
//  MainSponsorViewController.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 26/06/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit
import MessageUI

@objc class MainSponsorViewController: AbstractViewController, BarButtonItemDelegate, MFMailComposeViewControllerDelegate {
    func didTapLeftBarButtonItem() {
        
    }
    
    func didTapRightBarButtonItem() {
        
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "parrainner une amie".uppercased()
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.pinkMercu()]
        setMenuButtonForNavigationController()
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //[self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"nav_pink"] forBarMetrics:UIBarMetricsDefault];
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setMenuButtonForNavigationController()
    }
    
    
    @IBAction func clickMailButton(_ sender: Any) {
        let mailComposeViewController = configureMailController()
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposeViewController, animated: true, completion: nil)
        } else {
            //showMailError()
        }
    }
    
    
    func configureMailController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self
        
        mailComposerVC.setToRecipients([""])
        mailComposerVC.setSubject("UrgoMia")
        mailComposerVC.setMessageBody("ShareWithFriend", isHTML: false)
        
        return mailComposerVC
    }
    
    
    func showMailError() {
        let sendMailErrorAlert = UIAlertController(title: "Could not send email", message: "Your device could not send email", preferredStyle: .alert)
        let dismiss = UIAlertAction(title: "Ok", style: .default, handler: nil)
        sendMailErrorAlert.addAction(dismiss)
        self.present(sendMailErrorAlert, animated: true, completion: nil)
    }
    
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Swift.Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
}
