//
//  MainIntimateDatesFilterViewController.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 13/09/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit
import FSCalendar


class MainIntimateDatesFilterViewController: AbstractViewController, FSCalendarDataSource, FSCalendarDelegate, BarButtonItemDelegate {
    func didTapLeftBarButtonItem() {
        
    }
    
    func didTapRightBarButtonItem() {
     
    NotificationCenter.default.post(name: NSNotification.Name("intimate_date_filter"), object: nil, userInfo: ["selected_dates": calendar.selectedDates])
    navigationController?.popViewController(animated: true)
        
        
    }
    
    
    
    @IBOutlet var calendar: FSCalendar!
    var selectedDates: NSArray?
    var allDates: NSArray?
    @IBOutlet var resetButton: FilterButton!
    @IBOutlet var selectMonthButton: FilterButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setGradientBackground()
        self.setRightBarButtonItemWith(UIImage(named: "validate_white"))
        navigationItem.title = NSLocalizedString("filter_by_date", comment: "").uppercased()
        setBackButtonForNavigationController()
        selectedDates?.enumerateObjects({ obj, idx, stop in
            calendar.select(obj)
        })
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        super.barButtonItemDelegate = self
    }
    
    
    
    @IBAction func selectMonth(_ sender: Any) {
        

        allDates?.enumerateObjects({ date, idx, stop in
           /* if (date as! NSDate).isSameMonth(asDate: calendar.currentPage) != nil {
                calendar.select(date, scrollToDate: false)
            }*/
        })
        
    }

    
    
    
    @IBAction func resetSelection(_ sender: Any) {
        
    var count: Int = calendar.selectedDates.count
    for i in 0..<count {
        //calendar.deselectDate(calendar.selectedDates[0])
        }
    }
    
    
    func minimumDate(for calendar: FSCalendar?) -> Date? {
        if (allDates?.count)! > 0 {
            return allDates?[0] as! Date
        } else {
            return nil
        }
    }
    func maximumDate(for calendar: FSCalendar?) -> Date? {
        //return allDates.last as? Date
        return Date()
    }
    func calendar(_ calendar: FSCalendar?, shouldSelect date: Date?) -> Bool {
        var toReturn = false
        /*allDates?.enumerateObjects({ obj, idx, stop in
            if (obj as! NSDate).isEqual(toDateIgnoringTime: date) != nil {
                toReturn = true
                //stop = true
            }
        })*/
        return toReturn
    }
    
    
    func calendar(_ calendar: FSCalendar?, hasEventFor date: Date?) -> Bool {
        var toReturn = false
        /*allDates?.enumerateObjects({ obj, idx, stop in
            if obj?.isEqual(toDateIgnoringTime: date) != nil {
                toReturn = true
                stop = true
            }
        })*/
        return toReturn
    }
    
}
