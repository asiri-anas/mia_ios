//
//  UIView+Roundify.h
//  mymercurochrome
//
//  Created by Rémi Caroff on 29/09/2015.
//  Copyright © 2015 V-Labs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Roundify)

-(void)addRoundedCorners:(UIRectCorner)corners withRadii:(CGSize)radii;
-(CALayer*)maskForRoundedCorners:(UIRectCorner)corners withRadii:(CGSize)radii;

@end
