/*
 * AbstractLayer.h
 *
 *  Created on: Dec 16, 2015
 *      Author: kevin
 */

#ifndef ABSTRACTLAYER_H_
#define ABSTRACTLAYER_H_

#include "cocos2d.h"
#include "ui/UIButton.h"

using namespace cocos2d;

/**
 * Base layer for each layer (preloader, breath, exercise, calibration)
 */
class AbstractLayer : public LayerGradient {
public:

	virtual ~AbstractLayer();

    /** Init the abstract layer with a name, an exercise number and a duration. (exerciseName == -1, display the pause button) */
    bool initWithNameAndDuration(std::string nameExercise, int duration, int exerciseType=-1);

    /** Setter for the max duration of a pattern */
    void setMaxDuration(int maxDuration);

    /** Toggle play / pause button */
	void toggleButton();

	/** Start timer with a count down 5 seconds */
	bool startTimer();

	bool startTimerCalibration();

	bool startTimerBreath();

	/** Macro definition to initialize the view */
    CREATE_FUNC(AbstractLayer);

    /** Return true if timer is in count down (5 seconds), false otherwise */
    bool isInCountdown();

    bool isPaused();

    /** Method to handle back pressed for android device */
	virtual void onKeyReleased(EventKeyboard::KeyCode keyCode, Event* event) override;

	/** Calculate the max pressure to draw which is equal to 2/3 */
	void calculMaxPressureDraw();

	/** Calculate the max height to draw which is 10 percent lower than m_maxHeightPressureToDraw */
	void calculMaxDraw();

	/** Get the height for the leaf with ratio */
	float getHeightLeaf(int pressure, int maxPressure, int minPressure = 0);

    /** Method called with scheduleUpdate() to draw line */
	void updateParallax(float dt);

	float m_maxHeightPressureToDraw;
	float m_maxHeightGraphToDraw;
	Vec2 _newOrigin;

protected:
	Sprite *_leafSprite;
	ParallaxNode *_parallaxNode;
    Vec2 _lastPoint;

private:
	/** Format time with minute and second like 00:00 */
	std::string formatTime(int minute, int second);

	/** Method trigger from scheduleUpdate() each second */
	void updateTimer(float dt);
    void shouldPauseSelector(Node *sender, Value data);
	void pauseScene();

	void resumeScene();

	int m_minutes;
	int m_seconds;
	int m_maxDuration;
	bool m_inCountdown;
	bool m_isPause;
	int m_currentSeconds;

	Size _visibleSize; // Visible from director;
	Vec2 _originDirector;

	Label* m_labelTimer;
	Label* m_labelNameExercise;
	Label* m_labelExerciseNumber;
	cocos2d::ui::Button* m_button;
    
    int m_currentExerciseNumber;
    void pauseBreath();
    void resumeBreath();

    Vector<Node*> _segments;
};

#endif /* ABSTRACTLAYER_H_ */
