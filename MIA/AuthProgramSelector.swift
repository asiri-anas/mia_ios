//
//  AuthIntimateReasonViewControllerSwift.swift
//  mia
//
//  Created by Xavier Bohin on 18/04/2018.
//  Copyright © 2018 V-Labs. All rights reserved.
//

import Foundation
import UIKit

class AuthProgramSelector: AbstractViewController, UITextFieldDelegate, UIActionSheetDelegate, BarButtonItemDelegate, UIPickerViewDataSource, UIPickerViewDelegate {
    
    
    var intimateCoach: IntimateCoach?
    var intimateProgramType: IntimateProgramType?
    
    func didTapRightBarButtonItem() {
        
    }
    
    func didTapLeftBarButtonItem() {

    }
    
    var intimateStatus: IntimateStatus?
    var isEditingMode = false
    
    var programTypes = NSArray()
    var selectedProgramType: IntimateProgramType?
    var intimateState: IntimateState?
    
    @IBOutlet weak var spinnerProgram: UIPickerView!
    
    var pickerProgram =
        [NSLocalizedString("prog_perineal_discovery",  comment:""),
         NSLocalizedString("prog_post_childbirth",  comment:""),
        NSLocalizedString("prog_perineal_pain",  comment:""),
        NSLocalizedString("prog_urinary_discomfort",  comment:""),
        NSLocalizedString("prog_lack_of_sexual_pleasure",  comment:"")]
    
    var programId = 0
    
    var programDiscover = false
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var Description: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "MON PROGRAMME"
        

        setGradientBackground()
        spinnerProgram.delegate = self
        spinnerProgram.dataSource = self
        
        initContent()
        
    }
    
    
    @IBAction func clickButton(_ sender: Any) {
        
        intimateCoach = initProgramAndSave()
        
    }
    
    
    func initProgramAndSave() -> IntimateCoach? {
        
        do {
            var results = try CDManager.instance!.managedObjectContext!.fetch(IntimateCoach.intimateCoachFetchRequest())
            print("AUTH", "fetch intimateCoach")
            for singleData in results {
                if singleData.intimateState != nil {
                    print("AUTH", "intimateState fetched:", singleData.intimateState!.intimateProgramType)
                }
                intimateCoach = singleData
            }
        } catch {
            print("Failed")
        }
        
        intimateProgramType = intimateCoach?.intimateState?.intimateProgramType
        if intimateProgramType == nil {
            intimateProgramType = IntimateProgramType.create(withName: getPickerValue())
            print("program", " created")
        }
        
        intimateCoach?.intimateState?.intimateProgramType = intimateProgramType
        print("AUTH", "program selector", intimateCoach?.intimateState?.intimateProgramType?.label)
        //print("Intimate to send: ", intimateCoach)
        CDManager.instance!.saveContext()
        
        return intimateCoach
    }
    
    
    
    func initContent()
    {
        switch programId {
        case 1:
            titleLabel.text = NSLocalizedString("prog_perineal_pain", comment: "")
            Description.text = NSLocalizedString("prog_desc_perineal_pain", comment: "")
        case 2:
            titleLabel.text = NSLocalizedString("prog_urinary_discomfort", comment: "")
            Description.text = NSLocalizedString("prog_desc_urinary_discomfort", comment: "")
        case 3:
            titleLabel.text = NSLocalizedString("prog_lack_of_sexual_pleasure", comment: "")
             Description.text = NSLocalizedString("prog_desc_lack_of_sexual_pleasure", comment: "")
        case 4:
            titleLabel.text = NSLocalizedString("prog_post_childbirth", comment: "")
            Description.text = NSLocalizedString("prog_desc_post_childbirth", comment: "")
        default:
            titleLabel.text = "erreur"
        }
        
        if ( programDiscover )
        {
            Description.text = NSLocalizedString("prog_desc_perineal_discovery", comment: "")
            programId = 0
        }
        
        spinnerProgram.selectRow(programId, inComponent: 0, animated: false)
        
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerProgram.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerProgram[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var pickerLabel: UILabel? = (view as? UILabel)
        if pickerLabel == nil {
            pickerLabel = UILabel()
            pickerLabel?.font = UIFont(name: "System", size: 17)
            pickerLabel?.textAlignment = .center
        }
            pickerLabel?.text = pickerProgram[row]
        
        return pickerLabel!
    }
    
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        setBackButtonForNavigationController()
        
        
        
    }
    
    
    func getPickerValue () -> String
    {
        var value: String
        var row: Int
        
        row = spinnerProgram.selectedRow(inComponent: 0)
        value = pickerProgram[row]
        
        return value
    }
    
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "showProgram") {
            let airvc = segue.destination as? AuthIntimateProgramDetailViewController
            
            airvc?.stringTitle = getPickerValue()
            airvc?.intimateState = intimateState
        
        }
    }
    
    
}
