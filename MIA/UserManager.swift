//
//  UserManager.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 02/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit
import CoreData


class UserManager: NSObject {
    
    /// An instance of User object.
    var user: User?
    
    /// Returns an singleton instance of UserManager.
    class func sharedManager() -> UserManager? {
        
    var _shared: UserManager? = nil
    var onceToken: Int = 0
    if (onceToken == 0) {
                /* TODO: move below code to a static variable initializer (dispatch_once is deprecated) */
        _shared = UserManager()
}
        onceToken = 1
        return _shared
        
    }
    
    
    
    
    /// Returns the User object.
    func fetchUser() -> User? {

    if user == nil {
        let managedObjectContext = CDManager.instance!.managedObjectContext
        var entityDescription = NSEntityDescription.entity(forEntityName: "User", in: managedObjectContext!)
        var fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        fetchRequest.entity = entityDescription
        var userID = Registry.get("user_id")
        var predicate = NSPredicate(format: "userID == %i", userID as! CVarArg)
        fetchRequest.predicate = predicate
        var error: Error?
        var array = try? managedObjectContext!.fetch(fetchRequest)
        if error != nil {
            UIAlertView(title: NSLocalizedString("error", comment: ""), message: NSLocalizedString("error_fetch_stress_coach", comment: ""), delegate: nil, cancelButtonTitle: NSLocalizedString("ok", comment: ""), otherButtonTitles: "").show()
            return nil
        }
        if (array?.last != nil){
            user = array?.last as! User
        }
    }
    return user
        
    }
}
