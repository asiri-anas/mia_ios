//
//  AuthIntimateProfileFormViewControllerSwift.swift
//  mia
//
//  Created by Xavier Bohin on 17/04/2018.
//  Copyright © 2018 V-Labs. All rights reserved.
//

import Foundation
import UIKit
import TTTAttributedLabel
import CoreData
import ActionSheetPicker_3_0

@objc class AuthIntimateProfileFormViewController: AbstractViewController, UITextFieldDelegate, UIActionSheetDelegate, BarButtonItemDelegate, HyperLabelDelegate, TTTAttributedLabelDelegate {
    var isCguChecked = false
    var selectedBirthDate: Date?
    
    @IBOutlet weak var FIRSTNAME: UITextField!
    @IBOutlet weak var LASTNAME: UITextField!
    @IBOutlet weak var BIRTH: UIDatePicker!
    
    @IBOutlet weak var CHECK: UIButton!
    @IBOutlet weak var cguLabel: UILabel!
    
    var user: User?
    var intimateCoach: IntimateCoach?
    var isEditingMode = false
    var isCompletionMode = false
    
    @IBOutlet weak var datePicker: UIDatePicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.pinkMercu()]
        
        setGradientBackground()
        let gesture = UISwipeGestureRecognizer()
        gesture.direction = .down
        gesture.addTarget(self, action: #selector(self.dismissKeyboard))
        gesture.location(in: view)
        view.addGestureRecognizer(gesture)
        
        let tapGest = UITapGestureRecognizer()
        tapGest.addTarget(self, action: #selector(self.tapLabel(gesture:)))
        tapGest.location(in: view)
        cguLabel.addGestureRecognizer(tapGest)
        
        if user != nil {
            FIRSTNAME.text = user?.firstName
            LASTNAME.text = user?.lastName
            //BIRTH
            
        }
        
        FIRSTNAME?.setMercuStyle()
        LASTNAME?.setMercuStyle()
        BIRTH.maximumDate = Date()
        
        //setRightBarButtonItemWith(UIImage(named: "validate_white"))
        if isEditingMode {
            setLeftCloseButtonForModalNavigationController()
        } else {
            setBackButtonForNavigationController()
        }
        
        super.barButtonItemDelegate = self
        
        preFillUser()
        
        FIRSTNAME?.setMercuStyle()
        LASTNAME?.setMercuStyle()
        
        if isEditingMode {
            CHECK.isHidden = true
            cguLabel.isHidden = true
        } else {
            CHECK.isHidden = false
            cguLabel.isHidden = false
        }
        isCguChecked = false
        CHECK.setImage(UIImage(named: "case_unmarked"), for: .normal)
        cguLabel.setLink(fullText: "J'accepte les conditions générales d'utilisation", changeText: "conditions générales d'utilisation")
        
    }
    
    func preFillUser() {
        do {
            var results = try CDManager.instance!.managedObjectContext!.fetch(IntimateCoach.intimateCoachFetchRequest())
            print("AUTH", "fetch intimateCoach")
            for singleData in results {
                if singleData.user != nil {
                    print("AUTH", "user fetched:", singleData.user!.firstName)
                    FIRSTNAME.text = singleData.user!.firstName
                    LASTNAME.text = singleData.user!.lastName
                    BIRTH.date = singleData.user!.birthDate!
                }
                intimateCoach = singleData
            }
        } catch {
            print("Failed")
        }
    }
    
    @IBAction func tapLabel(gesture: UITapGestureRecognizer) {
        let text = (cguLabel.text)!
        let termsRange = (text as NSString).range(of: "conditions générales d'utilisation")
        
        if gesture.didTapAttributedTextInLabel(label: cguLabel, inRange: termsRange) {
            guard let url = URL(string: "http://www.mymercurochrome.fr//cgu") else {
                //print("non")
                return //be safe
            }
            UIApplication.shared.openURL(url)
            
            //print("Tapped terms")
        } else {
            //print("Tapped none")
        }
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //[self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"nav_pink"] forBarMetrics:UIBarMetricsDefault];
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    
    func didTapLeftBarButtonItem() {
        if navigationController?.viewControllers[0] == self {
            dismiss(animated: true) {() -> Void in }
        } else {
            if isCompletionMode {
                let app = UIApplication.shared.delegate as? NativeAppDelegate
                app?.loadMainStoryboard()
            } else {
                if isCompletionMode {
                    let app = UIApplication.shared.delegate as? NativeAppDelegate
                    app?.loadMainStoryboard()
                } else {
                    navigationController?.popViewController(animated: true)
                }
            }
        }
    }
    
    
    @IBAction func buttonValidate(_ sender: Any) {
        
        var isTFEmpty = false
        var textfields: NSArray
        
        textfields = [FIRSTNAME, LASTNAME]
        
        textfields.enumerateObjects({ obj, idx, stop in
            if (obj as AnyObject).text?.replacingOccurrences(of: " ", with: "").count == 0 {
                isTFEmpty = true
            }
        })
        
        if isTFEmpty {
            let alert = UIAlertController(title: "Champs obligatoires", message: "Merci de saisir tous les champs", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true)
            return
        }
        
        if !isCguChecked && !isEditingMode {
            let alert = UIAlertController(title: "Conditions générales d'utilisations", message: "Vous devez accepter les conditions générales d'utilisation", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true)
            return
        }
        
        intimateCoach = initUserAndSave()
        
        if isEditingMode {
            NotificationCenter.default.post(name: NSNotification.Name("update_intimate_user"), object: nil, userInfo: ["user": user])
            dismiss(animated: true) {() -> Void in }
        } else {
            Registry.set("user_id", withValue: user?.userID)
            NotificationCenter.default.post(name: NSNotification.Name("coach_created"), object: nil, userInfo: ["intimate_coach": intimateCoach])
            performSegue(withIdentifier: "showSpecialist", sender: nil)
        }
        
    }
    
    func initUserAndSave() -> IntimateCoach? {
        if intimateCoach == nil {
            intimateCoach = IntimateCoachManager.instance.createInitialIntimateCoach()
            print("AUTH", "intimateCoach created")
        }
        user = intimateCoach?.user
        if user == nil {
            user = User.create()
            let userObjectID: NSManagedObjectID? = user?.objectID
            let user_PK = Int(((userObjectID?.uriRepresentation().lastPathComponent as? NSString)?.substring(from: 1) ?? "")) ?? 0
            user?.userID = user_PK as NSNumber
            print("USER ID : ", user?.userID)
            user?.gender = "F"
            print("AUTH", "user created")
        }
        user?.firstName = FIRSTNAME.text!
        user?.lastName = LASTNAME.text!
        user?.birthDate = (selectedBirthDate == nil) ? BIRTH.date : selectedBirthDate
        
        intimateCoach?.user = user
        print("AUTH", "profil specialist", intimateCoach?.intimateSpecialist)
        //print("Intimate to send: ", intimateCoach)
        CDManager.instance!.saveContext()
        
        return intimateCoach
    }
    
    func didTapRightBarButtonItem() {
        
    }
    
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == FIRSTNAME {
            LASTNAME.becomeFirstResponder()
        }
        if textField == LASTNAME {
            dismissKeyboard()
        }
        return false
    }
    
    
    func hyperLabel(_ label: FRHyperLabel?, didSelectLinkAt index: Int) {
        //DebugLog("selected hyperlink index : %u", index)
    }
    
    @IBAction func checkboxTapped(_ sender: Any) {
        isCguChecked = !isCguChecked
        if isCguChecked {
            CHECK.setImage(#imageLiteral(resourceName: "case_marked"), for: .normal)
        } else {
            CHECK.setImage(#imageLiteral(resourceName: "case_unmarked"), for: .normal)
        }
    }
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "showSpecialist") {
            let aisvc = segue.destination as? AuthIntimateSpecialistViewController
            aisvc?.isEditingMode = false
        }
    }
    
}
