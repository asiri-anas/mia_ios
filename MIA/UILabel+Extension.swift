//
//  UILabel+Extension.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 15/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit

extension UILabel {
    /*!
     @brief This method is used to simplify multiple colors in a UILabel.
     @discussion The text string should respect a particular format to define what part of the text is highlighted. Just define the base font / color styles with UIKit methods and via IB as usual, and simply use the setText method to highlight some part of your text.
     @parameter highlightColor The font color of your highlighted part of text.
     @parameter highlightedFont You can provide a different font style if you want.
     @code This is a base text and #<B>this is the highlighted part# which has different color.
     */
    func setText(_ text: String, highlightColor: UIColor, highlightFont: UIFont?) {
        
            var finalString = NSMutableAttributedString()
            var split = text.components(separatedBy: "#")
            (split as NSArray).enumerateObjects({(_ stringPart: String?, _ idx: Int, _ stop: UnsafeMutablePointer<ObjCBool>) -> Void in
                if stringPart?.hasPrefix("<B>") ?? false {
                    var coloredStr = NSMutableAttributedString(string: stringPart?.replacingOccurrences(of: "<B>", with: "") ?? "", attributes: [NSAttributedStringKey.foregroundColor: highlightColor])
                    if highlightFont != nil {
                        coloredStr.addAttributes([NSAttributedStringKey.font: highlightFont], range: NSRange(location: 0, length: stringPart?.count ?? 0))
                    }
                    finalString.append(coloredStr)
                } else {
                    var baseString = NSAttributedString(string: stringPart ?? "", attributes: [NSAttributedStringKey.font: self.font, NSAttributedStringKey.foregroundColor: self.textColor])
                    finalString.append(baseString)
                }
                } as! (Any, Int, UnsafeMutablePointer<ObjCBool>) -> Void)
            attributedText = finalString
        
    }
    
    func setLink(fullText : String , changeText : String ) {
        let strNumber: NSString = fullText as NSString
        let range = (strNumber).range(of: changeText)
        let attribute = NSMutableAttributedString.init(string: fullText)
        attribute.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.blue , range: range)
        attribute.addAttribute(NSAttributedStringKey.link, value: "http://www.mymercurochrome.fr", range: range)
        self.attributedText = attribute
    }
}
