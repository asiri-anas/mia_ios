//
//  IntimateAbstractWidget.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 21/06/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit
class IntimateAbstractWidget: UIViewController {
    @IBOutlet var titleLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func setWidgetTitle(_ title: String?) {
    titleLabel.textColor = UIColor.darkGrayTextColorMercu()
        titleLabel.text = title?.uppercased()
    }
    
}
