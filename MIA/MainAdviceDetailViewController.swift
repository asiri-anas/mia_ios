//
//  MainAdviceDetailViewController.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 22/06/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import SDWebImage
import TTTAttributedLabel
import UIKit

@objc class MainAdviceDetailViewController: AbstractViewController, UIScrollViewDelegate, TTTAttributedLabelDelegate {
    
    @IBOutlet var adviceIV: UIImageView!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var contentView: UIView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var dateLabelCtn: UIView!
    @IBOutlet var sourceLabelCtn: UIView!
    @IBOutlet var webView: UIWebView!
    @IBOutlet var bodyLabel: UILabel!
    @IBOutlet var topLayoutConstraint: NSLayoutConstraint!
    @IBOutlet var heightWebViewConstraint: NSLayoutConstraint!
    @IBOutlet var hud: UIActivityIndicatorView!
    var adviceCategory: NSNumber?
    weak var advice: Advice?
    
    var initialTopConstraintConstant: CGFloat = 0.0
    
    
    override func viewDidLoad() {
    super.viewDidLoad()
    hud.startAnimating()
        adviceIV.sd_setImage(with: URL(string: (advice?.imageDefaultURL)! as String), completed: { image, error, cacheType, imageURL in
            self.adviceIV.alpha = 0
            self.hud.stopAnimating()
        UIView.animate(withDuration: 0.3, animations: {
            self.hud.stopAnimating()
            self.adviceIV.alpha = 1
        })
    })
        titleLabel.text = advice?.title as! String
        bodyLabel.text = advice?.body as! String
        if !(advice?.detail is NSNull) {
            if advice?.detail != nil && (advice?.detail?.length)! > 0 {
                webView.loadHTMLString(advice!.detail as! String, baseURL: nil)
        }
    }
        
    webView.scrollView.isScrollEnabled = false
    switch adviceCategory {
        case 7:
            adviceIV.backgroundColor = UIColor.pinkMercu()
        default:
            break
    }
}
    override func viewWillAppear(_ animated: Bool) {
    navigationController?.setNavigationBarHidden(true, animated: true)
    enableSwipeMenu(false)
}
    
    override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    initialTopConstraintConstant = topLayoutConstraint.constant
    dateLabelCtn.alpha = 0
    sourceLabelCtn.alpha = 0
    heightWebViewConstraint.constant = webView.scrollView.contentSize.height
    UIView.animate(withDuration: 0.2, animations: {
        self.view.layoutIfNeeded()
    })
    var cat: String
    var color: UIColor?
    switch adviceCategory {
        case 7:
            cat = NSLocalizedString("INTIMATE", comment: "")
            color = UIColor.pinkMercu()
            adviceIV.backgroundColor = UIColor.pinkMercu()
        default:
            break
    }
    let dateLabel = TTTAttributedLabel()
        
        dateLabel.text = DateHelper.serializeDate(fromNSDate: advice?.createadAt as! Date)
        
        if advice?.sourceURL != nil {
        let sourceLabel = TTTAttributedLabel()
        sourceLabel.delegate = self
        sourceLabel.tag = 2
            if let anURL = advice?.sourceURL {
            
            sourceLabel.setText(advice?.sourceURL)
        }
    }
    UIView.animate(withDuration: 0.3, animations: {
        self.dateLabelCtn.alpha = 1
        self.sourceLabelCtn.alpha = 1
    })
}
    
func resetLayout() {
    topLayoutConstraint.constant = initialTopConstraintConstant
    UIView.animate(withDuration: 0.2, animations: {
        self.view.layoutIfNeeded()
    })
}
func scrollViewDidScroll(_ scrollView: UIScrollView) {
    if topLayoutConstraint.constant > 64 && self.scrollView.contentOffset.y > 0 {
        if self.scrollView.contentSize.height < view.frame.size.height - 64 {
            topLayoutConstraint.constant = view.frame.size.height - self.scrollView.contentSize.height
        } else {
            topLayoutConstraint.constant = 64
        }
        UIView.animate(withDuration: 0.2, animations: {
            self.view.layoutIfNeeded()
        })
    } else if self.scrollView.contentOffset.y < -10 {
        resetLayout()
    }
}
    
    
    override func viewWillLayoutSubviews() {
    super.viewWillLayoutSubviews()
    bodyLabel.sizeToFit()
}
    override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    adviceIV.layer.masksToBounds = true
}
func attributedLabel(_ label: TTTAttributedLabel?, didSelectLinkWith url: URL?) {
    if label?.tag == 2 {
        if let anURL = URL(string: (advice?.sourceURL ?? "") as String) {
            UIApplication.shared.openURL(anURL)
        }
    }
}
/*
     #pragma mark - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
     // Get the new view controller using [segue destinationViewController].
     // Pass the selected object to the new view controller.
     }
     */

}
