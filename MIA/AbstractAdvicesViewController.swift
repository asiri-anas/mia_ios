//
//  AbstractAdvicesViewController.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 11/06/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit


class AbstractAdvicesViewController: AbstractViewController, UITableViewDataSource, UITableViewDelegate, ValidatorDelegate {

        
    var advicesList: AdvicesList?
    
    @IBOutlet var advicesTableView: UITableView!
    
    var categoryID: Int = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setGradientBackground()

        navigationItem.title = "Conseils".uppercased()
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.pinkMercu()]
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadDataSource(_:)), name: Notification.Name.reachabilityChanged, object: nil)
        advicesTableView.estimatedRowHeight = 153
        advicesTableView.rowHeight = 153
        advicesTableView.alpha = 0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setBackButtonForNavigationController()

        self.setPlayButtonWith(nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIHelper.hideHUD()
    }
    
    
    @objc func reloadDataSource(_ notification: Notification?) {
        //fetchAdvicesList()
    }
    
    func fetchAdvicesList() {
        switch categoryID {
            case Constants.ADVICE_CATEGORY_INTIMATE_ID:
                UIHelper.showHUD(withTitle: "", color: UIColor.pinkMercu()!)
        default:
            UIHelper.showHUD(withTitle: "nil", color: UIColor.pinkMercu()!)
        }
        if (ReachabilityManager.instance.isReachable())! {
            MercuClient.instance.getAdvicesList(fromCategoryID: categoryID as NSNumber, withCompletion: { advicesList, error in
                UIHelper.hideHUD()
                if error?.message == nil {
                    self.advicesList = advicesList
                    self.advicesTableView.reloadData()
                    UIView.animate(withDuration: 0.3, animations: {
                        self.advicesTableView.alpha = 1.0
                    })
                } else {
                    Validator.validateError(error, with: self)
                }
            })
        } else {
            UIHelper.hideHUD()
            UIView.animate(withDuration: 0.3, animations: {
                self.advicesTableView.alpha = 1.0
            })
            advicesTableView.reloadData()
        }
    }
    
    
    
    func didTokenRefresh() {
        //fetchAdvicesList()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        advicesTableView.deselectRow(at: indexPath, animated: true)
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let madvc = sb.instantiateViewController(withIdentifier: "MainAdviceDetailViewController") as? MainAdviceDetailViewController
        madvc?.advice = advicesList?.advicesArray[indexPath.row] as? Advice
        madvc?.adviceCategory = categoryID as NSNumber
        if let aMadvc = madvc {
            navigationController?.pushViewController(aMadvc, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 153
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if advicesList?.advicesArray.count == 0 || !(ReachabilityManager.instance.isReachable()) {
            return 1
        } else {
            //return advicesList!.advicesArray.count
            return 1
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "adviceCell"
        let phIdentifier = "phCell"
        let reachIdentifier = "reachCell"
        if (ReachabilityManager.instance.isReachable()) {
            if advicesList?.advicesArray.count == 0 {
                var phCell: UITableViewCell? = advicesTableView.dequeueReusableCell(withIdentifier: phIdentifier, for: indexPath)
                if phCell == nil {
                    phCell = UITableViewCell(style: .default, reuseIdentifier: phIdentifier)
                }
                if let aCell = phCell {
                    return aCell
                }
                return UITableViewCell()
            } else {
                var cell = advicesTableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? AdviceTableViewCell
                if cell == nil {
                    cell = AdviceTableViewCell()
                }
                cell?.setContentWith(advicesList?.advicesArray[indexPath.row] as? Advice)
                if let aCell = cell {
                    return aCell
                }
                return UITableViewCell()
            }
        } else {
            var reachCell = advicesTableView.dequeueReusableCell(withIdentifier: reachIdentifier, for: indexPath) as? TitleTableViewCell
            if reachCell == nil {
                reachCell = TitleTableViewCell()
            }
            reachCell?.setContentWithTitle(NSLocalizedString("not_reachable_placeholder", comment: ""))
            if let aCell = reachCell {
                return aCell
            }
            return UITableViewCell()
        }
    }
    
    
}
