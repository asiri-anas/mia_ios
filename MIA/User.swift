//
//  User.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 02/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import CoreData

public class User: NSManagedObject {
    
    @NSManaged var birthDate: Date?
    @NSManaged var firstName: String?
    @NSManaged var gender: String?
    @NSManaged var lastName: String?
    @NSManaged var userID: NSNumber?
    @NSManaged var intimateCoach: IntimateCoach?
    @NSManaged var intimateSpecialist: IntimateSpecialist?
    @NSManaged var intimateStatus: IntimateStatus?
}

public extension User {
    ///Assigns a key of type NSString that will be record in Core Data model to specify user's gender (M = Male / F = Female)
    func updateGender(with string: String?) {
        
        if (string == NSLocalizedString("male", comment: "")) {
            gender = "M"
        } else {
            gender = "F"
        }
        
    }
    
    ///Creates an instance of User
    class func create() -> User? {
        let user = NSEntityDescription.insertNewObject(forEntityName: "User", into: (CDManager.instance!.managedObjectContext)!) as! User
        return user
    }
    
    ///Returns the full gender's string from the key recorded in Core Data model.
    func getGenderFullString() -> String? {
        
        if (gender == "M") {
            return NSLocalizedString("male", comment: "")
        } else {
            return NSLocalizedString("female", comment: "")
        }
        
    }
}
