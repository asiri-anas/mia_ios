//
//  TutoMyPartner.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 14/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit

class TutoMyPartner: UIView {
    class func createView() -> TutoMyPartner? {
        
        var view = Bundle.main.loadNibNamed("TutoMyPartner", owner: nil, options: nil)?.last as? TutoMyPartner
        if (view is TutoMyPartner) {
        }
        return view
        
    }
}
