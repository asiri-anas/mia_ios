//
//  ExosIntimateInfosViewController.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 13/07/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit


class ExosIntimateInfosViewController: AbstractViewController {
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var bodyLabel: UILabel!
    @IBOutlet var picotIV: UIImageView!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var contentView: UIView!
    
    var exoViewModel: IntimateExoViewModel?
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let key = "\(exoViewModel?.activityType?.label)_info"
        bodyLabel.text = NSLocalizedString(key, comment: "")
        //picotIV.image = exoViewModel?.picto
        titleLabel.text = "\(NSLocalizedString((exoViewModel?.activityType?.label)!, comment: "")) ?"
        GradientHelper.setGradientOnLayer(view.layer, from: UIColor.colorWithHexString(stringToConvert: exoViewModel?.exoColorString as! NSString), to: UIColor.white)
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        bodyLabel.sizeToFit()
    }
    /*
         #pragma mark - Navigation
     
         // In a storyboard-based application, you will often want to do a little preparation before navigation
         - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
         // Get the new view controller using [segue destinationViewController].
         // Pass the selected object to the new view controller.
         }
         */
    
}
