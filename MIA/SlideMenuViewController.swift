//
//  SlideMenuViewController.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 02/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit
import iOS_Slide_Menu

class SlideMenuViewController:  AbstractViewController, UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate {
    
    
    @IBOutlet weak var tableView: UITableView!
    

    var mainIntimateTabBarController: MainIntimateTabBarController?
    var mainUserProfileViewController: MainUserProfileViewController?

    var infosViewController: InfosViewController?
    var mainSponsorViewController: MainSponsorViewController?
    var mainIntimateSynthesisViewController: MainIntimateSynthesisViewController?
    var exosListViewController: ExosListViewController?
    
    var user: User?
    var intimateCoach: IntimateCoach?
    
    var versionDate: Date?
    
    @IBOutlet weak var versionLabel: UILabel!
    
    var singleton: Singleton?
    
    override func viewDidLoad() {
    super.viewDidLoad()
        
    tableView.reloadData()
        
        let urlToDocumentsFolder = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last!
        //installDate is NSDate of install
        let installDate = (try! FileManager.default.attributesOfItem(atPath: urlToDocumentsFolder.path)[FileAttributeKey.creationDate])
        print(installDate)
        versionDate = installDate! as! Date
        
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date / server String
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let myString = formatter.string(from: versionDate!) // string purpose I add here
        // convert your string to date
        let yourDate = formatter.date(from: myString)
        //then again set the date format whhich type of output you need
        formatter.dateFormat = "dd-MMM-yyyy HH:mm:ss"
        // again convert your date to string
        let myStringafd = formatter.string(from: yourDate!)
        
        print(myStringafd)
        versionLabel.text = myStringafd
        
        singleton = Singleton.instance as! Singleton
    // Do any additional setup after loading the view.
        user = UserManager.sharedManager()?.fetchUser()
    let exoSB = UIStoryboard(name: "Exos", bundle: nil)
    let intimateSB = UIStoryboard(name: "Intimate", bundle: nil)
        exosListViewController = exoSB.instantiateViewController(withIdentifier: "ExosListViewController") as! ExosListViewController
        
        mainIntimateTabBarController = intimateSB.instantiateViewController(withIdentifier: "MainIntimateTabBarController") as! MainIntimateTabBarController
        mainUserProfileViewController = intimateSB.instantiateViewController(withIdentifier: "MainUserProfileViewController") as! MainUserProfileViewController
        
        
        mainIntimateSynthesisViewController = intimateSB.instantiateViewController(withIdentifier: "MainIntimateSynthesisViewController") as! MainIntimateSynthesisViewController
        infosViewController = intimateSB.instantiateViewController(withIdentifier: "InfosViewController") as! InfosViewController
        mainSponsorViewController = intimateSB.instantiateViewController(withIdentifier: "MainSponsorViewController") as! MainSponsorViewController
    NotificationCenter.default.addObserver(self, selector: #selector(self.updateUser(_:)), name: NSNotification.Name("update_intimate_user"), object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(self.coachCreated(_:)), name: NSNotification.Name("coach_created"), object: nil)

        
        do {
            var results = try CDManager.instance!.managedObjectContext!.fetch(IntimateCoach.intimateCoachFetchRequest())
            print("menu", "fetch intimateCoach")
            for singleData in results {
                if singleData.user != nil {
                    print("menu", "user fetched:", singleData.user!.firstName)
                }
                intimateCoach = singleData
                user = intimateCoach?.user
            }
        } catch {
            print("Failed")
        }
        
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        enableSwipeMenu(true)
    }
    
    
    
    @objc func coachCreated(_ notification: Notification?) {
    tableView.reloadData()
}
    @objc func updateUser(_ notification: Notification?) {
        user = notification?.userInfo!["user"] as? User
    tableView.reloadData()
}
    override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
}
func numberOfSections(in tableView: UITableView) -> Int {
    return 3
}
    
func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    if section == tableView.numberOfSections - 1 {
        return 20
    }
    return 0
}
    
func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    return UIView()
}
    
func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
    return UIView()
}
    
func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    if indexPath.section == 0 {
        return 70
    }
    if indexPath.section == 1 {
        return 60
    }
    return 50
}
    
func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if section == 0 {
        return 1
    } else if section == 1 {
        return 2
    } else {
        return 3
    }
    return 0
}

    
    
func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
    if buttonIndex == 1 {
        let app = UIApplication.shared.delegate as? NativeAppDelegate
        //app?.loadAuthStoryBoardToCompleteIntimateProfile()
    }
}
    
func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
    var state: IntimateState?
    if indexPath.section == 1 {
        switch indexPath.row {
            case 0:
                //SlideNavigationController.sharedInstance().popToRootViewController(animated: false)
                let app = UIApplication.shared.delegate as? NativeAppDelegate
                app?.loadMainStoryboard()
            
            case 1:
            SlideNavigationController.sharedInstance().popToRootViewController(animated: true)
            //SlideNavigationController.sharedInstance().present(exosListViewController!, animated: true, completion: nil)
            let app = UIApplication.shared.delegate as? NativeAppDelegate
            let aController = app?.loadExosStoryBoard()
            aController?.popToRootViewController(animated: false)
            SlideNavigationController.sharedInstance().present(aController!, animated: true) {() -> Void in}
            
            
        default:
             mainIntimateTabBarController?.selectedIndex=0
             SlideNavigationController.sharedInstance().popToRootAndSwitch(to: mainIntimateTabBarController, withSlideOutAnimation: true, andCompletion: nil)
        }
    }
    if indexPath.section == 2 {
        switch indexPath.row {
            case 0:
                state = IntimateStateManager.instance.fetchIntimateState()
                mainUserProfileViewController?.user = user
                SlideNavigationController.sharedInstance().popToRootAndSwitch(to: mainUserProfileViewController, withSlideOutAnimation: true, andCompletion: nil)
            case 1:
                SlideNavigationController.sharedInstance().popToRootAndSwitch(to: infosViewController, withSlideOutAnimation: true, andCompletion: nil)
            case 2:
                SlideNavigationController.sharedInstance().popToRootAndSwitch(to: mainSponsorViewController, withSlideOutAnimation: true, andCompletion: nil)
        default:
                SlideNavigationController.sharedInstance().popToRootAndSwitch(to: infosViewController, withSlideOutAnimation: true, andCompletion: nil)
        }
    }
}

    
func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let mainCellIdentifier = "mainCell"
    let profileCellIdentifier = "profileCell"
    let nameCellIdentifier = "nameCell"
    if indexPath.section == 0 {
        var nameCell = self.tableView.dequeueReusableCell(withIdentifier: nameCellIdentifier, for: indexPath) as? TitleTableViewCell
        if nameCell == nil {
            nameCell = TitleTableViewCell()
        }
        nameCell?.setContentWithTitle("\(user?.firstName ?? "") \(user?.lastName ?? "")")
        if let aCell = nameCell {
            return aCell
        }
        return UITableViewCell()
    }
    if indexPath.section == 1 {
        var mainCell = self.tableView.dequeueReusableCell(withIdentifier: mainCellIdentifier, for: indexPath) as? MenuTableViewCell
        if mainCell == nil {
            mainCell = MenuTableViewCell()
        }
        switch indexPath.row {
            case 0:
                mainCell?.setContentForSpace(Constants.SPACE_INTIMATE)
            case 1:
                mainCell?.setContentForSpace(Constants.SPACE_EXOS)
        default:
            mainCell?.setContentForSpace(Constants.SPACE_EXOS)
        }
        if let aCell = mainCell {
            return aCell
        }
        return UITableViewCell()
    } else {
        var profileCell = self.tableView.dequeueReusableCell(withIdentifier: profileCellIdentifier, for: indexPath) as? MenuTableViewCell
        if profileCell == nil {
            profileCell = MenuTableViewCell()
        }
        switch indexPath.row {
            case 0:
                profileCell?.setContentWithText(NSLocalizedString("my_profile", comment: ""), andImageName: "user_picto", withSeparator: true)
            case 1:
                profileCell?.setContentWithText(NSLocalizedString("Infos", comment: ""), andImageName: "help_picto", withSeparator: true)
            case 2:
                profileCell?.setContentWithText(NSLocalizedString("Parrainer une amie", comment: ""), andImageName: "share_white", withSeparator: false)
        default:
                profileCell?.setContentWithText(NSLocalizedString("tutos_title", comment: ""), andImageName: "info_grey_small", withSeparator: false)
        }
        if let aCell = profileCell {
            return aCell
        }
        return profileCell!
    }
}
    
    
    /*
     #pragma mark - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
     // Get the new view controller using [segue destinationViewController].
     // Pass the selected object to the new view controller.
     }
     */
    
    
}
