//
//  UIColor+Extension.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 02/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    class func yellowMercu() -> UIColor? {
        return UIColor(red: 255.0 / 255.0, green: 198.0 / 255.0, blue: 0, alpha: 1)
    }
    
    class func redMercu() -> UIColor? {
        return UIColor(red: 217.0 / 255.0, green: 50.0 / 255.0, blue: 42.0 / 255.0, alpha: 1)
    }
    
    class func blueSkyMercu() -> UIColor? {
        return UIColor(red: 13.0 / 255.0, green: 198.0 / 255.0, blue: 202.0 / 255.0, alpha: 1)
    }
    
    class func greenMercu() -> UIColor? {
        return UIColor(red: 151.0 / 255.0, green: 202.0 / 255.0, blue: 42.0 / 255.0, alpha: 1)
    }
    
    class func veryLightGrayMercu() -> UIColor? {
        return UIColor(red: 205 / 255.0, green: 203.0 / 255.0, blue: 206.0 / 255.0, alpha: 1)
    }
    
    class func grayForGradientMercu() -> UIColor? {
        return UIColor(red: 245.0 / 255.0, green: 245.0 / 255.0, blue: 245.0 / 255.0, alpha: 1)
    }
    
    class func darkGrayTextColorMercu() -> UIColor? {
        return UIColor(red: 66.0 / 255.0, green: 82.0 / 255.0, blue: 88.0 / 255.0, alpha: 1)
    }
    
    class func orangeMercu() -> UIColor? {
        return UIColor(red: 1, green: 150.0 / 255.0, blue: 0, alpha: 1)
    }
    
    class func pinkMercu() -> UIColor? {
        return UIColor(red: 210.0 / 255.0, green: 59.0 / 255.0, blue: 121.0 / 255.0, alpha: 1)
    }
    
 
    class func colorWithHexString(stringToConvert: NSString?) -> UIColor? {
        
        var noHashString = stringToConvert?.replacingOccurrences(of: "#", with: "")
            // remove the #
        var scanner = Scanner(string: noHashString!)
        scanner.charactersToBeSkipped = CharacterSet.symbols
            // remove + and $
        var hex = CUnsignedInt()
        
        scanner.scanHexInt32(&hex)
        
        var r = (hex >> 16) & 0xff
        var g = (hex >> 8) & 0xff
        var b = (hex) & 0xff
        return UIColor(red: CGFloat(Double(r) / 255.0), green: CGFloat(Double(g) / 255.0), blue: CGFloat(Double(b) / 255.0), alpha: 1.0)
        
    }
    
    
    
}
