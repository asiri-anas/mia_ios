//
//  AlgoIntime.h
//  AlgoIntime
//
//  Created by ARIAS Johann on 25/11/2015.
//  Copyright © 2015 UrgoTech. All rights reserved.
//

#ifndef __AlgoIntime__AlgoIntime__
#define __AlgoIntime__AlgoIntime__

#ifdef __cplusplus

#include <stdio.h>
#include <vector>
#include <algorithm>


class AlgoIntime{
    
    public :
    
    /**
     * List of activities
     */
    enum activityName {
        DEEP_STRENGTHENING_FLAT,   // 0
        LATCHING_WITH_TEXT,        // 1
        SUPERFICIAL_WITH_TEXT,     // 2
        SUPERFICIAL_STRENGTHENING, // 3
        LATCHING,                  // 4
        SLACKENING,                // 5
        LATCHING_STRENGTHENING,    // 6
        BREATH,                    // 111
        CALIBRATION,               // 222
        MESSAGE                    // 333
    };
    
    /**
     * activityResult
     */
    struct activityResult {
        int score;
        int percentage;
        int hearts;
        bool levelUp;
        int maxScore;
    };
    
    /**
     * Raw data for each graph point
     *
     * Parameters: double time the timestamp for this point in seconds, double pressure in hPa
     *
     */
    struct rawData {
        double time;
        double pressure;
    };
    
    /**
     * A pair of raw data
     * Each pair is compose with two points
     * The first contains a start time and start pressure
     * The second contains the next point and the next pressure
     *
     */
    struct pairRawData {
        rawData startData;
        rawData endData;
    };
    
    /**
     * Describe activity with name & level
     */
    struct activityDescription {
        int lvl;
        int name;
    };
    
    /**
     * Values representation into an activity
     */
    struct activityStep {
        double timestamp;
        double target;
        double pressure;
    };
    
    struct sensor {
        int minPressure;
        int maxPressure;
        int value;
    };
    
    struct calibration{
        
        int minPressure;
        int maxPressure;
        int calibrationValue;
        sensor sensor1;
        sensor sensor2;
    };
    
    /**
     * Get results for an exercice
     *
     * MUST BE CALLED AFTER EACH INTIMATE EXERCICE
     *
     * @return struct score
     */
    
    static activityResult getResult(std::vector<activityStep> activity);
    
    /**
     * Get acitivitySkeleton
     *
     * MUST BE CALLED AT THE BEGINNING OF EACH INTIMATE EXERCICE
     *
     * parameters :activityDescription activityDesc, calibration calibration
     *
     * @return struct std::vector<pairRawData>
     */
    static std::vector<pairRawData> getActivitySkeleton(activityDescription activityDesc, calibration calibration, struct calibration breathCalibration);
    
    /**
     * Get hearts for a session
     *
     * MUST BE CALLED AT THE END OF A PROGRAMM SESSION (NOT AT THE END OF EACH EXERCICE)
     *
     * parameters : std::vector<int> hearts, hearts for the exercices inside the session
     *
     * @return int hearts for the session
     */
    
    static int getSessionHearts(std::vector<int> hearts);
    
    /**
     *
     * Get leaf level
     *
     * MUST BE CALLED TO REFRESH LEAF STATUS
     *
     * parameters : int sessionsDone is the number of session the user has already done in her program, int totalSessions is the total amout of sessions to do inside the program
     */
    static int getLeafLevel(int sessionsDone, int totalSessions);
    
    
    /**
     * Get calibration
     *
     * MUST BE CALLED AT THE END OF EACH BREATHING EXERCICE. THE MUST BE A BREATHING EXERCICE BEFORE EVERY INTIMATE EXERCICE
     * THE "calibration" MUST BE SENT VIA getActivitySkeleton(); FOR THE INTIMATE EXERCICE. "calibration" DOESNT HAVE TO BE SAVED AFTER THIS.
     *
     * parameters : std::vector<int> pressures are the pressures measured during the breathing exercice
     *
     * @return calibration
     *
     */
    static calibration getCalibration(std::vector<int> pressures);
    
    /**
     * Get tonicity
     *
     * MUST BE CALLED AT THE END OF EACH EXERCICE OF TYPE "CALIBRATION". THE CALIBRATION
     *
     *parameters : std::vector<int> pressures are the pressures measured during the "CALIBRATION" exercice
     *
     */
    
    static int getTonicity(std::vector<int> pressures);
    
    static int getBaseline(std::vector<int> pressures);
    
};


#endif //__AlgoIntime__AlgoIntime__
#endif
