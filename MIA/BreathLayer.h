//
//  BreathLayer.hpp
//  mymercurochrome
//
//  Created by Rémi Caroff on 16/12/2015.
//  Copyright © 2015 V-Labs. All rights reserved.
//

#ifndef BreathLayer_h
#define BreathLayer_h

#include "cocos2d.h"
#include "AbstractLayer.h"

using namespace cocos2d;

/**
 * Layer for Breath exercise
 */
class BreathLayer : public AbstractLayer
{
    
public:
    ~BreathLayer();

    /** Create a Breath Scene */
    static Scene *createScene(Value data);

    /** Init the view */
    virtual bool init();

    /** Macro definition to initialize the view */
    CREATE_FUNC(BreathLayer);
    
private:


    cocos2d::EventListenerCustom* _customlistener;
    cocos2d::EventListenerCustom* _pauseListener;
    cocos2d::EventListenerCustom* _resumeListener;
    
    
    /** Unserialize data from Native (Android iOS) */
    void unserializeData();

    Sprite* leaf;
    int currentTime;
    Label* actionLabel;
    int _duration;
};


#endif /* BreathLayer_h */
