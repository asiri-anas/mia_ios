//
//  MainIntimateTabBarController.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 02/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit

class MainIntimateTabBarController: UITabBarController {
   
    override func viewDidLoad() {
        super.viewDidLoad()
        tabBar.items![0].title = NSLocalizedString("synthesis", comment: "")
        tabBar.items![1].title = NSLocalizedString("progress", comment: "")
        tabBar.items![2].title = NSLocalizedString("history", comment: "")
        tabBar.items![3].title = "Infos"
        tabBar.tintColor = UIColor.pinkMercu()
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool {
        return true
    }
    
}
