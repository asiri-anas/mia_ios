//
//  IntimateProgramFriezeWidget.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 21/06/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit

class IntimateProgramFriezeWidget: IntimateAbstractWidget, FriezeTileDelegate {
    
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var tile1: FriezeTileView!
    @IBOutlet weak var tile2: FriezeTileView!
    @IBOutlet weak var tile3: FriezeTileView!
    @IBOutlet weak var tile4: FriezeTileView!
    @IBOutlet weak var tile5: FriezeTileView!
    @IBOutlet weak var tile6: FriezeTileView!
    @IBOutlet weak var tile7: FriezeTileView!
    @IBOutlet weak var tile8: FriezeTileView!
    @IBOutlet weak var tile9: FriezeTileView!
    @IBOutlet weak var tile10: FriezeTileView!
    
    @IBOutlet weak var jeuImage: UIImageView!
    
    @IBOutlet weak var programLabel: UILabel!
    
    var intimateCoach: IntimateCoach?
    var intimateProgramType: IntimateProgramType?
    
    var tileViews = [FriezeTileView]()
    var scrollPoint = CGPoint.zero
    //var contentSize = CGSize.zero
    var currentSession = 15
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tileViews = [tile1, tile2, tile3, tile4, tile5, tile6, tile7, tile8, tile9, tile10]
        
        
        do {
            var results = try CDManager.instance!.managedObjectContext!.fetch(IntimateCoach.intimateCoachFetchRequest())
            print("frieze", "fetch intimateCoach")
            for singleData in results {
                if singleData.intimateState != nil {
                    print("fireze", "intimateState fetched:", singleData.intimateState!.intimateProgramType)
                }
                intimateCoach = singleData
            }
        } catch {
            print("Failed")
        }
        
        intimateProgramType = intimateCoach?.intimateState?.intimateProgramType
    
        currentSession = Int((intimateCoach?.intimateState?.currentSession)!)
        
        if (currentSession == 0)
        {
            currentSession = 1
            intimateCoach?.intimateState?.currentSession = 1
            CDManager.instance?.saveContext()
            
        }
        
        
        setFrieze(exoToDo: currentSession)
        
        
        programLabel.text = intimateProgramType?.label
        
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        /*scrollView.contentSize = contentSize*/
        scrollView.setContentOffset(scrollPoint, animated: true)
    }
    
    func friezeTileView(_ friezeTileView: FriezeTileView?, didSelect friezeTile: FriezeTile?) {
        
        var contentOffset = CGPoint.zero
        if friezeTileView?.tag == 1 {
            contentOffset = CGPoint.zero
        } else if friezeTileView?.tag == 10 {
            contentOffset = CGPoint(x: contentView.frame.size.width - scrollView.frame.size.width, y: 0)
        } else {
            contentOffset = CGPoint(x: (friezeTileView?.center.x)! - (view.frame.size.width / 2), y: 0)
        }
        scrollView.setContentOffset(contentOffset, animated: true)
        var sb = UIStoryboard(name: "Exos", bundle: nil)
        if (friezeTileView?.tag)! < Int((friezeTile?.intimateState?.currentSession)!) {
                // On envoie vers le détail de la session
            var nc = sb.instantiateViewController(withIdentifier: "ExosIntimateSessionDetailsNavigationController") as? UINavigationController
            var eiedvc = nc?.viewControllers[0] as? ExosIntimateSessionDetailsViewController
            eiedvc?.isFromListing = false
            eiedvc?.intimateSession = friezeTile?.intimateSession
            if let aNc = nc {
                parent?.present(aNc, animated: true)
            }
        } else if (friezeTileView?.tag)! == Int((friezeTile?.intimateState?.currentSession)!) {
            var nc = sb.instantiateViewController(withIdentifier: "ExosNavigationController") as? UINavigationController
            var eipvc = sb.instantiateViewController(withIdentifier: "ExosIntimateParamsViewController") as? ExosIntimateParamsViewController
            //eipvc?.isFreeExo = false
            if let anEipvc = eipvc {
                nc?.pushViewController(anEipvc, animated: false)
            }
            if let aNc = nc {
                parent?.present(aNc, animated: true)
            }
        }
        
    }
    
    func shouldScrollToCurrentFriezeTile(at point: CGPoint) {
        
        print("offset point : ", point.x, point.y)
        scrollPoint = point
        
    }

    
    
    func setContentWith(_ intimateState: IntimateState?) {
        var currentScore: Int = 0
        let tiles = IntimateProgramHandler.sharedManager()?.getFriezeTiles(for: intimateState)
        /*(tiles as NSArray).enumerateObjects({ tile, idx, stop in
            currentScore += (tile as AnyObject).intimateSession.score ?? 0
        })*/
        let maxScore = IntimateProgramHandler.sharedManager()?.getCumulatedSessionsMaxScore(forProgramKey: intimateState?.intimateProgramType?.label)
        var lastCenter: CGPoint = CGPoint.zero
        /*tileViews.enumerateObjects({ tileView, idx, stop in
            if idx < tiles.count {
                tileView?.hidden = false
                tileView?.contentWithFriezeTile = tiles[idx]
                lastCenter = tileView?.center ?? CGPoint.zero
            } else {
                tileView?.hidden = true
            }
        })*/
        
        /*if Int((intimateState?.maxSession)!) < 10 {
            contentSize = CGSize(width: lastCenter.x + (view.frame.size.width / 2), height: scrollView.frame.size.height)
        } else {
            contentSize = contentView.frame.size
        }*/
        
        var sessionsNumber = "\(intimateState?.currentSession)"
        var sessionsStr = String(format: NSLocalizedString("n_sessions", comment: ""), sessionsNumber, "")
        programLabel.text = intimateProgramType?.label
    }
    
    
    
    
    //Xavier
    
    //Display the correct frieze jeu1 jeu2 jeu3 jeu4
    func chooseFriezeImage (exoToDo: Int)
    {
        var exoTobeDone = exoToDo
        
        while ( exoTobeDone > 40 )
        {
            exoTobeDone = exoTobeDone - 20
        }
        
        jeuImage.image = #imageLiteral(resourceName: "Jeu-1")
        if ( exoTobeDone > 11)
        {
            jeuImage.image = #imageLiteral(resourceName: "jeu-2")
        }
        if (exoTobeDone > 21)
        {
            jeuImage.image = #imageLiteral(resourceName: "jeu-3")
        }
        if (exoTobeDone > 31)
        {
            jeuImage.image = #imageLiteral(resourceName: "jeu-4")
        }
    }
    
    //Color the passed tiles in blue, add the white leaf on exoTobeDone, show hearts on passed tiles
    func colorTiles(exoToDo: Int)
    {
        var exoTobeDone = exoToDo - 1
        //you'll need to pass a list of prev exos scores in parameters for hearts image.
        var score = [40, 50, 78, 90, 43, 78, 89, 20, 67, 0]
        //adapting séances number
        var nombreDeDizaine = 0
        
        while ( exoTobeDone > 10 )
        {
            exoTobeDone = exoTobeDone - 10
            nombreDeDizaine += 1
        }
        
        
        if (exoTobeDone>1)
        {
        for i in 0...exoTobeDone - 1
        {
            tileViews[i].tileButton.setBackgroundImage(UIImage(named: "tuile_green_shadow"), for: .normal)
            tileViews[i].heartsIV.image = pickCorrectHeartImage(sucessPercentage: score[i])
        }
        }
        
        
        for i in 0...9
        {
        tileViews[i].tileButton.setTitle(String(nombreDeDizaine*10 + i+1), for: .normal)
        }
        
        tileViews[exoTobeDone].tileButton.setBackgroundImage(UIImage(named: "tuile_green_shadow"), for: .normal)
        tileViews[exoTobeDone].cursorIV.image = UIImage(named: "curseur_feuille_white")
        
        
        //set the scroll point
        scrollPoint = CGPoint.init(x: 125+125*exoTobeDone, y: 0)
        
    }
    
    
    //Recieve succes Percentage and return the 3 hearts image corresponding
    func pickCorrectHeartImage(sucessPercentage: Int) -> UIImage
    {
        var imageToReturn: UIImage = UIImage(named: "hearts_0")!
        
        if (sucessPercentage>14)
        {
            imageToReturn = UIImage(named: "hearts_1")!
        }
        if (sucessPercentage>28)
        {
            imageToReturn = UIImage(named: "hearts_2")!
        }
        if (sucessPercentage>42)
        {
            imageToReturn = UIImage(named: "hearts_3")!
        }
        if (sucessPercentage>57)
        {
            imageToReturn = UIImage(named: "hearts_4")!
        }
        if (sucessPercentage>71)
        {
            imageToReturn = UIImage(named: "hearts_5")!
        }
        if (sucessPercentage>85)
        {
            imageToReturn = UIImage(named: "hearts_6")!
        }
        return imageToReturn
    }
    
    
    func setFrieze(exoToDo: Int)
    {
        chooseFriezeImage(exoToDo: exoToDo)
        colorTiles(exoToDo: exoToDo)
    }
    
    
}





