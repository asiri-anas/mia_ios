//
//  FAQSectionView.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 05/07/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import UIKit

protocol FAQSectionViewDelegate: NSObjectProtocol {
    func didTap(_ sectionView: FAQSectionView?, at index: Int)
}

class FAQSectionView: UIView {
    var index: Int = 0
    var selected = false
    
    @IBOutlet var pictoIV: UIImageView!
    
    @IBOutlet var titleLabel: UILabel!
    
    @IBOutlet var sectionButton: UIButton!
    
    weak var delegate: FAQSectionViewDelegate?
    
    class func createView(withTitle title: String?, at index: Int, delegate: FAQSectionViewDelegate?) -> FAQSectionView? {
        
        var view = Bundle.main.loadNibNamed("FAQSectionView", owner: nil, options: nil)?.last as? FAQSectionView
        if (view is FAQSectionView) {
            view?.titleLabel.text = title
            view?.index = index
            view?.delegate = delegate
            view?.tag = 1110 + index
        }
        return view
        
    }
    
    @IBAction func buttonTapped(_ sender: Any) {
        
        delegate?.didTap(self, at: index)
        
        
    }
    
    
    func setSelected(_ selected: Bool) {
        if selected {
            pictoIV.image = UIImage(named: "moins")
        } else {
            pictoIV.image = UIImage(named: "plus")
        }
    }
    /*
         // Only override drawRect: if you perform custom drawing.
         // An empty implementation adversely affects performance during animation.
         - (void)drawRect:(CGRect)rect {
         // Drawing code
         }
         */
    
    
}
