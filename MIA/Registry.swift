//
//  Registry.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 02/05/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation


class Registry: NSObject {

    
class func set(_ key: String?, withValue value: Any?) {
    var params = UserDefaults.standard
    params.set(value, forKey: key!)
    params.synchronize()
}
class func get(_ key: String?) -> Any? {
    let params = UserDefaults.standard
    return params.object(forKey: key!)
}
class func del(_ key: String?) {
    let params = UserDefaults.standard
    params.removeObject(forKey: key!)
    params.synchronize()
}
class func has(_ key: String?) -> Bool {
    let params = UserDefaults.standard
    let value = params.object(forKey: key!)
    return (value == nil) ? false : true
}
class func setIdentifiedImage(_ identifiedImage: IdentifiedImage?) {
    var params = UserDefaults.standard
    if let anImage = identifiedImage {
        params.set(NSKeyedArchiver.archivedData(withRootObject: anImage), forKey: "last_image_selected")
    }
    params.synchronize()
}
class func getIdentifiedImage() -> IdentifiedImage? {
    let params = UserDefaults.standard
    if let aKey = params.object(forKey: "last_image_selected") as? Data {
        return NSKeyedUnarchiver.unarchiveObject(with: aKey) as? IdentifiedImage
    }
    return nil
}
class func setToken(_ token: Token?) {
    var params = UserDefaults.standard
    if let aToken = token {
        params.set(NSKeyedArchiver.archivedData(withRootObject: aToken), forKey: "token")
    }
    params.synchronize()
}
class func getToken() -> Token? {
    let params = UserDefaults.standard
    if let aKey = params.object(forKey: "token") as? Data {
        return NSKeyedUnarchiver.unarchiveObject(with: aKey) as? Token
    }
    return nil
}
}
