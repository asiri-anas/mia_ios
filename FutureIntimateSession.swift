//
//  FutureIntimateSession.swift
//  MiaSwift
//
//  Created by Xavier Bohin on 12/07/2018.
//  Copyright © 2018 urgotech. All rights reserved.
//

import Foundation
import CoreData


public class FutureIntimateSession: NSManagedObject, IntimateDataSourceProtocol {
    
     @NSManaged var fireDate: Date?
    
    class func create() -> FutureIntimateSession? {
        
        var session = NSEntityDescription.insertNewObject(forEntityName: "FutureIntimateSession", into: (CDManager.instance?.managedObjectContext)!) as? FutureIntimateSession
        return session
        
    }
    
    func getCreationDate() -> Date? {
        
         return self.fireDate
        
    }
    
}
